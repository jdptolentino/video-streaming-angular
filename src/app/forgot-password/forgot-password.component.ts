import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '@app/core';
import { isEmpty, has } from 'lodash';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  token: string;
  resetForm: boolean;
  forgotForm!: FormGroup;
  loading$: Observable<boolean>;
  forgotData$: Observable<any>;
  forgotLoading = true;
  hide = true;
  constructor(
    private router: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService
  ) {
    this.loading$ = this.authenticationService.loading$;
    this.forgotData$ = this.authenticationService.forgotData$;
  }

  ngOnInit() {
    this.forgotData$.subscribe(data => {
      if (data.email) {
        this.resetForm = true;
        this.createFormFound(data.email);
      } else {
        this.resetForm = false;
        this.createForm();
      }
      this.forgotLoading = false;
    });
    this.router.queryParams.subscribe(param => {
      if (!isEmpty(param)) {
        if (has(param, 'token')) {
          this.token = param.token;
          this.authenticationService.find(param.token);
        }
      }
    });
  }

  submit() {
    this.authenticationService.forgot(this.forgotForm.value);
  }

  reset() {
    this.authenticationService.reset(this.forgotForm.value);
  }

  private createForm() {
    this.forgotForm = this.formBuilder.group({
      email: [{ value: '', disabled: false }, Validators.compose([Validators.required, Validators.email])]
    });
  }

  private createFormFound(email: string) {
    this.forgotForm = this.formBuilder.group({
      email: [email, Validators.compose([Validators.required, Validators.email])],
      password: [{ value: '', disabled: false }, Validators.required],
      token: [this.token, Validators.required]
    });
  }

  get f() {
    return this.forgotForm.controls;
  }
}
