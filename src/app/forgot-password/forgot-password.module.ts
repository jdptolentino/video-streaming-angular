import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotPasswordComponent } from './forgot-password.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material';
import { MatCardModule } from '@angular/material/card';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { ForgotPasswordRoutingModule } from './forgot-password-routing.module';
import { StoreModule } from '@ngrx/store';
import { StoreReducer } from '@app/core/authentication/store/reducers/store.reducer';
import { EffectsModule } from '@ngrx/effects';
import { StoreEffects } from '@app/home/store/effects/store.effect';
import { LoginHttpService } from '@app/shared/services/http/login-http.service';
import { ProfileHttpService } from '@app/shared/services/http/profile-http.service';
import { ForgotHttpService } from '@app/shared/services/http/forgot-http.service';
import { SharedModule } from '@app/shared';

@NgModule({
  declarations: [ForgotPasswordComponent],
  imports: [
    CommonModule,
    MatSnackBarModule,
    MatCardModule,
    MatIconModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule,
    SharedModule,
    ForgotPasswordRoutingModule,
    StoreModule.forRoot({ auth: StoreReducer }),
    EffectsModule.forFeature([StoreEffects])
  ],
  providers: [LoginHttpService, ProfileHttpService, ForgotHttpService]
})
export class ForgotPasswordModule {}
