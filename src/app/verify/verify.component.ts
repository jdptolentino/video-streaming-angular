import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit {
  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) {}
  info = 'Loading verification..';
  ngOnInit(): void {
    this.route.params.subscribe((param: any) => {
      if (param.token) {
        const verify = new Promise((resolve, reject) => {
          this.http.get(`${environment.serverUrl}/verify/email/${param.token}`).subscribe(
            data => resolve(data),
            error => reject(error)
          );
        });

        verify
          .then((data: any) => {
            this.info = 'Successfully verified. Logging you in..';
            this.router.navigate(['/login'], { queryParams: { token: data.token } });
          })
          .catch(error => (this.info = error));
      }
    });
  }
}
