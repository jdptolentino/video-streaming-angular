import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/core';
import { VerifyComponent } from './verify.component';

const routes: Routes = [{ path: 'verify/:token', component: VerifyComponent, data: { title: extract('Verify') } }];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class VerifyRoutingModule {}
