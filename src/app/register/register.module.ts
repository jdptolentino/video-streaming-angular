import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EffectsModule } from '@ngrx/effects';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';
import { StoreModule } from '@ngrx/store';
import { StoreReducer } from '@app/core/authentication/store/reducers/store.reducer';
import { StoreEffects } from '@app/core/authentication/store/effects/store.effect';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCardModule } from '@angular/material/card';
import { RegisterHttpService } from '@app/shared/services/http/register-http.service';
import { SharedModule } from '@app/shared';

@NgModule({
  imports: [
    CommonModule,
    MatSnackBarModule,
    MatCardModule,
    ReactiveFormsModule,
    TranslateModule,
    NgbModule,
    SharedModule,
    RegisterRoutingModule,
    StoreModule.forRoot({ auth: StoreReducer }),
    EffectsModule.forFeature([StoreEffects])
  ],
  declarations: [RegisterComponent],
  providers: [RegisterHttpService]
})
export class RegisterModule {}
