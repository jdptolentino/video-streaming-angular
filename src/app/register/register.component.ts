import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { environment } from '@env/environment';
import { Logger, I18nService, AuthenticationService, untilDestroyed } from '@app/core';
import { Observable } from 'rxjs';
import { isEmpty } from 'lodash';

const log = new Logger('Register');

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  version: string | null = environment.version;
  error: string | undefined;
  registerForm!: FormGroup;
  loading$: Observable<boolean>;
  errors$: Observable<any>;
  errorItems: any;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private i18nService: I18nService,
    private authenticationService: AuthenticationService
  ) {
    this.loading$ = this.authenticationService.loading$;
    this.createForm();

    this.errors$ = this.authenticationService.errors$;
    this.errors$.subscribe(res => {
      let errorList: any = [];
      if (!isEmpty(res)) {
        if (typeof res === 'string') {
          errorList = [res];
        } else {
          for (let items in res) {
            console.log(items);
            errorList = errorList.concat(res[items]);
          }
        }
      }
      this.errorItems = errorList;
    });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.authenticationService.resetErrors();
  }

  register() {
    this.authenticationService.register(this.registerForm.value);
  }

  setLanguage(language: string) {
    this.i18nService.language = language;
  }

  get currentLanguage(): string {
    return this.i18nService.language;
  }

  get languages(): string[] {
    return this.i18nService.supportedLanguages;
  }

  private createForm() {
    this.registerForm = this.formBuilder.group({
      // user_name: [
      //   { value: '', disabled: false },
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(8),
      //     Validators.maxLength(255),
      //     Validators.pattern('^[a-zA-Z0-9]+$')
      //   ])
      // ],
      email: [{ value: '', disabled: false }, Validators.compose([Validators.required, Validators.email])],
      password: [{ value: '', disabled: false }, Validators.required],
      agree: [{ value: '', disabled: false }, Validators.required]
    });
  }

  get f() {
    return this.registerForm.controls;
  }
}
