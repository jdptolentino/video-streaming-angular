import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionComponent } from './subscription.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '@app/shared';
import { CoreModule } from '@app/core';
import { SubscriptionRoutingModule } from './subscription-routing.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreEffects } from './store/effects/store.effect';
import { reducers } from './store/index';
import { SubscriptionService } from './subscription.service';
import { PlanHttpService } from '@app/shared/services/http/plan-http.service';
import { PaymentHttpService } from '@app/shared/services/http/payment-http.service';

@NgModule({
  declarations: [SubscriptionComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    CoreModule,
    SharedModule,
    SubscriptionRoutingModule,

    StoreModule.forFeature('subscriptionModule', reducers),
    EffectsModule.forFeature([StoreEffects])
  ],
  providers: [SubscriptionService, PlanHttpService, PaymentHttpService]
})
export class SubscriptionModule {}
