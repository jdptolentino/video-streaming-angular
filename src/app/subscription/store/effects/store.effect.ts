import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/internal/Observable';
import * as StoreActions from '../actions/store.action';
import * as FromStore from '../reducers/store.reducer';
import { map } from 'rxjs/internal/operators/map';
import { exhaustMap } from 'rxjs/internal/operators/exhaustMap';
import { PlanHttpService } from '@app/shared/services/http/plan-http.service';
import { catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { PaymentHttpService } from '@app/shared/services/http/payment-http.service';

@Injectable()
export class StoreEffects {
  // Get Videos
  @Effect()
  getPlans$: Observable<any> = this.actions$.pipe(
    ofType(StoreActions.loadSubscription),
    map(action => action.request),
    exhaustMap(payload =>
      this.planHttpService.all(payload).pipe(map((response: any) => StoreActions.loadSubscriptionSuccess({ response })))
    )
  );

  @Effect()
  choosePlan$: Observable<any> = this.actions$.pipe(
    ofType(StoreActions.choosePlan),
    exhaustMap(payload =>
      this.planHttpService.choosePlan(payload.id).pipe(
        map((response: any) => StoreActions.choosePlanSuccess({ response })),
        catchError((response: any) => {
          return of(StoreActions.failed({ payload: response }));
        })
      )
    )
  );

  // Set Forgot Success
  @Effect({ dispatch: false })
  choosePlanSuccess$ = this.actions$.pipe(
    ofType(StoreActions.choosePlanSuccess),
    tap(action => {
      this.snackbar.open(action.response.message, '', {
        duration: 2000,
        verticalPosition: 'top',
        horizontalPosition: 'center',
        panelClass: 'custome-toast'
      });
      this.router.navigate(['/subscription'], { queryParams: { request: 'success' } });
    })
  );

  @Effect()
  changePlan$: Observable<any> = this.actions$.pipe(
    ofType(StoreActions.changePlan),
    exhaustMap(payload =>
      this.planHttpService.changePlan(payload.subscription, payload.plan).pipe(
        map((response: any) => StoreActions.changePlanSuccess({ response })),
        catchError((response: any) => {
          return of(StoreActions.failed({ payload: response }));
        })
      )
    )
  );

  // Set Forgot Success
  @Effect({ dispatch: false })
  changePlanSuccess$ = this.actions$.pipe(
    ofType(StoreActions.changePlanSuccess),
    tap(action => {
      this.snackbar.open('Subscription Changed Successfully!', '', {
        duration: 2000,
        verticalPosition: 'top',
        horizontalPosition: 'center',
        panelClass: 'custome-toast'
      });
      this.router.navigate(['/home']);
    })
  );

  @Effect({ dispatch: false })
  failed$ = this.actions$.pipe(
    ofType(StoreActions.failed),
    map(action => action.payload),
    tap((payload: any) => {
      this.snackbar.open(payload, '', {
        duration: 2000,
        verticalPosition: 'top',
        horizontalPosition: 'center',
        panelClass: 'custome-toast'
      });
    })
  );

  constructor(
    private router: Router,
    private actions$: Actions,
    private planHttpService: PlanHttpService,
    private paymentHttpService: PaymentHttpService,
    private snackbar: MatSnackBar
  ) {}
}
