import { createSelector, createFeatureSelector } from '@ngrx/store';
import StoreState from './store.state';
import * as fromFeature from './reducers/store.reducer';

export interface FeatureModuleState {
  subscription: StoreState;
}

export const reducers = {
  subscription: fromFeature.StoreReducer
};

export interface AppState extends StoreState {
  subscription: FeatureModuleState;
}

export const selectFeatureModuleState = createFeatureSelector<FeatureModuleState>('subscriptionModule');

export const selectFeatureState = createSelector(
  selectFeatureModuleState,
  (state: FeatureModuleState) => state.subscription
);
export const getLoading = createSelector(selectFeatureState, (state: StoreState) => state.loading);
export const getResponse = createSelector(selectFeatureState, (state: StoreState) => state.response);
export const getData = createSelector(selectFeatureState, (state: StoreState) => state.response.data);
