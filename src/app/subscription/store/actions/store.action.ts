import { createAction, props } from '@ngrx/store';
import { HttpQueryRequest } from '@app/shared/models/http/http-query-request.model';
import { HttpQueryResponse } from '@app/shared/models/http/http-query-response.model';
import { Plan } from '@app/shared/models/db/plan.model';

export const loadSubscription = createAction(
  '[Subscription] Load Subscriptions',
  props<{ request: HttpQueryRequest }>()
);

export const loadSubscriptionSuccess = createAction(
  '[Subscription] Load Subscription Success',
  props<{ response: HttpQueryResponse<Plan> }>()
);

export const choosePlan = createAction('[Subscription] Choose Plan', props<{ id: number }>());

export const choosePlanSuccess = createAction('[Subscription] Choose Plan Success', props<{ response: any }>());

export const changePlan = createAction('[Subscription] Change Plan', props<{ subscription: number; plan: number }>());

export const changePlanSuccess = createAction('[Subscription] Change Plan Success', props<{}>());

export const initiatePayment = createAction(
  '[Subscription] Initiate Payment',
  props<{ user_id: number; plan_id: number }>()
);

export const paymentSuccess = createAction('[Subscription] Payment Success', props<{}>());

export const failed = createAction('[Subscription] Failed', props<{ payload: any }>());
