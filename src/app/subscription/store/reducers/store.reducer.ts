import { Action, createReducer, on } from '@ngrx/store';
import * as Actions from '../actions/store.action';
import StoreState, { initializeState } from '../store.state';

export const intialState = initializeState();

const reducer = createReducer(
  intialState,
  on(Actions.loadSubscription, (state: StoreState, {}) => {
    return { ...state, loading: true };
  }),
  on(Actions.loadSubscriptionSuccess, (state: StoreState, { response }) => ({ ...state, response, loading: false })),
  on(Actions.choosePlan, (state: StoreState, { id }) => ({ ...state, loading: true })),
  on(Actions.choosePlanSuccess, (state: StoreState, { response }) => ({ ...state, loading: false })),
  on(Actions.initiatePayment, (state: StoreState, { user_id, plan_id }) => ({ ...state, loading: false })),
  on(Actions.paymentSuccess, (state: StoreState, {}) => ({ ...state, loading: false })),
  on(Actions.failed, (state: StoreState, {}) => {
    return { ...state, loading: false };
  })
);

export function StoreReducer(state: StoreState | undefined, action: Action) {
  return reducer(state, action);
}
