import { Injectable } from '@angular/core';
import { Transaction } from '@app/shared/models/db/transaction.model';
import { Observable } from 'rxjs';
import { AppState } from './store/index';
import { Store, select } from '@ngrx/store';
import * as StoreActions from './store/actions/store.action';
import * as FromState from './store/index';
import { Plan } from '@app/shared/models/db/plan.model';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {
  subscription$: Observable<Plan[]>;
  loading$: Observable<boolean>;

  constructor(private store$: Store<AppState>) {
    this.subscription$ = this.store$.pipe(select(FromState.getData));
    this.loading$ = this.store$.pipe(select(FromState.getLoading));
  }

  loadSubscription() {
    const data = {
      perPage: 10,
      page: 1
    };
    this.store$.dispatch(StoreActions.loadSubscription({ request: data }));
  }

  choosePlan(id: number) {
    this.store$.dispatch(StoreActions.choosePlan({ id }));
  }

  changePlan(subscription: number, plan: number) {
    this.store$.dispatch(StoreActions.changePlan({ subscription, plan }));
  }
}
