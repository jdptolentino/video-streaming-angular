import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract, AuthenticationGuard } from '@app/core';
import { Shell } from '@app/shell/shell.service';
import { SubscriptionComponent } from './subscription.component';
import { NoSubscriptionGuard } from '@app/core/authentication/no-subscription.guard';

const routes: Routes = [
  Shell.childRoutes([
    {
      path: 'subscription',
      component: SubscriptionComponent,
      // canActivate: [NoSubscriptionGuard],
      canActivate: [AuthenticationGuard],
      data: { title: extract('Subscription') }
    }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class SubscriptionRoutingModule {}
