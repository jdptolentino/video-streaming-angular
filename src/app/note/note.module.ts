import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EffectsModule } from '@ngrx/effects';

import { NoteRoutingModule } from './note-routing.module';
import { NoteComponent } from './note.component';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCardModule } from '@angular/material/card';
import { SharedModule } from '@app/shared';

@NgModule({
  imports: [
    CommonModule,
    MatSnackBarModule,
    MatCardModule,
    ReactiveFormsModule,
    TranslateModule,
    NgbModule,
    SharedModule,
    NoteRoutingModule
  ],
  declarations: [NoteComponent]
})
export class NoteModule {}
