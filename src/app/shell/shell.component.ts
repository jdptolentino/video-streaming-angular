import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthenticationService } from '@app/core';
import { User } from '@app/shared/models/db/user.model';
import { isEmpty } from 'lodash';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})
export class ShellComponent implements OnInit, AfterViewInit {
  userData: User;

  constructor(private authenticationService: AuthenticationService, private router: Router) {}

  ngOnInit() {}

  logout() {
    this.authenticationService.logout().subscribe(() => this.router.navigate(['/login'], { replaceUrl: true }));
  }

  ngAfterViewInit() {
    $('.sidebar-toggle').on('click', function() {
      $(this).toggleClass('active');

      $('#sidebar').toggleClass('shrinked');
      $('.page-content').toggleClass('active');
      $(document).trigger('sidebarChanged');

      if ($('.sidebar-toggle').hasClass('active')) {
        $('.navbar-brand .brand-sm').addClass('visible');
        $('.navbar-brand .brand-big').removeClass('visible');
        $(this)
          .find('i')
          .attr('class', 'fa fa-long-arrow-right');
      } else {
        $('.navbar-brand .brand-sm').removeClass('visible');
        $('.navbar-brand .brand-big').addClass('visible');
        $(this)
          .find('i')
          .attr('class', 'fa fa-long-arrow-left');
      }
    });
  }
}
