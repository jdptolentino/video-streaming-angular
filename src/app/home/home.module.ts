import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { StoreModule } from '@ngrx/store';
import { StoreEffects } from './store/effects/store.effect';
import { EffectsModule } from '@ngrx/effects';
import { reducers } from './store/index';
import { VideoHttpService } from '@app/shared/services/http/video-http.service';
import { EmbedVideo } from 'ngx-embed-video';
import { SeriesHttpService } from '@app/shared/services/http/series-http.service';
import { FormsModule } from '@angular/forms';
import { SearchHttpService } from '@app/shared/services/http/search-http.service';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    HomeRoutingModule,
    StoreModule.forFeature('homeModule', reducers),
    EffectsModule.forFeature([StoreEffects]),
    EmbedVideo.forRoot(),
    FormsModule
  ],
  declarations: [HomeComponent],
  providers: [VideoHttpService, SeriesHttpService, SearchHttpService]
})
export class HomeModule {}
