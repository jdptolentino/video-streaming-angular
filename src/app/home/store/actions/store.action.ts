import { createAction, props } from '@ngrx/store';
// Models
import { HttpQueryRequest } from '@app/shared/models/http/http-query-request.model';
import { Video } from '@app/shared/models/db/video.model';
import { Series } from '@app/shared/models/db/series.model';

export const loadVideo = createAction('[Home] Load Video', props<{ request: HttpQueryRequest }>());
export const loadVideoSuccess = createAction('[Home] Load Video Success', props<{ videos: Video[] }>());

export const loadSeries = createAction('[Home] Load Series', props<{ request: HttpQueryRequest }>());
export const loadSeriesSuccess = createAction(
  '[Home] Load Series Success',
  props<{ series: any; page: number; total: number }>()
);
export const search = createAction('[Home] Search', props<{ request: HttpQueryRequest }>());
export const searchSuccess = createAction('[Home] Search Success', props<{ search: any }>());

export const reset = createAction('[Home] Reset Series', props<{}>());
