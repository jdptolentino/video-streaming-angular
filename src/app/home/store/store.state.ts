import { Video } from '@app/shared/models/db/video.model';
import { Series } from '@app/shared/models/db/series.model';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { HttpQueryRequest } from '@app/shared/models/http/http-query-request.model';
export default interface StoreState extends EntityState<any> {
  loading: boolean;
  loadingRef: string;
  processing: boolean;
  videos: Video[];
  series: Series[];
  search: any;
  request?: HttpQueryRequest;
}

export const adapter: EntityAdapter<any> = createEntityAdapter<any>({
  selectId: (response: any) => response.id
});

export const initializeState: StoreState = adapter.getInitialState({
  loading: false,
  loadingRef: null,
  processing: false,
  videos: [],
  series: [],
  search: null,
  request: {
    perPage: 16,
    page: 1
  }
});
