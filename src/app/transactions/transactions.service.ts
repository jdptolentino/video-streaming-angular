import { Injectable } from '@angular/core';
import { Transaction } from '@app/shared/models/db/transaction.model';
import { Observable } from 'rxjs';
import { AppState } from './store/index';
import { Store, select } from '@ngrx/store';
import * as StoreActions from './store/actions/store.action';
import * as FromState from './store/index';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {
  transactions$: Observable<Transaction[]>;

  constructor(private store$: Store<AppState>) {
    this.transactions$ = this.store$.pipe(select(FromState.getData));
  }

  loadTransactions() {
    const data = {
      perPage: 10,
      page: 1
    };
    this.store$.dispatch(StoreActions.loadTransactions({ request: data }));
  }
}
