import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionsRoutingModule } from './transactions-routing.module';
import { TransactionsService } from './transactions.service';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreEffects } from './store/effects/store.effect';
import { reducers } from './store/index';
import { TransactionsComponent } from './transactions.component';
import { TransactionHttpService } from '@app/shared/services/http/transaction-http.service';

@NgModule({
  declarations: [TransactionsComponent],
  imports: [
    CommonModule,
    TransactionsRoutingModule,
    FlexLayoutModule,
    CoreModule,
    SharedModule,
    StoreModule.forFeature('transactionModule', reducers),
    EffectsModule.forFeature([StoreEffects])
  ],
  providers: [TransactionsService, TransactionHttpService]
})
export class TransactionsModule {}
