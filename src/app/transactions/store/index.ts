import { createSelector, createFeatureSelector } from '@ngrx/store';
import StoreState from './store.state';
import * as fromFeature from './reducers/store.reducer';

export interface FeatureModuleState {
  transactions: StoreState;
}

export const reducers = {
  transactions: fromFeature.StoreReducer
};

export interface AppState extends StoreState {
  transactions: FeatureModuleState;
}

export const selectFeatureModuleState = createFeatureSelector<FeatureModuleState>('transactionModule');

export const selectFeatureState = createSelector(
  selectFeatureModuleState,
  (state: FeatureModuleState) => state.transactions
);
export const getLoading = createSelector(selectFeatureState, (state: StoreState) => state.loading);
export const getResponse = createSelector(selectFeatureState, (state: StoreState) => state.response);
export const getData = createSelector(selectFeatureState, (state: StoreState) => state.response.data);
