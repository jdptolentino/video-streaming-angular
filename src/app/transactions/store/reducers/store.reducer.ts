import { Action, createReducer, on } from '@ngrx/store';
import * as Actions from '../actions/store.action';
import StoreState, { initializeState } from '../store.state';

export const intialState = initializeState();

const reducer = createReducer(
  intialState,
  on(Actions.loadTransactions, (state: StoreState, {}) => {
    return { ...state, loading: true };
  }),
  on(Actions.loadTransactionsSuccess, (state: StoreState, { response }) => ({ ...state, response, loading: false }))
);

export function StoreReducer(state: StoreState | undefined, action: Action) {
  return reducer(state, action);
}
