import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/internal/Observable';
import * as StoreActions from '../actions/store.action';
import * as FromStore from '../reducers/store.reducer';
import { map } from 'rxjs/internal/operators/map';
import { exhaustMap } from 'rxjs/internal/operators/exhaustMap';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { tap, withLatestFrom, switchMap } from 'rxjs/operators';
import { SeriesHttpService } from '@app/shared/services/http/series-http.service';
import { Store, select } from '@ngrx/store';
import { AppState, getRequest } from '../index';
@Injectable()
export class StoreEffects {
  // Get All Videos
  @Effect()
  getSeries$: Observable<any> = this.actions$.pipe(
    ofType(StoreActions.loadSeries),
    withLatestFrom(this.store$.pipe(select(getRequest))),
    switchMap(([payload, request]) =>
      this.seriesHttpService
        .getBySlug(payload.slug, request)
        .pipe(
          map((response: any) =>
            StoreActions.loadSeriesSuccess({ series: response.data, videos: response.data.videos })
          )
        )
    )
  );

  // Set Token and Access
  @Effect({ dispatch: false })
  failed$ = this.actions$.pipe(
    ofType(StoreActions.failed),
    map(action => action.payload),
    tap((payload: any) => {
      this.snackbar.open(payload, '', {
        duration: 2000,
        verticalPosition: 'top',
        horizontalPosition: 'center',
        panelClass: 'custome-toast'
      });
    })
  );

  constructor(
    private router: Router,
    private actions$: Actions,
    private seriesHttpService: SeriesHttpService,
    private snackbar: MatSnackBar,
    private store$: Store<AppState>
  ) {}
}
