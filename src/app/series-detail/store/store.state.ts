import { Series } from '@app/shared/models/db/series.model';
import { HttpQueryRequest } from '@app/shared/models/http/http-query-request.model';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Video } from '@app/shared/models/db/video.model';

export default interface StoreState extends EntityState<any> {
  loading: boolean;
  loadingRef: string;
  processing: boolean;
  series: Series[];
  request?: HttpQueryRequest;
  videos: Video[];
}

export const adapter: EntityAdapter<any> = createEntityAdapter<any>({
  selectId: (response: any) => response.id
});

export const initializeState: StoreState = adapter.getInitialState({
  loading: false,
  loadingRef: null,
  processing: false,
  series: [],
  videos: [],
  request: {
    perPage: 16,
    page: 1
  }
});
