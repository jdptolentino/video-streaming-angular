import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Series } from '@app/shared/models/db/series.model';
import { AppState } from './store';
import { Store, select } from '@ngrx/store';
import * as FromState from './store/index';
import * as StoreActions from './store/actions/store.action';
import { Video } from '@app/shared/models/db/video.model';

@Injectable({
  providedIn: 'root'
})
export class SeriesDetailService {
  series$: Observable<Series[]>;
  videos$: Observable<Video[]>;
  loading$: Observable<boolean>;

  constructor(private store$: Store<AppState>) {
    this.series$ = this.store$.pipe(select(FromState.getSeries));
    this.videos$ = this.store$.pipe(select(FromState.selectAllData));
    this.loading$ = this.store$.pipe(select(FromState.getLoading));
  }

  loadSeriesBySlug(slug: string, request?: any) {
    this.store$.dispatch(StoreActions.loadSeries({ slug, request }));
  }

  reset() {
    this.store$.dispatch(StoreActions.reset({}));
  }

  resetVideos() {
    this.store$.dispatch(StoreActions.resetData({}));
  }
}
