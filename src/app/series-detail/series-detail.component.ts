import { Component, OnInit, ChangeDetectionStrategy, AfterViewInit, OnDestroy } from '@angular/core';
import { SeriesDetailService } from './series-detail.service';
import { Series } from '@app/shared/models/db/series.model';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { EmbedVideoService } from 'ngx-embed-video';
import { isEmpty } from 'lodash';
import { Video } from '@app/shared/models/db/video.model';
import { AuthenticationService } from '@app/core';
import { HttpClient } from '@angular/common/http';
import { stringToKeyValue } from '@angular/flex-layout/extended/typings/style/style-transforms';

@Component({
  selector: 'app-series-detail',
  templateUrl: './series-detail.component.html',
  styleUrls: ['./series-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SeriesDetailComponent implements OnInit, AfterViewInit, OnDestroy {
  series$: Observable<Series[]>;
  videos$: Observable<Video[]>;
  loading$: Observable<boolean>;
  series: any;
  videos: any;
  isLoading: boolean;
  slug: string;
  total: number;
  page: number;
  plan: any;
  activeRecordsCount: number;
  searchString: string = '';

  constructor(
    private router: ActivatedRoute,
    private seriesService: SeriesDetailService,
    private embedService: EmbedVideoService,
    private authService: AuthenticationService,
    private http: HttpClient
  ) {
    this.series$ = this.seriesService.series$;
    this.videos$ = this.seriesService.videos$;
    this.loading$ = this.seriesService.loading$;

    this.loading$.subscribe(res => (this.isLoading = res));
    this.series$.subscribe((response: any) => {
      if (!isEmpty(response)) {
        this.series = response.series;
        this.videos = response.videos;
        this.activeRecordsCount = response.perPage * response.page;
        this.total = response.total;
        this.page = response.page;
      }
    });

    this.authService.user$.subscribe(user => {
      this.plan = user.plan ? user.plan : null;
    });
  }

  ngOnInit() {
    this.router.params.subscribe(param => {
      if (param.slug) {
        this.slug = param.slug;
        this.seriesService.loadSeriesBySlug(param.slug);
      }
    });
  }

  ngOnDestroy() {
    this.seriesService.reset();
  }

  search() {
    this.seriesService.reset();
    this.seriesService.loadSeriesBySlug(this.slug, { search: this.searchString });
  }

  videoHtml(link: string): void {
    return this.embedService.embed(link, {
      query: { showinfo: 0 },
      attr: { width: '100%', height: '245' }
    });
  }

  scrollEvent = (event: any): void => {
    const pos = event.target.scrollingElement.scrollTop + event.target.scrollingElement.clientHeight;
    const max = document.documentElement.offsetHeight;
    if (Math.ceil(pos) >= max && !this.isLoading && this.total > this.activeRecordsCount) {
      this.page++;
      this.seriesService.loadSeriesBySlug(this.slug, { page: this.page });
    }
  };

  planStatus(plan: any) {
    if (isEmpty(plan)) {
      return false;
    }

    if (plan.status === 'LAPSED' || plan.status === 'CANCELLED') {
      return false;
    }

    return true;
  }
  ngAfterViewInit(): void {
    document.addEventListener('scroll', this.scrollEvent, true);
  }
}
