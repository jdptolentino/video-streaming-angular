import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { SeriesDetailRoutingModule } from './series-detail-routing.module';
import { EmbedVideo } from 'ngx-embed-video';
import { FlexLayoutModule, CoreModule } from '@angular/flex-layout';
import { SeriesHttpService } from '@app/shared/services/http/series-http.service';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreEffects } from './store/effects/store.effect';
import { reducers } from './store';
import { SeriesDetailComponent } from './series-detail.component';
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [SeriesDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    CoreModule,
    FlexLayoutModule,
    SeriesDetailRoutingModule,
    StoreModule.forFeature('seriesModule', reducers),
    EffectsModule.forFeature([StoreEffects]),
    EmbedVideo.forRoot(),
    FormsModule
  ],
  providers: [SeriesHttpService]
})
export class SeriesDetailModule {}
