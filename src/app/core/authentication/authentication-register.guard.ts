import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Logger } from '../logger.service';
import { CredentialsService } from './credentials.service';
import { Observable } from 'rxjs';
import { LoginHttpService } from '@app/shared/services/http/login-http.service';
import { take, map } from 'rxjs/operators';
import { isEmpty } from 'lodash';

const log = new Logger('AuthenticationRegisterGuard');

@Injectable({
  providedIn: 'root'
})
export class AuthenticationRegisterGuard implements CanActivate {
  constructor(
    private router: Router,
    private credentialsService: CredentialsService,
    private loginHttpService: LoginHttpService
  ) {}

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return this.loginHttpService.checkUser().pipe(
      take(1),
      map((auth: any) => {
        if (!isEmpty(auth)) {
          if (auth.authenticated) {
            return true;
          }

          return false;
        }

        this.router.navigate(['/login']);
        return false;
      })
    );
  }
}
