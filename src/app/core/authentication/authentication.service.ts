import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Credentials, CredentialsService } from './credentials.service';
import * as StoreActions from './store/actions/store.action';
import * as FromStore from './store/reducers/store.reducer';
import * as FromState from './store/index';
import { Store, select } from '@ngrx/store';
import StoreState from './store/store.state';
import { User } from '@app/shared/models/db/user.model';
import { LoginHttpService } from '@app/shared/services/http/login-http.service';

export interface LoginContext {
  email: string;
  password: string;
  remember?: boolean;
}

export interface RegisterContext {
  // user_name: string;
  email: string;
  password: string;
  agree: boolean;
}

export interface ForgotContext {
  email: string;
}

/**
 * Provides a base for authentication workflow.
 * The login/logout methods should be replaced with proper implementation.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  loading$: Observable<boolean>;
  user$: Observable<User>;
  errors$: Observable<any>;
  forgotData$: Observable<any>;
  forgotEmail$: Observable<string>;

  constructor(private store: Store<{ auth: StoreState }>, private loginHttp: LoginHttpService) {
    this.loading$ = this.store.pipe(select(FromState.getLoading));
    this.user$ = this.store.pipe(select(FromState.getUser));
    this.errors$ = this.store.pipe(select(FromState.getErrors));
    this.forgotData$ = this.store.pipe(select(FromState.getForgotData));
  }

  /**
   * Authenticates the user.
   * @param context The login parameters.
   * @return The user credentials.
   */

  login(context: LoginContext) {
    const data = {
      email: context.email,
      password: context.password
    };

    // this.store$.dispatch(new StoreActions.Login({ data: data }));
    this.store.dispatch(StoreActions.login({ data }));
  }

  loginWithToken(token: string) {
    // this.store$.dispatch(new StoreActions.Login({ data: data }));
    this.store.dispatch(StoreActions.loginWithToken({ token }));
  }

  forgot(context: LoginContext) {
    const data = {
      email: context.email
    };

    this.store.dispatch(StoreActions.forgot({ data }));
  }

  find(token: string) {
    this.store.dispatch(StoreActions.find({ payload: { token: token } }));
  }

  reset(context: any) {
    const data = {
      email: context.email,
      password: context.password,
      token: context.token
    };
    this.store.dispatch(StoreActions.reset({ data }));
  }

  resetErrors() {
    this.store.dispatch(StoreActions.resetErrors({}));
  }

  register(context: RegisterContext) {
    // this.store$.dispatch(new StoreActions.Login({ data: data }));
    this.store.dispatch(StoreActions.register({ data: context }));
  }

  update(id: number, user: User) {
    this.store.dispatch(StoreActions.update({ payload: { id, user } }));
  }
  getUser(): Observable<any> {
    // return this.store.pipe(select(FromState.getUser));
    return this.loginHttp.checkUser();
  }

  checkUser() {
    this.loginHttp.checkUser().subscribe((checkData: any) => {
      this.store.dispatch(StoreActions.setToken({ token: checkData.token, user: checkData.user }));
    });
  }

  /**
   * Logs out the user and clear credentials.
   * @return True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    // Customize credentials invalidation here

    const auth = JSON.parse(localStorage.getItem('auth'));

    localStorage.setItem('auth', JSON.stringify({ token: '' }));

    return of(true);
  }
}
