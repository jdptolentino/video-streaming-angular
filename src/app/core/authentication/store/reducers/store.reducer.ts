import { Action, createReducer, on } from '@ngrx/store';
import * as Actions from '../actions/store.action';
import StoreState, { initializeState } from '../store.state';

export const intialState = initializeState();

const reducer = createReducer(
  intialState,
  on(Actions.login, (state: StoreState, { data }) => {
    return { ...state, data, loading: true };
  }),
  on(Actions.loginWithToken, (state: StoreState, { token }) => {
    return { ...state, token, loading: true };
  }),
  on(Actions.register, (state: StoreState, { data }) => {
    return { ...state, data, loading: true };
  }),
  on(Actions.registerSuccess, (state: StoreState, {}) => {
    return { ...state, loading: false };
  }),
  on(Actions.update, (state: StoreState, { payload }) => {
    return { ...state, payload: { id: payload.id, user: payload.user }, loading: true };
  }),
  on(Actions.forgot, (state: StoreState, { data }) => {
    return { ...state, data, loading: true };
  }),
  on(Actions.forgotSuccess, (state: StoreState, {}) => {
    return { ...state, loading: false };
  }),
  on(Actions.find, (state: StoreState, { payload }) => {
    return { ...state, payload: { token: payload.token }, loading: false };
  }),
  on(Actions.findSuccess, (state: StoreState, { email }) => {
    return { ...state, forgotPasswordData: { email: email }, loading: false };
  }),
  on(Actions.reset, (state: StoreState, { data }) => {
    return { ...state, data, loading: true };
  }),
  on(Actions.resetSuccess, (state: StoreState, {}) => {
    return { ...state, forgotPasswordData: { email: '' }, loading: false };
  }),
  on(Actions.setToken, (state: StoreState, { token, user }) => {
    return { ...state, token, user, loading: false };
  }),
  on(Actions.setUser, (state: StoreState, { token, user }) => {
    return { ...state, token, user };
  }),
  on(Actions.setUserData, (state: StoreState, { user }) => {
    return { ...state, user, loading: false };
  }),
  on(Actions.setErrors, (state: StoreState, { errors }) => {
    return { ...state, errors, loading: false };
  }),
  on(Actions.resetErrors, (state: StoreState, {}) => {
    return { ...state, errors: null };
  }),
  on(Actions.failed, (state: StoreState, {}) => {
    return { ...state, loading: false };
  })
);

export function StoreReducer(state: StoreState | undefined, action: Action) {
  return reducer(state, action);
}
