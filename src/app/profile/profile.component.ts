import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@app/core';
import { Observable } from 'rxjs';
import { User } from '@app/shared/models/db/user.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Plan } from '@app/shared/models/db/plan.model';
import { SubscriptionService } from '@app/subscription/subscription.service';
import { isEmpty } from 'lodash';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  userData: User;
  loading$: Observable<boolean>;
  errors$: Observable<any>;
  errorItems: any;
  subscription$: Observable<Plan[]>;
  form!: FormGroup;
  plan: any;
  subscription: any;
  upgrade = false;
  constructor(
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
    private subscriptionService: SubscriptionService
  ) {
    this.loading$ = this.authenticationService.loading$;
    this.subscription$ = this.subscriptionService.subscription$;
    this.errors$ = this.authenticationService.errors$;
    this.errors$.subscribe(res => {
      let errorList: any = [];
      if (!isEmpty(res)) {
        for (let items in res) {
          errorList = errorList.concat(res[items]);
        }
      }
      this.errorItems = errorList;
    });
    this.authenticationService.user$.subscribe(user => {
      this.userData = user;
      this.plan = user.plan ? user.plan : null;
      this.subscription = user.subscription ? user.subscription.plan : null;
      this.createForm(this.userData);
    });
  }

  update() {
    if (this.form.valid) {
      this.authenticationService.update(this.userData.id, this.form.value);
    }
  }

  ngOnInit() {
    this.subscriptionService.loadSubscription();
  }

  changePlan(subscription: number, plan: number): void {
    this.subscriptionService.changePlan(subscription, plan);
  }

  private createForm(user: User) {
    this.form = this.formBuilder.group({
      first_name: [user.first_name, Validators.required],
      last_name: [user.last_name, Validators.required],
      user_name: [
        user.user_name,
        Validators.compose([Validators.minLength(8), Validators.maxLength(255), Validators.pattern('^[a-zA-Z0-9]+$')])
      ]
    });
  }

  get f() {
    return this.form.controls;
  }
}
