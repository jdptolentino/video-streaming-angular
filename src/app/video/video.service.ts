import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import * as StoreActions from './store/actions/store.action';
import * as FromState from './store/index';
import { Store, select } from '@ngrx/store';
import { AppState } from './store/index';
import { Video } from '@app/shared/models/db/video.model';

@Injectable({
  providedIn: 'root'
})
export class VideoService {
  videos$: Observable<Video[]>;

  constructor(private store$: Store<AppState>) {
    this.videos$ = this.store$.pipe(select(FromState.getVideo));
  }

  loadVideosbyCategory(categoryId: string) {
    const data = {
      perPage: 10,
      page: 1
    };
    this.store$.dispatch(StoreActions.loadVideosByCategory({ request: data, slug: categoryId }));
  }
}
