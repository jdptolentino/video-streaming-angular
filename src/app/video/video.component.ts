import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { VideoService } from './video.service';
import { Observable } from 'rxjs';
import { Video } from '@app/shared/models/db/video.model';
import { ActivatedRoute } from '@angular/router';
import { EmbedVideoService } from 'ngx-embed-video';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VideoComponent implements OnInit {
  videos$: Observable<Video[]>;

  category: string;
  constructor(
    private videoService: VideoService,
    private router: ActivatedRoute,
    private embedService: EmbedVideoService
  ) {
    this.videos$ = this.videoService.videos$;
  }

  ngOnInit() {
    this.router.params.subscribe(param => {
      if (param) {
        this.category = param.category.replace('-', ' ');
        this.videoService.loadVideosbyCategory(param.category);
      }
    });
  }

  videoHtml(link: string): void {
    return this.embedService.embed(link, {
      query: { controls: 0 },
      attr: { width: '100%', height: '280' }
    });
  }
}
