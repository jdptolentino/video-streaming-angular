import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/internal/Observable';
import * as StoreActions from '../actions/store.action';
import * as FromStore from '../reducers/store.reducer';
import { map } from 'rxjs/internal/operators/map';
import { exhaustMap } from 'rxjs/internal/operators/exhaustMap';
import { Router } from '@angular/router';
import { VideoHttpService } from '@app/shared/services/http/video-http.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { tap } from 'rxjs/operators';

@Injectable()
export class StoreEffects {
  // Get All Videos
  @Effect()
  getVideos$: Observable<any> = this.actions$.pipe(
    ofType(StoreActions.loadVideo),
    map(action => action.request),
    exhaustMap(payload =>
      this.videoHttpService
        .all(payload)
        .pipe(map((response: any) => StoreActions.loadVideoSuccess({ videos: response.data })))
    )
  );

  // Get Videos
  @Effect()
  getVideosByCategory$: Observable<any> = this.actions$.pipe(
    ofType(StoreActions.loadVideosByCategory),
    exhaustMap(payload =>
      this.videoHttpService
        .getByCategory(payload.slug, payload.request)
        .pipe(map((response: any) => StoreActions.loadVideoSuccess({ videos: response.data })))
    )
  );

  // Set Token and Access
  @Effect({ dispatch: false })
  failed$ = this.actions$.pipe(
    ofType(StoreActions.failed),
    map(action => action.payload),
    tap((payload: any) => {
      this.snackbar.open(payload, '', {
        duration: 2000,
        verticalPosition: 'top',
        horizontalPosition: 'center',
        panelClass: 'custome-toast'
      });
    })
  );

  constructor(
    private router: Router,
    private actions$: Actions,
    private videoHttpService: VideoHttpService,
    private snackbar: MatSnackBar
  ) {}
}
