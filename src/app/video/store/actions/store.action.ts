import { createAction, props } from '@ngrx/store';
import { HttpQueryRequest } from '@app/shared/models/http/http-query-request.model';
import { Video } from '@app/shared/models/db/video.model';

export const loadVideo = createAction('[Video] Load Video', props<{ request: HttpQueryRequest }>());

export const loadVideosByCategory = createAction(
  '[Video] Load Video By Category',
  props<{ slug: string; request: HttpQueryRequest }>()
);

export const loadVideoSuccess = createAction('[Video] Load Video Success', props<{ videos: Video[] }>());

export const failed = createAction('[Video] Failed', props<{ payload: any }>());
