import { Video } from '@app/shared/models/db/video.model';

export default class StoreState {
  loading: boolean;
  loadingRef: string;
  processing: boolean;
  videos: Video[];
}

export const initializeState = (): StoreState => {
  return {
    loading: false,
    loadingRef: null,
    processing: false,
    videos: []
  };
};
