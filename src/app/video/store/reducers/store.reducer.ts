import { Action, createReducer, on } from '@ngrx/store';
import * as Actions from '../actions/store.action';
import StoreState, { initializeState } from '../store.state';

export const intialState = initializeState();

const reducer = createReducer(
  intialState,
  on(Actions.loadVideo, (state: StoreState, {}) => {
    return { ...state, loading: true };
  }),
  on(Actions.loadVideosByCategory, (state: StoreState, {}) => {
    return { ...state, loading: true };
  }),
  on(Actions.loadVideoSuccess, (state: StoreState, { videos }) => ({ ...state, videos: videos, loading: false })),

  on(Actions.failed, (state: StoreState, {}) => {
    return { ...state, loading: false };
  })
);

export function StoreReducer(state: StoreState | undefined, action: Action) {
  return reducer(state, action);
}
