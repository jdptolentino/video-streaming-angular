import { Injectable } from '@angular/core';

import { HttpService } from './http.services';
import { User } from '@app/shared/models/db/user.model';

@Injectable()
export class ProfileHttpService extends HttpService<User> {
  private relativeUrl = '/user';

  load() {
    return this.post(this.relativeUrl + '/profile', {});
  }

  update(id: any, params: User) {
    return this.post(this.relativeUrl + '/' + id + '/update', params);
  }
}
