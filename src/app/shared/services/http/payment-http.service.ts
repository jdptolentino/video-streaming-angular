import { Injectable } from '@angular/core';

import { HttpService } from './http.services';
import { HttpClient } from '@angular/common/http';
import { Plan } from '@app/shared/models/db/plan.model';
import { environment } from '@env/environment';

@Injectable()
export class PaymentHttpService extends HttpService<Plan> {
  private relativeUrl = '/payment';

  pay(user_id: number, plan_id: number) {
    return this.post(this.relativeUrl, { plan_id, user_id });
  }
}
