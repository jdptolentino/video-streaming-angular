import { Injectable } from '@angular/core';

import { HttpService } from './http.services';
import { AuthLogin } from '@app/shared/models/view/auth.model';

@Injectable()
export class LoginHttpService extends HttpService<AuthLogin> {
  private relativeUrl = '/login';

  login(data: AuthLogin) {
    return this.post(this.relativeUrl, data);
  }

  loginWithToken(data: any) {
    return this.post(this.relativeUrl + '/token', data);
  }

  checkUser() {
    return this.getAll('/check-user');
  }
}
