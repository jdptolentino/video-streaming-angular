import { Injectable } from '@angular/core';

import { HttpService } from './http.services';
import { Transaction } from '@app/shared/models/db/transaction.model';

@Injectable()
export class TransactionHttpService extends HttpService<Transaction> {
  private relativeUrl = '/transactions';

  all(request: any) {
    return this.getAll(this.relativeUrl, request);
  }
}
