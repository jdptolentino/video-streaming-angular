import { Injectable } from '@angular/core';

import { HttpService } from './http.services';
import { AuthForgotPassword } from '@app/shared/models/view/auth.model';

@Injectable()
export class ForgotHttpService extends HttpService<AuthForgotPassword> {
  private relativeUrl = '/password';

  forgot(data: AuthForgotPassword) {
    return this.post(this.relativeUrl + '/create', data);
  }

  find(token: string) {
    return this.getAll(this.relativeUrl + '/find/' + token);
  }

  reset(data: any) {
    return this.post(this.relativeUrl + '/reset', data);
  }
}
