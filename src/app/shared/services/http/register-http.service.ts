import { Injectable } from '@angular/core';

import { HttpService } from './http.services';
import { AuthRegister } from '@app/shared/models/view/auth.model';

@Injectable()
export class RegisterHttpService extends HttpService<AuthRegister> {
  private relativeUrl = '/register';

  register(data: AuthRegister) {
    return this.post(this.relativeUrl, data);
  }
}
