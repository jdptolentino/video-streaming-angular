import { Injectable } from '@angular/core';

import { HttpService } from './http.services';
import { Plan } from '@app/shared/models/db/plan.model';

@Injectable()
export class PlanHttpService extends HttpService<Plan> {
  private relativeUrl = '/subscription';

  all(request: any) {
    return this.getAll(this.relativeUrl, request);
  }

  choosePlan(id: number) {
    return this.post(this.relativeUrl + '/choose-plan', { plan: id });
  }

  changePlan(subscription: number, plan: number) {
    return this.post(this.relativeUrl + '/change-plan/' + subscription, { plan });
  }
}
