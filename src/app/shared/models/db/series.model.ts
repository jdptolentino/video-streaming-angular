export interface Series {
  id?: string;
  title?: string;
  description?: string;
  photo?: string;
  slug?: string;
  created_at?: string;
  updated_at?: string;
}
