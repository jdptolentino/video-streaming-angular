import { User } from './user.model';
import { Plan } from './plan.model';

export interface Transaction {
  id?: number;
  user_id: number;
  subscription_plan_id: number;
  status: string;
  amount: number;
  external_response?: object;
  plan?: Plan;
  user?: User;
  created_at: Date;
  updated_at: Date;
}
