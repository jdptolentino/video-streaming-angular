export interface Plan {
  id?: number;
  title?: string;
  amount?: number;
  description?: string;
  status?: string;
  created_at?: Date;
  updated_at?: Date;
}
