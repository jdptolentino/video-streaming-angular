export interface HttpQueryRequest {
  [key: string]: any;
}
