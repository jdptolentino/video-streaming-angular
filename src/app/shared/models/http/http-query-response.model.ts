export interface HttpQueryResponse<T> {
  total?: number;
  perPage?: number;
  page?: number;
  data?: T[];
}
