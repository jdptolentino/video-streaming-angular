export interface AuthLogin {
  email: string;
  password: string;
}

export interface AuthRegister {
  // user_name: string;
  email: string;
  password: string;
}

export interface AuthForgotPassword {
  email: string;
}

export interface AuthResetPassword {
  password: string;
  confirmPassword: string;
}

export interface AuthSignup {
  email: string;
  name: string;
  title?: string;
  phone?: string;
  number?: string;
  password: string;
  confirmPassword: string;
}
