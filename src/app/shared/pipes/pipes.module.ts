import { NgModule } from '@angular/core';
import { SafePipe } from './SafePipe';

@NgModule({
  declarations: [SafePipe],
  imports: [],
  exports: [SafePipe],
  providers: []
})
export class PipesModule {}
