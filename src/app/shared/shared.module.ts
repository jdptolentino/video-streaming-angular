import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoaderComponent } from './loader/loader.component';
import { PipesModule } from './pipes/pipes.module';
import { ContentLoaderModule } from '@netbasal/ngx-content-loader';
import { PageLoaderModule } from './modules/page-loader/page-loader.module';
import { PageLoaderComponent } from './modules/page-loader/page-loader.component';
import { DirectivesModule } from './directives/directives.module';

@NgModule({
  imports: [CommonModule, PipesModule, ContentLoaderModule, PageLoaderModule, DirectivesModule],
  declarations: [LoaderComponent, PageLoaderComponent],
  exports: [LoaderComponent, PipesModule, PageLoaderComponent, DirectivesModule]
})
export class SharedModule {}
