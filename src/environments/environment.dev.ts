import { env } from './.env';

export const environment = {
  production: false,
  hmr: false,
  version: env.npm_package_version,
  serverUrl: 'http://admin-stream.staging.heypogi.com/api',
  defaultLanguage: 'en-US',
  supportedLanguages: ['en-US', 'fr-FR']
};
