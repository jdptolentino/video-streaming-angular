!(function(t, e) {
  'use strict';
  'object' == typeof module && 'object' == typeof module.exports
    ? (module.exports = t.document
        ? e(t, !0)
        : function(t) {
            if (!t.document) throw new Error('jQuery requires a window with a document');
            return e(t);
          })
    : e(t);
})('undefined' != typeof window ? window : this, function(t, e) {
  'use strict';
  var n = [],
    i = t.document,
    r = Object.getPrototypeOf,
    o = n.slice,
    a = n.concat,
    s = n.push,
    l = n.indexOf,
    u = {},
    c = u.toString,
    d = u.hasOwnProperty,
    h = d.toString,
    f = h.call(Object),
    p = {},
    g = function(t) {
      return 'function' == typeof t && 'number' != typeof t.nodeType;
    },
    m = function(t) {
      return null != t && t === t.window;
    },
    v = { type: !0, src: !0, nonce: !0, noModule: !0 };
  function b(t, e, n) {
    var r,
      o,
      a = (n = n || i).createElement('script');
    if (((a.text = t), e)) for (r in v) (o = e[r] || (e.getAttribute && e.getAttribute(r))) && a.setAttribute(r, o);
    n.head.appendChild(a).parentNode.removeChild(a);
  }
  function y(t) {
    return null == t ? t + '' : 'object' == typeof t || 'function' == typeof t ? u[c.call(t)] || 'object' : typeof t;
  }
  var x = '3.4.1',
    w = function(t, e) {
      return new w.fn.init(t, e);
    },
    _ = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
  function C(t) {
    var e = !!t && 'length' in t && t.length,
      n = y(t);
    return !g(t) && !m(t) && ('array' === n || 0 === e || ('number' == typeof e && 0 < e && e - 1 in t));
  }
  (w.fn = w.prototype = {
    jquery: x,
    constructor: w,
    length: 0,
    toArray: function() {
      return o.call(this);
    },
    get: function(t) {
      return null == t ? o.call(this) : t < 0 ? this[t + this.length] : this[t];
    },
    pushStack: function(t) {
      var e = w.merge(this.constructor(), t);
      return (e.prevObject = this), e;
    },
    each: function(t) {
      return w.each(this, t);
    },
    map: function(t) {
      return this.pushStack(
        w.map(this, function(e, n) {
          return t.call(e, n, e);
        })
      );
    },
    slice: function() {
      return this.pushStack(o.apply(this, arguments));
    },
    first: function() {
      return this.eq(0);
    },
    last: function() {
      return this.eq(-1);
    },
    eq: function(t) {
      var e = this.length,
        n = +t + (t < 0 ? e : 0);
      return this.pushStack(0 <= n && n < e ? [this[n]] : []);
    },
    end: function() {
      return this.prevObject || this.constructor();
    },
    push: s,
    sort: n.sort,
    splice: n.splice
  }),
    (w.extend = w.fn.extend = function() {
      var t,
        e,
        n,
        i,
        r,
        o,
        a = arguments[0] || {},
        s = 1,
        l = arguments.length,
        u = !1;
      for (
        'boolean' == typeof a && ((u = a), (a = arguments[s] || {}), s++),
          'object' == typeof a || g(a) || (a = {}),
          s === l && ((a = this), s--);
        s < l;
        s++
      )
        if (null != (t = arguments[s]))
          for (e in t)
            (i = t[e]),
              '__proto__' !== e &&
                a !== i &&
                (u && i && (w.isPlainObject(i) || (r = Array.isArray(i)))
                  ? ((n = a[e]),
                    (o = r && !Array.isArray(n) ? [] : r || w.isPlainObject(n) ? n : {}),
                    (r = !1),
                    (a[e] = w.extend(u, o, i)))
                  : void 0 !== i && (a[e] = i));
      return a;
    }),
    w.extend({
      expando: 'jQuery' + (x + Math.random()).replace(/\D/g, ''),
      isReady: !0,
      error: function(t) {
        throw new Error(t);
      },
      noop: function() {},
      isPlainObject: function(t) {
        var e, n;
        return !(
          !t ||
          '[object Object]' !== c.call(t) ||
          ((e = r(t)) && ('function' != typeof (n = d.call(e, 'constructor') && e.constructor) || h.call(n) !== f))
        );
      },
      isEmptyObject: function(t) {
        var e;
        for (e in t) return !1;
        return !0;
      },
      globalEval: function(t, e) {
        b(t, { nonce: e && e.nonce });
      },
      each: function(t, e) {
        var n,
          i = 0;
        if (C(t)) for (n = t.length; i < n && !1 !== e.call(t[i], i, t[i]); i++);
        else for (i in t) if (!1 === e.call(t[i], i, t[i])) break;
        return t;
      },
      trim: function(t) {
        return null == t ? '' : (t + '').replace(_, '');
      },
      makeArray: function(t, e) {
        var n = e || [];
        return null != t && (C(Object(t)) ? w.merge(n, 'string' == typeof t ? [t] : t) : s.call(n, t)), n;
      },
      inArray: function(t, e, n) {
        return null == e ? -1 : l.call(e, t, n);
      },
      merge: function(t, e) {
        for (var n = +e.length, i = 0, r = t.length; i < n; i++) t[r++] = e[i];
        return (t.length = r), t;
      },
      grep: function(t, e, n) {
        for (var i = [], r = 0, o = t.length, a = !n; r < o; r++) !e(t[r], r) !== a && i.push(t[r]);
        return i;
      },
      map: function(t, e, n) {
        var i,
          r,
          o = 0,
          s = [];
        if (C(t)) for (i = t.length; o < i; o++) null != (r = e(t[o], o, n)) && s.push(r);
        else for (o in t) null != (r = e(t[o], o, n)) && s.push(r);
        return a.apply([], s);
      },
      guid: 1,
      support: p
    }),
    'function' == typeof Symbol && (w.fn[Symbol.iterator] = n[Symbol.iterator]),
    w.each('Boolean Number String Function Array Date RegExp Object Error Symbol'.split(' '), function(t, e) {
      u['[object ' + e + ']'] = e.toLowerCase();
    });
  var k = (function(t) {
    var e,
      n,
      i,
      r,
      o,
      a,
      s,
      l,
      u,
      c,
      d,
      h,
      f,
      p,
      g,
      m,
      v,
      b,
      y,
      x = 'sizzle' + 1 * new Date(),
      w = t.document,
      _ = 0,
      C = 0,
      k = lt(),
      S = lt(),
      T = lt(),
      D = lt(),
      A = function(t, e) {
        return t === e && (d = !0), 0;
      },
      E = {}.hasOwnProperty,
      M = [],
      I = M.pop,
      P = M.push,
      O = M.push,
      N = M.slice,
      L = function(t, e) {
        for (var n = 0, i = t.length; n < i; n++) if (t[n] === e) return n;
        return -1;
      },
      F =
        'checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped',
      R = '[\\x20\\t\\r\\n\\f]',
      j = '(?:\\\\.|[\\w-]|[^\0-\\xa0])+',
      H =
        '\\[' +
        R +
        '*(' +
        j +
        ')(?:' +
        R +
        '*([*^$|!~]?=)' +
        R +
        '*(?:\'((?:\\\\.|[^\\\\\'])*)\'|"((?:\\\\.|[^\\\\"])*)"|(' +
        j +
        '))|)' +
        R +
        '*\\]',
      B =
        ':(' +
        j +
        ')(?:\\(((\'((?:\\\\.|[^\\\\\'])*)\'|"((?:\\\\.|[^\\\\"])*)")|((?:\\\\.|[^\\\\()[\\]]|' +
        H +
        ')*)|.*)\\)|)',
      W = new RegExp(R + '+', 'g'),
      z = new RegExp('^' + R + '+|((?:^|[^\\\\])(?:\\\\.)*)' + R + '+$', 'g'),
      q = new RegExp('^' + R + '*,' + R + '*'),
      V = new RegExp('^' + R + '*([>+~]|' + R + ')' + R + '*'),
      U = new RegExp(R + '|>'),
      $ = new RegExp(B),
      Y = new RegExp('^' + j + '$'),
      K = {
        ID: new RegExp('^#(' + j + ')'),
        CLASS: new RegExp('^\\.(' + j + ')'),
        TAG: new RegExp('^(' + j + '|[*])'),
        ATTR: new RegExp('^' + H),
        PSEUDO: new RegExp('^' + B),
        CHILD: new RegExp(
          '^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(' +
            R +
            '*(even|odd|(([+-]|)(\\d*)n|)' +
            R +
            '*(?:([+-]|)' +
            R +
            '*(\\d+)|))' +
            R +
            '*\\)|)',
          'i'
        ),
        bool: new RegExp('^(?:' + F + ')$', 'i'),
        needsContext: new RegExp(
          '^' +
            R +
            '*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(' +
            R +
            '*((?:-\\d)?\\d*)' +
            R +
            '*\\)|)(?=[^-]|$)',
          'i'
        )
      },
      Q = /HTML$/i,
      X = /^(?:input|select|textarea|button)$/i,
      G = /^h\d$/i,
      J = /^[^{]+\{\s*\[native \w/,
      Z = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
      tt = /[+~]/,
      et = new RegExp('\\\\([\\da-f]{1,6}' + R + '?|(' + R + ')|.)', 'ig'),
      nt = function(t, e, n) {
        var i = '0x' + e - 65536;
        return i != i || n
          ? e
          : i < 0
          ? String.fromCharCode(i + 65536)
          : String.fromCharCode((i >> 10) | 55296, (1023 & i) | 56320);
      },
      it = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
      rt = function(t, e) {
        return e
          ? '\0' === t
            ? '�'
            : t.slice(0, -1) + '\\' + t.charCodeAt(t.length - 1).toString(16) + ' '
          : '\\' + t;
      },
      ot = function() {
        h();
      },
      at = xt(
        function(t) {
          return !0 === t.disabled && 'fieldset' === t.nodeName.toLowerCase();
        },
        { dir: 'parentNode', next: 'legend' }
      );
    try {
      O.apply((M = N.call(w.childNodes)), w.childNodes);
    } catch (e) {
      O = {
        apply: M.length
          ? function(t, e) {
              P.apply(t, N.call(e));
            }
          : function(t, e) {
              for (var n = t.length, i = 0; (t[n++] = e[i++]); );
              t.length = n - 1;
            }
      };
    }
    function st(t, e, i, r) {
      var o,
        s,
        u,
        c,
        d,
        p,
        v,
        b = e && e.ownerDocument,
        _ = e ? e.nodeType : 9;
      if (((i = i || []), 'string' != typeof t || !t || (1 !== _ && 9 !== _ && 11 !== _))) return i;
      if (!r && ((e ? e.ownerDocument || e : w) !== f && h(e), (e = e || f), g)) {
        if (11 !== _ && (d = Z.exec(t)))
          if ((o = d[1])) {
            if (9 === _) {
              if (!(u = e.getElementById(o))) return i;
              if (u.id === o) return i.push(u), i;
            } else if (b && (u = b.getElementById(o)) && y(e, u) && u.id === o) return i.push(u), i;
          } else {
            if (d[2]) return O.apply(i, e.getElementsByTagName(t)), i;
            if ((o = d[3]) && n.getElementsByClassName && e.getElementsByClassName)
              return O.apply(i, e.getElementsByClassName(o)), i;
          }
        if (n.qsa && !D[t + ' '] && (!m || !m.test(t)) && (1 !== _ || 'object' !== e.nodeName.toLowerCase())) {
          if (((v = t), (b = e), 1 === _ && U.test(t))) {
            for (
              (c = e.getAttribute('id')) ? (c = c.replace(it, rt)) : e.setAttribute('id', (c = x)),
                s = (p = a(t)).length;
              s--;

            )
              p[s] = '#' + c + ' ' + yt(p[s]);
            (v = p.join(',')), (b = (tt.test(t) && vt(e.parentNode)) || e);
          }
          try {
            return O.apply(i, b.querySelectorAll(v)), i;
          } catch (e) {
            D(t, !0);
          } finally {
            c === x && e.removeAttribute('id');
          }
        }
      }
      return l(t.replace(z, '$1'), e, i, r);
    }
    function lt() {
      var t = [];
      return function e(n, r) {
        return t.push(n + ' ') > i.cacheLength && delete e[t.shift()], (e[n + ' '] = r);
      };
    }
    function ut(t) {
      return (t[x] = !0), t;
    }
    function ct(t) {
      var e = f.createElement('fieldset');
      try {
        return !!t(e);
      } catch (t) {
        return !1;
      } finally {
        e.parentNode && e.parentNode.removeChild(e), (e = null);
      }
    }
    function dt(t, e) {
      for (var n = t.split('|'), r = n.length; r--; ) i.attrHandle[n[r]] = e;
    }
    function ht(t, e) {
      var n = e && t,
        i = n && 1 === t.nodeType && 1 === e.nodeType && t.sourceIndex - e.sourceIndex;
      if (i) return i;
      if (n) for (; (n = n.nextSibling); ) if (n === e) return -1;
      return t ? 1 : -1;
    }
    function ft(t) {
      return function(e) {
        return 'input' === e.nodeName.toLowerCase() && e.type === t;
      };
    }
    function pt(t) {
      return function(e) {
        var n = e.nodeName.toLowerCase();
        return ('input' === n || 'button' === n) && e.type === t;
      };
    }
    function gt(t) {
      return function(e) {
        return 'form' in e
          ? e.parentNode && !1 === e.disabled
            ? 'label' in e
              ? 'label' in e.parentNode
                ? e.parentNode.disabled === t
                : e.disabled === t
              : e.isDisabled === t || (e.isDisabled !== !t && at(e) === t)
            : e.disabled === t
          : 'label' in e && e.disabled === t;
      };
    }
    function mt(t) {
      return ut(function(e) {
        return (
          (e = +e),
          ut(function(n, i) {
            for (var r, o = t([], n.length, e), a = o.length; a--; ) n[(r = o[a])] && (n[r] = !(i[r] = n[r]));
          })
        );
      });
    }
    function vt(t) {
      return t && void 0 !== t.getElementsByTagName && t;
    }
    for (e in ((n = st.support = {}),
    (o = st.isXML = function(t) {
      var e = (t.ownerDocument || t).documentElement;
      return !Q.test(t.namespaceURI || (e && e.nodeName) || 'HTML');
    }),
    (h = st.setDocument = function(t) {
      var e,
        r,
        a = t ? t.ownerDocument || t : w;
      return (
        a !== f &&
          9 === a.nodeType &&
          a.documentElement &&
          ((p = (f = a).documentElement),
          (g = !o(f)),
          w !== f &&
            (r = f.defaultView) &&
            r.top !== r &&
            (r.addEventListener
              ? r.addEventListener('unload', ot, !1)
              : r.attachEvent && r.attachEvent('onunload', ot)),
          (n.attributes = ct(function(t) {
            return (t.className = 'i'), !t.getAttribute('className');
          })),
          (n.getElementsByTagName = ct(function(t) {
            return t.appendChild(f.createComment('')), !t.getElementsByTagName('*').length;
          })),
          (n.getElementsByClassName = J.test(f.getElementsByClassName)),
          (n.getById = ct(function(t) {
            return (p.appendChild(t).id = x), !f.getElementsByName || !f.getElementsByName(x).length;
          })),
          n.getById
            ? ((i.filter.ID = function(t) {
                var e = t.replace(et, nt);
                return function(t) {
                  return t.getAttribute('id') === e;
                };
              }),
              (i.find.ID = function(t, e) {
                if (void 0 !== e.getElementById && g) {
                  var n = e.getElementById(t);
                  return n ? [n] : [];
                }
              }))
            : ((i.filter.ID = function(t) {
                var e = t.replace(et, nt);
                return function(t) {
                  var n = void 0 !== t.getAttributeNode && t.getAttributeNode('id');
                  return n && n.value === e;
                };
              }),
              (i.find.ID = function(t, e) {
                if (void 0 !== e.getElementById && g) {
                  var n,
                    i,
                    r,
                    o = e.getElementById(t);
                  if (o) {
                    if ((n = o.getAttributeNode('id')) && n.value === t) return [o];
                    for (r = e.getElementsByName(t), i = 0; (o = r[i++]); )
                      if ((n = o.getAttributeNode('id')) && n.value === t) return [o];
                  }
                  return [];
                }
              })),
          (i.find.TAG = n.getElementsByTagName
            ? function(t, e) {
                return void 0 !== e.getElementsByTagName
                  ? e.getElementsByTagName(t)
                  : n.qsa
                  ? e.querySelectorAll(t)
                  : void 0;
              }
            : function(t, e) {
                var n,
                  i = [],
                  r = 0,
                  o = e.getElementsByTagName(t);
                if ('*' === t) {
                  for (; (n = o[r++]); ) 1 === n.nodeType && i.push(n);
                  return i;
                }
                return o;
              }),
          (i.find.CLASS =
            n.getElementsByClassName &&
            function(t, e) {
              if (void 0 !== e.getElementsByClassName && g) return e.getElementsByClassName(t);
            }),
          (v = []),
          (m = []),
          (n.qsa = J.test(f.querySelectorAll)) &&
            (ct(function(t) {
              (p.appendChild(t).innerHTML =
                "<a id='" +
                x +
                "'></a><select id='" +
                x +
                "-\r\\' msallowcapture=''><option selected=''></option></select>"),
                t.querySelectorAll("[msallowcapture^='']").length && m.push('[*^$]=' + R + '*(?:\'\'|"")'),
                t.querySelectorAll('[selected]').length || m.push('\\[' + R + '*(?:value|' + F + ')'),
                t.querySelectorAll('[id~=' + x + '-]').length || m.push('~='),
                t.querySelectorAll(':checked').length || m.push(':checked'),
                t.querySelectorAll('a#' + x + '+*').length || m.push('.#.+[+~]');
            }),
            ct(function(t) {
              t.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
              var e = f.createElement('input');
              e.setAttribute('type', 'hidden'),
                t.appendChild(e).setAttribute('name', 'D'),
                t.querySelectorAll('[name=d]').length && m.push('name' + R + '*[*^$|!~]?='),
                2 !== t.querySelectorAll(':enabled').length && m.push(':enabled', ':disabled'),
                (p.appendChild(t).disabled = !0),
                2 !== t.querySelectorAll(':disabled').length && m.push(':enabled', ':disabled'),
                t.querySelectorAll('*,:x'),
                m.push(',.*:');
            })),
          (n.matchesSelector = J.test(
            (b =
              p.matches || p.webkitMatchesSelector || p.mozMatchesSelector || p.oMatchesSelector || p.msMatchesSelector)
          )) &&
            ct(function(t) {
              (n.disconnectedMatch = b.call(t, '*')), b.call(t, "[s!='']:x"), v.push('!=', B);
            }),
          (m = m.length && new RegExp(m.join('|'))),
          (v = v.length && new RegExp(v.join('|'))),
          (e = J.test(p.compareDocumentPosition)),
          (y =
            e || J.test(p.contains)
              ? function(t, e) {
                  var n = 9 === t.nodeType ? t.documentElement : t,
                    i = e && e.parentNode;
                  return (
                    t === i ||
                    !(
                      !i ||
                      1 !== i.nodeType ||
                      !(n.contains ? n.contains(i) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(i))
                    )
                  );
                }
              : function(t, e) {
                  if (e) for (; (e = e.parentNode); ) if (e === t) return !0;
                  return !1;
                }),
          (A = e
            ? function(t, e) {
                if (t === e) return (d = !0), 0;
                var i = !t.compareDocumentPosition - !e.compareDocumentPosition;
                return (
                  i ||
                  (1 & (i = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1) ||
                  (!n.sortDetached && e.compareDocumentPosition(t) === i)
                    ? t === f || (t.ownerDocument === w && y(w, t))
                      ? -1
                      : e === f || (e.ownerDocument === w && y(w, e))
                      ? 1
                      : c
                      ? L(c, t) - L(c, e)
                      : 0
                    : 4 & i
                    ? -1
                    : 1)
                );
              }
            : function(t, e) {
                if (t === e) return (d = !0), 0;
                var n,
                  i = 0,
                  r = t.parentNode,
                  o = e.parentNode,
                  a = [t],
                  s = [e];
                if (!r || !o) return t === f ? -1 : e === f ? 1 : r ? -1 : o ? 1 : c ? L(c, t) - L(c, e) : 0;
                if (r === o) return ht(t, e);
                for (n = t; (n = n.parentNode); ) a.unshift(n);
                for (n = e; (n = n.parentNode); ) s.unshift(n);
                for (; a[i] === s[i]; ) i++;
                return i ? ht(a[i], s[i]) : a[i] === w ? -1 : s[i] === w ? 1 : 0;
              })),
        f
      );
    }),
    (st.matches = function(t, e) {
      return st(t, null, null, e);
    }),
    (st.matchesSelector = function(t, e) {
      if (
        ((t.ownerDocument || t) !== f && h(t),
        n.matchesSelector && g && !D[e + ' '] && (!v || !v.test(e)) && (!m || !m.test(e)))
      )
        try {
          var i = b.call(t, e);
          if (i || n.disconnectedMatch || (t.document && 11 !== t.document.nodeType)) return i;
        } catch (t) {
          D(e, !0);
        }
      return 0 < st(e, f, null, [t]).length;
    }),
    (st.contains = function(t, e) {
      return (t.ownerDocument || t) !== f && h(t), y(t, e);
    }),
    (st.attr = function(t, e) {
      (t.ownerDocument || t) !== f && h(t);
      var r = i.attrHandle[e.toLowerCase()],
        o = r && E.call(i.attrHandle, e.toLowerCase()) ? r(t, e, !g) : void 0;
      return void 0 !== o
        ? o
        : n.attributes || !g
        ? t.getAttribute(e)
        : (o = t.getAttributeNode(e)) && o.specified
        ? o.value
        : null;
    }),
    (st.escape = function(t) {
      return (t + '').replace(it, rt);
    }),
    (st.error = function(t) {
      throw new Error('Syntax error, unrecognized expression: ' + t);
    }),
    (st.uniqueSort = function(t) {
      var e,
        i = [],
        r = 0,
        o = 0;
      if (((d = !n.detectDuplicates), (c = !n.sortStable && t.slice(0)), t.sort(A), d)) {
        for (; (e = t[o++]); ) e === t[o] && (r = i.push(o));
        for (; r--; ) t.splice(i[r], 1);
      }
      return (c = null), t;
    }),
    (r = st.getText = function(t) {
      var e,
        n = '',
        i = 0,
        o = t.nodeType;
      if (o) {
        if (1 === o || 9 === o || 11 === o) {
          if ('string' == typeof t.textContent) return t.textContent;
          for (t = t.firstChild; t; t = t.nextSibling) n += r(t);
        } else if (3 === o || 4 === o) return t.nodeValue;
      } else for (; (e = t[i++]); ) n += r(e);
      return n;
    }),
    ((i = st.selectors = {
      cacheLength: 50,
      createPseudo: ut,
      match: K,
      attrHandle: {},
      find: {},
      relative: {
        '>': { dir: 'parentNode', first: !0 },
        ' ': { dir: 'parentNode' },
        '+': { dir: 'previousSibling', first: !0 },
        '~': { dir: 'previousSibling' }
      },
      preFilter: {
        ATTR: function(t) {
          return (
            (t[1] = t[1].replace(et, nt)),
            (t[3] = (t[3] || t[4] || t[5] || '').replace(et, nt)),
            '~=' === t[2] && (t[3] = ' ' + t[3] + ' '),
            t.slice(0, 4)
          );
        },
        CHILD: function(t) {
          return (
            (t[1] = t[1].toLowerCase()),
            'nth' === t[1].slice(0, 3)
              ? (t[3] || st.error(t[0]),
                (t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ('even' === t[3] || 'odd' === t[3]))),
                (t[5] = +(t[7] + t[8] || 'odd' === t[3])))
              : t[3] && st.error(t[0]),
            t
          );
        },
        PSEUDO: function(t) {
          var e,
            n = !t[6] && t[2];
          return K.CHILD.test(t[0])
            ? null
            : (t[3]
                ? (t[2] = t[4] || t[5] || '')
                : n &&
                  $.test(n) &&
                  (e = a(n, !0)) &&
                  (e = n.indexOf(')', n.length - e) - n.length) &&
                  ((t[0] = t[0].slice(0, e)), (t[2] = n.slice(0, e))),
              t.slice(0, 3));
        }
      },
      filter: {
        TAG: function(t) {
          var e = t.replace(et, nt).toLowerCase();
          return '*' === t
            ? function() {
                return !0;
              }
            : function(t) {
                return t.nodeName && t.nodeName.toLowerCase() === e;
              };
        },
        CLASS: function(t) {
          var e = k[t + ' '];
          return (
            e ||
            ((e = new RegExp('(^|' + R + ')' + t + '(' + R + '|$)')) &&
              k(t, function(t) {
                return e.test(
                  ('string' == typeof t.className && t.className) ||
                    (void 0 !== t.getAttribute && t.getAttribute('class')) ||
                    ''
                );
              }))
          );
        },
        ATTR: function(t, e, n) {
          return function(i) {
            var r = st.attr(i, t);
            return null == r
              ? '!=' === e
              : !e ||
                  ((r += ''),
                  '=' === e
                    ? r === n
                    : '!=' === e
                    ? r !== n
                    : '^=' === e
                    ? n && 0 === r.indexOf(n)
                    : '*=' === e
                    ? n && -1 < r.indexOf(n)
                    : '$=' === e
                    ? n && r.slice(-n.length) === n
                    : '~=' === e
                    ? -1 < (' ' + r.replace(W, ' ') + ' ').indexOf(n)
                    : '|=' === e && (r === n || r.slice(0, n.length + 1) === n + '-'));
          };
        },
        CHILD: function(t, e, n, i, r) {
          var o = 'nth' !== t.slice(0, 3),
            a = 'last' !== t.slice(-4),
            s = 'of-type' === e;
          return 1 === i && 0 === r
            ? function(t) {
                return !!t.parentNode;
              }
            : function(e, n, l) {
                var u,
                  c,
                  d,
                  h,
                  f,
                  p,
                  g = o !== a ? 'nextSibling' : 'previousSibling',
                  m = e.parentNode,
                  v = s && e.nodeName.toLowerCase(),
                  b = !l && !s,
                  y = !1;
                if (m) {
                  if (o) {
                    for (; g; ) {
                      for (h = e; (h = h[g]); ) if (s ? h.nodeName.toLowerCase() === v : 1 === h.nodeType) return !1;
                      p = g = 'only' === t && !p && 'nextSibling';
                    }
                    return !0;
                  }
                  if (((p = [a ? m.firstChild : m.lastChild]), a && b)) {
                    for (
                      y =
                        (f =
                          (u =
                            (c = (d = (h = m)[x] || (h[x] = {}))[h.uniqueID] || (d[h.uniqueID] = {}))[t] || [])[0] ===
                            _ && u[1]) && u[2],
                        h = f && m.childNodes[f];
                      (h = (++f && h && h[g]) || (y = f = 0) || p.pop());

                    )
                      if (1 === h.nodeType && ++y && h === e) {
                        c[t] = [_, f, y];
                        break;
                      }
                  } else if (
                    (b &&
                      (y = f =
                        (u = (c = (d = (h = e)[x] || (h[x] = {}))[h.uniqueID] || (d[h.uniqueID] = {}))[t] || [])[0] ===
                          _ && u[1]),
                    !1 === y)
                  )
                    for (
                      ;
                      (h = (++f && h && h[g]) || (y = f = 0) || p.pop()) &&
                      ((s ? h.nodeName.toLowerCase() !== v : 1 !== h.nodeType) ||
                        !++y ||
                        (b && ((c = (d = h[x] || (h[x] = {}))[h.uniqueID] || (d[h.uniqueID] = {}))[t] = [_, y]),
                        h !== e));

                    );
                  return (y -= r) === i || (y % i == 0 && 0 <= y / i);
                }
              };
        },
        PSEUDO: function(t, e) {
          var n,
            r = i.pseudos[t] || i.setFilters[t.toLowerCase()] || st.error('unsupported pseudo: ' + t);
          return r[x]
            ? r(e)
            : 1 < r.length
            ? ((n = [t, t, '', e]),
              i.setFilters.hasOwnProperty(t.toLowerCase())
                ? ut(function(t, n) {
                    for (var i, o = r(t, e), a = o.length; a--; ) t[(i = L(t, o[a]))] = !(n[i] = o[a]);
                  })
                : function(t) {
                    return r(t, 0, n);
                  })
            : r;
        }
      },
      pseudos: {
        not: ut(function(t) {
          var e = [],
            n = [],
            i = s(t.replace(z, '$1'));
          return i[x]
            ? ut(function(t, e, n, r) {
                for (var o, a = i(t, null, r, []), s = t.length; s--; ) (o = a[s]) && (t[s] = !(e[s] = o));
              })
            : function(t, r, o) {
                return (e[0] = t), i(e, null, o, n), (e[0] = null), !n.pop();
              };
        }),
        has: ut(function(t) {
          return function(e) {
            return 0 < st(t, e).length;
          };
        }),
        contains: ut(function(t) {
          return (
            (t = t.replace(et, nt)),
            function(e) {
              return -1 < (e.textContent || r(e)).indexOf(t);
            }
          );
        }),
        lang: ut(function(t) {
          return (
            Y.test(t || '') || st.error('unsupported lang: ' + t),
            (t = t.replace(et, nt).toLowerCase()),
            function(e) {
              var n;
              do {
                if ((n = g ? e.lang : e.getAttribute('xml:lang') || e.getAttribute('lang')))
                  return (n = n.toLowerCase()) === t || 0 === n.indexOf(t + '-');
              } while ((e = e.parentNode) && 1 === e.nodeType);
              return !1;
            }
          );
        }),
        target: function(e) {
          var n = t.location && t.location.hash;
          return n && n.slice(1) === e.id;
        },
        root: function(t) {
          return t === p;
        },
        focus: function(t) {
          return t === f.activeElement && (!f.hasFocus || f.hasFocus()) && !!(t.type || t.href || ~t.tabIndex);
        },
        enabled: gt(!1),
        disabled: gt(!0),
        checked: function(t) {
          var e = t.nodeName.toLowerCase();
          return ('input' === e && !!t.checked) || ('option' === e && !!t.selected);
        },
        selected: function(t) {
          return !0 === t.selected;
        },
        empty: function(t) {
          for (t = t.firstChild; t; t = t.nextSibling) if (t.nodeType < 6) return !1;
          return !0;
        },
        parent: function(t) {
          return !i.pseudos.empty(t);
        },
        header: function(t) {
          return G.test(t.nodeName);
        },
        input: function(t) {
          return X.test(t.nodeName);
        },
        button: function(t) {
          var e = t.nodeName.toLowerCase();
          return ('input' === e && 'button' === t.type) || 'button' === e;
        },
        text: function(t) {
          var e;
          return (
            'input' === t.nodeName.toLowerCase() &&
            'text' === t.type &&
            (null == (e = t.getAttribute('type')) || 'text' === e.toLowerCase())
          );
        },
        first: mt(function() {
          return [0];
        }),
        last: mt(function(t, e) {
          return [e - 1];
        }),
        eq: mt(function(t, e, n) {
          return [n < 0 ? n + e : n];
        }),
        even: mt(function(t, e) {
          for (var n = 0; n < e; n += 2) t.push(n);
          return t;
        }),
        odd: mt(function(t, e) {
          for (var n = 1; n < e; n += 2) t.push(n);
          return t;
        }),
        lt: mt(function(t, e, n) {
          for (var i = n < 0 ? n + e : e < n ? e : n; 0 <= --i; ) t.push(i);
          return t;
        }),
        gt: mt(function(t, e, n) {
          for (var i = n < 0 ? n + e : n; ++i < e; ) t.push(i);
          return t;
        })
      }
    }).pseudos.nth = i.pseudos.eq),
    { radio: !0, checkbox: !0, file: !0, password: !0, image: !0 }))
      i.pseudos[e] = ft(e);
    for (e in { submit: !0, reset: !0 }) i.pseudos[e] = pt(e);
    function bt() {}
    function yt(t) {
      for (var e = 0, n = t.length, i = ''; e < n; e++) i += t[e].value;
      return i;
    }
    function xt(t, e, n) {
      var i = e.dir,
        r = e.next,
        o = r || i,
        a = n && 'parentNode' === o,
        s = C++;
      return e.first
        ? function(e, n, r) {
            for (; (e = e[i]); ) if (1 === e.nodeType || a) return t(e, n, r);
            return !1;
          }
        : function(e, n, l) {
            var u,
              c,
              d,
              h = [_, s];
            if (l) {
              for (; (e = e[i]); ) if ((1 === e.nodeType || a) && t(e, n, l)) return !0;
            } else
              for (; (e = e[i]); )
                if (1 === e.nodeType || a)
                  if (
                    ((c = (d = e[x] || (e[x] = {}))[e.uniqueID] || (d[e.uniqueID] = {})),
                    r && r === e.nodeName.toLowerCase())
                  )
                    e = e[i] || e;
                  else {
                    if ((u = c[o]) && u[0] === _ && u[1] === s) return (h[2] = u[2]);
                    if (((c[o] = h)[2] = t(e, n, l))) return !0;
                  }
            return !1;
          };
    }
    function wt(t) {
      return 1 < t.length
        ? function(e, n, i) {
            for (var r = t.length; r--; ) if (!t[r](e, n, i)) return !1;
            return !0;
          }
        : t[0];
    }
    function _t(t, e, n, i, r) {
      for (var o, a = [], s = 0, l = t.length, u = null != e; s < l; s++)
        (o = t[s]) && ((n && !n(o, i, r)) || (a.push(o), u && e.push(s)));
      return a;
    }
    function Ct(t, e, n, i, r, o) {
      return (
        i && !i[x] && (i = Ct(i)),
        r && !r[x] && (r = Ct(r, o)),
        ut(function(o, a, s, l) {
          var u,
            c,
            d,
            h = [],
            f = [],
            p = a.length,
            g =
              o ||
              (function(t, e, n) {
                for (var i = 0, r = e.length; i < r; i++) st(t, e[i], n);
                return n;
              })(e || '*', s.nodeType ? [s] : s, []),
            m = !t || (!o && e) ? g : _t(g, h, t, s, l),
            v = n ? (r || (o ? t : p || i) ? [] : a) : m;
          if ((n && n(m, v, s, l), i))
            for (u = _t(v, f), i(u, [], s, l), c = u.length; c--; ) (d = u[c]) && (v[f[c]] = !(m[f[c]] = d));
          if (o) {
            if (r || t) {
              if (r) {
                for (u = [], c = v.length; c--; ) (d = v[c]) && u.push((m[c] = d));
                r(null, (v = []), u, l);
              }
              for (c = v.length; c--; ) (d = v[c]) && -1 < (u = r ? L(o, d) : h[c]) && (o[u] = !(a[u] = d));
            }
          } else (v = _t(v === a ? v.splice(p, v.length) : v)), r ? r(null, a, v, l) : O.apply(a, v);
        })
      );
    }
    function kt(t) {
      for (
        var e,
          n,
          r,
          o = t.length,
          a = i.relative[t[0].type],
          s = a || i.relative[' '],
          l = a ? 1 : 0,
          c = xt(
            function(t) {
              return t === e;
            },
            s,
            !0
          ),
          d = xt(
            function(t) {
              return -1 < L(e, t);
            },
            s,
            !0
          ),
          h = [
            function(t, n, i) {
              var r = (!a && (i || n !== u)) || ((e = n).nodeType ? c(t, n, i) : d(t, n, i));
              return (e = null), r;
            }
          ];
        l < o;
        l++
      )
        if ((n = i.relative[t[l].type])) h = [xt(wt(h), n)];
        else {
          if ((n = i.filter[t[l].type].apply(null, t[l].matches))[x]) {
            for (r = ++l; r < o && !i.relative[t[r].type]; r++);
            return Ct(
              1 < l && wt(h),
              1 < l && yt(t.slice(0, l - 1).concat({ value: ' ' === t[l - 2].type ? '*' : '' })).replace(z, '$1'),
              n,
              l < r && kt(t.slice(l, r)),
              r < o && kt((t = t.slice(r))),
              r < o && yt(t)
            );
          }
          h.push(n);
        }
      return wt(h);
    }
    return (
      (bt.prototype = i.filters = i.pseudos),
      (i.setFilters = new bt()),
      (a = st.tokenize = function(t, e) {
        var n,
          r,
          o,
          a,
          s,
          l,
          u,
          c = S[t + ' '];
        if (c) return e ? 0 : c.slice(0);
        for (s = t, l = [], u = i.preFilter; s; ) {
          for (a in ((n && !(r = q.exec(s))) || (r && (s = s.slice(r[0].length) || s), l.push((o = []))),
          (n = !1),
          (r = V.exec(s)) &&
            ((n = r.shift()), o.push({ value: n, type: r[0].replace(z, ' ') }), (s = s.slice(n.length))),
          i.filter))
            !(r = K[a].exec(s)) ||
              (u[a] && !(r = u[a](r))) ||
              ((n = r.shift()), o.push({ value: n, type: a, matches: r }), (s = s.slice(n.length)));
          if (!n) break;
        }
        return e ? s.length : s ? st.error(t) : S(t, l).slice(0);
      }),
      (s = st.compile = function(t, e) {
        var n,
          r,
          o,
          s,
          l,
          c,
          d = [],
          p = [],
          m = T[t + ' '];
        if (!m) {
          for (e || (e = a(t)), n = e.length; n--; ) (m = kt(e[n]))[x] ? d.push(m) : p.push(m);
          (m = T(
            t,
            ((r = p),
            (s = 0 < (o = d).length),
            (l = 0 < r.length),
            (c = function(t, e, n, a, c) {
              var d,
                p,
                m,
                v = 0,
                b = '0',
                y = t && [],
                x = [],
                w = u,
                C = t || (l && i.find.TAG('*', c)),
                k = (_ += null == w ? 1 : Math.random() || 0.1),
                S = C.length;
              for (c && (u = e === f || e || c); b !== S && null != (d = C[b]); b++) {
                if (l && d) {
                  for (p = 0, e || d.ownerDocument === f || (h(d), (n = !g)); (m = r[p++]); )
                    if (m(d, e || f, n)) {
                      a.push(d);
                      break;
                    }
                  c && (_ = k);
                }
                s && ((d = !m && d) && v--, t && y.push(d));
              }
              if (((v += b), s && b !== v)) {
                for (p = 0; (m = o[p++]); ) m(y, x, e, n);
                if (t) {
                  if (0 < v) for (; b--; ) y[b] || x[b] || (x[b] = I.call(a));
                  x = _t(x);
                }
                O.apply(a, x), c && !t && 0 < x.length && 1 < v + o.length && st.uniqueSort(a);
              }
              return c && ((_ = k), (u = w)), y;
            }),
            s ? ut(c) : c)
          )).selector = t;
        }
        return m;
      }),
      (l = st.select = function(t, e, n, r) {
        var o,
          l,
          u,
          c,
          d,
          h = 'function' == typeof t && t,
          f = !r && a((t = h.selector || t));
        if (((n = n || []), 1 === f.length)) {
          if (
            2 < (l = f[0] = f[0].slice(0)).length &&
            'ID' === (u = l[0]).type &&
            9 === e.nodeType &&
            g &&
            i.relative[l[1].type]
          ) {
            if (!(e = (i.find.ID(u.matches[0].replace(et, nt), e) || [])[0])) return n;
            h && (e = e.parentNode), (t = t.slice(l.shift().value.length));
          }
          for (o = K.needsContext.test(t) ? 0 : l.length; o-- && !i.relative[(c = (u = l[o]).type)]; )
            if (
              (d = i.find[c]) &&
              (r = d(u.matches[0].replace(et, nt), (tt.test(l[0].type) && vt(e.parentNode)) || e))
            ) {
              if ((l.splice(o, 1), !(t = r.length && yt(l)))) return O.apply(n, r), n;
              break;
            }
        }
        return (h || s(t, f))(r, e, !g, n, !e || (tt.test(t) && vt(e.parentNode)) || e), n;
      }),
      (n.sortStable =
        x
          .split('')
          .sort(A)
          .join('') === x),
      (n.detectDuplicates = !!d),
      h(),
      (n.sortDetached = ct(function(t) {
        return 1 & t.compareDocumentPosition(f.createElement('fieldset'));
      })),
      ct(function(t) {
        return (t.innerHTML = "<a href='#'></a>"), '#' === t.firstChild.getAttribute('href');
      }) ||
        dt('type|href|height|width', function(t, e, n) {
          if (!n) return t.getAttribute(e, 'type' === e.toLowerCase() ? 1 : 2);
        }),
      (n.attributes &&
        ct(function(t) {
          return (
            (t.innerHTML = '<input/>'),
            t.firstChild.setAttribute('value', ''),
            '' === t.firstChild.getAttribute('value')
          );
        })) ||
        dt('value', function(t, e, n) {
          if (!n && 'input' === t.nodeName.toLowerCase()) return t.defaultValue;
        }),
      ct(function(t) {
        return null == t.getAttribute('disabled');
      }) ||
        dt(F, function(t, e, n) {
          var i;
          if (!n) return !0 === t[e] ? e.toLowerCase() : (i = t.getAttributeNode(e)) && i.specified ? i.value : null;
        }),
      st
    );
  })(t);
  (w.find = k),
    (w.expr = k.selectors),
    (w.expr[':'] = w.expr.pseudos),
    (w.uniqueSort = w.unique = k.uniqueSort),
    (w.text = k.getText),
    (w.isXMLDoc = k.isXML),
    (w.contains = k.contains),
    (w.escapeSelector = k.escape);
  var S = function(t, e, n) {
      for (var i = [], r = void 0 !== n; (t = t[e]) && 9 !== t.nodeType; )
        if (1 === t.nodeType) {
          if (r && w(t).is(n)) break;
          i.push(t);
        }
      return i;
    },
    T = function(t, e) {
      for (var n = []; t; t = t.nextSibling) 1 === t.nodeType && t !== e && n.push(t);
      return n;
    },
    D = w.expr.match.needsContext;
  function A(t, e) {
    return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase();
  }
  var E = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;
  function M(t, e, n) {
    return g(e)
      ? w.grep(t, function(t, i) {
          return !!e.call(t, i, t) !== n;
        })
      : e.nodeType
      ? w.grep(t, function(t) {
          return (t === e) !== n;
        })
      : 'string' != typeof e
      ? w.grep(t, function(t) {
          return -1 < l.call(e, t) !== n;
        })
      : w.filter(e, t, n);
  }
  (w.filter = function(t, e, n) {
    var i = e[0];
    return (
      n && (t = ':not(' + t + ')'),
      1 === e.length && 1 === i.nodeType
        ? w.find.matchesSelector(i, t)
          ? [i]
          : []
        : w.find.matches(
            t,
            w.grep(e, function(t) {
              return 1 === t.nodeType;
            })
          )
    );
  }),
    w.fn.extend({
      find: function(t) {
        var e,
          n,
          i = this.length,
          r = this;
        if ('string' != typeof t)
          return this.pushStack(
            w(t).filter(function() {
              for (e = 0; e < i; e++) if (w.contains(r[e], this)) return !0;
            })
          );
        for (n = this.pushStack([]), e = 0; e < i; e++) w.find(t, r[e], n);
        return 1 < i ? w.uniqueSort(n) : n;
      },
      filter: function(t) {
        return this.pushStack(M(this, t || [], !1));
      },
      not: function(t) {
        return this.pushStack(M(this, t || [], !0));
      },
      is: function(t) {
        return !!M(this, 'string' == typeof t && D.test(t) ? w(t) : t || [], !1).length;
      }
    });
  var I,
    P = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
  ((w.fn.init = function(t, e, n) {
    var r, o;
    if (!t) return this;
    if (((n = n || I), 'string' == typeof t)) {
      if (!(r = '<' === t[0] && '>' === t[t.length - 1] && 3 <= t.length ? [null, t, null] : P.exec(t)) || (!r[1] && e))
        return !e || e.jquery ? (e || n).find(t) : this.constructor(e).find(t);
      if (r[1]) {
        if (
          (w.merge(
            this,
            w.parseHTML(r[1], (e = e instanceof w ? e[0] : e) && e.nodeType ? e.ownerDocument || e : i, !0)
          ),
          E.test(r[1]) && w.isPlainObject(e))
        )
          for (r in e) g(this[r]) ? this[r](e[r]) : this.attr(r, e[r]);
        return this;
      }
      return (o = i.getElementById(r[2])) && ((this[0] = o), (this.length = 1)), this;
    }
    return t.nodeType
      ? ((this[0] = t), (this.length = 1), this)
      : g(t)
      ? void 0 !== n.ready
        ? n.ready(t)
        : t(w)
      : w.makeArray(t, this);
  }).prototype = w.fn),
    (I = w(i));
  var O = /^(?:parents|prev(?:Until|All))/,
    N = { children: !0, contents: !0, next: !0, prev: !0 };
  function L(t, e) {
    for (; (t = t[e]) && 1 !== t.nodeType; );
    return t;
  }
  w.fn.extend({
    has: function(t) {
      var e = w(t, this),
        n = e.length;
      return this.filter(function() {
        for (var t = 0; t < n; t++) if (w.contains(this, e[t])) return !0;
      });
    },
    closest: function(t, e) {
      var n,
        i = 0,
        r = this.length,
        o = [],
        a = 'string' != typeof t && w(t);
      if (!D.test(t))
        for (; i < r; i++)
          for (n = this[i]; n && n !== e; n = n.parentNode)
            if (n.nodeType < 11 && (a ? -1 < a.index(n) : 1 === n.nodeType && w.find.matchesSelector(n, t))) {
              o.push(n);
              break;
            }
      return this.pushStack(1 < o.length ? w.uniqueSort(o) : o);
    },
    index: function(t) {
      return t
        ? 'string' == typeof t
          ? l.call(w(t), this[0])
          : l.call(this, t.jquery ? t[0] : t)
        : this[0] && this[0].parentNode
        ? this.first().prevAll().length
        : -1;
    },
    add: function(t, e) {
      return this.pushStack(w.uniqueSort(w.merge(this.get(), w(t, e))));
    },
    addBack: function(t) {
      return this.add(null == t ? this.prevObject : this.prevObject.filter(t));
    }
  }),
    w.each(
      {
        parent: function(t) {
          var e = t.parentNode;
          return e && 11 !== e.nodeType ? e : null;
        },
        parents: function(t) {
          return S(t, 'parentNode');
        },
        parentsUntil: function(t, e, n) {
          return S(t, 'parentNode', n);
        },
        next: function(t) {
          return L(t, 'nextSibling');
        },
        prev: function(t) {
          return L(t, 'previousSibling');
        },
        nextAll: function(t) {
          return S(t, 'nextSibling');
        },
        prevAll: function(t) {
          return S(t, 'previousSibling');
        },
        nextUntil: function(t, e, n) {
          return S(t, 'nextSibling', n);
        },
        prevUntil: function(t, e, n) {
          return S(t, 'previousSibling', n);
        },
        siblings: function(t) {
          return T((t.parentNode || {}).firstChild, t);
        },
        children: function(t) {
          return T(t.firstChild);
        },
        contents: function(t) {
          return void 0 !== t.contentDocument
            ? t.contentDocument
            : (A(t, 'template') && (t = t.content || t), w.merge([], t.childNodes));
        }
      },
      function(t, e) {
        w.fn[t] = function(n, i) {
          var r = w.map(this, e, n);
          return (
            'Until' !== t.slice(-5) && (i = n),
            i && 'string' == typeof i && (r = w.filter(i, r)),
            1 < this.length && (N[t] || w.uniqueSort(r), O.test(t) && r.reverse()),
            this.pushStack(r)
          );
        };
      }
    );
  var F = /[^\x20\t\r\n\f]+/g;
  function R(t) {
    return t;
  }
  function j(t) {
    throw t;
  }
  function H(t, e, n, i) {
    var r;
    try {
      t && g((r = t.promise))
        ? r
            .call(t)
            .done(e)
            .fail(n)
        : t && g((r = t.then))
        ? r.call(t, e, n)
        : e.apply(void 0, [t].slice(i));
    } catch (t) {
      n.apply(void 0, [t]);
    }
  }
  (w.Callbacks = function(t) {
    var e;
    t =
      'string' == typeof t
        ? ((e = {}),
          w.each(t.match(F) || [], function(t, n) {
            e[n] = !0;
          }),
          e)
        : w.extend({}, t);
    var n,
      i,
      r,
      o,
      a = [],
      s = [],
      l = -1,
      u = function() {
        for (o = o || t.once, r = n = !0; s.length; l = -1)
          for (i = s.shift(); ++l < a.length; )
            !1 === a[l].apply(i[0], i[1]) && t.stopOnFalse && ((l = a.length), (i = !1));
        t.memory || (i = !1), (n = !1), o && (a = i ? [] : '');
      },
      c = {
        add: function() {
          return (
            a &&
              (i && !n && ((l = a.length - 1), s.push(i)),
              (function e(n) {
                w.each(n, function(n, i) {
                  g(i) ? (t.unique && c.has(i)) || a.push(i) : i && i.length && 'string' !== y(i) && e(i);
                });
              })(arguments),
              i && !n && u()),
            this
          );
        },
        remove: function() {
          return (
            w.each(arguments, function(t, e) {
              for (var n; -1 < (n = w.inArray(e, a, n)); ) a.splice(n, 1), n <= l && l--;
            }),
            this
          );
        },
        has: function(t) {
          return t ? -1 < w.inArray(t, a) : 0 < a.length;
        },
        empty: function() {
          return a && (a = []), this;
        },
        disable: function() {
          return (o = s = []), (a = i = ''), this;
        },
        disabled: function() {
          return !a;
        },
        lock: function() {
          return (o = s = []), i || n || (a = i = ''), this;
        },
        locked: function() {
          return !!o;
        },
        fireWith: function(t, e) {
          return o || ((e = [t, (e = e || []).slice ? e.slice() : e]), s.push(e), n || u()), this;
        },
        fire: function() {
          return c.fireWith(this, arguments), this;
        },
        fired: function() {
          return !!r;
        }
      };
    return c;
  }),
    w.extend({
      Deferred: function(e) {
        var n = [
            ['notify', 'progress', w.Callbacks('memory'), w.Callbacks('memory'), 2],
            ['resolve', 'done', w.Callbacks('once memory'), w.Callbacks('once memory'), 0, 'resolved'],
            ['reject', 'fail', w.Callbacks('once memory'), w.Callbacks('once memory'), 1, 'rejected']
          ],
          i = 'pending',
          r = {
            state: function() {
              return i;
            },
            always: function() {
              return o.done(arguments).fail(arguments), this;
            },
            catch: function(t) {
              return r.then(null, t);
            },
            pipe: function() {
              var t = arguments;
              return w
                .Deferred(function(e) {
                  w.each(n, function(n, i) {
                    var r = g(t[i[4]]) && t[i[4]];
                    o[i[1]](function() {
                      var t = r && r.apply(this, arguments);
                      t && g(t.promise)
                        ? t
                            .promise()
                            .progress(e.notify)
                            .done(e.resolve)
                            .fail(e.reject)
                        : e[i[0] + 'With'](this, r ? [t] : arguments);
                    });
                  }),
                    (t = null);
                })
                .promise();
            },
            then: function(e, i, r) {
              var o = 0;
              function a(e, n, i, r) {
                return function() {
                  var s = this,
                    l = arguments,
                    u = function() {
                      var t, u;
                      if (!(e < o)) {
                        if ((t = i.apply(s, l)) === n.promise()) throw new TypeError('Thenable self-resolution');
                        g((u = t && ('object' == typeof t || 'function' == typeof t) && t.then))
                          ? r
                            ? u.call(t, a(o, n, R, r), a(o, n, j, r))
                            : (o++, u.call(t, a(o, n, R, r), a(o, n, j, r), a(o, n, R, n.notifyWith)))
                          : (i !== R && ((s = void 0), (l = [t])), (r || n.resolveWith)(s, l));
                      }
                    },
                    c = r
                      ? u
                      : function() {
                          try {
                            u();
                          } catch (u) {
                            w.Deferred.exceptionHook && w.Deferred.exceptionHook(u, c.stackTrace),
                              o <= e + 1 && (i !== j && ((s = void 0), (l = [u])), n.rejectWith(s, l));
                          }
                        };
                  e ? c() : (w.Deferred.getStackHook && (c.stackTrace = w.Deferred.getStackHook()), t.setTimeout(c));
                };
              }
              return w
                .Deferred(function(t) {
                  n[0][3].add(a(0, t, g(r) ? r : R, t.notifyWith)),
                    n[1][3].add(a(0, t, g(e) ? e : R)),
                    n[2][3].add(a(0, t, g(i) ? i : j));
                })
                .promise();
            },
            promise: function(t) {
              return null != t ? w.extend(t, r) : r;
            }
          },
          o = {};
        return (
          w.each(n, function(t, e) {
            var a = e[2],
              s = e[5];
            (r[e[1]] = a.add),
              s &&
                a.add(
                  function() {
                    i = s;
                  },
                  n[3 - t][2].disable,
                  n[3 - t][3].disable,
                  n[0][2].lock,
                  n[0][3].lock
                ),
              a.add(e[3].fire),
              (o[e[0]] = function() {
                return o[e[0] + 'With'](this === o ? void 0 : this, arguments), this;
              }),
              (o[e[0] + 'With'] = a.fireWith);
          }),
          r.promise(o),
          e && e.call(o, o),
          o
        );
      },
      when: function(t) {
        var e = arguments.length,
          n = e,
          i = Array(n),
          r = o.call(arguments),
          a = w.Deferred(),
          s = function(t) {
            return function(n) {
              (i[t] = this), (r[t] = 1 < arguments.length ? o.call(arguments) : n), --e || a.resolveWith(i, r);
            };
          };
        if (e <= 1 && (H(t, a.done(s(n)).resolve, a.reject, !e), 'pending' === a.state() || g(r[n] && r[n].then)))
          return a.then();
        for (; n--; ) H(r[n], s(n), a.reject);
        return a.promise();
      }
    });
  var B = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
  (w.Deferred.exceptionHook = function(e, n) {
    t.console &&
      t.console.warn &&
      e &&
      B.test(e.name) &&
      t.console.warn('jQuery.Deferred exception: ' + e.message, e.stack, n);
  }),
    (w.readyException = function(e) {
      t.setTimeout(function() {
        throw e;
      });
    });
  var W = w.Deferred();
  function z() {
    i.removeEventListener('DOMContentLoaded', z), t.removeEventListener('load', z), w.ready();
  }
  (w.fn.ready = function(t) {
    return (
      W.then(t).catch(function(t) {
        w.readyException(t);
      }),
      this
    );
  }),
    w.extend({
      isReady: !1,
      readyWait: 1,
      ready: function(t) {
        (!0 === t ? --w.readyWait : w.isReady) ||
          ((w.isReady = !0) !== t && 0 < --w.readyWait) ||
          W.resolveWith(i, [w]);
      }
    }),
    (w.ready.then = W.then),
    'complete' === i.readyState || ('loading' !== i.readyState && !i.documentElement.doScroll)
      ? t.setTimeout(w.ready)
      : (i.addEventListener('DOMContentLoaded', z), t.addEventListener('load', z));
  var q = function(t, e, n, i, r, o, a) {
      var s = 0,
        l = t.length,
        u = null == n;
      if ('object' === y(n)) for (s in ((r = !0), n)) q(t, e, s, n[s], !0, o, a);
      else if (
        void 0 !== i &&
        ((r = !0),
        g(i) || (a = !0),
        u &&
          (a
            ? (e.call(t, i), (e = null))
            : ((u = e),
              (e = function(t, e, n) {
                return u.call(w(t), n);
              }))),
        e)
      )
        for (; s < l; s++) e(t[s], n, a ? i : i.call(t[s], s, e(t[s], n)));
      return r ? t : u ? e.call(t) : l ? e(t[0], n) : o;
    },
    V = /^-ms-/,
    U = /-([a-z])/g;
  function $(t, e) {
    return e.toUpperCase();
  }
  function Y(t) {
    return t.replace(V, 'ms-').replace(U, $);
  }
  var K = function(t) {
    return 1 === t.nodeType || 9 === t.nodeType || !+t.nodeType;
  };
  function Q() {
    this.expando = w.expando + Q.uid++;
  }
  (Q.uid = 1),
    (Q.prototype = {
      cache: function(t) {
        var e = t[this.expando];
        return (
          e ||
            ((e = {}),
            K(t) &&
              (t.nodeType
                ? (t[this.expando] = e)
                : Object.defineProperty(t, this.expando, { value: e, configurable: !0 }))),
          e
        );
      },
      set: function(t, e, n) {
        var i,
          r = this.cache(t);
        if ('string' == typeof e) r[Y(e)] = n;
        else for (i in e) r[Y(i)] = e[i];
        return r;
      },
      get: function(t, e) {
        return void 0 === e ? this.cache(t) : t[this.expando] && t[this.expando][Y(e)];
      },
      access: function(t, e, n) {
        return void 0 === e || (e && 'string' == typeof e && void 0 === n)
          ? this.get(t, e)
          : (this.set(t, e, n), void 0 !== n ? n : e);
      },
      remove: function(t, e) {
        var n,
          i = t[this.expando];
        if (void 0 !== i) {
          if (void 0 !== e) {
            n = (e = Array.isArray(e) ? e.map(Y) : (e = Y(e)) in i ? [e] : e.match(F) || []).length;
            for (; n--; ) delete i[e[n]];
          }
          (void 0 === e || w.isEmptyObject(i)) && (t.nodeType ? (t[this.expando] = void 0) : delete t[this.expando]);
        }
      },
      hasData: function(t) {
        var e = t[this.expando];
        return void 0 !== e && !w.isEmptyObject(e);
      }
    });
  var X = new Q(),
    G = new Q(),
    J = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
    Z = /[A-Z]/g;
  function tt(t, e, n) {
    var i, r;
    if (void 0 === n && 1 === t.nodeType)
      if (((i = 'data-' + e.replace(Z, '-$&').toLowerCase()), 'string' == typeof (n = t.getAttribute(i)))) {
        try {
          n =
            'true' === (r = n) ||
            ('false' !== r && ('null' === r ? null : r === +r + '' ? +r : J.test(r) ? JSON.parse(r) : r));
        } catch (t) {}
        G.set(t, e, n);
      } else n = void 0;
    return n;
  }
  w.extend({
    hasData: function(t) {
      return G.hasData(t) || X.hasData(t);
    },
    data: function(t, e, n) {
      return G.access(t, e, n);
    },
    removeData: function(t, e) {
      G.remove(t, e);
    },
    _data: function(t, e, n) {
      return X.access(t, e, n);
    },
    _removeData: function(t, e) {
      X.remove(t, e);
    }
  }),
    w.fn.extend({
      data: function(t, e) {
        var n,
          i,
          r,
          o = this[0],
          a = o && o.attributes;
        if (void 0 === t) {
          if (this.length && ((r = G.get(o)), 1 === o.nodeType && !X.get(o, 'hasDataAttrs'))) {
            for (n = a.length; n--; )
              a[n] && 0 === (i = a[n].name).indexOf('data-') && ((i = Y(i.slice(5))), tt(o, i, r[i]));
            X.set(o, 'hasDataAttrs', !0);
          }
          return r;
        }
        return 'object' == typeof t
          ? this.each(function() {
              G.set(this, t);
            })
          : q(
              this,
              function(e) {
                var n;
                if (o && void 0 === e) return void 0 !== (n = G.get(o, t)) || void 0 !== (n = tt(o, t)) ? n : void 0;
                this.each(function() {
                  G.set(this, t, e);
                });
              },
              null,
              e,
              1 < arguments.length,
              null,
              !0
            );
      },
      removeData: function(t) {
        return this.each(function() {
          G.remove(this, t);
        });
      }
    }),
    w.extend({
      queue: function(t, e, n) {
        var i;
        if (t)
          return (
            (i = X.get(t, (e = (e || 'fx') + 'queue'))),
            n && (!i || Array.isArray(n) ? (i = X.access(t, e, w.makeArray(n))) : i.push(n)),
            i || []
          );
      },
      dequeue: function(t, e) {
        var n = w.queue(t, (e = e || 'fx')),
          i = n.length,
          r = n.shift(),
          o = w._queueHooks(t, e);
        'inprogress' === r && ((r = n.shift()), i--),
          r &&
            ('fx' === e && n.unshift('inprogress'),
            delete o.stop,
            r.call(
              t,
              function() {
                w.dequeue(t, e);
              },
              o
            )),
          !i && o && o.empty.fire();
      },
      _queueHooks: function(t, e) {
        var n = e + 'queueHooks';
        return (
          X.get(t, n) ||
          X.access(t, n, {
            empty: w.Callbacks('once memory').add(function() {
              X.remove(t, [e + 'queue', n]);
            })
          })
        );
      }
    }),
    w.fn.extend({
      queue: function(t, e) {
        var n = 2;
        return (
          'string' != typeof t && ((e = t), (t = 'fx'), n--),
          arguments.length < n
            ? w.queue(this[0], t)
            : void 0 === e
            ? this
            : this.each(function() {
                var n = w.queue(this, t, e);
                w._queueHooks(this, t), 'fx' === t && 'inprogress' !== n[0] && w.dequeue(this, t);
              })
        );
      },
      dequeue: function(t) {
        return this.each(function() {
          w.dequeue(this, t);
        });
      },
      clearQueue: function(t) {
        return this.queue(t || 'fx', []);
      },
      promise: function(t, e) {
        var n,
          i = 1,
          r = w.Deferred(),
          o = this,
          a = this.length,
          s = function() {
            --i || r.resolveWith(o, [o]);
          };
        for ('string' != typeof t && ((e = t), (t = void 0)), t = t || 'fx'; a--; )
          (n = X.get(o[a], t + 'queueHooks')) && n.empty && (i++, n.empty.add(s));
        return s(), r.promise(e);
      }
    });
  var et = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
    nt = new RegExp('^(?:([+-])=|)(' + et + ')([a-z%]*)$', 'i'),
    it = ['Top', 'Right', 'Bottom', 'Left'],
    rt = i.documentElement,
    ot = function(t) {
      return w.contains(t.ownerDocument, t);
    },
    at = { composed: !0 };
  rt.getRootNode &&
    (ot = function(t) {
      return w.contains(t.ownerDocument, t) || t.getRootNode(at) === t.ownerDocument;
    });
  var st = function(t, e) {
      return (
        'none' === (t = e || t).style.display || ('' === t.style.display && ot(t) && 'none' === w.css(t, 'display'))
      );
    },
    lt = function(t, e, n, i) {
      var r,
        o,
        a = {};
      for (o in e) (a[o] = t.style[o]), (t.style[o] = e[o]);
      for (o in ((r = n.apply(t, i || [])), e)) t.style[o] = a[o];
      return r;
    };
  function ut(t, e, n, i) {
    var r,
      o,
      a = 20,
      s = i
        ? function() {
            return i.cur();
          }
        : function() {
            return w.css(t, e, '');
          },
      l = s(),
      u = (n && n[3]) || (w.cssNumber[e] ? '' : 'px'),
      c = t.nodeType && (w.cssNumber[e] || ('px' !== u && +l)) && nt.exec(w.css(t, e));
    if (c && c[3] !== u) {
      for (u = u || c[3], c = +(l /= 2) || 1; a--; )
        w.style(t, e, c + u), (1 - o) * (1 - (o = s() / l || 0.5)) <= 0 && (a = 0), (c /= o);
      w.style(t, e, (c *= 2) + u), (n = n || []);
    }
    return (
      n &&
        ((c = +c || +l || 0),
        (r = n[1] ? c + (n[1] + 1) * n[2] : +n[2]),
        i && ((i.unit = u), (i.start = c), (i.end = r))),
      r
    );
  }
  var ct = {};
  function dt(t, e) {
    for (var n, i, r, o, a, s, l, u = [], c = 0, d = t.length; c < d; c++)
      (i = t[c]).style &&
        ((n = i.style.display),
        e
          ? ('none' === n && ((u[c] = X.get(i, 'display') || null), u[c] || (i.style.display = '')),
            '' === i.style.display &&
              st(i) &&
              (u[c] =
                ((l = a = o = void 0),
                (a = (r = i).ownerDocument),
                (l = ct[(s = r.nodeName)]) ||
                  ((o = a.body.appendChild(a.createElement(s))),
                  (l = w.css(o, 'display')),
                  o.parentNode.removeChild(o),
                  'none' === l && (l = 'block'),
                  (ct[s] = l)))))
          : 'none' !== n && ((u[c] = 'none'), X.set(i, 'display', n)));
    for (c = 0; c < d; c++) null != u[c] && (t[c].style.display = u[c]);
    return t;
  }
  w.fn.extend({
    show: function() {
      return dt(this, !0);
    },
    hide: function() {
      return dt(this);
    },
    toggle: function(t) {
      return 'boolean' == typeof t
        ? t
          ? this.show()
          : this.hide()
        : this.each(function() {
            st(this) ? w(this).show() : w(this).hide();
          });
    }
  });
  var ht = /^(?:checkbox|radio)$/i,
    ft = /<([a-z][^\/\0>\x20\t\r\n\f]*)/i,
    pt = /^$|^module$|\/(?:java|ecma)script/i,
    gt = {
      option: [1, "<select multiple='multiple'>", '</select>'],
      thead: [1, '<table>', '</table>'],
      col: [2, '<table><colgroup>', '</colgroup></table>'],
      tr: [2, '<table><tbody>', '</tbody></table>'],
      td: [3, '<table><tbody><tr>', '</tr></tbody></table>'],
      _default: [0, '', '']
    };
  function mt(t, e) {
    var n;
    return (
      (n =
        void 0 !== t.getElementsByTagName
          ? t.getElementsByTagName(e || '*')
          : void 0 !== t.querySelectorAll
          ? t.querySelectorAll(e || '*')
          : []),
      void 0 === e || (e && A(t, e)) ? w.merge([t], n) : n
    );
  }
  function vt(t, e) {
    for (var n = 0, i = t.length; n < i; n++) X.set(t[n], 'globalEval', !e || X.get(e[n], 'globalEval'));
  }
  (gt.optgroup = gt.option), (gt.tbody = gt.tfoot = gt.colgroup = gt.caption = gt.thead), (gt.th = gt.td);
  var bt,
    yt,
    xt = /<|&#?\w+;/;
  function wt(t, e, n, i, r) {
    for (var o, a, s, l, u, c, d = e.createDocumentFragment(), h = [], f = 0, p = t.length; f < p; f++)
      if ((o = t[f]) || 0 === o)
        if ('object' === y(o)) w.merge(h, o.nodeType ? [o] : o);
        else if (xt.test(o)) {
          for (
            a = a || d.appendChild(e.createElement('div')),
              s = (ft.exec(o) || ['', ''])[1].toLowerCase(),
              a.innerHTML = (l = gt[s] || gt._default)[1] + w.htmlPrefilter(o) + l[2],
              c = l[0];
            c--;

          )
            a = a.lastChild;
          w.merge(h, a.childNodes), ((a = d.firstChild).textContent = '');
        } else h.push(e.createTextNode(o));
    for (d.textContent = '', f = 0; (o = h[f++]); )
      if (i && -1 < w.inArray(o, i)) r && r.push(o);
      else if (((u = ot(o)), (a = mt(d.appendChild(o), 'script')), u && vt(a), n))
        for (c = 0; (o = a[c++]); ) pt.test(o.type || '') && n.push(o);
    return d;
  }
  (bt = i.createDocumentFragment().appendChild(i.createElement('div'))),
    (yt = i.createElement('input')).setAttribute('type', 'radio'),
    yt.setAttribute('checked', 'checked'),
    yt.setAttribute('name', 't'),
    bt.appendChild(yt),
    (p.checkClone = bt.cloneNode(!0).cloneNode(!0).lastChild.checked),
    (bt.innerHTML = '<textarea>x</textarea>'),
    (p.noCloneChecked = !!bt.cloneNode(!0).lastChild.defaultValue);
  var _t = /^key/,
    Ct = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
    kt = /^([^.]*)(?:\.(.+)|)/;
  function St() {
    return !0;
  }
  function Tt() {
    return !1;
  }
  function Dt(t, e) {
    return (
      (t ===
        (function() {
          try {
            return i.activeElement;
          } catch (t) {}
        })()) ==
      ('focus' === e)
    );
  }
  function At(t, e, n, i, r, o) {
    var a, s;
    if ('object' == typeof e) {
      for (s in ('string' != typeof n && ((i = i || n), (n = void 0)), e)) At(t, s, n, i, e[s], o);
      return t;
    }
    if (
      (null == i && null == r
        ? ((r = n), (i = n = void 0))
        : null == r && ('string' == typeof n ? ((r = i), (i = void 0)) : ((r = i), (i = n), (n = void 0))),
      !1 === r)
    )
      r = Tt;
    else if (!r) return t;
    return (
      1 === o &&
        ((a = r),
        ((r = function(t) {
          return w().off(t), a.apply(this, arguments);
        }).guid = a.guid || (a.guid = w.guid++))),
      t.each(function() {
        w.event.add(this, e, r, i, n);
      })
    );
  }
  function Et(t, e, n) {
    n
      ? (X.set(t, e, !1),
        w.event.add(t, e, {
          namespace: !1,
          handler: function(t) {
            var i,
              r,
              a = X.get(this, e);
            if (1 & t.isTrigger && this[e]) {
              if (a.length) (w.event.special[e] || {}).delegateType && t.stopPropagation();
              else if (
                ((a = o.call(arguments)),
                X.set(this, e, a),
                (i = n(this, e)),
                this[e](),
                a !== (r = X.get(this, e)) || i ? X.set(this, e, !1) : (r = {}),
                a !== r)
              )
                return t.stopImmediatePropagation(), t.preventDefault(), r.value;
            } else
              a.length &&
                (X.set(this, e, { value: w.event.trigger(w.extend(a[0], w.Event.prototype), a.slice(1), this) }),
                t.stopImmediatePropagation());
          }
        }))
      : void 0 === X.get(t, e) && w.event.add(t, e, St);
  }
  (w.event = {
    global: {},
    add: function(t, e, n, i, r) {
      var o,
        a,
        s,
        l,
        u,
        c,
        d,
        h,
        f,
        p,
        g,
        m = X.get(t);
      if (m)
        for (
          n.handler && ((n = (o = n).handler), (r = o.selector)),
            r && w.find.matchesSelector(rt, r),
            n.guid || (n.guid = w.guid++),
            (l = m.events) || (l = m.events = {}),
            (a = m.handle) ||
              (a = m.handle = function(e) {
                return void 0 !== w && w.event.triggered !== e.type ? w.event.dispatch.apply(t, arguments) : void 0;
              }),
            u = (e = (e || '').match(F) || ['']).length;
          u--;

        )
          (f = g = (s = kt.exec(e[u]) || [])[1]),
            (p = (s[2] || '').split('.').sort()),
            f &&
              ((d = w.event.special[f] || {}),
              (d = w.event.special[(f = (r ? d.delegateType : d.bindType) || f)] || {}),
              (c = w.extend(
                {
                  type: f,
                  origType: g,
                  data: i,
                  handler: n,
                  guid: n.guid,
                  selector: r,
                  needsContext: r && w.expr.match.needsContext.test(r),
                  namespace: p.join('.')
                },
                o
              )),
              (h = l[f]) ||
                (((h = l[f] = []).delegateCount = 0),
                (d.setup && !1 !== d.setup.call(t, i, p, a)) || (t.addEventListener && t.addEventListener(f, a))),
              d.add && (d.add.call(t, c), c.handler.guid || (c.handler.guid = n.guid)),
              r ? h.splice(h.delegateCount++, 0, c) : h.push(c),
              (w.event.global[f] = !0));
    },
    remove: function(t, e, n, i, r) {
      var o,
        a,
        s,
        l,
        u,
        c,
        d,
        h,
        f,
        p,
        g,
        m = X.hasData(t) && X.get(t);
      if (m && (l = m.events)) {
        for (u = (e = (e || '').match(F) || ['']).length; u--; )
          if (((f = g = (s = kt.exec(e[u]) || [])[1]), (p = (s[2] || '').split('.').sort()), f)) {
            for (
              d = w.event.special[f] || {},
                h = l[(f = (i ? d.delegateType : d.bindType) || f)] || [],
                s = s[2] && new RegExp('(^|\\.)' + p.join('\\.(?:.*\\.|)') + '(\\.|$)'),
                a = o = h.length;
              o--;

            )
              (c = h[o]),
                (!r && g !== c.origType) ||
                  (n && n.guid !== c.guid) ||
                  (s && !s.test(c.namespace)) ||
                  (i && i !== c.selector && ('**' !== i || !c.selector)) ||
                  (h.splice(o, 1), c.selector && h.delegateCount--, d.remove && d.remove.call(t, c));
            a &&
              !h.length &&
              ((d.teardown && !1 !== d.teardown.call(t, p, m.handle)) || w.removeEvent(t, f, m.handle), delete l[f]);
          } else for (f in l) w.event.remove(t, f + e[u], n, i, !0);
        w.isEmptyObject(l) && X.remove(t, 'handle events');
      }
    },
    dispatch: function(t) {
      var e,
        n,
        i,
        r,
        o,
        a,
        s = w.event.fix(t),
        l = new Array(arguments.length),
        u = (X.get(this, 'events') || {})[s.type] || [],
        c = w.event.special[s.type] || {};
      for (l[0] = s, e = 1; e < arguments.length; e++) l[e] = arguments[e];
      if (((s.delegateTarget = this), !c.preDispatch || !1 !== c.preDispatch.call(this, s))) {
        for (a = w.event.handlers.call(this, s, u), e = 0; (r = a[e++]) && !s.isPropagationStopped(); )
          for (s.currentTarget = r.elem, n = 0; (o = r.handlers[n++]) && !s.isImmediatePropagationStopped(); )
            (s.rnamespace && !1 !== o.namespace && !s.rnamespace.test(o.namespace)) ||
              ((s.handleObj = o),
              (s.data = o.data),
              void 0 !== (i = ((w.event.special[o.origType] || {}).handle || o.handler).apply(r.elem, l)) &&
                !1 === (s.result = i) &&
                (s.preventDefault(), s.stopPropagation()));
        return c.postDispatch && c.postDispatch.call(this, s), s.result;
      }
    },
    handlers: function(t, e) {
      var n,
        i,
        r,
        o,
        a,
        s = [],
        l = e.delegateCount,
        u = t.target;
      if (l && u.nodeType && !('click' === t.type && 1 <= t.button))
        for (; u !== this; u = u.parentNode || this)
          if (1 === u.nodeType && ('click' !== t.type || !0 !== u.disabled)) {
            for (o = [], a = {}, n = 0; n < l; n++)
              void 0 === a[(r = (i = e[n]).selector + ' ')] &&
                (a[r] = i.needsContext ? -1 < w(r, this).index(u) : w.find(r, this, null, [u]).length),
                a[r] && o.push(i);
            o.length && s.push({ elem: u, handlers: o });
          }
      return (u = this), l < e.length && s.push({ elem: u, handlers: e.slice(l) }), s;
    },
    addProp: function(t, e) {
      Object.defineProperty(w.Event.prototype, t, {
        enumerable: !0,
        configurable: !0,
        get: g(e)
          ? function() {
              if (this.originalEvent) return e(this.originalEvent);
            }
          : function() {
              if (this.originalEvent) return this.originalEvent[t];
            },
        set: function(e) {
          Object.defineProperty(this, t, { enumerable: !0, configurable: !0, writable: !0, value: e });
        }
      });
    },
    fix: function(t) {
      return t[w.expando] ? t : new w.Event(t);
    },
    special: {
      load: { noBubble: !0 },
      click: {
        setup: function(t) {
          var e = this || t;
          return ht.test(e.type) && e.click && A(e, 'input') && Et(e, 'click', St), !1;
        },
        trigger: function(t) {
          var e = this || t;
          return ht.test(e.type) && e.click && A(e, 'input') && Et(e, 'click'), !0;
        },
        _default: function(t) {
          var e = t.target;
          return (ht.test(e.type) && e.click && A(e, 'input') && X.get(e, 'click')) || A(e, 'a');
        }
      },
      beforeunload: {
        postDispatch: function(t) {
          void 0 !== t.result && t.originalEvent && (t.originalEvent.returnValue = t.result);
        }
      }
    }
  }),
    (w.removeEvent = function(t, e, n) {
      t.removeEventListener && t.removeEventListener(e, n);
    }),
    (w.Event = function(t, e) {
      if (!(this instanceof w.Event)) return new w.Event(t, e);
      t && t.type
        ? ((this.originalEvent = t),
          (this.type = t.type),
          (this.isDefaultPrevented =
            t.defaultPrevented || (void 0 === t.defaultPrevented && !1 === t.returnValue) ? St : Tt),
          (this.target = t.target && 3 === t.target.nodeType ? t.target.parentNode : t.target),
          (this.currentTarget = t.currentTarget),
          (this.relatedTarget = t.relatedTarget))
        : (this.type = t),
        e && w.extend(this, e),
        (this.timeStamp = (t && t.timeStamp) || Date.now()),
        (this[w.expando] = !0);
    }),
    (w.Event.prototype = {
      constructor: w.Event,
      isDefaultPrevented: Tt,
      isPropagationStopped: Tt,
      isImmediatePropagationStopped: Tt,
      isSimulated: !1,
      preventDefault: function() {
        var t = this.originalEvent;
        (this.isDefaultPrevented = St), t && !this.isSimulated && t.preventDefault();
      },
      stopPropagation: function() {
        var t = this.originalEvent;
        (this.isPropagationStopped = St), t && !this.isSimulated && t.stopPropagation();
      },
      stopImmediatePropagation: function() {
        var t = this.originalEvent;
        (this.isImmediatePropagationStopped = St),
          t && !this.isSimulated && t.stopImmediatePropagation(),
          this.stopPropagation();
      }
    }),
    w.each(
      {
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        char: !0,
        code: !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function(t) {
          var e = t.button;
          return null == t.which && _t.test(t.type)
            ? null != t.charCode
              ? t.charCode
              : t.keyCode
            : !t.which && void 0 !== e && Ct.test(t.type)
            ? 1 & e
              ? 1
              : 2 & e
              ? 3
              : 4 & e
              ? 2
              : 0
            : t.which;
        }
      },
      w.event.addProp
    ),
    w.each({ focus: 'focusin', blur: 'focusout' }, function(t, e) {
      w.event.special[t] = {
        setup: function() {
          return Et(this, t, Dt), !1;
        },
        trigger: function() {
          return Et(this, t), !0;
        },
        delegateType: e
      };
    }),
    w.each(
      { mouseenter: 'mouseover', mouseleave: 'mouseout', pointerenter: 'pointerover', pointerleave: 'pointerout' },
      function(t, e) {
        w.event.special[t] = {
          delegateType: e,
          bindType: e,
          handle: function(t) {
            var n,
              i = t.relatedTarget,
              r = t.handleObj;
            return (
              (i && (i === this || w.contains(this, i))) ||
                ((t.type = r.origType), (n = r.handler.apply(this, arguments)), (t.type = e)),
              n
            );
          }
        };
      }
    ),
    w.fn.extend({
      on: function(t, e, n, i) {
        return At(this, t, e, n, i);
      },
      one: function(t, e, n, i) {
        return At(this, t, e, n, i, 1);
      },
      off: function(t, e, n) {
        var i, r;
        if (t && t.preventDefault && t.handleObj)
          return (
            (i = t.handleObj),
            w(t.delegateTarget).off(i.namespace ? i.origType + '.' + i.namespace : i.origType, i.selector, i.handler),
            this
          );
        if ('object' == typeof t) {
          for (r in t) this.off(r, e, t[r]);
          return this;
        }
        return (
          (!1 !== e && 'function' != typeof e) || ((n = e), (e = void 0)),
          !1 === n && (n = Tt),
          this.each(function() {
            w.event.remove(this, t, n, e);
          })
        );
      }
    });
  var Mt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
    It = /<script|<style|<link/i,
    Pt = /checked\s*(?:[^=]|=\s*.checked.)/i,
    Ot = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
  function Nt(t, e) {
    return (A(t, 'table') && A(11 !== e.nodeType ? e : e.firstChild, 'tr') && w(t).children('tbody')[0]) || t;
  }
  function Lt(t) {
    return (t.type = (null !== t.getAttribute('type')) + '/' + t.type), t;
  }
  function Ft(t) {
    return 'true/' === (t.type || '').slice(0, 5) ? (t.type = t.type.slice(5)) : t.removeAttribute('type'), t;
  }
  function Rt(t, e) {
    var n, i, r, o, a, s, l, u;
    if (1 === e.nodeType) {
      if (X.hasData(t) && ((o = X.access(t)), (a = X.set(e, o)), (u = o.events)))
        for (r in (delete a.handle, (a.events = {}), u))
          for (n = 0, i = u[r].length; n < i; n++) w.event.add(e, r, u[r][n]);
      G.hasData(t) && ((s = G.access(t)), (l = w.extend({}, s)), G.set(e, l));
    }
  }
  function jt(t, e, n, i) {
    e = a.apply([], e);
    var r,
      o,
      s,
      l,
      u,
      c,
      d = 0,
      h = t.length,
      f = h - 1,
      m = e[0],
      v = g(m);
    if (v || (1 < h && 'string' == typeof m && !p.checkClone && Pt.test(m)))
      return t.each(function(r) {
        var o = t.eq(r);
        v && (e[0] = m.call(this, r, o.html())), jt(o, e, n, i);
      });
    if (
      h &&
      ((o = (r = wt(e, t[0].ownerDocument, !1, t, i)).firstChild), 1 === r.childNodes.length && (r = o), o || i)
    ) {
      for (l = (s = w.map(mt(r, 'script'), Lt)).length; d < h; d++)
        (u = r), d !== f && ((u = w.clone(u, !0, !0)), l && w.merge(s, mt(u, 'script'))), n.call(t[d], u, d);
      if (l)
        for (c = s[s.length - 1].ownerDocument, w.map(s, Ft), d = 0; d < l; d++)
          pt.test((u = s[d]).type || '') &&
            !X.access(u, 'globalEval') &&
            w.contains(c, u) &&
            (u.src && 'module' !== (u.type || '').toLowerCase()
              ? w._evalUrl && !u.noModule && w._evalUrl(u.src, { nonce: u.nonce || u.getAttribute('nonce') })
              : b(u.textContent.replace(Ot, ''), u, c));
    }
    return t;
  }
  function Ht(t, e, n) {
    for (var i, r = e ? w.filter(e, t) : t, o = 0; null != (i = r[o]); o++)
      n || 1 !== i.nodeType || w.cleanData(mt(i)),
        i.parentNode && (n && ot(i) && vt(mt(i, 'script')), i.parentNode.removeChild(i));
    return t;
  }
  w.extend({
    htmlPrefilter: function(t) {
      return t.replace(Mt, '<$1></$2>');
    },
    clone: function(t, e, n) {
      var i,
        r,
        o,
        a,
        s,
        l,
        u,
        c = t.cloneNode(!0),
        d = ot(t);
      if (!(p.noCloneChecked || (1 !== t.nodeType && 11 !== t.nodeType) || w.isXMLDoc(t)))
        for (a = mt(c), i = 0, r = (o = mt(t)).length; i < r; i++)
          (s = o[i]),
            'input' === (u = (l = a[i]).nodeName.toLowerCase()) && ht.test(s.type)
              ? (l.checked = s.checked)
              : ('input' !== u && 'textarea' !== u) || (l.defaultValue = s.defaultValue);
      if (e)
        if (n) for (o = o || mt(t), a = a || mt(c), i = 0, r = o.length; i < r; i++) Rt(o[i], a[i]);
        else Rt(t, c);
      return 0 < (a = mt(c, 'script')).length && vt(a, !d && mt(t, 'script')), c;
    },
    cleanData: function(t) {
      for (var e, n, i, r = w.event.special, o = 0; void 0 !== (n = t[o]); o++)
        if (K(n)) {
          if ((e = n[X.expando])) {
            if (e.events) for (i in e.events) r[i] ? w.event.remove(n, i) : w.removeEvent(n, i, e.handle);
            n[X.expando] = void 0;
          }
          n[G.expando] && (n[G.expando] = void 0);
        }
    }
  }),
    w.fn.extend({
      detach: function(t) {
        return Ht(this, t, !0);
      },
      remove: function(t) {
        return Ht(this, t);
      },
      text: function(t) {
        return q(
          this,
          function(t) {
            return void 0 === t
              ? w.text(this)
              : this.empty().each(function() {
                  (1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType) || (this.textContent = t);
                });
          },
          null,
          t,
          arguments.length
        );
      },
      append: function() {
        return jt(this, arguments, function(t) {
          (1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType) || Nt(this, t).appendChild(t);
        });
      },
      prepend: function() {
        return jt(this, arguments, function(t) {
          if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
            var e = Nt(this, t);
            e.insertBefore(t, e.firstChild);
          }
        });
      },
      before: function() {
        return jt(this, arguments, function(t) {
          this.parentNode && this.parentNode.insertBefore(t, this);
        });
      },
      after: function() {
        return jt(this, arguments, function(t) {
          this.parentNode && this.parentNode.insertBefore(t, this.nextSibling);
        });
      },
      empty: function() {
        for (var t, e = 0; null != (t = this[e]); e++)
          1 === t.nodeType && (w.cleanData(mt(t, !1)), (t.textContent = ''));
        return this;
      },
      clone: function(t, e) {
        return (
          (t = null != t && t),
          (e = null == e ? t : e),
          this.map(function() {
            return w.clone(this, t, e);
          })
        );
      },
      html: function(t) {
        return q(
          this,
          function(t) {
            var e = this[0] || {},
              n = 0,
              i = this.length;
            if (void 0 === t && 1 === e.nodeType) return e.innerHTML;
            if ('string' == typeof t && !It.test(t) && !gt[(ft.exec(t) || ['', ''])[1].toLowerCase()]) {
              t = w.htmlPrefilter(t);
              try {
                for (; n < i; n++) 1 === (e = this[n] || {}).nodeType && (w.cleanData(mt(e, !1)), (e.innerHTML = t));
                e = 0;
              } catch (t) {}
            }
            e && this.empty().append(t);
          },
          null,
          t,
          arguments.length
        );
      },
      replaceWith: function() {
        var t = [];
        return jt(
          this,
          arguments,
          function(e) {
            var n = this.parentNode;
            w.inArray(this, t) < 0 && (w.cleanData(mt(this)), n && n.replaceChild(e, this));
          },
          t
        );
      }
    }),
    w.each(
      {
        appendTo: 'append',
        prependTo: 'prepend',
        insertBefore: 'before',
        insertAfter: 'after',
        replaceAll: 'replaceWith'
      },
      function(t, e) {
        w.fn[t] = function(t) {
          for (var n, i = [], r = w(t), o = r.length - 1, a = 0; a <= o; a++)
            (n = a === o ? this : this.clone(!0)), w(r[a])[e](n), s.apply(i, n.get());
          return this.pushStack(i);
        };
      }
    );
  var Bt = new RegExp('^(' + et + ')(?!px)[a-z%]+$', 'i'),
    Wt = function(e) {
      var n = e.ownerDocument.defaultView;
      return (n && n.opener) || (n = t), n.getComputedStyle(e);
    },
    zt = new RegExp(it.join('|'), 'i');
  function qt(t, e, n) {
    var i,
      r,
      o,
      a,
      s = t.style;
    return (
      (n = n || Wt(t)) &&
        ('' !== (a = n.getPropertyValue(e) || n[e]) || ot(t) || (a = w.style(t, e)),
        !p.pixelBoxStyles() &&
          Bt.test(a) &&
          zt.test(e) &&
          ((i = s.width),
          (r = s.minWidth),
          (o = s.maxWidth),
          (s.minWidth = s.maxWidth = s.width = a),
          (a = n.width),
          (s.width = i),
          (s.minWidth = r),
          (s.maxWidth = o))),
      void 0 !== a ? a + '' : a
    );
  }
  function Vt(t, e) {
    return {
      get: function() {
        if (!t()) return (this.get = e).apply(this, arguments);
        delete this.get;
      }
    };
  }
  !(function() {
    function e() {
      if (c) {
        (u.style.cssText = 'position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0'),
          (c.style.cssText =
            'position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%'),
          rt.appendChild(u).appendChild(c);
        var e = t.getComputedStyle(c);
        (r = '1%' !== e.top),
          (l = 12 === n(e.marginLeft)),
          (c.style.right = '60%'),
          (s = 36 === n(e.right)),
          (o = 36 === n(e.width)),
          (c.style.position = 'absolute'),
          (a = 12 === n(c.offsetWidth / 3)),
          rt.removeChild(u),
          (c = null);
      }
    }
    function n(t) {
      return Math.round(parseFloat(t));
    }
    var r,
      o,
      a,
      s,
      l,
      u = i.createElement('div'),
      c = i.createElement('div');
    c.style &&
      ((c.style.backgroundClip = 'content-box'),
      (c.cloneNode(!0).style.backgroundClip = ''),
      (p.clearCloneStyle = 'content-box' === c.style.backgroundClip),
      w.extend(p, {
        boxSizingReliable: function() {
          return e(), o;
        },
        pixelBoxStyles: function() {
          return e(), s;
        },
        pixelPosition: function() {
          return e(), r;
        },
        reliableMarginLeft: function() {
          return e(), l;
        },
        scrollboxSize: function() {
          return e(), a;
        }
      }));
  })();
  var Ut = ['Webkit', 'Moz', 'ms'],
    $t = i.createElement('div').style,
    Yt = {};
  function Kt(t) {
    return (
      w.cssProps[t] ||
      Yt[t] ||
      (t in $t
        ? t
        : (Yt[t] =
            (function(t) {
              for (var e = t[0].toUpperCase() + t.slice(1), n = Ut.length; n--; ) if ((t = Ut[n] + e) in $t) return t;
            })(t) || t))
    );
  }
  var Qt = /^(none|table(?!-c[ea]).+)/,
    Xt = /^--/,
    Gt = { position: 'absolute', visibility: 'hidden', display: 'block' },
    Jt = { letterSpacing: '0', fontWeight: '400' };
  function Zt(t, e, n) {
    var i = nt.exec(e);
    return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || 'px') : e;
  }
  function te(t, e, n, i, r, o) {
    var a = 'width' === e ? 1 : 0,
      s = 0,
      l = 0;
    if (n === (i ? 'border' : 'content')) return 0;
    for (; a < 4; a += 2)
      'margin' === n && (l += w.css(t, n + it[a], !0, r)),
        i
          ? ('content' === n && (l -= w.css(t, 'padding' + it[a], !0, r)),
            'margin' !== n && (l -= w.css(t, 'border' + it[a] + 'Width', !0, r)))
          : ((l += w.css(t, 'padding' + it[a], !0, r)),
            'padding' !== n
              ? (l += w.css(t, 'border' + it[a] + 'Width', !0, r))
              : (s += w.css(t, 'border' + it[a] + 'Width', !0, r)));
    return (
      !i &&
        0 <= o &&
        (l += Math.max(0, Math.ceil(t['offset' + e[0].toUpperCase() + e.slice(1)] - o - l - s - 0.5)) || 0),
      l
    );
  }
  function ee(t, e, n) {
    var i = Wt(t),
      r = (!p.boxSizingReliable() || n) && 'border-box' === w.css(t, 'boxSizing', !1, i),
      o = r,
      a = qt(t, e, i),
      s = 'offset' + e[0].toUpperCase() + e.slice(1);
    if (Bt.test(a)) {
      if (!n) return a;
      a = 'auto';
    }
    return (
      ((!p.boxSizingReliable() && r) || 'auto' === a || (!parseFloat(a) && 'inline' === w.css(t, 'display', !1, i))) &&
        t.getClientRects().length &&
        ((r = 'border-box' === w.css(t, 'boxSizing', !1, i)), (o = s in t) && (a = t[s])),
      (a = parseFloat(a) || 0) + te(t, e, n || (r ? 'border' : 'content'), o, i, a) + 'px'
    );
  }
  function ne(t, e, n, i, r) {
    return new ne.prototype.init(t, e, n, i, r);
  }
  w.extend({
    cssHooks: {
      opacity: {
        get: function(t, e) {
          if (e) {
            var n = qt(t, 'opacity');
            return '' === n ? '1' : n;
          }
        }
      }
    },
    cssNumber: {
      animationIterationCount: !0,
      columnCount: !0,
      fillOpacity: !0,
      flexGrow: !0,
      flexShrink: !0,
      fontWeight: !0,
      gridArea: !0,
      gridColumn: !0,
      gridColumnEnd: !0,
      gridColumnStart: !0,
      gridRow: !0,
      gridRowEnd: !0,
      gridRowStart: !0,
      lineHeight: !0,
      opacity: !0,
      order: !0,
      orphans: !0,
      widows: !0,
      zIndex: !0,
      zoom: !0
    },
    cssProps: {},
    style: function(t, e, n, i) {
      if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
        var r,
          o,
          a,
          s = Y(e),
          l = Xt.test(e),
          u = t.style;
        if ((l || (e = Kt(s)), (a = w.cssHooks[e] || w.cssHooks[s]), void 0 === n))
          return a && 'get' in a && void 0 !== (r = a.get(t, !1, i)) ? r : u[e];
        'string' == (o = typeof n) && (r = nt.exec(n)) && r[1] && ((n = ut(t, e, r)), (o = 'number')),
          null != n &&
            n == n &&
            ('number' !== o || l || (n += (r && r[3]) || (w.cssNumber[s] ? '' : 'px')),
            p.clearCloneStyle || '' !== n || 0 !== e.indexOf('background') || (u[e] = 'inherit'),
            (a && 'set' in a && void 0 === (n = a.set(t, n, i))) || (l ? u.setProperty(e, n) : (u[e] = n)));
      }
    },
    css: function(t, e, n, i) {
      var r,
        o,
        a,
        s = Y(e);
      return (
        Xt.test(e) || (e = Kt(s)),
        (a = w.cssHooks[e] || w.cssHooks[s]) && 'get' in a && (r = a.get(t, !0, n)),
        void 0 === r && (r = qt(t, e, i)),
        'normal' === r && e in Jt && (r = Jt[e]),
        '' === n || n ? ((o = parseFloat(r)), !0 === n || isFinite(o) ? o || 0 : r) : r
      );
    }
  }),
    w.each(['height', 'width'], function(t, e) {
      w.cssHooks[e] = {
        get: function(t, n, i) {
          if (n)
            return !Qt.test(w.css(t, 'display')) || (t.getClientRects().length && t.getBoundingClientRect().width)
              ? ee(t, e, i)
              : lt(t, Gt, function() {
                  return ee(t, e, i);
                });
        },
        set: function(t, n, i) {
          var r,
            o = Wt(t),
            a = !p.scrollboxSize() && 'absolute' === o.position,
            s = (a || i) && 'border-box' === w.css(t, 'boxSizing', !1, o),
            l = i ? te(t, e, i, s, o) : 0;
          return (
            s &&
              a &&
              (l -= Math.ceil(
                t['offset' + e[0].toUpperCase() + e.slice(1)] - parseFloat(o[e]) - te(t, e, 'border', !1, o) - 0.5
              )),
            l && (r = nt.exec(n)) && 'px' !== (r[3] || 'px') && ((t.style[e] = n), (n = w.css(t, e))),
            Zt(0, n, l)
          );
        }
      };
    }),
    (w.cssHooks.marginLeft = Vt(p.reliableMarginLeft, function(t, e) {
      if (e)
        return (
          (parseFloat(qt(t, 'marginLeft')) ||
            t.getBoundingClientRect().left -
              lt(t, { marginLeft: 0 }, function() {
                return t.getBoundingClientRect().left;
              })) + 'px'
        );
    })),
    w.each({ margin: '', padding: '', border: 'Width' }, function(t, e) {
      (w.cssHooks[t + e] = {
        expand: function(n) {
          for (var i = 0, r = {}, o = 'string' == typeof n ? n.split(' ') : [n]; i < 4; i++)
            r[t + it[i] + e] = o[i] || o[i - 2] || o[0];
          return r;
        }
      }),
        'margin' !== t && (w.cssHooks[t + e].set = Zt);
    }),
    w.fn.extend({
      css: function(t, e) {
        return q(
          this,
          function(t, e, n) {
            var i,
              r,
              o = {},
              a = 0;
            if (Array.isArray(e)) {
              for (i = Wt(t), r = e.length; a < r; a++) o[e[a]] = w.css(t, e[a], !1, i);
              return o;
            }
            return void 0 !== n ? w.style(t, e, n) : w.css(t, e);
          },
          t,
          e,
          1 < arguments.length
        );
      }
    }),
    (((w.Tween = ne).prototype = {
      constructor: ne,
      init: function(t, e, n, i, r, o) {
        (this.elem = t),
          (this.prop = n),
          (this.easing = r || w.easing._default),
          (this.options = e),
          (this.start = this.now = this.cur()),
          (this.end = i),
          (this.unit = o || (w.cssNumber[n] ? '' : 'px'));
      },
      cur: function() {
        var t = ne.propHooks[this.prop];
        return t && t.get ? t.get(this) : ne.propHooks._default.get(this);
      },
      run: function(t) {
        var e,
          n = ne.propHooks[this.prop];
        return (
          (this.pos = e = this.options.duration
            ? w.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration)
            : t),
          (this.now = (this.end - this.start) * e + this.start),
          this.options.step && this.options.step.call(this.elem, this.now, this),
          n && n.set ? n.set(this) : ne.propHooks._default.set(this),
          this
        );
      }
    }).init.prototype = ne.prototype),
    ((ne.propHooks = {
      _default: {
        get: function(t) {
          var e;
          return 1 !== t.elem.nodeType || (null != t.elem[t.prop] && null == t.elem.style[t.prop])
            ? t.elem[t.prop]
            : (e = w.css(t.elem, t.prop, '')) && 'auto' !== e
            ? e
            : 0;
        },
        set: function(t) {
          w.fx.step[t.prop]
            ? w.fx.step[t.prop](t)
            : 1 !== t.elem.nodeType || (!w.cssHooks[t.prop] && null == t.elem.style[Kt(t.prop)])
            ? (t.elem[t.prop] = t.now)
            : w.style(t.elem, t.prop, t.now + t.unit);
        }
      }
    }).scrollTop = ne.propHooks.scrollLeft = {
      set: function(t) {
        t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now);
      }
    }),
    (w.easing = {
      linear: function(t) {
        return t;
      },
      swing: function(t) {
        return 0.5 - Math.cos(t * Math.PI) / 2;
      },
      _default: 'swing'
    }),
    (w.fx = ne.prototype.init),
    (w.fx.step = {});
  var ie,
    re,
    oe,
    ae,
    se = /^(?:toggle|show|hide)$/,
    le = /queueHooks$/;
  function ue() {
    re &&
      (!1 === i.hidden && t.requestAnimationFrame ? t.requestAnimationFrame(ue) : t.setTimeout(ue, w.fx.interval),
      w.fx.tick());
  }
  function ce() {
    return (
      t.setTimeout(function() {
        ie = void 0;
      }),
      (ie = Date.now())
    );
  }
  function de(t, e) {
    var n,
      i = 0,
      r = { height: t };
    for (e = e ? 1 : 0; i < 4; i += 2 - e) r['margin' + (n = it[i])] = r['padding' + n] = t;
    return e && (r.opacity = r.width = t), r;
  }
  function he(t, e, n) {
    for (var i, r = (fe.tweeners[e] || []).concat(fe.tweeners['*']), o = 0, a = r.length; o < a; o++)
      if ((i = r[o].call(n, e, t))) return i;
  }
  function fe(t, e, n) {
    var i,
      r,
      o = 0,
      a = fe.prefilters.length,
      s = w.Deferred().always(function() {
        delete l.elem;
      }),
      l = function() {
        if (r) return !1;
        for (
          var e = ie || ce(),
            n = Math.max(0, u.startTime + u.duration - e),
            i = 1 - (n / u.duration || 0),
            o = 0,
            a = u.tweens.length;
          o < a;
          o++
        )
          u.tweens[o].run(i);
        return (
          s.notifyWith(t, [u, i, n]), i < 1 && a ? n : (a || s.notifyWith(t, [u, 1, 0]), s.resolveWith(t, [u]), !1)
        );
      },
      u = s.promise({
        elem: t,
        props: w.extend({}, e),
        opts: w.extend(!0, { specialEasing: {}, easing: w.easing._default }, n),
        originalProperties: e,
        originalOptions: n,
        startTime: ie || ce(),
        duration: n.duration,
        tweens: [],
        createTween: function(e, n) {
          var i = w.Tween(t, u.opts, e, n, u.opts.specialEasing[e] || u.opts.easing);
          return u.tweens.push(i), i;
        },
        stop: function(e) {
          var n = 0,
            i = e ? u.tweens.length : 0;
          if (r) return this;
          for (r = !0; n < i; n++) u.tweens[n].run(1);
          return e ? (s.notifyWith(t, [u, 1, 0]), s.resolveWith(t, [u, e])) : s.rejectWith(t, [u, e]), this;
        }
      }),
      c = u.props;
    for (
      (function(t, e) {
        var n, i, r, o, a;
        for (n in t)
          if (
            ((r = e[(i = Y(n))]),
            (o = t[n]),
            Array.isArray(o) && ((r = o[1]), (o = t[n] = o[0])),
            n !== i && ((t[i] = o), delete t[n]),
            (a = w.cssHooks[i]) && ('expand' in a))
          )
            for (n in ((o = a.expand(o)), delete t[i], o)) (n in t) || ((t[n] = o[n]), (e[n] = r));
          else e[i] = r;
      })(c, u.opts.specialEasing);
      o < a;
      o++
    )
      if ((i = fe.prefilters[o].call(u, t, c, u.opts)))
        return g(i.stop) && (w._queueHooks(u.elem, u.opts.queue).stop = i.stop.bind(i)), i;
    return (
      w.map(c, he, u),
      g(u.opts.start) && u.opts.start.call(t, u),
      u
        .progress(u.opts.progress)
        .done(u.opts.done, u.opts.complete)
        .fail(u.opts.fail)
        .always(u.opts.always),
      w.fx.timer(w.extend(l, { elem: t, anim: u, queue: u.opts.queue })),
      u
    );
  }
  (w.Animation = w.extend(fe, {
    tweeners: {
      '*': [
        function(t, e) {
          var n = this.createTween(t, e);
          return ut(n.elem, t, nt.exec(e), n), n;
        }
      ]
    },
    tweener: function(t, e) {
      g(t) ? ((e = t), (t = ['*'])) : (t = t.match(F));
      for (var n, i = 0, r = t.length; i < r; i++) (fe.tweeners[(n = t[i])] = fe.tweeners[n] || []).unshift(e);
    },
    prefilters: [
      function(t, e, n) {
        var i,
          r,
          o,
          a,
          s,
          l,
          u,
          c,
          d = 'width' in e || 'height' in e,
          h = this,
          f = {},
          p = t.style,
          g = t.nodeType && st(t),
          m = X.get(t, 'fxshow');
        for (i in (n.queue ||
          (null == (a = w._queueHooks(t, 'fx')).unqueued &&
            ((a.unqueued = 0),
            (s = a.empty.fire),
            (a.empty.fire = function() {
              a.unqueued || s();
            })),
          a.unqueued++,
          h.always(function() {
            h.always(function() {
              a.unqueued--, w.queue(t, 'fx').length || a.empty.fire();
            });
          })),
        e))
          if (se.test((r = e[i]))) {
            if ((delete e[i], (o = o || 'toggle' === r), r === (g ? 'hide' : 'show'))) {
              if ('show' !== r || !m || void 0 === m[i]) continue;
              g = !0;
            }
            f[i] = (m && m[i]) || w.style(t, i);
          }
        if ((l = !w.isEmptyObject(e)) || !w.isEmptyObject(f))
          for (i in (d &&
            1 === t.nodeType &&
            ((n.overflow = [p.overflow, p.overflowX, p.overflowY]),
            null == (u = m && m.display) && (u = X.get(t, 'display')),
            'none' === (c = w.css(t, 'display')) &&
              (u ? (c = u) : (dt([t], !0), (u = t.style.display || u), (c = w.css(t, 'display')), dt([t]))),
            ('inline' === c || ('inline-block' === c && null != u)) &&
              'none' === w.css(t, 'float') &&
              (l ||
                (h.done(function() {
                  p.display = u;
                }),
                null == u && (u = 'none' === (c = p.display) ? '' : c)),
              (p.display = 'inline-block'))),
          n.overflow &&
            ((p.overflow = 'hidden'),
            h.always(function() {
              (p.overflow = n.overflow[0]), (p.overflowX = n.overflow[1]), (p.overflowY = n.overflow[2]);
            })),
          (l = !1),
          f))
            l ||
              (m ? 'hidden' in m && (g = m.hidden) : (m = X.access(t, 'fxshow', { display: u })),
              o && (m.hidden = !g),
              g && dt([t], !0),
              h.done(function() {
                for (i in (g || dt([t]), X.remove(t, 'fxshow'), f)) w.style(t, i, f[i]);
              })),
              (l = he(g ? m[i] : 0, i, h)),
              i in m || ((m[i] = l.start), g && ((l.end = l.start), (l.start = 0)));
      }
    ],
    prefilter: function(t, e) {
      e ? fe.prefilters.unshift(t) : fe.prefilters.push(t);
    }
  })),
    (w.speed = function(t, e, n) {
      var i =
        t && 'object' == typeof t
          ? w.extend({}, t)
          : { complete: n || (!n && e) || (g(t) && t), duration: t, easing: (n && e) || (e && !g(e) && e) };
      return (
        w.fx.off
          ? (i.duration = 0)
          : 'number' != typeof i.duration &&
            (i.duration = i.duration in w.fx.speeds ? w.fx.speeds[i.duration] : w.fx.speeds._default),
        (null != i.queue && !0 !== i.queue) || (i.queue = 'fx'),
        (i.old = i.complete),
        (i.complete = function() {
          g(i.old) && i.old.call(this), i.queue && w.dequeue(this, i.queue);
        }),
        i
      );
    }),
    w.fn.extend({
      fadeTo: function(t, e, n, i) {
        return this.filter(st)
          .css('opacity', 0)
          .show()
          .end()
          .animate({ opacity: e }, t, n, i);
      },
      animate: function(t, e, n, i) {
        var r = w.isEmptyObject(t),
          o = w.speed(e, n, i),
          a = function() {
            var e = fe(this, w.extend({}, t), o);
            (r || X.get(this, 'finish')) && e.stop(!0);
          };
        return (a.finish = a), r || !1 === o.queue ? this.each(a) : this.queue(o.queue, a);
      },
      stop: function(t, e, n) {
        var i = function(t) {
          var e = t.stop;
          delete t.stop, e(n);
        };
        return (
          'string' != typeof t && ((n = e), (e = t), (t = void 0)),
          e && !1 !== t && this.queue(t || 'fx', []),
          this.each(function() {
            var e = !0,
              r = null != t && t + 'queueHooks',
              o = w.timers,
              a = X.get(this);
            if (r) a[r] && a[r].stop && i(a[r]);
            else for (r in a) a[r] && a[r].stop && le.test(r) && i(a[r]);
            for (r = o.length; r--; )
              o[r].elem !== this || (null != t && o[r].queue !== t) || (o[r].anim.stop(n), (e = !1), o.splice(r, 1));
            (!e && n) || w.dequeue(this, t);
          })
        );
      },
      finish: function(t) {
        return (
          !1 !== t && (t = t || 'fx'),
          this.each(function() {
            var e,
              n = X.get(this),
              i = n[t + 'queue'],
              r = n[t + 'queueHooks'],
              o = w.timers,
              a = i ? i.length : 0;
            for (n.finish = !0, w.queue(this, t, []), r && r.stop && r.stop.call(this, !0), e = o.length; e--; )
              o[e].elem === this && o[e].queue === t && (o[e].anim.stop(!0), o.splice(e, 1));
            for (e = 0; e < a; e++) i[e] && i[e].finish && i[e].finish.call(this);
            delete n.finish;
          })
        );
      }
    }),
    w.each(['toggle', 'show', 'hide'], function(t, e) {
      var n = w.fn[e];
      w.fn[e] = function(t, i, r) {
        return null == t || 'boolean' == typeof t ? n.apply(this, arguments) : this.animate(de(e, !0), t, i, r);
      };
    }),
    w.each(
      {
        slideDown: de('show'),
        slideUp: de('hide'),
        slideToggle: de('toggle'),
        fadeIn: { opacity: 'show' },
        fadeOut: { opacity: 'hide' },
        fadeToggle: { opacity: 'toggle' }
      },
      function(t, e) {
        w.fn[t] = function(t, n, i) {
          return this.animate(e, t, n, i);
        };
      }
    ),
    (w.timers = []),
    (w.fx.tick = function() {
      var t,
        e = 0,
        n = w.timers;
      for (ie = Date.now(); e < n.length; e++) (t = n[e])() || n[e] !== t || n.splice(e--, 1);
      n.length || w.fx.stop(), (ie = void 0);
    }),
    (w.fx.timer = function(t) {
      w.timers.push(t), w.fx.start();
    }),
    (w.fx.interval = 13),
    (w.fx.start = function() {
      re || ((re = !0), ue());
    }),
    (w.fx.stop = function() {
      re = null;
    }),
    (w.fx.speeds = { slow: 600, fast: 200, _default: 400 }),
    (w.fn.delay = function(e, n) {
      return (
        (e = (w.fx && w.fx.speeds[e]) || e),
        this.queue((n = n || 'fx'), function(n, i) {
          var r = t.setTimeout(n, e);
          i.stop = function() {
            t.clearTimeout(r);
          };
        })
      );
    }),
    (oe = i.createElement('input')),
    (ae = i.createElement('select').appendChild(i.createElement('option'))),
    (oe.type = 'checkbox'),
    (p.checkOn = '' !== oe.value),
    (p.optSelected = ae.selected),
    ((oe = i.createElement('input')).value = 't'),
    (oe.type = 'radio'),
    (p.radioValue = 't' === oe.value);
  var pe,
    ge = w.expr.attrHandle;
  w.fn.extend({
    attr: function(t, e) {
      return q(this, w.attr, t, e, 1 < arguments.length);
    },
    removeAttr: function(t) {
      return this.each(function() {
        w.removeAttr(this, t);
      });
    }
  }),
    w.extend({
      attr: function(t, e, n) {
        var i,
          r,
          o = t.nodeType;
        if (3 !== o && 8 !== o && 2 !== o)
          return void 0 === t.getAttribute
            ? w.prop(t, e, n)
            : ((1 === o && w.isXMLDoc(t)) ||
                (r = w.attrHooks[e.toLowerCase()] || (w.expr.match.bool.test(e) ? pe : void 0)),
              void 0 !== n
                ? null === n
                  ? void w.removeAttr(t, e)
                  : r && 'set' in r && void 0 !== (i = r.set(t, n, e))
                  ? i
                  : (t.setAttribute(e, n + ''), n)
                : r && 'get' in r && null !== (i = r.get(t, e))
                ? i
                : null == (i = w.find.attr(t, e))
                ? void 0
                : i);
      },
      attrHooks: {
        type: {
          set: function(t, e) {
            if (!p.radioValue && 'radio' === e && A(t, 'input')) {
              var n = t.value;
              return t.setAttribute('type', e), n && (t.value = n), e;
            }
          }
        }
      },
      removeAttr: function(t, e) {
        var n,
          i = 0,
          r = e && e.match(F);
        if (r && 1 === t.nodeType) for (; (n = r[i++]); ) t.removeAttribute(n);
      }
    }),
    (pe = {
      set: function(t, e, n) {
        return !1 === e ? w.removeAttr(t, n) : t.setAttribute(n, n), n;
      }
    }),
    w.each(w.expr.match.bool.source.match(/\w+/g), function(t, e) {
      var n = ge[e] || w.find.attr;
      ge[e] = function(t, e, i) {
        var r,
          o,
          a = e.toLowerCase();
        return i || ((o = ge[a]), (ge[a] = r), (r = null != n(t, e, i) ? a : null), (ge[a] = o)), r;
      };
    });
  var me = /^(?:input|select|textarea|button)$/i,
    ve = /^(?:a|area)$/i;
  function be(t) {
    return (t.match(F) || []).join(' ');
  }
  function ye(t) {
    return (t.getAttribute && t.getAttribute('class')) || '';
  }
  function xe(t) {
    return Array.isArray(t) ? t : ('string' == typeof t && t.match(F)) || [];
  }
  w.fn.extend({
    prop: function(t, e) {
      return q(this, w.prop, t, e, 1 < arguments.length);
    },
    removeProp: function(t) {
      return this.each(function() {
        delete this[w.propFix[t] || t];
      });
    }
  }),
    w.extend({
      prop: function(t, e, n) {
        var i,
          r,
          o = t.nodeType;
        if (3 !== o && 8 !== o && 2 !== o)
          return (
            (1 === o && w.isXMLDoc(t)) || (r = w.propHooks[(e = w.propFix[e] || e)]),
            void 0 !== n
              ? r && 'set' in r && void 0 !== (i = r.set(t, n, e))
                ? i
                : (t[e] = n)
              : r && 'get' in r && null !== (i = r.get(t, e))
              ? i
              : t[e]
          );
      },
      propHooks: {
        tabIndex: {
          get: function(t) {
            var e = w.find.attr(t, 'tabindex');
            return e ? parseInt(e, 10) : me.test(t.nodeName) || (ve.test(t.nodeName) && t.href) ? 0 : -1;
          }
        }
      },
      propFix: { for: 'htmlFor', class: 'className' }
    }),
    p.optSelected ||
      (w.propHooks.selected = {
        get: function(t) {
          return null;
        },
        set: function(t) {}
      }),
    w.each(
      [
        'tabIndex',
        'readOnly',
        'maxLength',
        'cellSpacing',
        'cellPadding',
        'rowSpan',
        'colSpan',
        'useMap',
        'frameBorder',
        'contentEditable'
      ],
      function() {
        w.propFix[this.toLowerCase()] = this;
      }
    ),
    w.fn.extend({
      addClass: function(t) {
        var e,
          n,
          i,
          r,
          o,
          a,
          s,
          l = 0;
        if (g(t))
          return this.each(function(e) {
            w(this).addClass(t.call(this, e, ye(this)));
          });
        if ((e = xe(t)).length)
          for (; (n = this[l++]); )
            if (((r = ye(n)), (i = 1 === n.nodeType && ' ' + be(r) + ' '))) {
              for (a = 0; (o = e[a++]); ) i.indexOf(' ' + o + ' ') < 0 && (i += o + ' ');
              r !== (s = be(i)) && n.setAttribute('class', s);
            }
        return this;
      },
      removeClass: function(t) {
        var e,
          n,
          i,
          r,
          o,
          a,
          s,
          l = 0;
        if (g(t))
          return this.each(function(e) {
            w(this).removeClass(t.call(this, e, ye(this)));
          });
        if (!arguments.length) return this.attr('class', '');
        if ((e = xe(t)).length)
          for (; (n = this[l++]); )
            if (((r = ye(n)), (i = 1 === n.nodeType && ' ' + be(r) + ' '))) {
              for (a = 0; (o = e[a++]); ) for (; -1 < i.indexOf(' ' + o + ' '); ) i = i.replace(' ' + o + ' ', ' ');
              r !== (s = be(i)) && n.setAttribute('class', s);
            }
        return this;
      },
      toggleClass: function(t, e) {
        var n = typeof t,
          i = 'string' === n || Array.isArray(t);
        return 'boolean' == typeof e && i
          ? e
            ? this.addClass(t)
            : this.removeClass(t)
          : g(t)
          ? this.each(function(n) {
              w(this).toggleClass(t.call(this, n, ye(this), e), e);
            })
          : this.each(function() {
              var e, r, o, a;
              if (i)
                for (r = 0, o = w(this), a = xe(t); (e = a[r++]); ) o.hasClass(e) ? o.removeClass(e) : o.addClass(e);
              else
                (void 0 !== t && 'boolean' !== n) ||
                  ((e = ye(this)) && X.set(this, '__className__', e),
                  this.setAttribute &&
                    this.setAttribute('class', e || !1 === t ? '' : X.get(this, '__className__') || ''));
            });
      },
      hasClass: function(t) {
        var e,
          n,
          i = 0;
        for (e = ' ' + t + ' '; (n = this[i++]); )
          if (1 === n.nodeType && -1 < (' ' + be(ye(n)) + ' ').indexOf(e)) return !0;
        return !1;
      }
    });
  var we = /\r/g;
  w.fn.extend({
    val: function(t) {
      var e,
        n,
        i,
        r = this[0];
      return arguments.length
        ? ((i = g(t)),
          this.each(function(n) {
            var r;
            1 === this.nodeType &&
              (null == (r = i ? t.call(this, n, w(this).val()) : t)
                ? (r = '')
                : 'number' == typeof r
                ? (r += '')
                : Array.isArray(r) &&
                  (r = w.map(r, function(t) {
                    return null == t ? '' : t + '';
                  })),
              ((e = w.valHooks[this.type] || w.valHooks[this.nodeName.toLowerCase()]) &&
                'set' in e &&
                void 0 !== e.set(this, r, 'value')) ||
                (this.value = r));
          }))
        : r
        ? (e = w.valHooks[r.type] || w.valHooks[r.nodeName.toLowerCase()]) &&
          'get' in e &&
          void 0 !== (n = e.get(r, 'value'))
          ? n
          : 'string' == typeof (n = r.value)
          ? n.replace(we, '')
          : null == n
          ? ''
          : n
        : void 0;
    }
  }),
    w.extend({
      valHooks: {
        option: {
          get: function(t) {
            var e = w.find.attr(t, 'value');
            return null != e ? e : be(w.text(t));
          }
        },
        select: {
          get: function(t) {
            var e,
              n,
              i,
              r = t.options,
              o = t.selectedIndex,
              a = 'select-one' === t.type,
              s = a ? null : [],
              l = a ? o + 1 : r.length;
            for (i = o < 0 ? l : a ? o : 0; i < l; i++)
              if (
                ((n = r[i]).selected || i === o) &&
                !n.disabled &&
                (!n.parentNode.disabled || !A(n.parentNode, 'optgroup'))
              ) {
                if (((e = w(n).val()), a)) return e;
                s.push(e);
              }
            return s;
          },
          set: function(t, e) {
            for (var n, i, r = t.options, o = w.makeArray(e), a = r.length; a--; )
              ((i = r[a]).selected = -1 < w.inArray(w.valHooks.option.get(i), o)) && (n = !0);
            return n || (t.selectedIndex = -1), o;
          }
        }
      }
    }),
    w.each(['radio', 'checkbox'], function() {
      (w.valHooks[this] = {
        set: function(t, e) {
          if (Array.isArray(e)) return (t.checked = -1 < w.inArray(w(t).val(), e));
        }
      }),
        p.checkOn ||
          (w.valHooks[this].get = function(t) {
            return null === t.getAttribute('value') ? 'on' : t.value;
          });
    }),
    (p.focusin = 'onfocusin' in t);
  var _e = /^(?:focusinfocus|focusoutblur)$/,
    Ce = function(t) {
      t.stopPropagation();
    };
  w.extend(w.event, {
    trigger: function(e, n, r, o) {
      var a,
        s,
        l,
        u,
        c,
        h,
        f,
        p,
        v = [r || i],
        b = d.call(e, 'type') ? e.type : e,
        y = d.call(e, 'namespace') ? e.namespace.split('.') : [];
      if (
        ((s = p = l = r = r || i),
        3 !== r.nodeType &&
          8 !== r.nodeType &&
          !_e.test(b + w.event.triggered) &&
          (-1 < b.indexOf('.') && ((b = (y = b.split('.')).shift()), y.sort()),
          (c = b.indexOf(':') < 0 && 'on' + b),
          ((e = e[w.expando] ? e : new w.Event(b, 'object' == typeof e && e)).isTrigger = o ? 2 : 3),
          (e.namespace = y.join('.')),
          (e.rnamespace = e.namespace ? new RegExp('(^|\\.)' + y.join('\\.(?:.*\\.|)') + '(\\.|$)') : null),
          (e.result = void 0),
          e.target || (e.target = r),
          (n = null == n ? [e] : w.makeArray(n, [e])),
          (f = w.event.special[b] || {}),
          o || !f.trigger || !1 !== f.trigger.apply(r, n)))
      ) {
        if (!o && !f.noBubble && !m(r)) {
          for (_e.test((u = f.delegateType || b) + b) || (s = s.parentNode); s; s = s.parentNode) v.push(s), (l = s);
          l === (r.ownerDocument || i) && v.push(l.defaultView || l.parentWindow || t);
        }
        for (a = 0; (s = v[a++]) && !e.isPropagationStopped(); )
          (p = s),
            (e.type = 1 < a ? u : f.bindType || b),
            (h = (X.get(s, 'events') || {})[e.type] && X.get(s, 'handle')) && h.apply(s, n),
            (h = c && s[c]) && h.apply && K(s) && ((e.result = h.apply(s, n)), !1 === e.result && e.preventDefault());
        return (
          (e.type = b),
          o ||
            e.isDefaultPrevented() ||
            (f._default && !1 !== f._default.apply(v.pop(), n)) ||
            !K(r) ||
            (c &&
              g(r[b]) &&
              !m(r) &&
              ((l = r[c]) && (r[c] = null),
              (w.event.triggered = b),
              e.isPropagationStopped() && p.addEventListener(b, Ce),
              r[b](),
              e.isPropagationStopped() && p.removeEventListener(b, Ce),
              (w.event.triggered = void 0),
              l && (r[c] = l))),
          e.result
        );
      }
    },
    simulate: function(t, e, n) {
      var i = w.extend(new w.Event(), n, { type: t, isSimulated: !0 });
      w.event.trigger(i, null, e);
    }
  }),
    w.fn.extend({
      trigger: function(t, e) {
        return this.each(function() {
          w.event.trigger(t, e, this);
        });
      },
      triggerHandler: function(t, e) {
        var n = this[0];
        if (n) return w.event.trigger(t, e, n, !0);
      }
    }),
    p.focusin ||
      w.each({ focus: 'focusin', blur: 'focusout' }, function(t, e) {
        var n = function(t) {
          w.event.simulate(e, t.target, w.event.fix(t));
        };
        w.event.special[e] = {
          setup: function() {
            var i = this.ownerDocument || this,
              r = X.access(i, e);
            r || i.addEventListener(t, n, !0), X.access(i, e, (r || 0) + 1);
          },
          teardown: function() {
            var i = this.ownerDocument || this,
              r = X.access(i, e) - 1;
            r ? X.access(i, e, r) : (i.removeEventListener(t, n, !0), X.remove(i, e));
          }
        };
      });
  var ke = t.location,
    Se = Date.now(),
    Te = /\?/;
  w.parseXML = function(e) {
    var n;
    if (!e || 'string' != typeof e) return null;
    try {
      n = new t.DOMParser().parseFromString(e, 'text/xml');
    } catch (e) {
      n = void 0;
    }
    return (n && !n.getElementsByTagName('parsererror').length) || w.error('Invalid XML: ' + e), n;
  };
  var De = /\[\]$/,
    Ae = /\r?\n/g,
    Ee = /^(?:submit|button|image|reset|file)$/i,
    Me = /^(?:input|select|textarea|keygen)/i;
  function Ie(t, e, n, i) {
    var r;
    if (Array.isArray(e))
      w.each(e, function(e, r) {
        n || De.test(t) ? i(t, r) : Ie(t + '[' + ('object' == typeof r && null != r ? e : '') + ']', r, n, i);
      });
    else if (n || 'object' !== y(e)) i(t, e);
    else for (r in e) Ie(t + '[' + r + ']', e[r], n, i);
  }
  (w.param = function(t, e) {
    var n,
      i = [],
      r = function(t, e) {
        var n = g(e) ? e() : e;
        i[i.length] = encodeURIComponent(t) + '=' + encodeURIComponent(null == n ? '' : n);
      };
    if (null == t) return '';
    if (Array.isArray(t) || (t.jquery && !w.isPlainObject(t)))
      w.each(t, function() {
        r(this.name, this.value);
      });
    else for (n in t) Ie(n, t[n], e, r);
    return i.join('&');
  }),
    w.fn.extend({
      serialize: function() {
        return w.param(this.serializeArray());
      },
      serializeArray: function() {
        return this.map(function() {
          var t = w.prop(this, 'elements');
          return t ? w.makeArray(t) : this;
        })
          .filter(function() {
            var t = this.type;
            return (
              this.name &&
              !w(this).is(':disabled') &&
              Me.test(this.nodeName) &&
              !Ee.test(t) &&
              (this.checked || !ht.test(t))
            );
          })
          .map(function(t, e) {
            var n = w(this).val();
            return null == n
              ? null
              : Array.isArray(n)
              ? w.map(n, function(t) {
                  return { name: e.name, value: t.replace(Ae, '\r\n') };
                })
              : { name: e.name, value: n.replace(Ae, '\r\n') };
          })
          .get();
      }
    });
  var Pe = /%20/g,
    Oe = /#.*$/,
    Ne = /([?&])_=[^&]*/,
    Le = /^(.*?):[ \t]*([^\r\n]*)$/gm,
    Fe = /^(?:GET|HEAD)$/,
    Re = /^\/\//,
    je = {},
    He = {},
    Be = '*/'.concat('*'),
    We = i.createElement('a');
  function ze(t) {
    return function(e, n) {
      'string' != typeof e && ((n = e), (e = '*'));
      var i,
        r = 0,
        o = e.toLowerCase().match(F) || [];
      if (g(n))
        for (; (i = o[r++]); )
          '+' === i[0] ? ((i = i.slice(1) || '*'), (t[i] = t[i] || []).unshift(n)) : (t[i] = t[i] || []).push(n);
    };
  }
  function qe(t, e, n, i) {
    var r = {},
      o = t === He;
    function a(s) {
      var l;
      return (
        (r[s] = !0),
        w.each(t[s] || [], function(t, s) {
          var u = s(e, n, i);
          return 'string' != typeof u || o || r[u] ? (o ? !(l = u) : void 0) : (e.dataTypes.unshift(u), a(u), !1);
        }),
        l
      );
    }
    return a(e.dataTypes[0]) || (!r['*'] && a('*'));
  }
  function Ve(t, e) {
    var n,
      i,
      r = w.ajaxSettings.flatOptions || {};
    for (n in e) void 0 !== e[n] && ((r[n] ? t : i || (i = {}))[n] = e[n]);
    return i && w.extend(!0, t, i), t;
  }
  (We.href = ke.href),
    w.extend({
      active: 0,
      lastModified: {},
      etag: {},
      ajaxSettings: {
        url: ke.href,
        type: 'GET',
        isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(ke.protocol),
        global: !0,
        processData: !0,
        async: !0,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        accepts: {
          '*': Be,
          text: 'text/plain',
          html: 'text/html',
          xml: 'application/xml, text/xml',
          json: 'application/json, text/javascript'
        },
        contents: { xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/ },
        responseFields: { xml: 'responseXML', text: 'responseText', json: 'responseJSON' },
        converters: { '* text': String, 'text html': !0, 'text json': JSON.parse, 'text xml': w.parseXML },
        flatOptions: { url: !0, context: !0 }
      },
      ajaxSetup: function(t, e) {
        return e ? Ve(Ve(t, w.ajaxSettings), e) : Ve(w.ajaxSettings, t);
      },
      ajaxPrefilter: ze(je),
      ajaxTransport: ze(He),
      ajax: function(e, n) {
        'object' == typeof e && ((n = e), (e = void 0));
        var r,
          o,
          a,
          s,
          l,
          u,
          c,
          d,
          h,
          f,
          p = w.ajaxSetup({}, (n = n || {})),
          g = p.context || p,
          m = p.context && (g.nodeType || g.jquery) ? w(g) : w.event,
          v = w.Deferred(),
          b = w.Callbacks('once memory'),
          y = p.statusCode || {},
          x = {},
          _ = {},
          C = 'canceled',
          k = {
            readyState: 0,
            getResponseHeader: function(t) {
              var e;
              if (c) {
                if (!s)
                  for (s = {}; (e = Le.exec(a)); )
                    s[e[1].toLowerCase() + ' '] = (s[e[1].toLowerCase() + ' '] || []).concat(e[2]);
                e = s[t.toLowerCase() + ' '];
              }
              return null == e ? null : e.join(', ');
            },
            getAllResponseHeaders: function() {
              return c ? a : null;
            },
            setRequestHeader: function(t, e) {
              return null == c && ((t = _[t.toLowerCase()] = _[t.toLowerCase()] || t), (x[t] = e)), this;
            },
            overrideMimeType: function(t) {
              return null == c && (p.mimeType = t), this;
            },
            statusCode: function(t) {
              var e;
              if (t)
                if (c) k.always(t[k.status]);
                else for (e in t) y[e] = [y[e], t[e]];
              return this;
            },
            abort: function(t) {
              var e = t || C;
              return r && r.abort(e), S(0, e), this;
            }
          };
        if (
          (v.promise(k),
          (p.url = ((e || p.url || ke.href) + '').replace(Re, ke.protocol + '//')),
          (p.type = n.method || n.type || p.method || p.type),
          (p.dataTypes = (p.dataType || '*').toLowerCase().match(F) || ['']),
          null == p.crossDomain)
        ) {
          u = i.createElement('a');
          try {
            (u.href = p.url),
              (u.href = u.href),
              (p.crossDomain = We.protocol + '//' + We.host != u.protocol + '//' + u.host);
          } catch (e) {
            p.crossDomain = !0;
          }
        }
        if (
          (p.data && p.processData && 'string' != typeof p.data && (p.data = w.param(p.data, p.traditional)),
          qe(je, p, n, k),
          c)
        )
          return k;
        for (h in ((d = w.event && p.global) && 0 == w.active++ && w.event.trigger('ajaxStart'),
        (p.type = p.type.toUpperCase()),
        (p.hasContent = !Fe.test(p.type)),
        (o = p.url.replace(Oe, '')),
        p.hasContent
          ? p.data &&
            p.processData &&
            0 === (p.contentType || '').indexOf('application/x-www-form-urlencoded') &&
            (p.data = p.data.replace(Pe, '+'))
          : ((f = p.url.slice(o.length)),
            p.data &&
              (p.processData || 'string' == typeof p.data) &&
              ((o += (Te.test(o) ? '&' : '?') + p.data), delete p.data),
            !1 === p.cache && ((o = o.replace(Ne, '$1')), (f = (Te.test(o) ? '&' : '?') + '_=' + Se++ + f)),
            (p.url = o + f)),
        p.ifModified &&
          (w.lastModified[o] && k.setRequestHeader('If-Modified-Since', w.lastModified[o]),
          w.etag[o] && k.setRequestHeader('If-None-Match', w.etag[o])),
        ((p.data && p.hasContent && !1 !== p.contentType) || n.contentType) &&
          k.setRequestHeader('Content-Type', p.contentType),
        k.setRequestHeader(
          'Accept',
          p.dataTypes[0] && p.accepts[p.dataTypes[0]]
            ? p.accepts[p.dataTypes[0]] + ('*' !== p.dataTypes[0] ? ', ' + Be + '; q=0.01' : '')
            : p.accepts['*']
        ),
        p.headers))
          k.setRequestHeader(h, p.headers[h]);
        if (p.beforeSend && (!1 === p.beforeSend.call(g, k, p) || c)) return k.abort();
        if (((C = 'abort'), b.add(p.complete), k.done(p.success), k.fail(p.error), (r = qe(He, p, n, k)))) {
          if (((k.readyState = 1), d && m.trigger('ajaxSend', [k, p]), c)) return k;
          p.async &&
            0 < p.timeout &&
            (l = t.setTimeout(function() {
              k.abort('timeout');
            }, p.timeout));
          try {
            (c = !1), r.send(x, S);
          } catch (e) {
            if (c) throw e;
            S(-1, e);
          }
        } else S(-1, 'No Transport');
        function S(e, n, i, s) {
          var u,
            h,
            f,
            x,
            _,
            C = n;
          c ||
            ((c = !0),
            l && t.clearTimeout(l),
            (r = void 0),
            (a = s || ''),
            (k.readyState = 0 < e ? 4 : 0),
            (u = (200 <= e && e < 300) || 304 === e),
            i &&
              (x = (function(t, e, n) {
                for (var i, r, o, a, s = t.contents, l = t.dataTypes; '*' === l[0]; )
                  l.shift(), void 0 === i && (i = t.mimeType || e.getResponseHeader('Content-Type'));
                if (i)
                  for (r in s)
                    if (s[r] && s[r].test(i)) {
                      l.unshift(r);
                      break;
                    }
                if (l[0] in n) o = l[0];
                else {
                  for (r in n) {
                    if (!l[0] || t.converters[r + ' ' + l[0]]) {
                      o = r;
                      break;
                    }
                    a || (a = r);
                  }
                  o = o || a;
                }
                if (o) return o !== l[0] && l.unshift(o), n[o];
              })(p, k, i)),
            (x = (function(t, e, n, i) {
              var r,
                o,
                a,
                s,
                l,
                u = {},
                c = t.dataTypes.slice();
              if (c[1]) for (a in t.converters) u[a.toLowerCase()] = t.converters[a];
              for (o = c.shift(); o; )
                if (
                  (t.responseFields[o] && (n[t.responseFields[o]] = e),
                  !l && i && t.dataFilter && (e = t.dataFilter(e, t.dataType)),
                  (l = o),
                  (o = c.shift()))
                )
                  if ('*' === o) o = l;
                  else if ('*' !== l && l !== o) {
                    if (!(a = u[l + ' ' + o] || u['* ' + o]))
                      for (r in u)
                        if ((s = r.split(' '))[1] === o && (a = u[l + ' ' + s[0]] || u['* ' + s[0]])) {
                          !0 === a ? (a = u[r]) : !0 !== u[r] && ((o = s[0]), c.unshift(s[1]));
                          break;
                        }
                    if (!0 !== a)
                      if (a && t.throws) e = a(e);
                      else
                        try {
                          e = a(e);
                        } catch (t) {
                          return { state: 'parsererror', error: a ? t : 'No conversion from ' + l + ' to ' + o };
                        }
                  }
              return { state: 'success', data: e };
            })(p, x, k, u)),
            u
              ? (p.ifModified &&
                  ((_ = k.getResponseHeader('Last-Modified')) && (w.lastModified[o] = _),
                  (_ = k.getResponseHeader('etag')) && (w.etag[o] = _)),
                204 === e || 'HEAD' === p.type
                  ? (C = 'nocontent')
                  : 304 === e
                  ? (C = 'notmodified')
                  : ((C = x.state), (h = x.data), (u = !(f = x.error))))
              : ((f = C), (!e && C) || ((C = 'error'), e < 0 && (e = 0))),
            (k.status = e),
            (k.statusText = (n || C) + ''),
            u ? v.resolveWith(g, [h, C, k]) : v.rejectWith(g, [k, C, f]),
            k.statusCode(y),
            (y = void 0),
            d && m.trigger(u ? 'ajaxSuccess' : 'ajaxError', [k, p, u ? h : f]),
            b.fireWith(g, [k, C]),
            d && (m.trigger('ajaxComplete', [k, p]), --w.active || w.event.trigger('ajaxStop')));
        }
        return k;
      },
      getJSON: function(t, e, n) {
        return w.get(t, e, n, 'json');
      },
      getScript: function(t, e) {
        return w.get(t, void 0, e, 'script');
      }
    }),
    w.each(['get', 'post'], function(t, e) {
      w[e] = function(t, n, i, r) {
        return (
          g(n) && ((r = r || i), (i = n), (n = void 0)),
          w.ajax(w.extend({ url: t, type: e, dataType: r, data: n, success: i }, w.isPlainObject(t) && t))
        );
      };
    }),
    (w._evalUrl = function(t, e) {
      return w.ajax({
        url: t,
        type: 'GET',
        dataType: 'script',
        cache: !0,
        async: !1,
        global: !1,
        converters: { 'text script': function() {} },
        dataFilter: function(t) {
          w.globalEval(t, e);
        }
      });
    }),
    w.fn.extend({
      wrapAll: function(t) {
        var e;
        return (
          this[0] &&
            (g(t) && (t = t.call(this[0])),
            (e = w(t, this[0].ownerDocument)
              .eq(0)
              .clone(!0)),
            this[0].parentNode && e.insertBefore(this[0]),
            e
              .map(function() {
                for (var t = this; t.firstElementChild; ) t = t.firstElementChild;
                return t;
              })
              .append(this)),
          this
        );
      },
      wrapInner: function(t) {
        return g(t)
          ? this.each(function(e) {
              w(this).wrapInner(t.call(this, e));
            })
          : this.each(function() {
              var e = w(this),
                n = e.contents();
              n.length ? n.wrapAll(t) : e.append(t);
            });
      },
      wrap: function(t) {
        var e = g(t);
        return this.each(function(n) {
          w(this).wrapAll(e ? t.call(this, n) : t);
        });
      },
      unwrap: function(t) {
        return (
          this.parent(t)
            .not('body')
            .each(function() {
              w(this).replaceWith(this.childNodes);
            }),
          this
        );
      }
    }),
    (w.expr.pseudos.hidden = function(t) {
      return !w.expr.pseudos.visible(t);
    }),
    (w.expr.pseudos.visible = function(t) {
      return !!(t.offsetWidth || t.offsetHeight || t.getClientRects().length);
    }),
    (w.ajaxSettings.xhr = function() {
      try {
        return new t.XMLHttpRequest();
      } catch (e) {}
    });
  var Ue = { 0: 200, 1223: 204 },
    $e = w.ajaxSettings.xhr();
  (p.cors = !!$e && 'withCredentials' in $e),
    (p.ajax = $e = !!$e),
    w.ajaxTransport(function(e) {
      var n, i;
      if (p.cors || ($e && !e.crossDomain))
        return {
          send: function(r, o) {
            var a,
              s = e.xhr();
            if ((s.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields))
              for (a in e.xhrFields) s[a] = e.xhrFields[a];
            for (a in (e.mimeType && s.overrideMimeType && s.overrideMimeType(e.mimeType),
            e.crossDomain || r['X-Requested-With'] || (r['X-Requested-With'] = 'XMLHttpRequest'),
            r))
              s.setRequestHeader(a, r[a]);
            (n = function(t) {
              return function() {
                n &&
                  ((n = i = s.onload = s.onerror = s.onabort = s.ontimeout = s.onreadystatechange = null),
                  'abort' === t
                    ? s.abort()
                    : 'error' === t
                    ? 'number' != typeof s.status
                      ? o(0, 'error')
                      : o(s.status, s.statusText)
                    : o(
                        Ue[s.status] || s.status,
                        s.statusText,
                        'text' !== (s.responseType || 'text') || 'string' != typeof s.responseText
                          ? { binary: s.response }
                          : { text: s.responseText },
                        s.getAllResponseHeaders()
                      ));
              };
            }),
              (s.onload = n()),
              (i = s.onerror = s.ontimeout = n('error')),
              void 0 !== s.onabort
                ? (s.onabort = i)
                : (s.onreadystatechange = function() {
                    4 === s.readyState &&
                      t.setTimeout(function() {
                        n && i();
                      });
                  }),
              (n = n('abort'));
            try {
              s.send((e.hasContent && e.data) || null);
            } catch (r) {
              if (n) throw r;
            }
          },
          abort: function() {
            n && n();
          }
        };
    }),
    w.ajaxPrefilter(function(t) {
      t.crossDomain && (t.contents.script = !1);
    }),
    w.ajaxSetup({
      accepts: { script: 'text/javascript, application/javascript, application/ecmascript, application/x-ecmascript' },
      contents: { script: /\b(?:java|ecma)script\b/ },
      converters: {
        'text script': function(t) {
          return w.globalEval(t), t;
        }
      }
    }),
    w.ajaxPrefilter('script', function(t) {
      void 0 === t.cache && (t.cache = !1), t.crossDomain && (t.type = 'GET');
    }),
    w.ajaxTransport('script', function(t) {
      var e, n;
      if (t.crossDomain || t.scriptAttrs)
        return {
          send: function(r, o) {
            (e = w('<script>')
              .attr(t.scriptAttrs || {})
              .prop({ charset: t.scriptCharset, src: t.url })
              .on(
                'load error',
                (n = function(t) {
                  e.remove(), (n = null), t && o('error' === t.type ? 404 : 200, t.type);
                })
              )),
              i.head.appendChild(e[0]);
          },
          abort: function() {
            n && n();
          }
        };
    });
  var Ye,
    Ke = [],
    Qe = /(=)\?(?=&|$)|\?\?/;
  w.ajaxSetup({
    jsonp: 'callback',
    jsonpCallback: function() {
      var t = Ke.pop() || w.expando + '_' + Se++;
      return (this[t] = !0), t;
    }
  }),
    w.ajaxPrefilter('json jsonp', function(e, n, i) {
      var r,
        o,
        a,
        s =
          !1 !== e.jsonp &&
          (Qe.test(e.url)
            ? 'url'
            : 'string' == typeof e.data &&
              0 === (e.contentType || '').indexOf('application/x-www-form-urlencoded') &&
              Qe.test(e.data) &&
              'data');
      if (s || 'jsonp' === e.dataTypes[0])
        return (
          (r = e.jsonpCallback = g(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback),
          s
            ? (e[s] = e[s].replace(Qe, '$1' + r))
            : !1 !== e.jsonp && (e.url += (Te.test(e.url) ? '&' : '?') + e.jsonp + '=' + r),
          (e.converters['script json'] = function() {
            return a || w.error(r + ' was not called'), a[0];
          }),
          (e.dataTypes[0] = 'json'),
          (o = t[r]),
          (t[r] = function() {
            a = arguments;
          }),
          i.always(function() {
            void 0 === o ? w(t).removeProp(r) : (t[r] = o),
              e[r] && ((e.jsonpCallback = n.jsonpCallback), Ke.push(r)),
              a && g(o) && o(a[0]),
              (a = o = void 0);
          }),
          'script'
        );
    }),
    (p.createHTMLDocument =
      (((Ye = i.implementation.createHTMLDocument('').body).innerHTML = '<form></form><form></form>'),
      2 === Ye.childNodes.length)),
    (w.parseHTML = function(t, e, n) {
      return 'string' != typeof t
        ? []
        : ('boolean' == typeof e && ((n = e), (e = !1)),
          e ||
            (p.createHTMLDocument
              ? (((r = (e = i.implementation.createHTMLDocument('')).createElement('base')).href = i.location.href),
                e.head.appendChild(r))
              : (e = i)),
          (a = !n && []),
          (o = E.exec(t))
            ? [e.createElement(o[1])]
            : ((o = wt([t], e, a)), a && a.length && w(a).remove(), w.merge([], o.childNodes)));
      var r, o, a;
    }),
    (w.fn.load = function(t, e, n) {
      var i,
        r,
        o,
        a = this,
        s = t.indexOf(' ');
      return (
        -1 < s && ((i = be(t.slice(s))), (t = t.slice(0, s))),
        g(e) ? ((n = e), (e = void 0)) : e && 'object' == typeof e && (r = 'POST'),
        0 < a.length &&
          w
            .ajax({ url: t, type: r || 'GET', dataType: 'html', data: e })
            .done(function(t) {
              (o = arguments),
                a.html(
                  i
                    ? w('<div>')
                        .append(w.parseHTML(t))
                        .find(i)
                    : t
                );
            })
            .always(
              n &&
                function(t, e) {
                  a.each(function() {
                    n.apply(this, o || [t.responseText, e, t]);
                  });
                }
            ),
        this
      );
    }),
    w.each(['ajaxStart', 'ajaxStop', 'ajaxComplete', 'ajaxError', 'ajaxSuccess', 'ajaxSend'], function(t, e) {
      w.fn[e] = function(t) {
        return this.on(e, t);
      };
    }),
    (w.expr.pseudos.animated = function(t) {
      return w.grep(w.timers, function(e) {
        return t === e.elem;
      }).length;
    }),
    (w.offset = {
      setOffset: function(t, e, n) {
        var i,
          r,
          o,
          a,
          s,
          l,
          u = w.css(t, 'position'),
          c = w(t),
          d = {};
        'static' === u && (t.style.position = 'relative'),
          (s = c.offset()),
          (o = w.css(t, 'top')),
          (l = w.css(t, 'left')),
          ('absolute' === u || 'fixed' === u) && -1 < (o + l).indexOf('auto')
            ? ((a = (i = c.position()).top), (r = i.left))
            : ((a = parseFloat(o) || 0), (r = parseFloat(l) || 0)),
          g(e) && (e = e.call(t, n, w.extend({}, s))),
          null != e.top && (d.top = e.top - s.top + a),
          null != e.left && (d.left = e.left - s.left + r),
          'using' in e ? e.using.call(t, d) : c.css(d);
      }
    }),
    w.fn.extend({
      offset: function(t) {
        if (arguments.length)
          return void 0 === t
            ? this
            : this.each(function(e) {
                w.offset.setOffset(this, t, e);
              });
        var e,
          n,
          i = this[0];
        return i
          ? i.getClientRects().length
            ? {
                top: (e = i.getBoundingClientRect()).top + (n = i.ownerDocument.defaultView).pageYOffset,
                left: e.left + n.pageXOffset
              }
            : { top: 0, left: 0 }
          : void 0;
      },
      position: function() {
        if (this[0]) {
          var t,
            e,
            n,
            i = this[0],
            r = { top: 0, left: 0 };
          if ('fixed' === w.css(i, 'position')) e = i.getBoundingClientRect();
          else {
            for (
              e = this.offset(), n = i.ownerDocument, t = i.offsetParent || n.documentElement;
              t && (t === n.body || t === n.documentElement) && 'static' === w.css(t, 'position');

            )
              t = t.parentNode;
            t &&
              t !== i &&
              1 === t.nodeType &&
              (((r = w(t).offset()).top += w.css(t, 'borderTopWidth', !0)),
              (r.left += w.css(t, 'borderLeftWidth', !0)));
          }
          return { top: e.top - r.top - w.css(i, 'marginTop', !0), left: e.left - r.left - w.css(i, 'marginLeft', !0) };
        }
      },
      offsetParent: function() {
        return this.map(function() {
          for (var t = this.offsetParent; t && 'static' === w.css(t, 'position'); ) t = t.offsetParent;
          return t || rt;
        });
      }
    }),
    w.each({ scrollLeft: 'pageXOffset', scrollTop: 'pageYOffset' }, function(t, e) {
      var n = 'pageYOffset' === e;
      w.fn[t] = function(i) {
        return q(
          this,
          function(t, i, r) {
            var o;
            if ((m(t) ? (o = t) : 9 === t.nodeType && (o = t.defaultView), void 0 === r)) return o ? o[e] : t[i];
            o ? o.scrollTo(n ? o.pageXOffset : r, n ? r : o.pageYOffset) : (t[i] = r);
          },
          t,
          i,
          arguments.length
        );
      };
    }),
    w.each(['top', 'left'], function(t, e) {
      w.cssHooks[e] = Vt(p.pixelPosition, function(t, n) {
        if (n) return (n = qt(t, e)), Bt.test(n) ? w(t).position()[e] + 'px' : n;
      });
    }),
    w.each({ Height: 'height', Width: 'width' }, function(t, e) {
      w.each({ padding: 'inner' + t, content: e, '': 'outer' + t }, function(n, i) {
        w.fn[i] = function(r, o) {
          var a = arguments.length && (n || 'boolean' != typeof r),
            s = n || (!0 === r || !0 === o ? 'margin' : 'border');
          return q(
            this,
            function(e, n, r) {
              var o;
              return m(e)
                ? 0 === i.indexOf('outer')
                  ? e['inner' + t]
                  : e.document.documentElement['client' + t]
                : 9 === e.nodeType
                ? ((o = e.documentElement),
                  Math.max(
                    e.body['scroll' + t],
                    o['scroll' + t],
                    e.body['offset' + t],
                    o['offset' + t],
                    o['client' + t]
                  ))
                : void 0 === r
                ? w.css(e, n, s)
                : w.style(e, n, r, s);
            },
            e,
            a ? r : void 0,
            a
          );
        };
      });
    }),
    w.each(
      'blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu'.split(
        ' '
      ),
      function(t, e) {
        w.fn[e] = function(t, n) {
          return 0 < arguments.length ? this.on(e, null, t, n) : this.trigger(e);
        };
      }
    ),
    w.fn.extend({
      hover: function(t, e) {
        return this.mouseenter(t).mouseleave(e || t);
      }
    }),
    w.fn.extend({
      bind: function(t, e, n) {
        return this.on(t, null, e, n);
      },
      unbind: function(t, e) {
        return this.off(t, null, e);
      },
      delegate: function(t, e, n, i) {
        return this.on(e, t, n, i);
      },
      undelegate: function(t, e, n) {
        return 1 === arguments.length ? this.off(t, '**') : this.off(e, t || '**', n);
      }
    }),
    (w.proxy = function(t, e) {
      var n, i, r;
      if (('string' == typeof e && ((n = t[e]), (e = t), (t = n)), g(t)))
        return (
          (i = o.call(arguments, 2)),
          ((r = function() {
            return t.apply(e || this, i.concat(o.call(arguments)));
          }).guid = t.guid = t.guid || w.guid++),
          r
        );
    }),
    (w.holdReady = function(t) {
      t ? w.readyWait++ : w.ready(!0);
    }),
    (w.isArray = Array.isArray),
    (w.parseJSON = JSON.parse),
    (w.nodeName = A),
    (w.isFunction = g),
    (w.isWindow = m),
    (w.camelCase = Y),
    (w.type = y),
    (w.now = Date.now),
    (w.isNumeric = function(t) {
      var e = w.type(t);
      return ('number' === e || 'string' === e) && !isNaN(t - parseFloat(t));
    }),
    'function' == typeof define &&
      define.amd &&
      define('jquery', [], function() {
        return w;
      });
  var Xe = t.jQuery,
    Ge = t.$;
  return (
    (w.noConflict = function(e) {
      return t.$ === w && (t.$ = Ge), e && t.jQuery === w && (t.jQuery = Xe), w;
    }),
    e || (t.jQuery = t.$ = w),
    w
  );
}),
  (function(t, e) {
    'object' == typeof exports && 'undefined' != typeof module
      ? (module.exports = e())
      : 'function' == typeof define && define.amd
      ? define(e)
      : (t.Popper = e());
  })(this, function() {
    'use strict';
    function t(t) {
      return t && '[object Function]' === {}.toString.call(t);
    }
    function e(t, e) {
      if (1 !== t.nodeType) return [];
      var n = getComputedStyle(t, null);
      return e ? n[e] : n;
    }
    function n(t) {
      return 'HTML' === t.nodeName ? t : t.parentNode || t.host;
    }
    function i(t) {
      if (!t) return document.body;
      switch (t.nodeName) {
        case 'HTML':
        case 'BODY':
          return t.ownerDocument.body;
        case '#document':
          return t.body;
      }
      var r = e(t);
      return /(auto|scroll|overlay)/.test(r.overflow + r.overflowY + r.overflowX) ? t : i(n(t));
    }
    function r(t) {
      return 11 === t ? K : 10 === t ? Q : K || Q;
    }
    function o(t) {
      if (!t) return document.documentElement;
      for (var n = r(10) ? document.body : null, i = t.offsetParent; i === n && t.nextElementSibling; )
        i = (t = t.nextElementSibling).offsetParent;
      var a = i && i.nodeName;
      return a && 'BODY' !== a && 'HTML' !== a
        ? -1 !== ['TD', 'TABLE'].indexOf(i.nodeName) && 'static' === e(i, 'position')
          ? o(i)
          : i
        : t
        ? t.ownerDocument.documentElement
        : document.documentElement;
    }
    function a(t) {
      return null === t.parentNode ? t : a(t.parentNode);
    }
    function s(t, e) {
      if (!(t && t.nodeType && e && e.nodeType)) return document.documentElement;
      var n = t.compareDocumentPosition(e) & Node.DOCUMENT_POSITION_FOLLOWING,
        i = n ? t : e,
        r = n ? e : t,
        l = document.createRange();
      l.setStart(i, 0), l.setEnd(r, 0);
      var u = l.commonAncestorContainer;
      if ((t !== u && e !== u) || i.contains(r))
        return (function(t) {
          var e = t.nodeName;
          return 'BODY' !== e && ('HTML' === e || o(t.firstElementChild) === t);
        })(u)
          ? u
          : o(u);
      var c = a(t);
      return c.host ? s(c.host, e) : s(t, a(e).host);
    }
    function l(t) {
      var e = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : 'top',
        n = 'top' === e ? 'scrollTop' : 'scrollLeft',
        i = t.nodeName;
      if ('BODY' === i || 'HTML' === i) {
        var r = t.ownerDocument.documentElement,
          o = t.ownerDocument.scrollingElement || r;
        return o[n];
      }
      return t[n];
    }
    function u(t, e) {
      var n = 2 < arguments.length && void 0 !== arguments[2] && arguments[2],
        i = l(e, 'top'),
        r = l(e, 'left'),
        o = n ? -1 : 1;
      return (t.top += i * o), (t.bottom += i * o), (t.left += r * o), (t.right += r * o), t;
    }
    function c(t, e) {
      var n = 'x' === e ? 'Left' : 'Top',
        i = 'Left' == n ? 'Right' : 'Bottom';
      return parseFloat(t['border' + n + 'Width'], 10) + parseFloat(t['border' + i + 'Width'], 10);
    }
    function d(t, e, n, i) {
      return z(
        e['offset' + t],
        e['scroll' + t],
        n['client' + t],
        n['offset' + t],
        n['scroll' + t],
        r(10)
          ? n['offset' + t] +
              i['margin' + ('Height' === t ? 'Top' : 'Left')] +
              i['margin' + ('Height' === t ? 'Bottom' : 'Right')]
          : 0
      );
    }
    function h() {
      var t = document.body,
        e = document.documentElement,
        n = r(10) && getComputedStyle(e);
      return { height: d('Height', t, e, n), width: d('Width', t, e, n) };
    }
    function f(t) {
      return Z({}, t, { right: t.left + t.width, bottom: t.top + t.height });
    }
    function p(t) {
      var n = {};
      try {
        if (r(10)) {
          n = t.getBoundingClientRect();
          var i = l(t, 'top'),
            o = l(t, 'left');
          (n.top += i), (n.left += o), (n.bottom += i), (n.right += o);
        } else n = t.getBoundingClientRect();
      } catch (e) {}
      var a = { left: n.left, top: n.top, width: n.right - n.left, height: n.bottom - n.top },
        s = 'HTML' === t.nodeName ? h() : {},
        u = t.offsetWidth - (s.width || t.clientWidth || a.right - a.left),
        d = t.offsetHeight - (s.height || t.clientHeight || a.bottom - a.top);
      if (u || d) {
        var p = e(t);
        (u -= c(p, 'x')), (d -= c(p, 'y')), (a.width -= u), (a.height -= d);
      }
      return f(a);
    }
    function g(t, n) {
      var o = 2 < arguments.length && void 0 !== arguments[2] && arguments[2],
        a = r(10),
        s = 'HTML' === n.nodeName,
        l = p(t),
        c = p(n),
        d = i(t),
        h = e(n),
        g = parseFloat(h.borderTopWidth, 10),
        m = parseFloat(h.borderLeftWidth, 10);
      o && 'HTML' === n.nodeName && ((c.top = z(c.top, 0)), (c.left = z(c.left, 0)));
      var v = f({ top: l.top - c.top - g, left: l.left - c.left - m, width: l.width, height: l.height });
      if (((v.marginTop = 0), (v.marginLeft = 0), !a && s)) {
        var b = parseFloat(h.marginTop, 10),
          y = parseFloat(h.marginLeft, 10);
        (v.top -= g - b),
          (v.bottom -= g - b),
          (v.left -= m - y),
          (v.right -= m - y),
          (v.marginTop = b),
          (v.marginLeft = y);
      }
      return (a && !o ? n.contains(d) : n === d && 'BODY' !== d.nodeName) && (v = u(v, n)), v;
    }
    function m(t) {
      var e = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
        n = t.ownerDocument.documentElement,
        i = g(t, n),
        r = z(n.clientWidth, window.innerWidth || 0),
        o = z(n.clientHeight, window.innerHeight || 0),
        a = e ? 0 : l(n),
        s = e ? 0 : l(n, 'left'),
        u = { top: a - i.top + i.marginTop, left: s - i.left + i.marginLeft, width: r, height: o };
      return f(u);
    }
    function v(t) {
      var i = t.nodeName;
      return 'BODY' !== i && 'HTML' !== i && ('fixed' === e(t, 'position') || v(n(t)));
    }
    function b(t) {
      if (!t || !t.parentElement || r()) return document.documentElement;
      for (var n = t.parentElement; n && 'none' === e(n, 'transform'); ) n = n.parentElement;
      return n || document.documentElement;
    }
    function y(t, e, r, o) {
      var a = 4 < arguments.length && void 0 !== arguments[4] && arguments[4],
        l = { top: 0, left: 0 },
        u = a ? b(t) : s(t, e);
      if ('viewport' === o) l = m(u, a);
      else {
        var c;
        'scrollParent' === o
          ? 'BODY' === (c = i(n(e))).nodeName && (c = t.ownerDocument.documentElement)
          : (c = 'window' === o ? t.ownerDocument.documentElement : o);
        var d = g(c, u, a);
        if ('HTML' !== c.nodeName || v(u)) l = d;
        else {
          var f = h(),
            p = f.height,
            y = f.width;
          (l.top += d.top - d.marginTop),
            (l.bottom = p + d.top),
            (l.left += d.left - d.marginLeft),
            (l.right = y + d.left);
        }
      }
      return (l.left += r), (l.top += r), (l.right -= r), (l.bottom -= r), l;
    }
    function x(t) {
      return t.width * t.height;
    }
    function w(t, e, n, i, r) {
      var o = 5 < arguments.length && void 0 !== arguments[5] ? arguments[5] : 0;
      if (-1 === t.indexOf('auto')) return t;
      var a = y(n, i, o, r),
        s = {
          top: { width: a.width, height: e.top - a.top },
          right: { width: a.right - e.right, height: a.height },
          bottom: { width: a.width, height: a.bottom - e.bottom },
          left: { width: e.left - a.left, height: a.height }
        },
        l = Object.keys(s)
          .map(function(t) {
            return Z({ key: t }, s[t], { area: x(s[t]) });
          })
          .sort(function(t, e) {
            return e.area - t.area;
          }),
        u = l.filter(function(t) {
          return t.width >= n.clientWidth && t.height >= n.clientHeight;
        }),
        c = 0 < u.length ? u[0].key : l[0].key,
        d = t.split('-')[1];
      return c + (d ? '-' + d : '');
    }
    function _(t, e, n) {
      var i = 3 < arguments.length && void 0 !== arguments[3] ? arguments[3] : null,
        r = i ? b(e) : s(e, n);
      return g(n, r, i);
    }
    function C(t) {
      var e = getComputedStyle(t),
        n = parseFloat(e.marginTop) + parseFloat(e.marginBottom),
        i = parseFloat(e.marginLeft) + parseFloat(e.marginRight);
      return { width: t.offsetWidth + i, height: t.offsetHeight + n };
    }
    function k(t) {
      var e = { left: 'right', right: 'left', bottom: 'top', top: 'bottom' };
      return t.replace(/left|right|bottom|top/g, function(t) {
        return e[t];
      });
    }
    function S(t, e, n) {
      n = n.split('-')[0];
      var i = C(t),
        r = { width: i.width, height: i.height },
        o = -1 !== ['right', 'left'].indexOf(n),
        a = o ? 'top' : 'left',
        s = o ? 'left' : 'top',
        l = o ? 'height' : 'width',
        u = o ? 'width' : 'height';
      return (r[a] = e[a] + e[l] / 2 - i[l] / 2), (r[s] = n === s ? e[s] - i[u] : e[k(s)]), r;
    }
    function T(t, e) {
      return Array.prototype.find ? t.find(e) : t.filter(e)[0];
    }
    function D(e, n, i) {
      return (
        (void 0 === i
          ? e
          : e.slice(
              0,
              (function(t, e, n) {
                if (Array.prototype.findIndex)
                  return t.findIndex(function(t) {
                    return t[e] === n;
                  });
                var i = T(t, function(t) {
                  return t[e] === n;
                });
                return t.indexOf(i);
              })(e, 'name', i)
            )
        ).forEach(function(e) {
          e.function && console.warn('`modifier.function` is deprecated, use `modifier.fn`!');
          var i = e.function || e.fn;
          e.enabled &&
            t(i) &&
            ((n.offsets.popper = f(n.offsets.popper)), (n.offsets.reference = f(n.offsets.reference)), (n = i(n, e)));
        }),
        n
      );
    }
    function A() {
      if (!this.state.isDestroyed) {
        var t = { instance: this, styles: {}, arrowStyles: {}, attributes: {}, flipped: !1, offsets: {} };
        (t.offsets.reference = _(this.state, this.popper, this.reference, this.options.positionFixed)),
          (t.placement = w(
            this.options.placement,
            t.offsets.reference,
            this.popper,
            this.reference,
            this.options.modifiers.flip.boundariesElement,
            this.options.modifiers.flip.padding
          )),
          (t.originalPlacement = t.placement),
          (t.positionFixed = this.options.positionFixed),
          (t.offsets.popper = S(this.popper, t.offsets.reference, t.placement)),
          (t.offsets.popper.position = this.options.positionFixed ? 'fixed' : 'absolute'),
          (t = D(this.modifiers, t)),
          this.state.isCreated ? this.options.onUpdate(t) : ((this.state.isCreated = !0), this.options.onCreate(t));
      }
    }
    function E(t, e) {
      return t.some(function(t) {
        return t.enabled && t.name === e;
      });
    }
    function M(t) {
      for (
        var e = [!1, 'ms', 'Webkit', 'Moz', 'O'], n = t.charAt(0).toUpperCase() + t.slice(1), i = 0;
        i < e.length;
        i++
      ) {
        var r = e[i],
          o = r ? '' + r + n : t;
        if (void 0 !== document.body.style[o]) return o;
      }
      return null;
    }
    function I() {
      return (
        (this.state.isDestroyed = !0),
        E(this.modifiers, 'applyStyle') &&
          (this.popper.removeAttribute('x-placement'),
          (this.popper.style.position = ''),
          (this.popper.style.top = ''),
          (this.popper.style.left = ''),
          (this.popper.style.right = ''),
          (this.popper.style.bottom = ''),
          (this.popper.style.willChange = ''),
          (this.popper.style[M('transform')] = '')),
        this.disableEventListeners(),
        this.options.removeOnDestroy && this.popper.parentNode.removeChild(this.popper),
        this
      );
    }
    function P(t) {
      var e = t.ownerDocument;
      return e ? e.defaultView : window;
    }
    function O() {
      this.state.eventsEnabled ||
        (this.state = (function(t, e, n, r) {
          (n.updateBound = r), P(t).addEventListener('resize', n.updateBound, { passive: !0 });
          var o = i(t);
          return (
            (function t(e, n, r, o) {
              var a = 'BODY' === e.nodeName,
                s = a ? e.ownerDocument.defaultView : e;
              s.addEventListener(n, r, { passive: !0 }), a || t(i(s.parentNode), n, r, o), o.push(s);
            })(o, 'scroll', n.updateBound, n.scrollParents),
            (n.scrollElement = o),
            (n.eventsEnabled = !0),
            n
          );
        })(this.reference, 0, this.state, this.scheduleUpdate));
    }
    function N() {
      this.state.eventsEnabled &&
        (cancelAnimationFrame(this.scheduleUpdate),
        (this.state = (function(t, e) {
          return (
            P(t).removeEventListener('resize', e.updateBound),
            e.scrollParents.forEach(function(t) {
              t.removeEventListener('scroll', e.updateBound);
            }),
            (e.updateBound = null),
            (e.scrollParents = []),
            (e.scrollElement = null),
            (e.eventsEnabled = !1),
            e
          );
        })(this.reference, this.state)));
    }
    function L(t) {
      return '' !== t && !isNaN(parseFloat(t)) && isFinite(t);
    }
    function F(t, e) {
      Object.keys(e).forEach(function(n) {
        var i = '';
        -1 !== ['width', 'height', 'top', 'right', 'bottom', 'left'].indexOf(n) && L(e[n]) && (i = 'px'),
          (t.style[n] = e[n] + i);
      });
    }
    function R(t, e, n) {
      var i = T(t, function(t) {
          return t.name === e;
        }),
        r =
          !!i &&
          t.some(function(t) {
            return t.name === n && t.enabled && t.order < i.order;
          });
      if (!r) {
        var o = '`' + e + '`';
        console.warn(
          '`' +
            n +
            '` modifier is required by ' +
            o +
            ' modifier in order to work, be sure to include it before ' +
            o +
            '!'
        );
      }
      return r;
    }
    function j(t) {
      var e = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
        n = et.indexOf(t),
        i = et.slice(n + 1).concat(et.slice(0, n));
      return e ? i.reverse() : i;
    }
    for (
      var H = Math.min,
        B = Math.round,
        W = Math.floor,
        z = Math.max,
        q = 'undefined' != typeof window && 'undefined' != typeof document,
        V = ['Edge', 'Trident', 'Firefox'],
        U = 0,
        $ = 0;
      $ < V.length;
      $ += 1
    )
      if (q && 0 <= navigator.userAgent.indexOf(V[$])) {
        U = 1;
        break;
      }
    var Y =
        q && window.Promise
          ? function(t) {
              var e = !1;
              return function() {
                e ||
                  ((e = !0),
                  window.Promise.resolve().then(function() {
                    (e = !1), t();
                  }));
              };
            }
          : function(t) {
              var e = !1;
              return function() {
                e ||
                  ((e = !0),
                  setTimeout(function() {
                    (e = !1), t();
                  }, U));
              };
            },
      K = q && !(!window.MSInputMethodContext || !document.documentMode),
      Q = q && /MSIE 10/.test(navigator.userAgent),
      X = function(t, e) {
        if (!(t instanceof e)) throw new TypeError('Cannot call a class as a function');
      },
      G = (function() {
        function t(t, e) {
          for (var n, i = 0; i < e.length; i++)
            ((n = e[i]).enumerable = n.enumerable || !1),
              (n.configurable = !0),
              'value' in n && (n.writable = !0),
              Object.defineProperty(t, n.key, n);
        }
        return function(e, n, i) {
          return n && t(e.prototype, n), i && t(e, i), e;
        };
      })(),
      J = function(t, e, n) {
        return (
          e in t
            ? Object.defineProperty(t, e, { value: n, enumerable: !0, configurable: !0, writable: !0 })
            : (t[e] = n),
          t
        );
      },
      Z =
        Object.assign ||
        function(t) {
          for (var e, n = 1; n < arguments.length; n++)
            for (var i in (e = arguments[n])) Object.prototype.hasOwnProperty.call(e, i) && (t[i] = e[i]);
          return t;
        },
      tt = [
        'auto-start',
        'auto',
        'auto-end',
        'top-start',
        'top',
        'top-end',
        'right-start',
        'right',
        'right-end',
        'bottom-end',
        'bottom',
        'bottom-start',
        'left-end',
        'left',
        'left-start'
      ],
      et = tt.slice(3),
      nt = (function() {
        function e(n, i) {
          var r = this,
            o = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {};
          X(this, e),
            (this.scheduleUpdate = function() {
              return requestAnimationFrame(r.update);
            }),
            (this.update = Y(this.update.bind(this))),
            (this.options = Z({}, e.Defaults, o)),
            (this.state = { isDestroyed: !1, isCreated: !1, scrollParents: [] }),
            (this.reference = n && n.jquery ? n[0] : n),
            (this.popper = i && i.jquery ? i[0] : i),
            (this.options.modifiers = {}),
            Object.keys(Z({}, e.Defaults.modifiers, o.modifiers)).forEach(function(t) {
              r.options.modifiers[t] = Z({}, e.Defaults.modifiers[t] || {}, o.modifiers ? o.modifiers[t] : {});
            }),
            (this.modifiers = Object.keys(this.options.modifiers)
              .map(function(t) {
                return Z({ name: t }, r.options.modifiers[t]);
              })
              .sort(function(t, e) {
                return t.order - e.order;
              })),
            this.modifiers.forEach(function(e) {
              e.enabled && t(e.onLoad) && e.onLoad(r.reference, r.popper, r.options, e, r.state);
            }),
            this.update();
          var a = this.options.eventsEnabled;
          a && this.enableEventListeners(), (this.state.eventsEnabled = a);
        }
        return (
          G(e, [
            {
              key: 'update',
              value: function() {
                return A.call(this);
              }
            },
            {
              key: 'destroy',
              value: function() {
                return I.call(this);
              }
            },
            {
              key: 'enableEventListeners',
              value: function() {
                return O.call(this);
              }
            },
            {
              key: 'disableEventListeners',
              value: function() {
                return N.call(this);
              }
            }
          ]),
          e
        );
      })();
    return (
      (nt.Utils = ('undefined' == typeof window ? global : window).PopperUtils),
      (nt.placements = tt),
      (nt.Defaults = {
        placement: 'bottom',
        positionFixed: !1,
        eventsEnabled: !0,
        removeOnDestroy: !1,
        onCreate: function() {},
        onUpdate: function() {},
        modifiers: {
          shift: {
            order: 100,
            enabled: !0,
            fn: function(t) {
              var e = t.placement,
                n = e.split('-')[0],
                i = e.split('-')[1];
              if (i) {
                var r = t.offsets,
                  o = r.reference,
                  a = r.popper,
                  s = -1 !== ['bottom', 'top'].indexOf(n),
                  l = s ? 'left' : 'top',
                  u = s ? 'width' : 'height',
                  c = { start: J({}, l, o[l]), end: J({}, l, o[l] + o[u] - a[u]) };
                t.offsets.popper = Z({}, a, c[i]);
              }
              return t;
            }
          },
          offset: {
            order: 200,
            enabled: !0,
            fn: function(t, e) {
              var n,
                i = e.offset,
                r = t.offsets,
                o = r.popper,
                a = r.reference,
                s = t.placement.split('-')[0];
              return (
                (n = L(+i)
                  ? [+i, 0]
                  : (function(t, e, n, i) {
                      var r = [0, 0],
                        o = -1 !== ['right', 'left'].indexOf(i),
                        a = t.split(/(\+|\-)/).map(function(t) {
                          return t.trim();
                        }),
                        s = a.indexOf(
                          T(a, function(t) {
                            return -1 !== t.search(/,|\s/);
                          })
                        );
                      a[s] &&
                        -1 === a[s].indexOf(',') &&
                        console.warn('Offsets separated by white space(s) are deprecated, use a comma (,) instead.');
                      var l = /\s*,\s*|\s+/,
                        u =
                          -1 === s
                            ? [a]
                            : [a.slice(0, s).concat([a[s].split(l)[0]]), [a[s].split(l)[1]].concat(a.slice(s + 1))];
                      return (
                        (u = u.map(function(t, i) {
                          var r = (1 === i ? !o : o) ? 'height' : 'width',
                            a = !1;
                          return t
                            .reduce(function(t, e) {
                              return '' === t[t.length - 1] && -1 !== ['+', '-'].indexOf(e)
                                ? ((t[t.length - 1] = e), (a = !0), t)
                                : a
                                ? ((t[t.length - 1] += e), (a = !1), t)
                                : t.concat(e);
                            }, [])
                            .map(function(t) {
                              return (function(t, e, n, i) {
                                var r = t.match(/((?:\-|\+)?\d*\.?\d*)(.*)/),
                                  o = +r[1],
                                  a = r[2];
                                if (!o) return t;
                                if (0 === a.indexOf('%')) {
                                  var s;
                                  switch (a) {
                                    case '%p':
                                      s = n;
                                      break;
                                    case '%':
                                    case '%r':
                                    default:
                                      s = i;
                                  }
                                  return (f(s)[e] / 100) * o;
                                }
                                return 'vh' === a || 'vw' === a
                                  ? (('vh' === a
                                      ? z(document.documentElement.clientHeight, window.innerHeight || 0)
                                      : z(document.documentElement.clientWidth, window.innerWidth || 0)) /
                                      100) *
                                      o
                                  : o;
                              })(t, r, e, n);
                            });
                        })).forEach(function(t, e) {
                          t.forEach(function(n, i) {
                            L(n) && (r[e] += n * ('-' === t[i - 1] ? -1 : 1));
                          });
                        }),
                        r
                      );
                    })(i, o, a, s)),
                'left' === s
                  ? ((o.top += n[0]), (o.left -= n[1]))
                  : 'right' === s
                  ? ((o.top += n[0]), (o.left += n[1]))
                  : 'top' === s
                  ? ((o.left += n[0]), (o.top -= n[1]))
                  : 'bottom' === s && ((o.left += n[0]), (o.top += n[1])),
                (t.popper = o),
                t
              );
            },
            offset: 0
          },
          preventOverflow: {
            order: 300,
            enabled: !0,
            fn: function(t, e) {
              var n = e.boundariesElement || o(t.instance.popper);
              t.instance.reference === n && (n = o(n));
              var i = M('transform'),
                r = t.instance.popper.style,
                a = r.top,
                s = r.left,
                l = r[i];
              (r.top = ''), (r.left = ''), (r[i] = '');
              var u = y(t.instance.popper, t.instance.reference, e.padding, n, t.positionFixed);
              (r.top = a), (r.left = s), (r[i] = l), (e.boundaries = u);
              var c = t.offsets.popper,
                d = {
                  primary: function(t) {
                    var n = c[t];
                    return c[t] < u[t] && !e.escapeWithReference && (n = z(c[t], u[t])), J({}, t, n);
                  },
                  secondary: function(t) {
                    var n = 'right' === t ? 'left' : 'top',
                      i = c[n];
                    return (
                      c[t] > u[t] &&
                        !e.escapeWithReference &&
                        (i = H(c[n], u[t] - ('right' === t ? c.width : c.height))),
                      J({}, n, i)
                    );
                  }
                };
              return (
                e.priority.forEach(function(t) {
                  var e = -1 === ['left', 'top'].indexOf(t) ? 'secondary' : 'primary';
                  c = Z({}, c, d[e](t));
                }),
                (t.offsets.popper = c),
                t
              );
            },
            priority: ['left', 'right', 'top', 'bottom'],
            padding: 5,
            boundariesElement: 'scrollParent'
          },
          keepTogether: {
            order: 400,
            enabled: !0,
            fn: function(t) {
              var e = t.offsets,
                n = e.popper,
                i = e.reference,
                r = t.placement.split('-')[0],
                o = W,
                a = -1 !== ['top', 'bottom'].indexOf(r),
                s = a ? 'right' : 'bottom',
                l = a ? 'left' : 'top',
                u = a ? 'width' : 'height';
              return (
                n[s] < o(i[l]) && (t.offsets.popper[l] = o(i[l]) - n[u]),
                n[l] > o(i[s]) && (t.offsets.popper[l] = o(i[s])),
                t
              );
            }
          },
          arrow: {
            order: 500,
            enabled: !0,
            fn: function(t, n) {
              var i;
              if (!R(t.instance.modifiers, 'arrow', 'keepTogether')) return t;
              var r = n.element;
              if ('string' == typeof r) {
                if (!(r = t.instance.popper.querySelector(r))) return t;
              } else if (!t.instance.popper.contains(r))
                return console.warn('WARNING: `arrow.element` must be child of its popper element!'), t;
              var o = t.placement.split('-')[0],
                a = t.offsets,
                s = a.popper,
                l = a.reference,
                u = -1 !== ['left', 'right'].indexOf(o),
                c = u ? 'height' : 'width',
                d = u ? 'Top' : 'Left',
                h = d.toLowerCase(),
                p = u ? 'left' : 'top',
                g = u ? 'bottom' : 'right',
                m = C(r)[c];
              l[g] - m < s[h] && (t.offsets.popper[h] -= s[h] - (l[g] - m)),
                l[h] + m > s[g] && (t.offsets.popper[h] += l[h] + m - s[g]),
                (t.offsets.popper = f(t.offsets.popper));
              var v = l[h] + l[c] / 2 - m / 2,
                b = e(t.instance.popper),
                y = parseFloat(b['margin' + d], 10),
                x = parseFloat(b['border' + d + 'Width'], 10),
                w = v - t.offsets.popper[h] - y - x;
              return (
                (w = z(H(s[c] - m, w), 0)),
                (t.arrowElement = r),
                (t.offsets.arrow = (J((i = {}), h, B(w)), J(i, p, ''), i)),
                t
              );
            },
            element: '[x-arrow]'
          },
          flip: {
            order: 600,
            enabled: !0,
            fn: function(t, e) {
              if (E(t.instance.modifiers, 'inner')) return t;
              if (t.flipped && t.placement === t.originalPlacement) return t;
              var n = y(t.instance.popper, t.instance.reference, e.padding, e.boundariesElement, t.positionFixed),
                i = t.placement.split('-')[0],
                r = k(i),
                o = t.placement.split('-')[1] || '',
                a = [];
              switch (e.behavior) {
                case 'flip':
                  a = [i, r];
                  break;
                case 'clockwise':
                  a = j(i);
                  break;
                case 'counterclockwise':
                  a = j(i, !0);
                  break;
                default:
                  a = e.behavior;
              }
              return (
                a.forEach(function(s, l) {
                  if (i !== s || a.length === l + 1) return t;
                  (i = t.placement.split('-')[0]), (r = k(i));
                  var u = t.offsets.popper,
                    c = t.offsets.reference,
                    d = W,
                    h =
                      ('left' === i && d(u.right) > d(c.left)) ||
                      ('right' === i && d(u.left) < d(c.right)) ||
                      ('top' === i && d(u.bottom) > d(c.top)) ||
                      ('bottom' === i && d(u.top) < d(c.bottom)),
                    f = d(u.left) < d(n.left),
                    p = d(u.right) > d(n.right),
                    g = d(u.top) < d(n.top),
                    m = d(u.bottom) > d(n.bottom),
                    v = ('left' === i && f) || ('right' === i && p) || ('top' === i && g) || ('bottom' === i && m),
                    b = -1 !== ['top', 'bottom'].indexOf(i),
                    y =
                      !!e.flipVariations &&
                      ((b && 'start' === o && f) ||
                        (b && 'end' === o && p) ||
                        (!b && 'start' === o && g) ||
                        (!b && 'end' === o && m));
                  (h || v || y) &&
                    ((t.flipped = !0),
                    (h || v) && (i = a[l + 1]),
                    y &&
                      (o = (function(t) {
                        return 'end' === t ? 'start' : 'start' === t ? 'end' : t;
                      })(o)),
                    (t.placement = i + (o ? '-' + o : '')),
                    (t.offsets.popper = Z(
                      {},
                      t.offsets.popper,
                      S(t.instance.popper, t.offsets.reference, t.placement)
                    )),
                    (t = D(t.instance.modifiers, t, 'flip')));
                }),
                t
              );
            },
            behavior: 'flip',
            padding: 5,
            boundariesElement: 'viewport'
          },
          inner: {
            order: 700,
            enabled: !1,
            fn: function(t) {
              var e = t.placement,
                n = e.split('-')[0],
                i = t.offsets,
                r = i.popper,
                o = i.reference,
                a = -1 !== ['left', 'right'].indexOf(n),
                s = -1 === ['top', 'left'].indexOf(n);
              return (
                (r[a ? 'left' : 'top'] = o[n] - (s ? r[a ? 'width' : 'height'] : 0)),
                (t.placement = k(e)),
                (t.offsets.popper = f(r)),
                t
              );
            }
          },
          hide: {
            order: 800,
            enabled: !0,
            fn: function(t) {
              if (!R(t.instance.modifiers, 'hide', 'preventOverflow')) return t;
              var e = t.offsets.reference,
                n = T(t.instance.modifiers, function(t) {
                  return 'preventOverflow' === t.name;
                }).boundaries;
              if (e.bottom < n.top || e.left > n.right || e.top > n.bottom || e.right < n.left) {
                if (!0 === t.hide) return t;
                (t.hide = !0), (t.attributes['x-out-of-boundaries'] = '');
              } else {
                if (!1 === t.hide) return t;
                (t.hide = !1), (t.attributes['x-out-of-boundaries'] = !1);
              }
              return t;
            }
          },
          computeStyle: {
            order: 850,
            enabled: !0,
            fn: function(t, e) {
              var n = e.x,
                i = e.y,
                r = t.offsets.popper,
                a = T(t.instance.modifiers, function(t) {
                  return 'applyStyle' === t.name;
                }).gpuAcceleration;
              void 0 !== a &&
                console.warn(
                  'WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!'
                );
              var s,
                l,
                u = void 0 === a ? e.gpuAcceleration : a,
                c = p(o(t.instance.popper)),
                d = { position: r.position },
                h = { left: W(r.left), top: B(r.top), bottom: B(r.bottom), right: W(r.right) },
                f = 'bottom' === n ? 'top' : 'bottom',
                g = 'right' === i ? 'left' : 'right',
                m = M('transform');
              if (
                ((l = 'bottom' == f ? -c.height + h.bottom : h.top),
                (s = 'right' == g ? -c.width + h.right : h.left),
                u && m)
              )
                (d[m] = 'translate3d(' + s + 'px, ' + l + 'px, 0)'),
                  (d[f] = 0),
                  (d[g] = 0),
                  (d.willChange = 'transform');
              else {
                var v = 'right' == g ? -1 : 1;
                (d[f] = l * ('bottom' == f ? -1 : 1)), (d[g] = s * v), (d.willChange = f + ', ' + g);
              }
              return (
                (t.attributes = Z({}, { 'x-placement': t.placement }, t.attributes)),
                (t.styles = Z({}, d, t.styles)),
                (t.arrowStyles = Z({}, t.offsets.arrow, t.arrowStyles)),
                t
              );
            },
            gpuAcceleration: !0,
            x: 'bottom',
            y: 'right'
          },
          applyStyle: {
            order: 900,
            enabled: !0,
            fn: function(t) {
              return (
                F(t.instance.popper, t.styles),
                (function(t, e) {
                  Object.keys(e).forEach(function(n) {
                    !1 === e[n] ? t.removeAttribute(n) : t.setAttribute(n, e[n]);
                  });
                })(t.instance.popper, t.attributes),
                t.arrowElement && Object.keys(t.arrowStyles).length && F(t.arrowElement, t.arrowStyles),
                t
              );
            },
            onLoad: function(t, e, n, i, r) {
              var o = _(r, e, t, n.positionFixed),
                a = w(n.placement, o, e, t, n.modifiers.flip.boundariesElement, n.modifiers.flip.padding);
              return e.setAttribute('x-placement', a), F(e, { position: n.positionFixed ? 'fixed' : 'absolute' }), n;
            },
            gpuAcceleration: void 0
          }
        }
      }),
      nt
    );
  }),
  (function(t, e) {
    'object' == typeof exports && 'undefined' != typeof module
      ? e(exports, require('jquery'), require('popper.js'))
      : 'function' == typeof define && define.amd
      ? define(['exports', 'jquery', 'popper.js'], e)
      : e((t.bootstrap = {}), t.jQuery, t.Popper);
  })(this, function(t, e, n) {
    'use strict';
    function i(t, e) {
      for (var n = 0; n < e.length; n++) {
        var i = e[n];
        (i.enumerable = i.enumerable || !1),
          (i.configurable = !0),
          'value' in i && (i.writable = !0),
          Object.defineProperty(t, i.key, i);
      }
    }
    function r(t, e, n) {
      return e && i(t.prototype, e), n && i(t, n), t;
    }
    function o(t) {
      for (var e = 1; e < arguments.length; e++) {
        var n = null != arguments[e] ? arguments[e] : {},
          i = Object.keys(n);
        'function' == typeof Object.getOwnPropertySymbols &&
          (i = i.concat(
            Object.getOwnPropertySymbols(n).filter(function(t) {
              return Object.getOwnPropertyDescriptor(n, t).enumerable;
            })
          )),
          i.forEach(function(e) {
            var i, r, o;
            (o = n[(r = e)]),
              r in (i = t)
                ? Object.defineProperty(i, r, { value: o, enumerable: !0, configurable: !0, writable: !0 })
                : (i[r] = o);
          });
      }
      return t;
    }
    (e = e && e.hasOwnProperty('default') ? e.default : e), (n = n && n.hasOwnProperty('default') ? n.default : n);
    var a,
      s,
      l,
      u,
      c,
      d,
      h,
      f,
      p,
      g,
      m,
      v,
      b,
      y,
      x,
      w,
      _,
      C,
      k,
      S,
      T,
      D,
      A,
      E,
      M,
      I,
      P,
      O,
      N,
      L,
      F,
      R,
      j,
      H,
      B,
      W,
      z,
      q,
      V,
      U,
      $,
      Y,
      K,
      Q,
      X,
      G,
      J,
      Z,
      tt,
      et,
      nt,
      it,
      rt,
      ot,
      at,
      st,
      lt,
      ut,
      ct,
      dt,
      ht,
      ft,
      pt,
      gt,
      mt,
      vt,
      bt,
      yt,
      xt,
      wt,
      _t,
      Ct,
      kt,
      St,
      Tt,
      Dt,
      At,
      Et,
      Mt,
      It,
      Pt,
      Ot,
      Nt,
      Lt,
      Ft,
      Rt,
      jt,
      Ht,
      Bt,
      Wt,
      zt,
      qt,
      Vt,
      Ut,
      $t,
      Yt,
      Kt,
      Qt,
      Xt,
      Gt,
      Jt,
      Zt,
      te,
      ee,
      ne,
      ie,
      re,
      oe,
      ae,
      se,
      le,
      ue,
      ce,
      de,
      he,
      fe,
      pe,
      ge,
      me,
      ve,
      be,
      ye,
      xe,
      we,
      _e,
      Ce,
      ke = (function(t) {
        var e = 'transitionend',
          n = {
            TRANSITION_END: 'bsTransitionEnd',
            getUID: function(t) {
              for (; (t += ~~(1e6 * Math.random())), document.getElementById(t); );
              return t;
            },
            getSelectorFromElement: function(t) {
              var e = t.getAttribute('data-target');
              (e && '#' !== e) || (e = t.getAttribute('href') || '');
              try {
                return document.querySelector(e) ? e : null;
              } catch (t) {
                return null;
              }
            },
            getTransitionDurationFromElement: function(e) {
              if (!e) return 0;
              var n = t(e).css('transition-duration');
              return parseFloat(n) ? ((n = n.split(',')[0]), 1e3 * parseFloat(n)) : 0;
            },
            reflow: function(t) {
              return t.offsetHeight;
            },
            triggerTransitionEnd: function(n) {
              t(n).trigger(e);
            },
            supportsTransitionEnd: function() {
              return Boolean(e);
            },
            isElement: function(t) {
              return (t[0] || t).nodeType;
            },
            typeCheckConfig: function(t, e, i) {
              for (var r in i)
                if (Object.prototype.hasOwnProperty.call(i, r)) {
                  var o = i[r],
                    a = e[r],
                    s =
                      a && n.isElement(a)
                        ? 'element'
                        : {}.toString
                            .call(a)
                            .match(/\s([a-z]+)/i)[1]
                            .toLowerCase();
                  if (!new RegExp(o).test(s))
                    throw new Error(
                      t.toUpperCase() + ': Option "' + r + '" provided type "' + s + '" but expected type "' + o + '".'
                    );
                }
            }
          };
        return (
          (t.fn.emulateTransitionEnd = function(e) {
            var i = this,
              r = !1;
            return (
              t(this).one(n.TRANSITION_END, function() {
                r = !0;
              }),
              setTimeout(function() {
                r || n.triggerTransitionEnd(i);
              }, e),
              this
            );
          }),
          (t.event.special[n.TRANSITION_END] = {
            bindType: e,
            delegateType: e,
            handle: function(e) {
              if (t(e.target).is(this)) return e.handleObj.handler.apply(this, arguments);
            }
          }),
          n
        );
      })(e),
      Se =
        ((u = '.' + (l = 'bs.alert')),
        (c = (a = e).fn[(s = 'alert')]),
        (d = { CLOSE: 'close' + u, CLOSED: 'closed' + u, CLICK_DATA_API: 'click' + u + '.data-api' }),
        (h = (function() {
          function t(t) {
            this._element = t;
          }
          var e = t.prototype;
          return (
            (e.close = function(t) {
              var e = this._element;
              t && (e = this._getRootElement(t)),
                this._triggerCloseEvent(e).isDefaultPrevented() || this._removeElement(e);
            }),
            (e.dispose = function() {
              a.removeData(this._element, l), (this._element = null);
            }),
            (e._getRootElement = function(t) {
              var e = ke.getSelectorFromElement(t),
                n = !1;
              return e && (n = document.querySelector(e)), n || (n = a(t).closest('.alert')[0]), n;
            }),
            (e._triggerCloseEvent = function(t) {
              var e = a.Event(d.CLOSE);
              return a(t).trigger(e), e;
            }),
            (e._removeElement = function(t) {
              var e = this;
              if ((a(t).removeClass('show'), a(t).hasClass('fade'))) {
                var n = ke.getTransitionDurationFromElement(t);
                a(t)
                  .one(ke.TRANSITION_END, function(n) {
                    return e._destroyElement(t, n);
                  })
                  .emulateTransitionEnd(n);
              } else this._destroyElement(t);
            }),
            (e._destroyElement = function(t) {
              a(t)
                .detach()
                .trigger(d.CLOSED)
                .remove();
            }),
            (t._jQueryInterface = function(e) {
              return this.each(function() {
                var n = a(this),
                  i = n.data(l);
                i || ((i = new t(this)), n.data(l, i)), 'close' === e && i[e](this);
              });
            }),
            (t._handleDismiss = function(t) {
              return function(e) {
                e && e.preventDefault(), t.close(this);
              };
            }),
            r(t, null, [
              {
                key: 'VERSION',
                get: function() {
                  return '4.1.3';
                }
              }
            ]),
            t
          );
        })()),
        a(document).on(d.CLICK_DATA_API, '[data-dismiss="alert"]', h._handleDismiss(new h())),
        (a.fn[s] = h._jQueryInterface),
        (a.fn[s].Constructor = h),
        (a.fn[s].noConflict = function() {
          return (a.fn[s] = c), h._jQueryInterface;
        }),
        h),
      Te =
        ((m = '.' + (g = 'bs.button')),
        (b = (f = e).fn[(p = 'button')]),
        (y = 'active'),
        (x = '[data-toggle^="button"]'),
        (w = '.btn'),
        (_ = {
          CLICK_DATA_API: 'click' + m + (v = '.data-api'),
          FOCUS_BLUR_DATA_API: 'focus' + m + v + ' blur' + m + v
        }),
        (C = (function() {
          function t(t) {
            this._element = t;
          }
          var e = t.prototype;
          return (
            (e.toggle = function() {
              var t = !0,
                e = !0,
                n = f(this._element).closest('[data-toggle="buttons"]')[0];
              if (n) {
                var i = this._element.querySelector('input');
                if (i) {
                  if ('radio' === i.type)
                    if (i.checked && this._element.classList.contains(y)) t = !1;
                    else {
                      var r = n.querySelector('.active');
                      r && f(r).removeClass(y);
                    }
                  if (t) {
                    if (
                      i.hasAttribute('disabled') ||
                      n.hasAttribute('disabled') ||
                      i.classList.contains('disabled') ||
                      n.classList.contains('disabled')
                    )
                      return;
                    (i.checked = !this._element.classList.contains(y)), f(i).trigger('change');
                  }
                  i.focus(), (e = !1);
                }
              }
              e && this._element.setAttribute('aria-pressed', !this._element.classList.contains(y)),
                t && f(this._element).toggleClass(y);
            }),
            (e.dispose = function() {
              f.removeData(this._element, g), (this._element = null);
            }),
            (t._jQueryInterface = function(e) {
              return this.each(function() {
                var n = f(this).data(g);
                n || ((n = new t(this)), f(this).data(g, n)), 'toggle' === e && n[e]();
              });
            }),
            r(t, null, [
              {
                key: 'VERSION',
                get: function() {
                  return '4.1.3';
                }
              }
            ]),
            t
          );
        })()),
        f(document)
          .on(_.CLICK_DATA_API, x, function(t) {
            t.preventDefault();
            var e = t.target;
            f(e).hasClass('btn') || (e = f(e).closest(w)), C._jQueryInterface.call(f(e), 'toggle');
          })
          .on(_.FOCUS_BLUR_DATA_API, x, function(t) {
            var e = f(t.target).closest(w)[0];
            f(e).toggleClass('focus', /^focus(in)?$/.test(t.type));
          }),
        (f.fn[p] = C._jQueryInterface),
        (f.fn[p].Constructor = C),
        (f.fn[p].noConflict = function() {
          return (f.fn[p] = b), C._jQueryInterface;
        }),
        C),
      De =
        ((D = '.' + (T = 'bs.carousel')),
        (A = (k = e).fn[(S = 'carousel')]),
        (E = { interval: 5e3, keyboard: !0, slide: !1, pause: 'hover', wrap: !0 }),
        (M = {
          interval: '(number|boolean)',
          keyboard: 'boolean',
          slide: '(boolean|string)',
          pause: '(string|boolean)',
          wrap: 'boolean'
        }),
        (I = 'next'),
        (P = 'prev'),
        (O = {
          SLIDE: 'slide' + D,
          SLID: 'slid' + D,
          KEYDOWN: 'keydown' + D,
          MOUSEENTER: 'mouseenter' + D,
          MOUSELEAVE: 'mouseleave' + D,
          TOUCHEND: 'touchend' + D,
          LOAD_DATA_API: 'load' + D + '.data-api',
          CLICK_DATA_API: 'click' + D + '.data-api'
        }),
        (N = 'active'),
        (L = '.active.carousel-item'),
        (F = (function() {
          function t(t, e) {
            (this._items = null),
              (this._interval = null),
              (this._activeElement = null),
              (this._isPaused = !1),
              (this._isSliding = !1),
              (this.touchTimeout = null),
              (this._config = this._getConfig(e)),
              (this._element = k(t)[0]),
              (this._indicatorsElement = this._element.querySelector('.carousel-indicators')),
              this._addEventListeners();
          }
          var e = t.prototype;
          return (
            (e.next = function() {
              this._isSliding || this._slide(I);
            }),
            (e.nextWhenVisible = function() {
              !document.hidden &&
                k(this._element).is(':visible') &&
                'hidden' !== k(this._element).css('visibility') &&
                this.next();
            }),
            (e.prev = function() {
              this._isSliding || this._slide(P);
            }),
            (e.pause = function(t) {
              t || (this._isPaused = !0),
                this._element.querySelector('.carousel-item-next, .carousel-item-prev') &&
                  (ke.triggerTransitionEnd(this._element), this.cycle(!0)),
                clearInterval(this._interval),
                (this._interval = null);
            }),
            (e.cycle = function(t) {
              t || (this._isPaused = !1),
                this._interval && (clearInterval(this._interval), (this._interval = null)),
                this._config.interval &&
                  !this._isPaused &&
                  (this._interval = setInterval(
                    (document.visibilityState ? this.nextWhenVisible : this.next).bind(this),
                    this._config.interval
                  ));
            }),
            (e.to = function(t) {
              var e = this;
              this._activeElement = this._element.querySelector(L);
              var n = this._getItemIndex(this._activeElement);
              if (!(t > this._items.length - 1 || t < 0))
                if (this._isSliding)
                  k(this._element).one(O.SLID, function() {
                    return e.to(t);
                  });
                else {
                  if (n === t) return this.pause(), void this.cycle();
                  this._slide(n < t ? I : P, this._items[t]);
                }
            }),
            (e.dispose = function() {
              k(this._element).off(D),
                k.removeData(this._element, T),
                (this._items = null),
                (this._config = null),
                (this._element = null),
                (this._interval = null),
                (this._isPaused = null),
                (this._isSliding = null),
                (this._activeElement = null),
                (this._indicatorsElement = null);
            }),
            (e._getConfig = function(t) {
              return (t = o({}, E, t)), ke.typeCheckConfig(S, t, M), t;
            }),
            (e._addEventListeners = function() {
              var t = this;
              this._config.keyboard &&
                k(this._element).on(O.KEYDOWN, function(e) {
                  return t._keydown(e);
                }),
                'hover' === this._config.pause &&
                  (k(this._element)
                    .on(O.MOUSEENTER, function(e) {
                      return t.pause(e);
                    })
                    .on(O.MOUSELEAVE, function(e) {
                      return t.cycle(e);
                    }),
                  'ontouchstart' in document.documentElement &&
                    k(this._element).on(O.TOUCHEND, function() {
                      t.pause(),
                        t.touchTimeout && clearTimeout(t.touchTimeout),
                        (t.touchTimeout = setTimeout(function(e) {
                          return t.cycle(e);
                        }, 500 + t._config.interval));
                    }));
            }),
            (e._keydown = function(t) {
              if (!/input|textarea/i.test(t.target.tagName))
                switch (t.which) {
                  case 37:
                    t.preventDefault(), this.prev();
                    break;
                  case 39:
                    t.preventDefault(), this.next();
                }
            }),
            (e._getItemIndex = function(t) {
              return (
                (this._items = t && t.parentNode ? [].slice.call(t.parentNode.querySelectorAll('.carousel-item')) : []),
                this._items.indexOf(t)
              );
            }),
            (e._getItemByDirection = function(t, e) {
              var n = t === I,
                i = t === P,
                r = this._getItemIndex(e);
              if (((i && 0 === r) || (n && r === this._items.length - 1)) && !this._config.wrap) return e;
              var o = (r + (t === P ? -1 : 1)) % this._items.length;
              return -1 === o ? this._items[this._items.length - 1] : this._items[o];
            }),
            (e._triggerSlideEvent = function(t, e) {
              var n = this._getItemIndex(t),
                i = this._getItemIndex(this._element.querySelector(L)),
                r = k.Event(O.SLIDE, { relatedTarget: t, direction: e, from: i, to: n });
              return k(this._element).trigger(r), r;
            }),
            (e._setActiveIndicatorElement = function(t) {
              if (this._indicatorsElement) {
                var e = [].slice.call(this._indicatorsElement.querySelectorAll('.active'));
                k(e).removeClass(N);
                var n = this._indicatorsElement.children[this._getItemIndex(t)];
                n && k(n).addClass(N);
              }
            }),
            (e._slide = function(t, e) {
              var n,
                i,
                r,
                o = this,
                a = this._element.querySelector(L),
                s = this._getItemIndex(a),
                l = e || (a && this._getItemByDirection(t, a)),
                u = this._getItemIndex(l),
                c = Boolean(this._interval);
              if (
                (t === I
                  ? ((n = 'carousel-item-left'), (i = 'carousel-item-next'), (r = 'left'))
                  : ((n = 'carousel-item-right'), (i = 'carousel-item-prev'), (r = 'right')),
                l && k(l).hasClass(N))
              )
                this._isSliding = !1;
              else if (!this._triggerSlideEvent(l, r).isDefaultPrevented() && a && l) {
                (this._isSliding = !0), c && this.pause(), this._setActiveIndicatorElement(l);
                var d = k.Event(O.SLID, { relatedTarget: l, direction: r, from: s, to: u });
                if (k(this._element).hasClass('slide')) {
                  k(l).addClass(i), ke.reflow(l), k(a).addClass(n), k(l).addClass(n);
                  var h = ke.getTransitionDurationFromElement(a);
                  k(a)
                    .one(ke.TRANSITION_END, function() {
                      k(l)
                        .removeClass(n + ' ' + i)
                        .addClass(N),
                        k(a).removeClass(N + ' ' + i + ' ' + n),
                        (o._isSliding = !1),
                        setTimeout(function() {
                          return k(o._element).trigger(d);
                        }, 0);
                    })
                    .emulateTransitionEnd(h);
                } else k(a).removeClass(N), k(l).addClass(N), (this._isSliding = !1), k(this._element).trigger(d);
                c && this.cycle();
              }
            }),
            (t._jQueryInterface = function(e) {
              return this.each(function() {
                var n = k(this).data(T),
                  i = o({}, E, k(this).data());
                'object' == typeof e && (i = o({}, i, e));
                var r = 'string' == typeof e ? e : i.slide;
                if ((n || ((n = new t(this, i)), k(this).data(T, n)), 'number' == typeof e)) n.to(e);
                else if ('string' == typeof r) {
                  if (void 0 === n[r]) throw new TypeError('No method named "' + r + '"');
                  n[r]();
                } else i.interval && (n.pause(), n.cycle());
              });
            }),
            (t._dataApiClickHandler = function(e) {
              var n = ke.getSelectorFromElement(this);
              if (n) {
                var i = k(n)[0];
                if (i && k(i).hasClass('carousel')) {
                  var r = o({}, k(i).data(), k(this).data()),
                    a = this.getAttribute('data-slide-to');
                  a && (r.interval = !1),
                    t._jQueryInterface.call(k(i), r),
                    a &&
                      k(i)
                        .data(T)
                        .to(a),
                    e.preventDefault();
                }
              }
            }),
            r(t, null, [
              {
                key: 'VERSION',
                get: function() {
                  return '4.1.3';
                }
              },
              {
                key: 'Default',
                get: function() {
                  return E;
                }
              }
            ]),
            t
          );
        })()),
        k(document).on(O.CLICK_DATA_API, '[data-slide], [data-slide-to]', F._dataApiClickHandler),
        k(window).on(O.LOAD_DATA_API, function() {
          for (
            var t = [].slice.call(document.querySelectorAll('[data-ride="carousel"]')), e = 0, n = t.length;
            e < n;
            e++
          ) {
            var i = k(t[e]);
            F._jQueryInterface.call(i, i.data());
          }
        }),
        (k.fn[S] = F._jQueryInterface),
        (k.fn[S].Constructor = F),
        (k.fn[S].noConflict = function() {
          return (k.fn[S] = A), F._jQueryInterface;
        }),
        F),
      Ae =
        ((B = '.' + (H = 'bs.collapse')),
        (W = (R = e).fn[(j = 'collapse')]),
        (z = { toggle: !0, parent: '' }),
        (q = { toggle: 'boolean', parent: '(string|element)' }),
        (V = {
          SHOW: 'show' + B,
          SHOWN: 'shown' + B,
          HIDE: 'hide' + B,
          HIDDEN: 'hidden' + B,
          CLICK_DATA_API: 'click' + B + '.data-api'
        }),
        (U = 'show'),
        ($ = 'collapse'),
        (Y = 'collapsing'),
        (K = 'collapsed'),
        (Q = '[data-toggle="collapse"]'),
        (X = (function() {
          function t(t, e) {
            (this._isTransitioning = !1),
              (this._element = t),
              (this._config = this._getConfig(e)),
              (this._triggerArray = R.makeArray(
                document.querySelectorAll(
                  '[data-toggle="collapse"][href="#' + t.id + '"],[data-toggle="collapse"][data-target="#' + t.id + '"]'
                )
              ));
            for (var n = [].slice.call(document.querySelectorAll(Q)), i = 0, r = n.length; i < r; i++) {
              var o = n[i],
                a = ke.getSelectorFromElement(o),
                s = [].slice.call(document.querySelectorAll(a)).filter(function(e) {
                  return e === t;
                });
              null !== a && 0 < s.length && ((this._selector = a), this._triggerArray.push(o));
            }
            (this._parent = this._config.parent ? this._getParent() : null),
              this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray),
              this._config.toggle && this.toggle();
          }
          var e = t.prototype;
          return (
            (e.toggle = function() {
              R(this._element).hasClass(U) ? this.hide() : this.show();
            }),
            (e.show = function() {
              var e,
                n,
                i = this;
              if (
                !(
                  this._isTransitioning ||
                  R(this._element).hasClass(U) ||
                  (this._parent &&
                    0 ===
                      (e = [].slice.call(this._parent.querySelectorAll('.show, .collapsing')).filter(function(t) {
                        return t.getAttribute('data-parent') === i._config.parent;
                      })).length &&
                    (e = null),
                  e &&
                    (n = R(e)
                      .not(this._selector)
                      .data(H)) &&
                    n._isTransitioning)
                )
              ) {
                var r = R.Event(V.SHOW);
                if ((R(this._element).trigger(r), !r.isDefaultPrevented())) {
                  e && (t._jQueryInterface.call(R(e).not(this._selector), 'hide'), n || R(e).data(H, null));
                  var o = this._getDimension();
                  R(this._element)
                    .removeClass($)
                    .addClass(Y),
                    (this._element.style[o] = 0),
                    this._triggerArray.length &&
                      R(this._triggerArray)
                        .removeClass(K)
                        .attr('aria-expanded', !0),
                    this.setTransitioning(!0);
                  var a = 'scroll' + (o[0].toUpperCase() + o.slice(1)),
                    s = ke.getTransitionDurationFromElement(this._element);
                  R(this._element)
                    .one(ke.TRANSITION_END, function() {
                      R(i._element)
                        .removeClass(Y)
                        .addClass($)
                        .addClass(U),
                        (i._element.style[o] = ''),
                        i.setTransitioning(!1),
                        R(i._element).trigger(V.SHOWN);
                    })
                    .emulateTransitionEnd(s),
                    (this._element.style[o] = this._element[a] + 'px');
                }
              }
            }),
            (e.hide = function() {
              var t = this;
              if (!this._isTransitioning && R(this._element).hasClass(U)) {
                var e = R.Event(V.HIDE);
                if ((R(this._element).trigger(e), !e.isDefaultPrevented())) {
                  var n = this._getDimension();
                  (this._element.style[n] = this._element.getBoundingClientRect()[n] + 'px'),
                    ke.reflow(this._element),
                    R(this._element)
                      .addClass(Y)
                      .removeClass($)
                      .removeClass(U);
                  var i = this._triggerArray.length;
                  if (0 < i)
                    for (var r = 0; r < i; r++) {
                      var o = this._triggerArray[r],
                        a = ke.getSelectorFromElement(o);
                      null !== a &&
                        (R([].slice.call(document.querySelectorAll(a))).hasClass(U) ||
                          R(o)
                            .addClass(K)
                            .attr('aria-expanded', !1));
                    }
                  this.setTransitioning(!0), (this._element.style[n] = '');
                  var s = ke.getTransitionDurationFromElement(this._element);
                  R(this._element)
                    .one(ke.TRANSITION_END, function() {
                      t.setTransitioning(!1),
                        R(t._element)
                          .removeClass(Y)
                          .addClass($)
                          .trigger(V.HIDDEN);
                    })
                    .emulateTransitionEnd(s);
                }
              }
            }),
            (e.setTransitioning = function(t) {
              this._isTransitioning = t;
            }),
            (e.dispose = function() {
              R.removeData(this._element, H),
                (this._config = null),
                (this._parent = null),
                (this._element = null),
                (this._triggerArray = null),
                (this._isTransitioning = null);
            }),
            (e._getConfig = function(t) {
              return ((t = o({}, z, t)).toggle = Boolean(t.toggle)), ke.typeCheckConfig(j, t, q), t;
            }),
            (e._getDimension = function() {
              return R(this._element).hasClass('width') ? 'width' : 'height';
            }),
            (e._getParent = function() {
              var e = this,
                n = null;
              ke.isElement(this._config.parent)
                ? ((n = this._config.parent), void 0 !== this._config.parent.jquery && (n = this._config.parent[0]))
                : (n = document.querySelector(this._config.parent));
              var i = [].slice.call(
                n.querySelectorAll('[data-toggle="collapse"][data-parent="' + this._config.parent + '"]')
              );
              return (
                R(i).each(function(n, i) {
                  e._addAriaAndCollapsedClass(t._getTargetFromElement(i), [i]);
                }),
                n
              );
            }),
            (e._addAriaAndCollapsedClass = function(t, e) {
              if (t) {
                var n = R(t).hasClass(U);
                e.length &&
                  R(e)
                    .toggleClass(K, !n)
                    .attr('aria-expanded', n);
              }
            }),
            (t._getTargetFromElement = function(t) {
              var e = ke.getSelectorFromElement(t);
              return e ? document.querySelector(e) : null;
            }),
            (t._jQueryInterface = function(e) {
              return this.each(function() {
                var n = R(this),
                  i = n.data(H),
                  r = o({}, z, n.data(), 'object' == typeof e && e ? e : {});
                if (
                  (!i && r.toggle && /show|hide/.test(e) && (r.toggle = !1),
                  i || ((i = new t(this, r)), n.data(H, i)),
                  'string' == typeof e)
                ) {
                  if (void 0 === i[e]) throw new TypeError('No method named "' + e + '"');
                  i[e]();
                }
              });
            }),
            r(t, null, [
              {
                key: 'VERSION',
                get: function() {
                  return '4.1.3';
                }
              },
              {
                key: 'Default',
                get: function() {
                  return z;
                }
              }
            ]),
            t
          );
        })()),
        R(document).on(V.CLICK_DATA_API, Q, function(t) {
          'A' === t.currentTarget.tagName && t.preventDefault();
          var e = R(this),
            n = ke.getSelectorFromElement(this),
            i = [].slice.call(document.querySelectorAll(n));
          R(i).each(function() {
            var t = R(this),
              n = t.data(H) ? 'toggle' : e.data();
            X._jQueryInterface.call(t, n);
          });
        }),
        (R.fn[j] = X._jQueryInterface),
        (R.fn[j].Constructor = X),
        (R.fn[j].noConflict = function() {
          return (R.fn[j] = W), X._jQueryInterface;
        }),
        X),
      Ee =
        ((tt = '.' + (Z = 'bs.dropdown')),
        (et = '.data-api'),
        (nt = (G = e).fn[(J = 'dropdown')]),
        (it = new RegExp('38|40|27')),
        (rt = {
          HIDE: 'hide' + tt,
          HIDDEN: 'hidden' + tt,
          SHOW: 'show' + tt,
          SHOWN: 'shown' + tt,
          CLICK: 'click' + tt,
          CLICK_DATA_API: 'click' + tt + et,
          KEYDOWN_DATA_API: 'keydown' + tt + et,
          KEYUP_DATA_API: 'keyup' + tt + et
        }),
        (ot = 'disabled'),
        (at = 'show'),
        (st = 'dropdown-menu-right'),
        (lt = '[data-toggle="dropdown"]'),
        (ut = '.dropdown-menu'),
        (ct = { offset: 0, flip: !0, boundary: 'scrollParent', reference: 'toggle', display: 'dynamic' }),
        (dt = {
          offset: '(number|string|function)',
          flip: 'boolean',
          boundary: '(string|element)',
          reference: '(string|element)',
          display: 'string'
        }),
        (ht = (function() {
          function t(t, e) {
            (this._element = t),
              (this._popper = null),
              (this._config = this._getConfig(e)),
              (this._menu = this._getMenuElement()),
              (this._inNavbar = this._detectNavbar()),
              this._addEventListeners();
          }
          var e = t.prototype;
          return (
            (e.toggle = function() {
              if (!this._element.disabled && !G(this._element).hasClass(ot)) {
                var e = t._getParentFromElement(this._element),
                  i = G(this._menu).hasClass(at);
                if ((t._clearMenus(), !i)) {
                  var r = { relatedTarget: this._element },
                    o = G.Event(rt.SHOW, r);
                  if ((G(e).trigger(o), !o.isDefaultPrevented())) {
                    if (!this._inNavbar) {
                      if (void 0 === n)
                        throw new TypeError('Bootstrap dropdown require Popper.js (https://popper.js.org)');
                      var a = this._element;
                      'parent' === this._config.reference
                        ? (a = e)
                        : ke.isElement(this._config.reference) &&
                          ((a = this._config.reference),
                          void 0 !== this._config.reference.jquery && (a = this._config.reference[0])),
                        'scrollParent' !== this._config.boundary && G(e).addClass('position-static'),
                        (this._popper = new n(a, this._menu, this._getPopperConfig()));
                    }
                    'ontouchstart' in document.documentElement &&
                      0 === G(e).closest('.navbar-nav').length &&
                      G(document.body)
                        .children()
                        .on('mouseover', null, G.noop),
                      this._element.focus(),
                      this._element.setAttribute('aria-expanded', !0),
                      G(this._menu).toggleClass(at),
                      G(e)
                        .toggleClass(at)
                        .trigger(G.Event(rt.SHOWN, r));
                  }
                }
              }
            }),
            (e.dispose = function() {
              G.removeData(this._element, Z),
                G(this._element).off(tt),
                (this._element = null),
                (this._menu = null) !== this._popper && (this._popper.destroy(), (this._popper = null));
            }),
            (e.update = function() {
              (this._inNavbar = this._detectNavbar()), null !== this._popper && this._popper.scheduleUpdate();
            }),
            (e._addEventListeners = function() {
              var t = this;
              G(this._element).on(rt.CLICK, function(e) {
                e.preventDefault(), e.stopPropagation(), t.toggle();
              });
            }),
            (e._getConfig = function(t) {
              return (
                (t = o({}, this.constructor.Default, G(this._element).data(), t)),
                ke.typeCheckConfig(J, t, this.constructor.DefaultType),
                t
              );
            }),
            (e._getMenuElement = function() {
              if (!this._menu) {
                var e = t._getParentFromElement(this._element);
                e && (this._menu = e.querySelector(ut));
              }
              return this._menu;
            }),
            (e._getPlacement = function() {
              var t = G(this._element.parentNode),
                e = 'bottom-start';
              return (
                t.hasClass('dropup')
                  ? ((e = 'top-start'), G(this._menu).hasClass(st) && (e = 'top-end'))
                  : t.hasClass('dropright')
                  ? (e = 'right-start')
                  : t.hasClass('dropleft')
                  ? (e = 'left-start')
                  : G(this._menu).hasClass(st) && (e = 'bottom-end'),
                e
              );
            }),
            (e._detectNavbar = function() {
              return 0 < G(this._element).closest('.navbar').length;
            }),
            (e._getPopperConfig = function() {
              var t = this,
                e = {};
              'function' == typeof this._config.offset
                ? (e.fn = function(e) {
                    return (e.offsets = o({}, e.offsets, t._config.offset(e.offsets) || {})), e;
                  })
                : (e.offset = this._config.offset);
              var n = {
                placement: this._getPlacement(),
                modifiers: {
                  offset: e,
                  flip: { enabled: this._config.flip },
                  preventOverflow: { boundariesElement: this._config.boundary }
                }
              };
              return 'static' === this._config.display && (n.modifiers.applyStyle = { enabled: !1 }), n;
            }),
            (t._jQueryInterface = function(e) {
              return this.each(function() {
                var n = G(this).data(Z);
                if (
                  (n || ((n = new t(this, 'object' == typeof e ? e : null)), G(this).data(Z, n)), 'string' == typeof e)
                ) {
                  if (void 0 === n[e]) throw new TypeError('No method named "' + e + '"');
                  n[e]();
                }
              });
            }),
            (t._clearMenus = function(e) {
              if (!e || (3 !== e.which && ('keyup' !== e.type || 9 === e.which)))
                for (var n = [].slice.call(document.querySelectorAll(lt)), i = 0, r = n.length; i < r; i++) {
                  var o = t._getParentFromElement(n[i]),
                    a = G(n[i]).data(Z),
                    s = { relatedTarget: n[i] };
                  if ((e && 'click' === e.type && (s.clickEvent = e), a)) {
                    var l = a._menu;
                    if (
                      G(o).hasClass(at) &&
                      !(
                        e &&
                        (('click' === e.type && /input|textarea/i.test(e.target.tagName)) ||
                          ('keyup' === e.type && 9 === e.which)) &&
                        G.contains(o, e.target)
                      )
                    ) {
                      var u = G.Event(rt.HIDE, s);
                      G(o).trigger(u),
                        u.isDefaultPrevented() ||
                          ('ontouchstart' in document.documentElement &&
                            G(document.body)
                              .children()
                              .off('mouseover', null, G.noop),
                          n[i].setAttribute('aria-expanded', 'false'),
                          G(l).removeClass(at),
                          G(o)
                            .removeClass(at)
                            .trigger(G.Event(rt.HIDDEN, s)));
                    }
                  }
                }
            }),
            (t._getParentFromElement = function(t) {
              var e,
                n = ke.getSelectorFromElement(t);
              return n && (e = document.querySelector(n)), e || t.parentNode;
            }),
            (t._dataApiKeydownHandler = function(e) {
              if (
                (/input|textarea/i.test(e.target.tagName)
                  ? !(
                      32 === e.which ||
                      (27 !== e.which && ((40 !== e.which && 38 !== e.which) || G(e.target).closest(ut).length))
                    )
                  : it.test(e.which)) &&
                (e.preventDefault(), e.stopPropagation(), !this.disabled && !G(this).hasClass(ot))
              ) {
                var n = t._getParentFromElement(this),
                  i = G(n).hasClass(at);
                if ((i || (27 === e.which && 32 === e.which)) && (!i || (27 !== e.which && 32 !== e.which))) {
                  var r = [].slice.call(
                    n.querySelectorAll('.dropdown-menu .dropdown-item:not(.disabled):not(:disabled)')
                  );
                  if (0 !== r.length) {
                    var o = r.indexOf(e.target);
                    38 === e.which && 0 < o && o--,
                      40 === e.which && o < r.length - 1 && o++,
                      o < 0 && (o = 0),
                      r[o].focus();
                  }
                } else {
                  if (27 === e.which) {
                    var a = n.querySelector(lt);
                    G(a).trigger('focus');
                  }
                  G(this).trigger('click');
                }
              }
            }),
            r(t, null, [
              {
                key: 'VERSION',
                get: function() {
                  return '4.1.3';
                }
              },
              {
                key: 'Default',
                get: function() {
                  return ct;
                }
              },
              {
                key: 'DefaultType',
                get: function() {
                  return dt;
                }
              }
            ]),
            t
          );
        })()),
        G(document)
          .on(rt.KEYDOWN_DATA_API, lt, ht._dataApiKeydownHandler)
          .on(rt.KEYDOWN_DATA_API, ut, ht._dataApiKeydownHandler)
          .on(rt.CLICK_DATA_API + ' ' + rt.KEYUP_DATA_API, ht._clearMenus)
          .on(rt.CLICK_DATA_API, lt, function(t) {
            t.preventDefault(), t.stopPropagation(), ht._jQueryInterface.call(G(this), 'toggle');
          })
          .on(rt.CLICK_DATA_API, '.dropdown form', function(t) {
            t.stopPropagation();
          }),
        (G.fn[J] = ht._jQueryInterface),
        (G.fn[J].Constructor = ht),
        (G.fn[J].noConflict = function() {
          return (G.fn[J] = nt), ht._jQueryInterface;
        }),
        ht),
      Me =
        ((mt = '.' + (gt = 'bs.modal')),
        (vt = (ft = e).fn[(pt = 'modal')]),
        (bt = { backdrop: !0, keyboard: !0, focus: !0, show: !0 }),
        (yt = { backdrop: '(boolean|string)', keyboard: 'boolean', focus: 'boolean', show: 'boolean' }),
        (xt = {
          HIDE: 'hide' + mt,
          HIDDEN: 'hidden' + mt,
          SHOW: 'show' + mt,
          SHOWN: 'shown' + mt,
          FOCUSIN: 'focusin' + mt,
          RESIZE: 'resize' + mt,
          CLICK_DISMISS: 'click.dismiss' + mt,
          KEYDOWN_DISMISS: 'keydown.dismiss' + mt,
          MOUSEUP_DISMISS: 'mouseup.dismiss' + mt,
          MOUSEDOWN_DISMISS: 'mousedown.dismiss' + mt,
          CLICK_DATA_API: 'click' + mt + '.data-api'
        }),
        (wt = 'modal-open'),
        (_t = 'fade'),
        (Ct = 'show'),
        (kt = '.fixed-top, .fixed-bottom, .is-fixed, .sticky-top'),
        (St = '.sticky-top'),
        (Tt = (function() {
          function t(t, e) {
            (this._config = this._getConfig(e)),
              (this._element = t),
              (this._dialog = t.querySelector('.modal-dialog')),
              (this._backdrop = null),
              (this._isShown = !1),
              (this._isBodyOverflowing = !1),
              (this._ignoreBackdropClick = !1),
              (this._scrollbarWidth = 0);
          }
          var e = t.prototype;
          return (
            (e.toggle = function(t) {
              return this._isShown ? this.hide() : this.show(t);
            }),
            (e.show = function(t) {
              var e = this;
              if (!this._isTransitioning && !this._isShown) {
                ft(this._element).hasClass(_t) && (this._isTransitioning = !0);
                var n = ft.Event(xt.SHOW, { relatedTarget: t });
                ft(this._element).trigger(n),
                  this._isShown ||
                    n.isDefaultPrevented() ||
                    ((this._isShown = !0),
                    this._checkScrollbar(),
                    this._setScrollbar(),
                    this._adjustDialog(),
                    ft(document.body).addClass(wt),
                    this._setEscapeEvent(),
                    this._setResizeEvent(),
                    ft(this._element).on(xt.CLICK_DISMISS, '[data-dismiss="modal"]', function(t) {
                      return e.hide(t);
                    }),
                    ft(this._dialog).on(xt.MOUSEDOWN_DISMISS, function() {
                      ft(e._element).one(xt.MOUSEUP_DISMISS, function(t) {
                        ft(t.target).is(e._element) && (e._ignoreBackdropClick = !0);
                      });
                    }),
                    this._showBackdrop(function() {
                      return e._showElement(t);
                    }));
              }
            }),
            (e.hide = function(t) {
              var e = this;
              if ((t && t.preventDefault(), !this._isTransitioning && this._isShown)) {
                var n = ft.Event(xt.HIDE);
                if ((ft(this._element).trigger(n), this._isShown && !n.isDefaultPrevented())) {
                  this._isShown = !1;
                  var i = ft(this._element).hasClass(_t);
                  if (
                    (i && (this._isTransitioning = !0),
                    this._setEscapeEvent(),
                    this._setResizeEvent(),
                    ft(document).off(xt.FOCUSIN),
                    ft(this._element).removeClass(Ct),
                    ft(this._element).off(xt.CLICK_DISMISS),
                    ft(this._dialog).off(xt.MOUSEDOWN_DISMISS),
                    i)
                  ) {
                    var r = ke.getTransitionDurationFromElement(this._element);
                    ft(this._element)
                      .one(ke.TRANSITION_END, function(t) {
                        return e._hideModal(t);
                      })
                      .emulateTransitionEnd(r);
                  } else this._hideModal();
                }
              }
            }),
            (e.dispose = function() {
              ft.removeData(this._element, gt),
                ft(window, document, this._element, this._backdrop).off(mt),
                (this._config = null),
                (this._element = null),
                (this._dialog = null),
                (this._backdrop = null),
                (this._isShown = null),
                (this._isBodyOverflowing = null),
                (this._ignoreBackdropClick = null),
                (this._scrollbarWidth = null);
            }),
            (e.handleUpdate = function() {
              this._adjustDialog();
            }),
            (e._getConfig = function(t) {
              return (t = o({}, bt, t)), ke.typeCheckConfig(pt, t, yt), t;
            }),
            (e._showElement = function(t) {
              var e = this,
                n = ft(this._element).hasClass(_t);
              (this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE) ||
                document.body.appendChild(this._element),
                (this._element.style.display = 'block'),
                this._element.removeAttribute('aria-hidden'),
                (this._element.scrollTop = 0),
                n && ke.reflow(this._element),
                ft(this._element).addClass(Ct),
                this._config.focus && this._enforceFocus();
              var i = ft.Event(xt.SHOWN, { relatedTarget: t }),
                r = function() {
                  e._config.focus && e._element.focus(), (e._isTransitioning = !1), ft(e._element).trigger(i);
                };
              if (n) {
                var o = ke.getTransitionDurationFromElement(this._element);
                ft(this._dialog)
                  .one(ke.TRANSITION_END, r)
                  .emulateTransitionEnd(o);
              } else r();
            }),
            (e._enforceFocus = function() {
              var t = this;
              ft(document)
                .off(xt.FOCUSIN)
                .on(xt.FOCUSIN, function(e) {
                  document !== e.target &&
                    t._element !== e.target &&
                    0 === ft(t._element).has(e.target).length &&
                    t._element.focus();
                });
            }),
            (e._setEscapeEvent = function() {
              var t = this;
              this._isShown && this._config.keyboard
                ? ft(this._element).on(xt.KEYDOWN_DISMISS, function(e) {
                    27 === e.which && (e.preventDefault(), t.hide());
                  })
                : this._isShown || ft(this._element).off(xt.KEYDOWN_DISMISS);
            }),
            (e._setResizeEvent = function() {
              var t = this;
              this._isShown
                ? ft(window).on(xt.RESIZE, function(e) {
                    return t.handleUpdate(e);
                  })
                : ft(window).off(xt.RESIZE);
            }),
            (e._hideModal = function() {
              var t = this;
              (this._element.style.display = 'none'),
                this._element.setAttribute('aria-hidden', !0),
                (this._isTransitioning = !1),
                this._showBackdrop(function() {
                  ft(document.body).removeClass(wt),
                    t._resetAdjustments(),
                    t._resetScrollbar(),
                    ft(t._element).trigger(xt.HIDDEN);
                });
            }),
            (e._removeBackdrop = function() {
              this._backdrop && (ft(this._backdrop).remove(), (this._backdrop = null));
            }),
            (e._showBackdrop = function(t) {
              var e = this,
                n = ft(this._element).hasClass(_t) ? _t : '';
              if (this._isShown && this._config.backdrop) {
                if (
                  ((this._backdrop = document.createElement('div')),
                  (this._backdrop.className = 'modal-backdrop'),
                  n && this._backdrop.classList.add(n),
                  ft(this._backdrop).appendTo(document.body),
                  ft(this._element).on(xt.CLICK_DISMISS, function(t) {
                    e._ignoreBackdropClick
                      ? (e._ignoreBackdropClick = !1)
                      : t.target === t.currentTarget &&
                        ('static' === e._config.backdrop ? e._element.focus() : e.hide());
                  }),
                  n && ke.reflow(this._backdrop),
                  ft(this._backdrop).addClass(Ct),
                  !t)
                )
                  return;
                if (!n) return void t();
                var i = ke.getTransitionDurationFromElement(this._backdrop);
                ft(this._backdrop)
                  .one(ke.TRANSITION_END, t)
                  .emulateTransitionEnd(i);
              } else if (!this._isShown && this._backdrop) {
                ft(this._backdrop).removeClass(Ct);
                var r = function() {
                  e._removeBackdrop(), t && t();
                };
                if (ft(this._element).hasClass(_t)) {
                  var o = ke.getTransitionDurationFromElement(this._backdrop);
                  ft(this._backdrop)
                    .one(ke.TRANSITION_END, r)
                    .emulateTransitionEnd(o);
                } else r();
              } else t && t();
            }),
            (e._adjustDialog = function() {
              var t = this._element.scrollHeight > document.documentElement.clientHeight;
              !this._isBodyOverflowing && t && (this._element.style.paddingLeft = this._scrollbarWidth + 'px'),
                this._isBodyOverflowing && !t && (this._element.style.paddingRight = this._scrollbarWidth + 'px');
            }),
            (e._resetAdjustments = function() {
              (this._element.style.paddingLeft = ''), (this._element.style.paddingRight = '');
            }),
            (e._checkScrollbar = function() {
              var t = document.body.getBoundingClientRect();
              (this._isBodyOverflowing = t.left + t.right < window.innerWidth),
                (this._scrollbarWidth = this._getScrollbarWidth());
            }),
            (e._setScrollbar = function() {
              var t = this;
              if (this._isBodyOverflowing) {
                var e = [].slice.call(document.querySelectorAll(kt)),
                  n = [].slice.call(document.querySelectorAll(St));
                ft(e).each(function(e, n) {
                  var i = n.style.paddingRight,
                    r = ft(n).css('padding-right');
                  ft(n)
                    .data('padding-right', i)
                    .css('padding-right', parseFloat(r) + t._scrollbarWidth + 'px');
                }),
                  ft(n).each(function(e, n) {
                    var i = n.style.marginRight,
                      r = ft(n).css('margin-right');
                    ft(n)
                      .data('margin-right', i)
                      .css('margin-right', parseFloat(r) - t._scrollbarWidth + 'px');
                  });
                var i = document.body.style.paddingRight,
                  r = ft(document.body).css('padding-right');
                ft(document.body)
                  .data('padding-right', i)
                  .css('padding-right', parseFloat(r) + this._scrollbarWidth + 'px');
              }
            }),
            (e._resetScrollbar = function() {
              var t = [].slice.call(document.querySelectorAll(kt));
              ft(t).each(function(t, e) {
                var n = ft(e).data('padding-right');
                ft(e).removeData('padding-right'), (e.style.paddingRight = n || '');
              });
              var e = [].slice.call(document.querySelectorAll('' + St));
              ft(e).each(function(t, e) {
                var n = ft(e).data('margin-right');
                void 0 !== n &&
                  ft(e)
                    .css('margin-right', n)
                    .removeData('margin-right');
              });
              var n = ft(document.body).data('padding-right');
              ft(document.body).removeData('padding-right'), (document.body.style.paddingRight = n || '');
            }),
            (e._getScrollbarWidth = function() {
              var t = document.createElement('div');
              (t.className = 'modal-scrollbar-measure'), document.body.appendChild(t);
              var e = t.getBoundingClientRect().width - t.clientWidth;
              return document.body.removeChild(t), e;
            }),
            (t._jQueryInterface = function(e, n) {
              return this.each(function() {
                var i = ft(this).data(gt),
                  r = o({}, bt, ft(this).data(), 'object' == typeof e && e ? e : {});
                if ((i || ((i = new t(this, r)), ft(this).data(gt, i)), 'string' == typeof e)) {
                  if (void 0 === i[e]) throw new TypeError('No method named "' + e + '"');
                  i[e](n);
                } else r.show && i.show(n);
              });
            }),
            r(t, null, [
              {
                key: 'VERSION',
                get: function() {
                  return '4.1.3';
                }
              },
              {
                key: 'Default',
                get: function() {
                  return bt;
                }
              }
            ]),
            t
          );
        })()),
        ft(document).on(xt.CLICK_DATA_API, '[data-toggle="modal"]', function(t) {
          var e,
            n = this,
            i = ke.getSelectorFromElement(this);
          i && (e = document.querySelector(i));
          var r = ft(e).data(gt) ? 'toggle' : o({}, ft(e).data(), ft(this).data());
          ('A' !== this.tagName && 'AREA' !== this.tagName) || t.preventDefault();
          var a = ft(e).one(xt.SHOW, function(t) {
            t.isDefaultPrevented() ||
              a.one(xt.HIDDEN, function() {
                ft(n).is(':visible') && n.focus();
              });
          });
          Tt._jQueryInterface.call(ft(e), r, this);
        }),
        (ft.fn[pt] = Tt._jQueryInterface),
        (ft.fn[pt].Constructor = Tt),
        (ft.fn[pt].noConflict = function() {
          return (ft.fn[pt] = vt), Tt._jQueryInterface;
        }),
        Tt),
      Ie =
        ((Mt = '.' + (Et = 'bs.tooltip')),
        (It = (Dt = e).fn[(At = 'tooltip')]),
        (Pt = 'bs-tooltip'),
        (Ot = new RegExp('(^|\\s)' + Pt + '\\S+', 'g')),
        (Ft = {
          animation: !0,
          template:
            '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
          trigger: 'hover focus',
          title: '',
          delay: 0,
          html: !(Lt = { AUTO: 'auto', TOP: 'top', RIGHT: 'right', BOTTOM: 'bottom', LEFT: 'left' }),
          selector: !(Nt = {
            animation: 'boolean',
            template: 'string',
            title: '(string|element|function)',
            trigger: 'string',
            delay: '(number|object)',
            html: 'boolean',
            selector: '(string|boolean)',
            placement: '(string|function)',
            offset: '(number|string)',
            container: '(string|element|boolean)',
            fallbackPlacement: '(string|array)',
            boundary: '(string|element)'
          }),
          placement: 'top',
          offset: 0,
          container: !1,
          fallbackPlacement: 'flip',
          boundary: 'scrollParent'
        }),
        (jt = {
          HIDE: 'hide' + Mt,
          HIDDEN: 'hidden' + Mt,
          SHOW: (Rt = 'show') + Mt,
          SHOWN: 'shown' + Mt,
          INSERTED: 'inserted' + Mt,
          CLICK: 'click' + Mt,
          FOCUSIN: 'focusin' + Mt,
          FOCUSOUT: 'focusout' + Mt,
          MOUSEENTER: 'mouseenter' + Mt,
          MOUSELEAVE: 'mouseleave' + Mt
        }),
        (Ht = 'fade'),
        (Bt = 'show'),
        (Wt = 'hover'),
        (zt = 'focus'),
        (qt = (function() {
          function t(t, e) {
            if (void 0 === n) throw new TypeError('Bootstrap tooltips require Popper.js (https://popper.js.org)');
            (this._isEnabled = !0),
              (this._timeout = 0),
              (this._hoverState = ''),
              (this._activeTrigger = {}),
              (this._popper = null),
              (this.element = t),
              (this.config = this._getConfig(e)),
              (this.tip = null),
              this._setListeners();
          }
          var e = t.prototype;
          return (
            (e.enable = function() {
              this._isEnabled = !0;
            }),
            (e.disable = function() {
              this._isEnabled = !1;
            }),
            (e.toggleEnabled = function() {
              this._isEnabled = !this._isEnabled;
            }),
            (e.toggle = function(t) {
              if (this._isEnabled)
                if (t) {
                  var e = this.constructor.DATA_KEY,
                    n = Dt(t.currentTarget).data(e);
                  n ||
                    ((n = new this.constructor(t.currentTarget, this._getDelegateConfig())),
                    Dt(t.currentTarget).data(e, n)),
                    (n._activeTrigger.click = !n._activeTrigger.click),
                    n._isWithActiveTrigger() ? n._enter(null, n) : n._leave(null, n);
                } else {
                  if (Dt(this.getTipElement()).hasClass(Bt)) return void this._leave(null, this);
                  this._enter(null, this);
                }
            }),
            (e.dispose = function() {
              clearTimeout(this._timeout),
                Dt.removeData(this.element, this.constructor.DATA_KEY),
                Dt(this.element).off(this.constructor.EVENT_KEY),
                Dt(this.element)
                  .closest('.modal')
                  .off('hide.bs.modal'),
                this.tip && Dt(this.tip).remove(),
                (this._isEnabled = null),
                (this._timeout = null),
                (this._hoverState = null),
                (this._activeTrigger = null) !== this._popper && this._popper.destroy(),
                (this._popper = null),
                (this.element = null),
                (this.config = null),
                (this.tip = null);
            }),
            (e.show = function() {
              var t = this;
              if ('none' === Dt(this.element).css('display')) throw new Error('Please use show on visible elements');
              var e = Dt.Event(this.constructor.Event.SHOW);
              if (this.isWithContent() && this._isEnabled) {
                Dt(this.element).trigger(e);
                var i = Dt.contains(this.element.ownerDocument.documentElement, this.element);
                if (e.isDefaultPrevented() || !i) return;
                var r = this.getTipElement(),
                  o = ke.getUID(this.constructor.NAME);
                r.setAttribute('id', o),
                  this.element.setAttribute('aria-describedby', o),
                  this.setContent(),
                  this.config.animation && Dt(r).addClass(Ht);
                var a =
                    'function' == typeof this.config.placement
                      ? this.config.placement.call(this, r, this.element)
                      : this.config.placement,
                  s = this._getAttachment(a);
                this.addAttachmentClass(s);
                var l = !1 === this.config.container ? document.body : Dt(document).find(this.config.container);
                Dt(r).data(this.constructor.DATA_KEY, this),
                  Dt.contains(this.element.ownerDocument.documentElement, this.tip) || Dt(r).appendTo(l),
                  Dt(this.element).trigger(this.constructor.Event.INSERTED),
                  (this._popper = new n(this.element, r, {
                    placement: s,
                    modifiers: {
                      offset: { offset: this.config.offset },
                      flip: { behavior: this.config.fallbackPlacement },
                      arrow: { element: '.arrow' },
                      preventOverflow: { boundariesElement: this.config.boundary }
                    },
                    onCreate: function(e) {
                      e.originalPlacement !== e.placement && t._handlePopperPlacementChange(e);
                    },
                    onUpdate: function(e) {
                      t._handlePopperPlacementChange(e);
                    }
                  })),
                  Dt(r).addClass(Bt),
                  'ontouchstart' in document.documentElement &&
                    Dt(document.body)
                      .children()
                      .on('mouseover', null, Dt.noop);
                var u = function() {
                  t.config.animation && t._fixTransition();
                  var e = t._hoverState;
                  (t._hoverState = null),
                    Dt(t.element).trigger(t.constructor.Event.SHOWN),
                    'out' === e && t._leave(null, t);
                };
                if (Dt(this.tip).hasClass(Ht)) {
                  var c = ke.getTransitionDurationFromElement(this.tip);
                  Dt(this.tip)
                    .one(ke.TRANSITION_END, u)
                    .emulateTransitionEnd(c);
                } else u();
              }
            }),
            (e.hide = function(t) {
              var e = this,
                n = this.getTipElement(),
                i = Dt.Event(this.constructor.Event.HIDE),
                r = function() {
                  e._hoverState !== Rt && n.parentNode && n.parentNode.removeChild(n),
                    e._cleanTipClass(),
                    e.element.removeAttribute('aria-describedby'),
                    Dt(e.element).trigger(e.constructor.Event.HIDDEN),
                    null !== e._popper && e._popper.destroy(),
                    t && t();
                };
              if ((Dt(this.element).trigger(i), !i.isDefaultPrevented())) {
                if (
                  (Dt(n).removeClass(Bt),
                  'ontouchstart' in document.documentElement &&
                    Dt(document.body)
                      .children()
                      .off('mouseover', null, Dt.noop),
                  (this._activeTrigger.click = !1),
                  (this._activeTrigger[zt] = !1),
                  (this._activeTrigger[Wt] = !1),
                  Dt(this.tip).hasClass(Ht))
                ) {
                  var o = ke.getTransitionDurationFromElement(n);
                  Dt(n)
                    .one(ke.TRANSITION_END, r)
                    .emulateTransitionEnd(o);
                } else r();
                this._hoverState = '';
              }
            }),
            (e.update = function() {
              null !== this._popper && this._popper.scheduleUpdate();
            }),
            (e.isWithContent = function() {
              return Boolean(this.getTitle());
            }),
            (e.addAttachmentClass = function(t) {
              Dt(this.getTipElement()).addClass(Pt + '-' + t);
            }),
            (e.getTipElement = function() {
              return (this.tip = this.tip || Dt(this.config.template)[0]), this.tip;
            }),
            (e.setContent = function() {
              var t = this.getTipElement();
              this.setElementContent(Dt(t.querySelectorAll('.tooltip-inner')), this.getTitle()),
                Dt(t).removeClass(Ht + ' ' + Bt);
            }),
            (e.setElementContent = function(t, e) {
              var n = this.config.html;
              'object' == typeof e && (e.nodeType || e.jquery)
                ? n
                  ? Dt(e)
                      .parent()
                      .is(t) || t.empty().append(e)
                  : t.text(Dt(e).text())
                : t[n ? 'html' : 'text'](e);
            }),
            (e.getTitle = function() {
              var t = this.element.getAttribute('data-original-title');
              return (
                t ||
                  (t =
                    'function' == typeof this.config.title ? this.config.title.call(this.element) : this.config.title),
                t
              );
            }),
            (e._getAttachment = function(t) {
              return Lt[t.toUpperCase()];
            }),
            (e._setListeners = function() {
              var t = this;
              this.config.trigger.split(' ').forEach(function(e) {
                if ('click' === e)
                  Dt(t.element).on(t.constructor.Event.CLICK, t.config.selector, function(e) {
                    return t.toggle(e);
                  });
                else if ('manual' !== e) {
                  var n = e === Wt ? t.constructor.Event.MOUSEENTER : t.constructor.Event.FOCUSIN,
                    i = e === Wt ? t.constructor.Event.MOUSELEAVE : t.constructor.Event.FOCUSOUT;
                  Dt(t.element)
                    .on(n, t.config.selector, function(e) {
                      return t._enter(e);
                    })
                    .on(i, t.config.selector, function(e) {
                      return t._leave(e);
                    });
                }
                Dt(t.element)
                  .closest('.modal')
                  .on('hide.bs.modal', function() {
                    return t.hide();
                  });
              }),
                this.config.selector
                  ? (this.config = o({}, this.config, { trigger: 'manual', selector: '' }))
                  : this._fixTitle();
            }),
            (e._fixTitle = function() {
              var t = typeof this.element.getAttribute('data-original-title');
              (this.element.getAttribute('title') || 'string' !== t) &&
                (this.element.setAttribute('data-original-title', this.element.getAttribute('title') || ''),
                this.element.setAttribute('title', ''));
            }),
            (e._enter = function(t, e) {
              var n = this.constructor.DATA_KEY;
              (e = e || Dt(t.currentTarget).data(n)) ||
                ((e = new this.constructor(t.currentTarget, this._getDelegateConfig())),
                Dt(t.currentTarget).data(n, e)),
                t && (e._activeTrigger['focusin' === t.type ? zt : Wt] = !0),
                Dt(e.getTipElement()).hasClass(Bt) || e._hoverState === Rt
                  ? (e._hoverState = Rt)
                  : (clearTimeout(e._timeout),
                    (e._hoverState = Rt),
                    e.config.delay && e.config.delay.show
                      ? (e._timeout = setTimeout(function() {
                          e._hoverState === Rt && e.show();
                        }, e.config.delay.show))
                      : e.show());
            }),
            (e._leave = function(t, e) {
              var n = this.constructor.DATA_KEY;
              (e = e || Dt(t.currentTarget).data(n)) ||
                ((e = new this.constructor(t.currentTarget, this._getDelegateConfig())),
                Dt(t.currentTarget).data(n, e)),
                t && (e._activeTrigger['focusout' === t.type ? zt : Wt] = !1),
                e._isWithActiveTrigger() ||
                  (clearTimeout(e._timeout),
                  (e._hoverState = 'out'),
                  e.config.delay && e.config.delay.hide
                    ? (e._timeout = setTimeout(function() {
                        'out' === e._hoverState && e.hide();
                      }, e.config.delay.hide))
                    : e.hide());
            }),
            (e._isWithActiveTrigger = function() {
              for (var t in this._activeTrigger) if (this._activeTrigger[t]) return !0;
              return !1;
            }),
            (e._getConfig = function(t) {
              return (
                'number' ==
                  typeof (t = o(
                    {},
                    this.constructor.Default,
                    Dt(this.element).data(),
                    'object' == typeof t && t ? t : {}
                  )).delay && (t.delay = { show: t.delay, hide: t.delay }),
                'number' == typeof t.title && (t.title = t.title.toString()),
                'number' == typeof t.content && (t.content = t.content.toString()),
                ke.typeCheckConfig(At, t, this.constructor.DefaultType),
                t
              );
            }),
            (e._getDelegateConfig = function() {
              var t = {};
              if (this.config)
                for (var e in this.config) this.constructor.Default[e] !== this.config[e] && (t[e] = this.config[e]);
              return t;
            }),
            (e._cleanTipClass = function() {
              var t = Dt(this.getTipElement()),
                e = t.attr('class').match(Ot);
              null !== e && e.length && t.removeClass(e.join(''));
            }),
            (e._handlePopperPlacementChange = function(t) {
              (this.tip = t.instance.popper),
                this._cleanTipClass(),
                this.addAttachmentClass(this._getAttachment(t.placement));
            }),
            (e._fixTransition = function() {
              var t = this.getTipElement(),
                e = this.config.animation;
              null === t.getAttribute('x-placement') &&
                (Dt(t).removeClass(Ht),
                (this.config.animation = !1),
                this.hide(),
                this.show(),
                (this.config.animation = e));
            }),
            (t._jQueryInterface = function(e) {
              return this.each(function() {
                var n = Dt(this).data(Et),
                  i = 'object' == typeof e && e;
                if (
                  (n || !/dispose|hide/.test(e)) &&
                  (n || ((n = new t(this, i)), Dt(this).data(Et, n)), 'string' == typeof e)
                ) {
                  if (void 0 === n[e]) throw new TypeError('No method named "' + e + '"');
                  n[e]();
                }
              });
            }),
            r(t, null, [
              {
                key: 'VERSION',
                get: function() {
                  return '4.1.3';
                }
              },
              {
                key: 'Default',
                get: function() {
                  return Ft;
                }
              },
              {
                key: 'NAME',
                get: function() {
                  return At;
                }
              },
              {
                key: 'DATA_KEY',
                get: function() {
                  return Et;
                }
              },
              {
                key: 'Event',
                get: function() {
                  return jt;
                }
              },
              {
                key: 'EVENT_KEY',
                get: function() {
                  return Mt;
                }
              },
              {
                key: 'DefaultType',
                get: function() {
                  return Nt;
                }
              }
            ]),
            t
          );
        })()),
        (Dt.fn[At] = qt._jQueryInterface),
        (Dt.fn[At].Constructor = qt),
        (Dt.fn[At].noConflict = function() {
          return (Dt.fn[At] = It), qt._jQueryInterface;
        }),
        qt),
      Pe =
        ((Yt = '.' + ($t = 'bs.popover')),
        (Kt = (Vt = e).fn[(Ut = 'popover')]),
        (Qt = 'bs-popover'),
        (Xt = new RegExp('(^|\\s)' + Qt + '\\S+', 'g')),
        (Gt = o({}, Ie.Default, {
          placement: 'right',
          trigger: 'click',
          content: '',
          template:
            '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
        })),
        (Jt = o({}, Ie.DefaultType, { content: '(string|element|function)' })),
        (Zt = {
          HIDE: 'hide' + Yt,
          HIDDEN: 'hidden' + Yt,
          SHOW: 'show' + Yt,
          SHOWN: 'shown' + Yt,
          INSERTED: 'inserted' + Yt,
          CLICK: 'click' + Yt,
          FOCUSIN: 'focusin' + Yt,
          FOCUSOUT: 'focusout' + Yt,
          MOUSEENTER: 'mouseenter' + Yt,
          MOUSELEAVE: 'mouseleave' + Yt
        }),
        (te = (function(t) {
          var e, n;
          function i() {
            return t.apply(this, arguments) || this;
          }
          (n = t), ((e = i).prototype = Object.create(n.prototype)), ((e.prototype.constructor = e).__proto__ = n);
          var o = i.prototype;
          return (
            (o.isWithContent = function() {
              return this.getTitle() || this._getContent();
            }),
            (o.addAttachmentClass = function(t) {
              Vt(this.getTipElement()).addClass(Qt + '-' + t);
            }),
            (o.getTipElement = function() {
              return (this.tip = this.tip || Vt(this.config.template)[0]), this.tip;
            }),
            (o.setContent = function() {
              var t = Vt(this.getTipElement());
              this.setElementContent(t.find('.popover-header'), this.getTitle());
              var e = this._getContent();
              'function' == typeof e && (e = e.call(this.element)),
                this.setElementContent(t.find('.popover-body'), e),
                t.removeClass('fade show');
            }),
            (o._getContent = function() {
              return this.element.getAttribute('data-content') || this.config.content;
            }),
            (o._cleanTipClass = function() {
              var t = Vt(this.getTipElement()),
                e = t.attr('class').match(Xt);
              null !== e && 0 < e.length && t.removeClass(e.join(''));
            }),
            (i._jQueryInterface = function(t) {
              return this.each(function() {
                var e = Vt(this).data($t),
                  n = 'object' == typeof t ? t : null;
                if (
                  (e || !/destroy|hide/.test(t)) &&
                  (e || ((e = new i(this, n)), Vt(this).data($t, e)), 'string' == typeof t)
                ) {
                  if (void 0 === e[t]) throw new TypeError('No method named "' + t + '"');
                  e[t]();
                }
              });
            }),
            r(i, null, [
              {
                key: 'VERSION',
                get: function() {
                  return '4.1.3';
                }
              },
              {
                key: 'Default',
                get: function() {
                  return Gt;
                }
              },
              {
                key: 'NAME',
                get: function() {
                  return Ut;
                }
              },
              {
                key: 'DATA_KEY',
                get: function() {
                  return $t;
                }
              },
              {
                key: 'Event',
                get: function() {
                  return Zt;
                }
              },
              {
                key: 'EVENT_KEY',
                get: function() {
                  return Yt;
                }
              },
              {
                key: 'DefaultType',
                get: function() {
                  return Jt;
                }
              }
            ]),
            i
          );
        })(Ie)),
        (Vt.fn[Ut] = te._jQueryInterface),
        (Vt.fn[Ut].Constructor = te),
        (Vt.fn[Ut].noConflict = function() {
          return (Vt.fn[Ut] = Kt), te._jQueryInterface;
        }),
        te),
      Oe =
        ((re = '.' + (ie = 'bs.scrollspy')),
        (oe = (ee = e).fn[(ne = 'scrollspy')]),
        (ae = { offset: 10, method: 'auto', target: '' }),
        (se = { offset: 'number', method: 'string', target: '(string|element)' }),
        (le = { ACTIVATE: 'activate' + re, SCROLL: 'scroll' + re, LOAD_DATA_API: 'load' + re + '.data-api' }),
        (ue = 'active'),
        (ce = '.nav, .list-group'),
        (de = '.nav-link'),
        (he = '.list-group-item'),
        (fe = 'position'),
        (pe = (function() {
          function t(t, e) {
            var n = this;
            (this._element = t),
              (this._scrollElement = 'BODY' === t.tagName ? window : t),
              (this._config = this._getConfig(e)),
              (this._selector =
                this._config.target +
                ' ' +
                de +
                ',' +
                this._config.target +
                ' ' +
                he +
                ',' +
                this._config.target +
                ' .dropdown-item'),
              (this._offsets = []),
              (this._targets = []),
              (this._activeTarget = null),
              (this._scrollHeight = 0),
              ee(this._scrollElement).on(le.SCROLL, function(t) {
                return n._process(t);
              }),
              this.refresh(),
              this._process();
          }
          var e = t.prototype;
          return (
            (e.refresh = function() {
              var t = this,
                e =
                  'auto' === this._config.method
                    ? this._scrollElement === this._scrollElement.window
                      ? 'offset'
                      : fe
                    : this._config.method,
                n = e === fe ? this._getScrollTop() : 0;
              (this._offsets = []),
                (this._targets = []),
                (this._scrollHeight = this._getScrollHeight()),
                [].slice
                  .call(document.querySelectorAll(this._selector))
                  .map(function(t) {
                    var i,
                      r = ke.getSelectorFromElement(t);
                    if ((r && (i = document.querySelector(r)), i)) {
                      var o = i.getBoundingClientRect();
                      if (o.width || o.height) return [ee(i)[e]().top + n, r];
                    }
                    return null;
                  })
                  .filter(function(t) {
                    return t;
                  })
                  .sort(function(t, e) {
                    return t[0] - e[0];
                  })
                  .forEach(function(e) {
                    t._offsets.push(e[0]), t._targets.push(e[1]);
                  });
            }),
            (e.dispose = function() {
              ee.removeData(this._element, ie),
                ee(this._scrollElement).off(re),
                (this._element = null),
                (this._scrollElement = null),
                (this._config = null),
                (this._selector = null),
                (this._offsets = null),
                (this._targets = null),
                (this._activeTarget = null),
                (this._scrollHeight = null);
            }),
            (e._getConfig = function(t) {
              if ('string' != typeof (t = o({}, ae, 'object' == typeof t && t ? t : {})).target) {
                var e = ee(t.target).attr('id');
                e || ((e = ke.getUID(ne)), ee(t.target).attr('id', e)), (t.target = '#' + e);
              }
              return ke.typeCheckConfig(ne, t, se), t;
            }),
            (e._getScrollTop = function() {
              return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop;
            }),
            (e._getScrollHeight = function() {
              return (
                this._scrollElement.scrollHeight ||
                Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
              );
            }),
            (e._getOffsetHeight = function() {
              return this._scrollElement === window
                ? window.innerHeight
                : this._scrollElement.getBoundingClientRect().height;
            }),
            (e._process = function() {
              var t = this._getScrollTop() + this._config.offset,
                e = this._getScrollHeight(),
                n = this._config.offset + e - this._getOffsetHeight();
              if ((this._scrollHeight !== e && this.refresh(), n <= t)) {
                var i = this._targets[this._targets.length - 1];
                this._activeTarget !== i && this._activate(i);
              } else {
                if (this._activeTarget && t < this._offsets[0] && 0 < this._offsets[0])
                  return (this._activeTarget = null), void this._clear();
                for (var r = this._offsets.length; r--; )
                  this._activeTarget !== this._targets[r] &&
                    t >= this._offsets[r] &&
                    (void 0 === this._offsets[r + 1] || t < this._offsets[r + 1]) &&
                    this._activate(this._targets[r]);
              }
            }),
            (e._activate = function(t) {
              (this._activeTarget = t), this._clear();
              var e = this._selector.split(',');
              e = e.map(function(e) {
                return e + '[data-target="' + t + '"],' + e + '[href="' + t + '"]';
              });
              var n = ee([].slice.call(document.querySelectorAll(e.join(','))));
              n.hasClass('dropdown-item')
                ? (n
                    .closest('.dropdown')
                    .find('.dropdown-toggle')
                    .addClass(ue),
                  n.addClass(ue))
                : (n.addClass(ue),
                  n
                    .parents(ce)
                    .prev(de + ', ' + he)
                    .addClass(ue),
                  n
                    .parents(ce)
                    .prev('.nav-item')
                    .children(de)
                    .addClass(ue)),
                ee(this._scrollElement).trigger(le.ACTIVATE, { relatedTarget: t });
            }),
            (e._clear = function() {
              var t = [].slice.call(document.querySelectorAll(this._selector));
              ee(t)
                .filter('.active')
                .removeClass(ue);
            }),
            (t._jQueryInterface = function(e) {
              return this.each(function() {
                var n = ee(this).data(ie);
                if ((n || ((n = new t(this, 'object' == typeof e && e)), ee(this).data(ie, n)), 'string' == typeof e)) {
                  if (void 0 === n[e]) throw new TypeError('No method named "' + e + '"');
                  n[e]();
                }
              });
            }),
            r(t, null, [
              {
                key: 'VERSION',
                get: function() {
                  return '4.1.3';
                }
              },
              {
                key: 'Default',
                get: function() {
                  return ae;
                }
              }
            ]),
            t
          );
        })()),
        ee(window).on(le.LOAD_DATA_API, function() {
          for (var t = [].slice.call(document.querySelectorAll('[data-spy="scroll"]')), e = t.length; e--; ) {
            var n = ee(t[e]);
            pe._jQueryInterface.call(n, n.data());
          }
        }),
        (ee.fn[ne] = pe._jQueryInterface),
        (ee.fn[ne].Constructor = pe),
        (ee.fn[ne].noConflict = function() {
          return (ee.fn[ne] = oe), pe._jQueryInterface;
        }),
        pe),
      Ne =
        ((ve = '.' + (me = 'bs.tab')),
        (be = (ge = e).fn.tab),
        (ye = {
          HIDE: 'hide' + ve,
          HIDDEN: 'hidden' + ve,
          SHOW: 'show' + ve,
          SHOWN: 'shown' + ve,
          CLICK_DATA_API: 'click' + ve + '.data-api'
        }),
        (xe = 'active'),
        (we = '.active'),
        (_e = '> li > .active'),
        (Ce = (function() {
          function t(t) {
            this._element = t;
          }
          var e = t.prototype;
          return (
            (e.show = function() {
              var t = this;
              if (
                !(
                  (this._element.parentNode &&
                    this._element.parentNode.nodeType === Node.ELEMENT_NODE &&
                    ge(this._element).hasClass(xe)) ||
                  ge(this._element).hasClass('disabled')
                )
              ) {
                var e,
                  n,
                  i = ge(this._element).closest('.nav, .list-group')[0],
                  r = ke.getSelectorFromElement(this._element);
                if (i) {
                  var o = 'UL' === i.nodeName ? _e : we;
                  n = (n = ge.makeArray(ge(i).find(o)))[n.length - 1];
                }
                var a = ge.Event(ye.HIDE, { relatedTarget: this._element }),
                  s = ge.Event(ye.SHOW, { relatedTarget: n });
                if (
                  (n && ge(n).trigger(a),
                  ge(this._element).trigger(s),
                  !s.isDefaultPrevented() && !a.isDefaultPrevented())
                ) {
                  r && (e = document.querySelector(r)), this._activate(this._element, i);
                  var l = function() {
                    var e = ge.Event(ye.HIDDEN, { relatedTarget: t._element }),
                      i = ge.Event(ye.SHOWN, { relatedTarget: n });
                    ge(n).trigger(e), ge(t._element).trigger(i);
                  };
                  e ? this._activate(e, e.parentNode, l) : l();
                }
              }
            }),
            (e.dispose = function() {
              ge.removeData(this._element, me), (this._element = null);
            }),
            (e._activate = function(t, e, n) {
              var i = this,
                r = ('UL' === e.nodeName ? ge(e).find(_e) : ge(e).children(we))[0],
                o = n && r && ge(r).hasClass('fade'),
                a = function() {
                  return i._transitionComplete(t, r, n);
                };
              if (r && o) {
                var s = ke.getTransitionDurationFromElement(r);
                ge(r)
                  .one(ke.TRANSITION_END, a)
                  .emulateTransitionEnd(s);
              } else a();
            }),
            (e._transitionComplete = function(t, e, n) {
              if (e) {
                ge(e).removeClass('show ' + xe);
                var i = ge(e.parentNode).find('> .dropdown-menu .active')[0];
                i && ge(i).removeClass(xe), 'tab' === e.getAttribute('role') && e.setAttribute('aria-selected', !1);
              }
              if (
                (ge(t).addClass(xe),
                'tab' === t.getAttribute('role') && t.setAttribute('aria-selected', !0),
                ke.reflow(t),
                ge(t).addClass('show'),
                t.parentNode && ge(t.parentNode).hasClass('dropdown-menu'))
              ) {
                var r = ge(t).closest('.dropdown')[0];
                if (r) {
                  var o = [].slice.call(r.querySelectorAll('.dropdown-toggle'));
                  ge(o).addClass(xe);
                }
                t.setAttribute('aria-expanded', !0);
              }
              n && n();
            }),
            (t._jQueryInterface = function(e) {
              return this.each(function() {
                var n = ge(this),
                  i = n.data(me);
                if ((i || ((i = new t(this)), n.data(me, i)), 'string' == typeof e)) {
                  if (void 0 === i[e]) throw new TypeError('No method named "' + e + '"');
                  i[e]();
                }
              });
            }),
            r(t, null, [
              {
                key: 'VERSION',
                get: function() {
                  return '4.1.3';
                }
              }
            ]),
            t
          );
        })()),
        ge(document).on(ye.CLICK_DATA_API, '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]', function(
          t
        ) {
          t.preventDefault(), Ce._jQueryInterface.call(ge(this), 'show');
        }),
        (ge.fn.tab = Ce._jQueryInterface),
        (ge.fn.tab.Constructor = Ce),
        (ge.fn.tab.noConflict = function() {
          return (ge.fn.tab = be), Ce._jQueryInterface;
        }),
        Ce);
    !(function(t) {
      if (void 0 === t)
        throw new TypeError(
          "Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript."
        );
      var e = t.fn.jquery.split(' ')[0].split('.');
      if ((e[0] < 2 && e[1] < 9) || (1 === e[0] && 9 === e[1] && e[2] < 1) || 4 <= e[0])
        throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0");
    })(e),
      (t.Util = ke),
      (t.Alert = Se),
      (t.Button = Te),
      (t.Carousel = De),
      (t.Collapse = Ae),
      (t.Dropdown = Ee),
      (t.Modal = Me),
      (t.Popover = Pe),
      (t.Scrollspy = Oe),
      (t.Tab = Ne),
      (t.Tooltip = Ie),
      Object.defineProperty(t, '__esModule', { value: !0 });
  }),
  (function(t) {
    'function' == typeof define && define.amd
      ? define(['jquery'], t)
      : 'object' == typeof exports
      ? t(require('jquery'))
      : t(jQuery);
  })(function(t) {
    var e = /\+/g;
    function n(t) {
      return o.raw ? t : encodeURIComponent(t);
    }
    function i(t) {
      return n(o.json ? JSON.stringify(t) : String(t));
    }
    function r(n, i) {
      var r = o.raw
        ? n
        : (function(t) {
            0 === t.indexOf('"') &&
              (t = t
                .slice(1, -1)
                .replace(/\\"/g, '"')
                .replace(/\\\\/g, '\\'));
            try {
              return (t = decodeURIComponent(t.replace(e, ' '))), o.json ? JSON.parse(t) : t;
            } catch (n) {}
          })(n);
      return t.isFunction(i) ? i(r) : r;
    }
    var o = (t.cookie = function(e, a, s) {
      if (void 0 !== a && !t.isFunction(a)) {
        if ('number' == typeof (s = t.extend({}, o.defaults, s)).expires) {
          var l = s.expires,
            u = (s.expires = new Date());
          u.setTime(+u + 864e5 * l);
        }
        return (document.cookie = [
          n(e),
          '=',
          i(a),
          s.expires ? '; expires=' + s.expires.toUTCString() : '',
          s.path ? '; path=' + s.path : '',
          s.domain ? '; domain=' + s.domain : '',
          s.secure ? '; secure' : ''
        ].join(''));
      }
      for (
        var c, d = e ? void 0 : {}, h = document.cookie ? document.cookie.split('; ') : [], f = 0, p = h.length;
        f < p;
        f++
      ) {
        var g = h[f].split('='),
          m = ((c = g.shift()), o.raw ? c : decodeURIComponent(c)),
          v = g.join('=');
        if (e && e === m) {
          d = r(v, a);
          break;
        }
        e || void 0 === (v = r(v)) || (d[m] = v);
      }
      return d;
    });
    (o.defaults = {}),
      (t.removeCookie = function(e, n) {
        return void 0 !== t.cookie(e) && (t.cookie(e, '', t.extend({}, n, { expires: -1 })), !t.cookie(e));
      });
  }),
  (function(t) {
    'object' == typeof exports && 'undefined' != typeof module
      ? (module.exports = t())
      : 'function' == typeof define && define.amd
      ? define([], t)
      : (('undefined' != typeof window
          ? window
          : 'undefined' != typeof global
          ? global
          : 'undefined' != typeof self
          ? self
          : this
        ).Chart = t());
  })(function() {
    return (function t(e, n, i) {
      function r(a, s) {
        if (!n[a]) {
          if (!e[a]) {
            var l = 'function' == typeof require && require;
            if (!s && l) return l(a, !0);
            if (o) return o(a, !0);
            var u = new Error("Cannot find module '" + a + "'");
            throw ((u.code = 'MODULE_NOT_FOUND'), u);
          }
          var c = (n[a] = { exports: {} });
          e[a][0].call(
            c.exports,
            function(t) {
              return r(e[a][1][t] || t);
            },
            c,
            c.exports,
            t,
            e,
            n,
            i
          );
        }
        return n[a].exports;
      }
      for (var o = 'function' == typeof require && require, a = 0; a < i.length; a++) r(i[a]);
      return r;
    })(
      {
        1: [function(t, e, n) {}, {}],
        2: [
          function(t, e, n) {
            var i = t(6);
            function r(t) {
              if (t) {
                var e = [0, 0, 0],
                  n = 1,
                  r = t.match(/^#([a-fA-F0-9]{3})$/i);
                if (r) {
                  r = r[1];
                  for (var o = 0; o < e.length; o++) e[o] = parseInt(r[o] + r[o], 16);
                } else if ((r = t.match(/^#([a-fA-F0-9]{6})$/i)))
                  for (r = r[1], o = 0; o < e.length; o++) e[o] = parseInt(r.slice(2 * o, 2 * o + 2), 16);
                else if (
                  (r = t.match(
                    /^rgba?\(\s*([+-]?\d+)\s*,\s*([+-]?\d+)\s*,\s*([+-]?\d+)\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/i
                  ))
                ) {
                  for (o = 0; o < e.length; o++) e[o] = parseInt(r[o + 1]);
                  n = parseFloat(r[4]);
                } else if (
                  (r = t.match(
                    /^rgba?\(\s*([+-]?[\d\.]+)\%\s*,\s*([+-]?[\d\.]+)\%\s*,\s*([+-]?[\d\.]+)\%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/i
                  ))
                ) {
                  for (o = 0; o < e.length; o++) e[o] = Math.round(2.55 * parseFloat(r[o + 1]));
                  n = parseFloat(r[4]);
                } else if ((r = t.match(/(\w+)/))) {
                  if ('transparent' == r[1]) return [0, 0, 0, 0];
                  if (!(e = i[r[1]])) return;
                }
                for (o = 0; o < e.length; o++) e[o] = c(e[o], 0, 255);
                return (n = n || 0 == n ? c(n, 0, 1) : 1), (e[3] = n), e;
              }
            }
            function o(t) {
              if (t) {
                var e = t.match(
                  /^hsla?\(\s*([+-]?\d+)(?:deg)?\s*,\s*([+-]?[\d\.]+)%\s*,\s*([+-]?[\d\.]+)%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)/
                );
                if (e) {
                  var n = parseFloat(e[4]);
                  return [
                    c(parseInt(e[1]), 0, 360),
                    c(parseFloat(e[2]), 0, 100),
                    c(parseFloat(e[3]), 0, 100),
                    c(isNaN(n) ? 1 : n, 0, 1)
                  ];
                }
              }
            }
            function a(t) {
              if (t) {
                var e = t.match(
                  /^hwb\(\s*([+-]?\d+)(?:deg)?\s*,\s*([+-]?[\d\.]+)%\s*,\s*([+-]?[\d\.]+)%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)/
                );
                if (e) {
                  var n = parseFloat(e[4]);
                  return [
                    c(parseInt(e[1]), 0, 360),
                    c(parseFloat(e[2]), 0, 100),
                    c(parseFloat(e[3]), 0, 100),
                    c(isNaN(n) ? 1 : n, 0, 1)
                  ];
                }
              }
            }
            function s(t, e) {
              return (
                void 0 === e && (e = void 0 !== t[3] ? t[3] : 1),
                'rgba(' + t[0] + ', ' + t[1] + ', ' + t[2] + ', ' + e + ')'
              );
            }
            function l(t, e) {
              return (
                'rgba(' +
                Math.round((t[0] / 255) * 100) +
                '%, ' +
                Math.round((t[1] / 255) * 100) +
                '%, ' +
                Math.round((t[2] / 255) * 100) +
                '%, ' +
                (e || t[3] || 1) +
                ')'
              );
            }
            function u(t, e) {
              return (
                void 0 === e && (e = void 0 !== t[3] ? t[3] : 1),
                'hsla(' + t[0] + ', ' + t[1] + '%, ' + t[2] + '%, ' + e + ')'
              );
            }
            function c(t, e, n) {
              return Math.min(Math.max(e, t), n);
            }
            function d(t) {
              var e = t.toString(16).toUpperCase();
              return e.length < 2 ? '0' + e : e;
            }
            e.exports = {
              getRgba: r,
              getHsla: o,
              getRgb: function(t) {
                var e = r(t);
                return e && e.slice(0, 3);
              },
              getHsl: function(t) {
                var e = o(t);
                return e && e.slice(0, 3);
              },
              getHwb: a,
              getAlpha: function(t) {
                var e = r(t);
                return e || (e = o(t)) || (e = a(t)) ? e[3] : void 0;
              },
              hexString: function(t) {
                return '#' + d(t[0]) + d(t[1]) + d(t[2]);
              },
              rgbString: function(t, e) {
                return e < 1 || (t[3] && t[3] < 1) ? s(t, e) : 'rgb(' + t[0] + ', ' + t[1] + ', ' + t[2] + ')';
              },
              rgbaString: s,
              percentString: function(t, e) {
                return e < 1 || (t[3] && t[3] < 1)
                  ? l(t, e)
                  : 'rgb(' +
                      Math.round((t[0] / 255) * 100) +
                      '%, ' +
                      Math.round((t[1] / 255) * 100) +
                      '%, ' +
                      Math.round((t[2] / 255) * 100) +
                      '%)';
              },
              percentaString: l,
              hslString: function(t, e) {
                return e < 1 || (t[3] && t[3] < 1) ? u(t, e) : 'hsl(' + t[0] + ', ' + t[1] + '%, ' + t[2] + '%)';
              },
              hslaString: u,
              hwbString: function(t, e) {
                return (
                  void 0 === e && (e = void 0 !== t[3] ? t[3] : 1),
                  'hwb(' + t[0] + ', ' + t[1] + '%, ' + t[2] + '%' + (void 0 !== e && 1 !== e ? ', ' + e : '') + ')'
                );
              },
              keyword: function(t) {
                return h[t.slice(0, 3)];
              }
            };
            var h = {};
            for (var f in i) h[i[f]] = f;
          },
          { 6: 6 }
        ],
        3: [
          function(t, e, n) {
            var i = t(5),
              r = t(2),
              o = function(t) {
                return t instanceof o
                  ? t
                  : this instanceof o
                  ? ((this.valid = !1),
                    (this.values = {
                      rgb: [0, 0, 0],
                      hsl: [0, 0, 0],
                      hsv: [0, 0, 0],
                      hwb: [0, 0, 0],
                      cmyk: [0, 0, 0, 0],
                      alpha: 1
                    }),
                    void ('string' == typeof t
                      ? (e = r.getRgba(t))
                        ? this.setValues('rgb', e)
                        : (e = r.getHsla(t))
                        ? this.setValues('hsl', e)
                        : (e = r.getHwb(t)) && this.setValues('hwb', e)
                      : 'object' == typeof t &&
                        (void 0 !== (e = t).r || void 0 !== e.red
                          ? this.setValues('rgb', e)
                          : void 0 !== e.l || void 0 !== e.lightness
                          ? this.setValues('hsl', e)
                          : void 0 !== e.v || void 0 !== e.value
                          ? this.setValues('hsv', e)
                          : void 0 !== e.w || void 0 !== e.whiteness
                          ? this.setValues('hwb', e)
                          : (void 0 === e.c && void 0 === e.cyan) || this.setValues('cmyk', e))))
                  : new o(t);
                var e;
              };
            (o.prototype = {
              isValid: function() {
                return this.valid;
              },
              rgb: function() {
                return this.setSpace('rgb', arguments);
              },
              hsl: function() {
                return this.setSpace('hsl', arguments);
              },
              hsv: function() {
                return this.setSpace('hsv', arguments);
              },
              hwb: function() {
                return this.setSpace('hwb', arguments);
              },
              cmyk: function() {
                return this.setSpace('cmyk', arguments);
              },
              rgbArray: function() {
                return this.values.rgb;
              },
              hslArray: function() {
                return this.values.hsl;
              },
              hsvArray: function() {
                return this.values.hsv;
              },
              hwbArray: function() {
                var t = this.values;
                return 1 !== t.alpha ? t.hwb.concat([t.alpha]) : t.hwb;
              },
              cmykArray: function() {
                return this.values.cmyk;
              },
              rgbaArray: function() {
                var t = this.values;
                return t.rgb.concat([t.alpha]);
              },
              hslaArray: function() {
                var t = this.values;
                return t.hsl.concat([t.alpha]);
              },
              alpha: function(t) {
                return void 0 === t ? this.values.alpha : (this.setValues('alpha', t), this);
              },
              red: function(t) {
                return this.setChannel('rgb', 0, t);
              },
              green: function(t) {
                return this.setChannel('rgb', 1, t);
              },
              blue: function(t) {
                return this.setChannel('rgb', 2, t);
              },
              hue: function(t) {
                return t && (t = (t %= 360) < 0 ? 360 + t : t), this.setChannel('hsl', 0, t);
              },
              saturation: function(t) {
                return this.setChannel('hsl', 1, t);
              },
              lightness: function(t) {
                return this.setChannel('hsl', 2, t);
              },
              saturationv: function(t) {
                return this.setChannel('hsv', 1, t);
              },
              whiteness: function(t) {
                return this.setChannel('hwb', 1, t);
              },
              blackness: function(t) {
                return this.setChannel('hwb', 2, t);
              },
              value: function(t) {
                return this.setChannel('hsv', 2, t);
              },
              cyan: function(t) {
                return this.setChannel('cmyk', 0, t);
              },
              magenta: function(t) {
                return this.setChannel('cmyk', 1, t);
              },
              yellow: function(t) {
                return this.setChannel('cmyk', 2, t);
              },
              black: function(t) {
                return this.setChannel('cmyk', 3, t);
              },
              hexString: function() {
                return r.hexString(this.values.rgb);
              },
              rgbString: function() {
                return r.rgbString(this.values.rgb, this.values.alpha);
              },
              rgbaString: function() {
                return r.rgbaString(this.values.rgb, this.values.alpha);
              },
              percentString: function() {
                return r.percentString(this.values.rgb, this.values.alpha);
              },
              hslString: function() {
                return r.hslString(this.values.hsl, this.values.alpha);
              },
              hslaString: function() {
                return r.hslaString(this.values.hsl, this.values.alpha);
              },
              hwbString: function() {
                return r.hwbString(this.values.hwb, this.values.alpha);
              },
              keyword: function() {
                return r.keyword(this.values.rgb, this.values.alpha);
              },
              rgbNumber: function() {
                var t = this.values.rgb;
                return (t[0] << 16) | (t[1] << 8) | t[2];
              },
              luminosity: function() {
                for (var t = this.values.rgb, e = [], n = 0; n < t.length; n++) {
                  var i = t[n] / 255;
                  e[n] = i <= 0.03928 ? i / 12.92 : Math.pow((i + 0.055) / 1.055, 2.4);
                }
                return 0.2126 * e[0] + 0.7152 * e[1] + 0.0722 * e[2];
              },
              contrast: function(t) {
                var e = this.luminosity(),
                  n = t.luminosity();
                return e > n ? (e + 0.05) / (n + 0.05) : (n + 0.05) / (e + 0.05);
              },
              level: function(t) {
                var e = this.contrast(t);
                return e >= 7.1 ? 'AAA' : e >= 4.5 ? 'AA' : '';
              },
              dark: function() {
                var t = this.values.rgb;
                return (299 * t[0] + 587 * t[1] + 114 * t[2]) / 1e3 < 128;
              },
              light: function() {
                return !this.dark();
              },
              negate: function() {
                for (var t = [], e = 0; e < 3; e++) t[e] = 255 - this.values.rgb[e];
                return this.setValues('rgb', t), this;
              },
              lighten: function(t) {
                var e = this.values.hsl;
                return (e[2] += e[2] * t), this.setValues('hsl', e), this;
              },
              darken: function(t) {
                var e = this.values.hsl;
                return (e[2] -= e[2] * t), this.setValues('hsl', e), this;
              },
              saturate: function(t) {
                var e = this.values.hsl;
                return (e[1] += e[1] * t), this.setValues('hsl', e), this;
              },
              desaturate: function(t) {
                var e = this.values.hsl;
                return (e[1] -= e[1] * t), this.setValues('hsl', e), this;
              },
              whiten: function(t) {
                var e = this.values.hwb;
                return (e[1] += e[1] * t), this.setValues('hwb', e), this;
              },
              blacken: function(t) {
                var e = this.values.hwb;
                return (e[2] += e[2] * t), this.setValues('hwb', e), this;
              },
              greyscale: function() {
                var t = this.values.rgb,
                  e = 0.3 * t[0] + 0.59 * t[1] + 0.11 * t[2];
                return this.setValues('rgb', [e, e, e]), this;
              },
              clearer: function(t) {
                var e = this.values.alpha;
                return this.setValues('alpha', e - e * t), this;
              },
              opaquer: function(t) {
                var e = this.values.alpha;
                return this.setValues('alpha', e + e * t), this;
              },
              rotate: function(t) {
                var e = this.values.hsl,
                  n = (e[0] + t) % 360;
                return (e[0] = n < 0 ? 360 + n : n), this.setValues('hsl', e), this;
              },
              mix: function(t, e) {
                var n = this,
                  i = t,
                  r = void 0 === e ? 0.5 : e,
                  o = 2 * r - 1,
                  a = n.alpha() - i.alpha(),
                  s = ((o * a == -1 ? o : (o + a) / (1 + o * a)) + 1) / 2,
                  l = 1 - s;
                return this.rgb(
                  s * n.red() + l * i.red(),
                  s * n.green() + l * i.green(),
                  s * n.blue() + l * i.blue()
                ).alpha(n.alpha() * r + i.alpha() * (1 - r));
              },
              toJSON: function() {
                return this.rgb();
              },
              clone: function() {
                var t,
                  e,
                  n = new o(),
                  i = this.values,
                  r = n.values;
                for (var a in i)
                  i.hasOwnProperty(a) &&
                    ('[object Array]' === (e = {}.toString.call((t = i[a])))
                      ? (r[a] = t.slice(0))
                      : '[object Number]' === e
                      ? (r[a] = t)
                      : console.error('unexpected color value:', t));
                return n;
              }
            }),
              (o.prototype.spaces = {
                rgb: ['red', 'green', 'blue'],
                hsl: ['hue', 'saturation', 'lightness'],
                hsv: ['hue', 'saturation', 'value'],
                hwb: ['hue', 'whiteness', 'blackness'],
                cmyk: ['cyan', 'magenta', 'yellow', 'black']
              }),
              (o.prototype.maxes = {
                rgb: [255, 255, 255],
                hsl: [360, 100, 100],
                hsv: [360, 100, 100],
                hwb: [360, 100, 100],
                cmyk: [100, 100, 100, 100]
              }),
              (o.prototype.getValues = function(t) {
                for (var e = this.values, n = {}, i = 0; i < t.length; i++) n[t.charAt(i)] = e[t][i];
                return 1 !== e.alpha && (n.a = e.alpha), n;
              }),
              (o.prototype.setValues = function(t, e) {
                var n,
                  r,
                  o = this.values,
                  a = this.spaces,
                  s = this.maxes,
                  l = 1;
                if (((this.valid = !0), 'alpha' === t)) l = e;
                else if (e.length) (o[t] = e.slice(0, t.length)), (l = e[t.length]);
                else if (void 0 !== e[t.charAt(0)]) {
                  for (n = 0; n < t.length; n++) o[t][n] = e[t.charAt(n)];
                  l = e.a;
                } else if (void 0 !== e[a[t][0]]) {
                  var u = a[t];
                  for (n = 0; n < t.length; n++) o[t][n] = e[u[n]];
                  l = e.alpha;
                }
                if (((o.alpha = Math.max(0, Math.min(1, void 0 === l ? o.alpha : l))), 'alpha' === t)) return !1;
                for (n = 0; n < t.length; n++) (r = Math.max(0, Math.min(s[t][n], o[t][n]))), (o[t][n] = Math.round(r));
                for (var c in a) c !== t && (o[c] = i[t][c](o[t]));
                return !0;
              }),
              (o.prototype.setSpace = function(t, e) {
                var n = e[0];
                return void 0 === n
                  ? this.getValues(t)
                  : ('number' == typeof n && (n = Array.prototype.slice.call(e)), this.setValues(t, n), this);
              }),
              (o.prototype.setChannel = function(t, e, n) {
                var i = this.values[t];
                return void 0 === n ? i[e] : (n === i[e] || ((i[e] = n), this.setValues(t, i)), this);
              }),
              'undefined' != typeof window && (window.Color = o),
              (e.exports = o);
          },
          { 2: 2, 5: 5 }
        ],
        4: [
          function(t, e, n) {
            function i(t) {
              var e,
                n,
                i = t[0] / 255,
                r = t[1] / 255,
                o = t[2] / 255,
                a = Math.min(i, r, o),
                s = Math.max(i, r, o),
                l = s - a;
              return (
                s == a
                  ? (e = 0)
                  : i == s
                  ? (e = (r - o) / l)
                  : r == s
                  ? (e = 2 + (o - i) / l)
                  : o == s && (e = 4 + (i - r) / l),
                (e = Math.min(60 * e, 360)) < 0 && (e += 360),
                (n = (a + s) / 2),
                [e, 100 * (s == a ? 0 : n <= 0.5 ? l / (s + a) : l / (2 - s - a)), 100 * n]
              );
            }
            function o(t) {
              var e,
                n,
                i = t[0],
                r = t[1],
                o = t[2],
                a = Math.min(i, r, o),
                s = Math.max(i, r, o),
                l = s - a;
              return (
                (n = 0 == s ? 0 : ((l / s) * 1e3) / 10),
                s == a
                  ? (e = 0)
                  : i == s
                  ? (e = (r - o) / l)
                  : r == s
                  ? (e = 2 + (o - i) / l)
                  : o == s && (e = 4 + (i - r) / l),
                (e = Math.min(60 * e, 360)) < 0 && (e += 360),
                [e, n, ((s / 255) * 1e3) / 10]
              );
            }
            function a(t) {
              var e = t[0],
                n = t[1],
                r = t[2];
              return [
                i(t)[0],
                (1 / 255) * Math.min(e, Math.min(n, r)) * 100,
                100 * (r = 1 - (1 / 255) * Math.max(e, Math.max(n, r)))
              ];
            }
            function s(t) {
              var e,
                n = t[0] / 255,
                i = t[1] / 255,
                r = t[2] / 255;
              return [
                100 * ((1 - n - (e = Math.min(1 - n, 1 - i, 1 - r))) / (1 - e) || 0),
                100 * ((1 - i - e) / (1 - e) || 0),
                100 * ((1 - r - e) / (1 - e) || 0),
                100 * e
              ];
            }
            function l(t) {
              return S[JSON.stringify(t)];
            }
            function u(t) {
              var e = t[0] / 255,
                n = t[1] / 255,
                i = t[2] / 255;
              return [
                100 *
                  (0.4124 * (e = e > 0.04045 ? Math.pow((e + 0.055) / 1.055, 2.4) : e / 12.92) +
                    0.3576 * (n = n > 0.04045 ? Math.pow((n + 0.055) / 1.055, 2.4) : n / 12.92) +
                    0.1805 * (i = i > 0.04045 ? Math.pow((i + 0.055) / 1.055, 2.4) : i / 12.92)),
                100 * (0.2126 * e + 0.7152 * n + 0.0722 * i),
                100 * (0.0193 * e + 0.1192 * n + 0.9505 * i)
              ];
            }
            function c(t) {
              var e = u(t),
                n = e[0],
                i = e[1],
                r = e[2];
              return (
                (i /= 100),
                (r /= 108.883),
                (n = (n /= 95.047) > 0.008856 ? Math.pow(n, 1 / 3) : 7.787 * n + 16 / 116),
                [
                  116 * (i = i > 0.008856 ? Math.pow(i, 1 / 3) : 7.787 * i + 16 / 116) - 16,
                  500 * (n - i),
                  200 * (i - (r = r > 0.008856 ? Math.pow(r, 1 / 3) : 7.787 * r + 16 / 116))
                ]
              );
            }
            function d(t) {
              var e,
                n,
                i,
                r,
                o,
                a = t[0] / 360,
                s = t[1] / 100,
                l = t[2] / 100;
              if (0 == s) return [(o = 255 * l), o, o];
              (e = 2 * l - (n = l < 0.5 ? l * (1 + s) : l + s - l * s)), (r = [0, 0, 0]);
              for (var u = 0; u < 3; u++)
                (i = a + (1 / 3) * -(u - 1)) < 0 && i++,
                  i > 1 && i--,
                  (r[u] =
                    255 *
                    (o =
                      6 * i < 1 ? e + 6 * (n - e) * i : 2 * i < 1 ? n : 3 * i < 2 ? e + (n - e) * (2 / 3 - i) * 6 : e));
              return r;
            }
            function h(t) {
              var e = t[0] / 60,
                n = t[1] / 100,
                i = t[2] / 100,
                r = Math.floor(e) % 6,
                o = e - Math.floor(e),
                a = 255 * i * (1 - n),
                s = 255 * i * (1 - n * o),
                l = 255 * i * (1 - n * (1 - o));
              switch (((i *= 255), r)) {
                case 0:
                  return [i, l, a];
                case 1:
                  return [s, i, a];
                case 2:
                  return [a, i, l];
                case 3:
                  return [a, s, i];
                case 4:
                  return [l, a, i];
                case 5:
                  return [i, a, s];
              }
            }
            function f(t) {
              var e,
                n,
                i,
                o,
                a = t[0] / 360,
                s = t[1] / 100,
                l = t[2] / 100,
                u = s + l;
              switch (
                (u > 1 && ((s /= u), (l /= u)),
                (i = 6 * a - (e = Math.floor(6 * a))),
                0 != (1 & e) && (i = 1 - i),
                (o = s + i * ((n = 1 - l) - s)),
                e)
              ) {
                default:
                case 6:
                case 0:
                  (r = n), (g = o), (b = s);
                  break;
                case 1:
                  (r = o), (g = n), (b = s);
                  break;
                case 2:
                  (r = s), (g = n), (b = o);
                  break;
                case 3:
                  (r = s), (g = o), (b = n);
                  break;
                case 4:
                  (r = o), (g = s), (b = n);
                  break;
                case 5:
                  (r = n), (g = s), (b = o);
              }
              return [255 * r, 255 * g, 255 * b];
            }
            function p(t) {
              var e = t[1] / 100,
                n = t[2] / 100,
                i = t[3] / 100;
              return [
                255 * (1 - Math.min(1, (t[0] / 100) * (1 - i) + i)),
                255 * (1 - Math.min(1, e * (1 - i) + i)),
                255 * (1 - Math.min(1, n * (1 - i) + i))
              ];
            }
            function m(t) {
              var e,
                n,
                i,
                r = t[0] / 100,
                o = t[1] / 100,
                a = t[2] / 100;
              return (
                (n = -0.9689 * r + 1.8758 * o + 0.0415 * a),
                (i = 0.0557 * r + -0.204 * o + 1.057 * a),
                (e =
                  (e = 3.2406 * r + -1.5372 * o + -0.4986 * a) > 0.0031308
                    ? 1.055 * Math.pow(e, 1 / 2.4) - 0.055
                    : (e *= 12.92)),
                (n = n > 0.0031308 ? 1.055 * Math.pow(n, 1 / 2.4) - 0.055 : (n *= 12.92)),
                (i = i > 0.0031308 ? 1.055 * Math.pow(i, 1 / 2.4) - 0.055 : (i *= 12.92)),
                [
                  255 * (e = Math.min(Math.max(0, e), 1)),
                  255 * (n = Math.min(Math.max(0, n), 1)),
                  255 * (i = Math.min(Math.max(0, i), 1))
                ]
              );
            }
            function v(t) {
              var e = t[0],
                n = t[1],
                i = t[2];
              return (
                (n /= 100),
                (i /= 108.883),
                (e = (e /= 95.047) > 0.008856 ? Math.pow(e, 1 / 3) : 7.787 * e + 16 / 116),
                [
                  116 * (n = n > 0.008856 ? Math.pow(n, 1 / 3) : 7.787 * n + 16 / 116) - 16,
                  500 * (e - n),
                  200 * (n - (i = i > 0.008856 ? Math.pow(i, 1 / 3) : 7.787 * i + 16 / 116))
                ]
              );
            }
            function y(t) {
              var e,
                n,
                i,
                r,
                o = t[0],
                a = t[1],
                s = t[2];
              return (
                o <= 8
                  ? (r = ((n = (100 * o) / 903.3) / 100) * 7.787 + 16 / 116)
                  : ((n = 100 * Math.pow((o + 16) / 116, 3)), (r = Math.pow(n / 100, 1 / 3))),
                [
                  (e =
                    e / 95.047 <= 0.008856
                      ? (e = (95.047 * (a / 500 + r - 16 / 116)) / 7.787)
                      : 95.047 * Math.pow(a / 500 + r, 3)),
                  n,
                  (i =
                    i / 108.883 <= 0.008859
                      ? (i = (108.883 * (r - s / 200 - 16 / 116)) / 7.787)
                      : 108.883 * Math.pow(r - s / 200, 3))
                ]
              );
            }
            function x(t) {
              var e,
                n = t[0],
                i = t[1],
                r = t[2];
              return (e = (360 * Math.atan2(r, i)) / 2 / Math.PI) < 0 && (e += 360), [n, Math.sqrt(i * i + r * r), e];
            }
            function w(t) {
              return m(y(t));
            }
            function _(t) {
              var e,
                n = t[1];
              return (e = (t[2] / 360) * 2 * Math.PI), [t[0], n * Math.cos(e), n * Math.sin(e)];
            }
            function C(t) {
              return k[t];
            }
            e.exports = {
              rgb2hsl: i,
              rgb2hsv: o,
              rgb2hwb: a,
              rgb2cmyk: s,
              rgb2keyword: l,
              rgb2xyz: u,
              rgb2lab: c,
              rgb2lch: function(t) {
                return x(c(t));
              },
              hsl2rgb: d,
              hsl2hsv: function(t) {
                var e = t[1] / 100,
                  n = t[2] / 100;
                return 0 === n
                  ? [0, 0, 0]
                  : [t[0], ((2 * (e *= (n *= 2) <= 1 ? n : 2 - n)) / (n + e)) * 100, ((n + e) / 2) * 100];
              },
              hsl2hwb: function(t) {
                return a(d(t));
              },
              hsl2cmyk: function(t) {
                return s(d(t));
              },
              hsl2keyword: function(t) {
                return l(d(t));
              },
              hsv2rgb: h,
              hsv2hsl: function(t) {
                var e,
                  n,
                  i = t[1] / 100,
                  r = t[2] / 100;
                return (e = i * r), [t[0], 100 * (e = (e /= (n = (2 - i) * r) <= 1 ? n : 2 - n) || 0), 100 * (n /= 2)];
              },
              hsv2hwb: function(t) {
                return a(h(t));
              },
              hsv2cmyk: function(t) {
                return s(h(t));
              },
              hsv2keyword: function(t) {
                return l(h(t));
              },
              hwb2rgb: f,
              hwb2hsl: function(t) {
                return i(f(t));
              },
              hwb2hsv: function(t) {
                return o(f(t));
              },
              hwb2cmyk: function(t) {
                return s(f(t));
              },
              hwb2keyword: function(t) {
                return l(f(t));
              },
              cmyk2rgb: p,
              cmyk2hsl: function(t) {
                return i(p(t));
              },
              cmyk2hsv: function(t) {
                return o(p(t));
              },
              cmyk2hwb: function(t) {
                return a(p(t));
              },
              cmyk2keyword: function(t) {
                return l(p(t));
              },
              keyword2rgb: C,
              keyword2hsl: function(t) {
                return i(C(t));
              },
              keyword2hsv: function(t) {
                return o(C(t));
              },
              keyword2hwb: function(t) {
                return a(C(t));
              },
              keyword2cmyk: function(t) {
                return s(C(t));
              },
              keyword2lab: function(t) {
                return c(C(t));
              },
              keyword2xyz: function(t) {
                return u(C(t));
              },
              xyz2rgb: m,
              xyz2lab: v,
              xyz2lch: function(t) {
                return x(v(t));
              },
              lab2xyz: y,
              lab2rgb: w,
              lab2lch: x,
              lch2lab: _,
              lch2xyz: function(t) {
                return y(_(t));
              },
              lch2rgb: function(t) {
                return w(_(t));
              }
            };
            var k = {
                aliceblue: [240, 248, 255],
                antiquewhite: [250, 235, 215],
                aqua: [0, 255, 255],
                aquamarine: [127, 255, 212],
                azure: [240, 255, 255],
                beige: [245, 245, 220],
                bisque: [255, 228, 196],
                black: [0, 0, 0],
                blanchedalmond: [255, 235, 205],
                blue: [0, 0, 255],
                blueviolet: [138, 43, 226],
                brown: [165, 42, 42],
                burlywood: [222, 184, 135],
                cadetblue: [95, 158, 160],
                chartreuse: [127, 255, 0],
                chocolate: [210, 105, 30],
                coral: [255, 127, 80],
                cornflowerblue: [100, 149, 237],
                cornsilk: [255, 248, 220],
                crimson: [220, 20, 60],
                cyan: [0, 255, 255],
                darkblue: [0, 0, 139],
                darkcyan: [0, 139, 139],
                darkgoldenrod: [184, 134, 11],
                darkgray: [169, 169, 169],
                darkgreen: [0, 100, 0],
                darkgrey: [169, 169, 169],
                darkkhaki: [189, 183, 107],
                darkmagenta: [139, 0, 139],
                darkolivegreen: [85, 107, 47],
                darkorange: [255, 140, 0],
                darkorchid: [153, 50, 204],
                darkred: [139, 0, 0],
                darksalmon: [233, 150, 122],
                darkseagreen: [143, 188, 143],
                darkslateblue: [72, 61, 139],
                darkslategray: [47, 79, 79],
                darkslategrey: [47, 79, 79],
                darkturquoise: [0, 206, 209],
                darkviolet: [148, 0, 211],
                deeppink: [255, 20, 147],
                deepskyblue: [0, 191, 255],
                dimgray: [105, 105, 105],
                dimgrey: [105, 105, 105],
                dodgerblue: [30, 144, 255],
                firebrick: [178, 34, 34],
                floralwhite: [255, 250, 240],
                forestgreen: [34, 139, 34],
                fuchsia: [255, 0, 255],
                gainsboro: [220, 220, 220],
                ghostwhite: [248, 248, 255],
                gold: [255, 215, 0],
                goldenrod: [218, 165, 32],
                gray: [128, 128, 128],
                green: [0, 128, 0],
                greenyellow: [173, 255, 47],
                grey: [128, 128, 128],
                honeydew: [240, 255, 240],
                hotpink: [255, 105, 180],
                indianred: [205, 92, 92],
                indigo: [75, 0, 130],
                ivory: [255, 255, 240],
                khaki: [240, 230, 140],
                lavender: [230, 230, 250],
                lavenderblush: [255, 240, 245],
                lawngreen: [124, 252, 0],
                lemonchiffon: [255, 250, 205],
                lightblue: [173, 216, 230],
                lightcoral: [240, 128, 128],
                lightcyan: [224, 255, 255],
                lightgoldenrodyellow: [250, 250, 210],
                lightgray: [211, 211, 211],
                lightgreen: [144, 238, 144],
                lightgrey: [211, 211, 211],
                lightpink: [255, 182, 193],
                lightsalmon: [255, 160, 122],
                lightseagreen: [32, 178, 170],
                lightskyblue: [135, 206, 250],
                lightslategray: [119, 136, 153],
                lightslategrey: [119, 136, 153],
                lightsteelblue: [176, 196, 222],
                lightyellow: [255, 255, 224],
                lime: [0, 255, 0],
                limegreen: [50, 205, 50],
                linen: [250, 240, 230],
                magenta: [255, 0, 255],
                maroon: [128, 0, 0],
                mediumaquamarine: [102, 205, 170],
                mediumblue: [0, 0, 205],
                mediumorchid: [186, 85, 211],
                mediumpurple: [147, 112, 219],
                mediumseagreen: [60, 179, 113],
                mediumslateblue: [123, 104, 238],
                mediumspringgreen: [0, 250, 154],
                mediumturquoise: [72, 209, 204],
                mediumvioletred: [199, 21, 133],
                midnightblue: [25, 25, 112],
                mintcream: [245, 255, 250],
                mistyrose: [255, 228, 225],
                moccasin: [255, 228, 181],
                navajowhite: [255, 222, 173],
                navy: [0, 0, 128],
                oldlace: [253, 245, 230],
                olive: [128, 128, 0],
                olivedrab: [107, 142, 35],
                orange: [255, 165, 0],
                orangered: [255, 69, 0],
                orchid: [218, 112, 214],
                palegoldenrod: [238, 232, 170],
                palegreen: [152, 251, 152],
                paleturquoise: [175, 238, 238],
                palevioletred: [219, 112, 147],
                papayawhip: [255, 239, 213],
                peachpuff: [255, 218, 185],
                peru: [205, 133, 63],
                pink: [255, 192, 203],
                plum: [221, 160, 221],
                powderblue: [176, 224, 230],
                purple: [128, 0, 128],
                rebeccapurple: [102, 51, 153],
                red: [255, 0, 0],
                rosybrown: [188, 143, 143],
                royalblue: [65, 105, 225],
                saddlebrown: [139, 69, 19],
                salmon: [250, 128, 114],
                sandybrown: [244, 164, 96],
                seagreen: [46, 139, 87],
                seashell: [255, 245, 238],
                sienna: [160, 82, 45],
                silver: [192, 192, 192],
                skyblue: [135, 206, 235],
                slateblue: [106, 90, 205],
                slategray: [112, 128, 144],
                slategrey: [112, 128, 144],
                snow: [255, 250, 250],
                springgreen: [0, 255, 127],
                steelblue: [70, 130, 180],
                tan: [210, 180, 140],
                teal: [0, 128, 128],
                thistle: [216, 191, 216],
                tomato: [255, 99, 71],
                turquoise: [64, 224, 208],
                violet: [238, 130, 238],
                wheat: [245, 222, 179],
                white: [255, 255, 255],
                whitesmoke: [245, 245, 245],
                yellow: [255, 255, 0],
                yellowgreen: [154, 205, 50]
              },
              S = {};
            for (var T in k) S[JSON.stringify(k[T])] = T;
          },
          {}
        ],
        5: [
          function(t, e, n) {
            var i = t(4),
              r = function() {
                return new u();
              };
            for (var o in i) {
              r[o + 'Raw'] = (function(t) {
                return function(e) {
                  return 'number' == typeof e && (e = Array.prototype.slice.call(arguments)), i[t](e);
                };
              })(o);
              var a = /(\w+)2(\w+)/.exec(o),
                s = a[1],
                l = a[2];
              (r[s] = r[s] || {})[l] = r[o] = (function(t) {
                return function(e) {
                  'number' == typeof e && (e = Array.prototype.slice.call(arguments));
                  var n = i[t](e);
                  if ('string' == typeof n || void 0 === n) return n;
                  for (var r = 0; r < n.length; r++) n[r] = Math.round(n[r]);
                  return n;
                };
              })(o);
            }
            var u = function() {
              this.convs = {};
            };
            (u.prototype.routeSpace = function(t, e) {
              var n = e[0];
              return void 0 === n
                ? this.getValues(t)
                : ('number' == typeof n && (n = Array.prototype.slice.call(e)), this.setValues(t, n));
            }),
              (u.prototype.setValues = function(t, e) {
                return (this.space = t), (this.convs = {}), (this.convs[t] = e), this;
              }),
              (u.prototype.getValues = function(t) {
                var e = this.convs[t];
                if (!e) {
                  var n = this.space;
                  (e = r[n][t](this.convs[n])), (this.convs[t] = e);
                }
                return e;
              }),
              ['rgb', 'hsl', 'hsv', 'cmyk', 'keyword'].forEach(function(t) {
                u.prototype[t] = function(e) {
                  return this.routeSpace(t, arguments);
                };
              }),
              (e.exports = r);
          },
          { 4: 4 }
        ],
        6: [
          function(t, e, n) {
            'use strict';
            e.exports = {
              aliceblue: [240, 248, 255],
              antiquewhite: [250, 235, 215],
              aqua: [0, 255, 255],
              aquamarine: [127, 255, 212],
              azure: [240, 255, 255],
              beige: [245, 245, 220],
              bisque: [255, 228, 196],
              black: [0, 0, 0],
              blanchedalmond: [255, 235, 205],
              blue: [0, 0, 255],
              blueviolet: [138, 43, 226],
              brown: [165, 42, 42],
              burlywood: [222, 184, 135],
              cadetblue: [95, 158, 160],
              chartreuse: [127, 255, 0],
              chocolate: [210, 105, 30],
              coral: [255, 127, 80],
              cornflowerblue: [100, 149, 237],
              cornsilk: [255, 248, 220],
              crimson: [220, 20, 60],
              cyan: [0, 255, 255],
              darkblue: [0, 0, 139],
              darkcyan: [0, 139, 139],
              darkgoldenrod: [184, 134, 11],
              darkgray: [169, 169, 169],
              darkgreen: [0, 100, 0],
              darkgrey: [169, 169, 169],
              darkkhaki: [189, 183, 107],
              darkmagenta: [139, 0, 139],
              darkolivegreen: [85, 107, 47],
              darkorange: [255, 140, 0],
              darkorchid: [153, 50, 204],
              darkred: [139, 0, 0],
              darksalmon: [233, 150, 122],
              darkseagreen: [143, 188, 143],
              darkslateblue: [72, 61, 139],
              darkslategray: [47, 79, 79],
              darkslategrey: [47, 79, 79],
              darkturquoise: [0, 206, 209],
              darkviolet: [148, 0, 211],
              deeppink: [255, 20, 147],
              deepskyblue: [0, 191, 255],
              dimgray: [105, 105, 105],
              dimgrey: [105, 105, 105],
              dodgerblue: [30, 144, 255],
              firebrick: [178, 34, 34],
              floralwhite: [255, 250, 240],
              forestgreen: [34, 139, 34],
              fuchsia: [255, 0, 255],
              gainsboro: [220, 220, 220],
              ghostwhite: [248, 248, 255],
              gold: [255, 215, 0],
              goldenrod: [218, 165, 32],
              gray: [128, 128, 128],
              green: [0, 128, 0],
              greenyellow: [173, 255, 47],
              grey: [128, 128, 128],
              honeydew: [240, 255, 240],
              hotpink: [255, 105, 180],
              indianred: [205, 92, 92],
              indigo: [75, 0, 130],
              ivory: [255, 255, 240],
              khaki: [240, 230, 140],
              lavender: [230, 230, 250],
              lavenderblush: [255, 240, 245],
              lawngreen: [124, 252, 0],
              lemonchiffon: [255, 250, 205],
              lightblue: [173, 216, 230],
              lightcoral: [240, 128, 128],
              lightcyan: [224, 255, 255],
              lightgoldenrodyellow: [250, 250, 210],
              lightgray: [211, 211, 211],
              lightgreen: [144, 238, 144],
              lightgrey: [211, 211, 211],
              lightpink: [255, 182, 193],
              lightsalmon: [255, 160, 122],
              lightseagreen: [32, 178, 170],
              lightskyblue: [135, 206, 250],
              lightslategray: [119, 136, 153],
              lightslategrey: [119, 136, 153],
              lightsteelblue: [176, 196, 222],
              lightyellow: [255, 255, 224],
              lime: [0, 255, 0],
              limegreen: [50, 205, 50],
              linen: [250, 240, 230],
              magenta: [255, 0, 255],
              maroon: [128, 0, 0],
              mediumaquamarine: [102, 205, 170],
              mediumblue: [0, 0, 205],
              mediumorchid: [186, 85, 211],
              mediumpurple: [147, 112, 219],
              mediumseagreen: [60, 179, 113],
              mediumslateblue: [123, 104, 238],
              mediumspringgreen: [0, 250, 154],
              mediumturquoise: [72, 209, 204],
              mediumvioletred: [199, 21, 133],
              midnightblue: [25, 25, 112],
              mintcream: [245, 255, 250],
              mistyrose: [255, 228, 225],
              moccasin: [255, 228, 181],
              navajowhite: [255, 222, 173],
              navy: [0, 0, 128],
              oldlace: [253, 245, 230],
              olive: [128, 128, 0],
              olivedrab: [107, 142, 35],
              orange: [255, 165, 0],
              orangered: [255, 69, 0],
              orchid: [218, 112, 214],
              palegoldenrod: [238, 232, 170],
              palegreen: [152, 251, 152],
              paleturquoise: [175, 238, 238],
              palevioletred: [219, 112, 147],
              papayawhip: [255, 239, 213],
              peachpuff: [255, 218, 185],
              peru: [205, 133, 63],
              pink: [255, 192, 203],
              plum: [221, 160, 221],
              powderblue: [176, 224, 230],
              purple: [128, 0, 128],
              rebeccapurple: [102, 51, 153],
              red: [255, 0, 0],
              rosybrown: [188, 143, 143],
              royalblue: [65, 105, 225],
              saddlebrown: [139, 69, 19],
              salmon: [250, 128, 114],
              sandybrown: [244, 164, 96],
              seagreen: [46, 139, 87],
              seashell: [255, 245, 238],
              sienna: [160, 82, 45],
              silver: [192, 192, 192],
              skyblue: [135, 206, 235],
              slateblue: [106, 90, 205],
              slategray: [112, 128, 144],
              slategrey: [112, 128, 144],
              snow: [255, 250, 250],
              springgreen: [0, 255, 127],
              steelblue: [70, 130, 180],
              tan: [210, 180, 140],
              teal: [0, 128, 128],
              thistle: [216, 191, 216],
              tomato: [255, 99, 71],
              turquoise: [64, 224, 208],
              violet: [238, 130, 238],
              wheat: [245, 222, 179],
              white: [255, 255, 255],
              whitesmoke: [245, 245, 245],
              yellow: [255, 255, 0],
              yellowgreen: [154, 205, 50]
            };
          },
          {}
        ],
        7: [
          function(t, e, n) {
            var i = t(29)();
            (i.helpers = t(45)),
              t(27)(i),
              (i.defaults = t(25)),
              (i.Element = t(26)),
              (i.elements = t(40)),
              (i.Interaction = t(28)),
              (i.layouts = t(30)),
              (i.platform = t(48)),
              (i.plugins = t(31)),
              (i.Ticks = t(34)),
              t(22)(i),
              t(23)(i),
              t(24)(i),
              t(33)(i),
              t(32)(i),
              t(35)(i),
              t(55)(i),
              t(53)(i),
              t(54)(i),
              t(56)(i),
              t(57)(i),
              t(58)(i),
              t(15)(i),
              t(16)(i),
              t(17)(i),
              t(18)(i),
              t(19)(i),
              t(20)(i),
              t(21)(i),
              t(8)(i),
              t(9)(i),
              t(10)(i),
              t(11)(i),
              t(12)(i),
              t(13)(i),
              t(14)(i);
            var r = t(49);
            for (var o in r) r.hasOwnProperty(o) && i.plugins.register(r[o]);
            i.platform.initialize(),
              (e.exports = i),
              'undefined' != typeof window && (window.Chart = i),
              (i.Legend = r.legend._element),
              (i.Title = r.title._element),
              (i.pluginService = i.plugins),
              (i.PluginBase = i.Element.extend({})),
              (i.canvasHelpers = i.helpers.canvas),
              (i.layoutService = i.layouts);
          },
          {
            10: 10,
            11: 11,
            12: 12,
            13: 13,
            14: 14,
            15: 15,
            16: 16,
            17: 17,
            18: 18,
            19: 19,
            20: 20,
            21: 21,
            22: 22,
            23: 23,
            24: 24,
            25: 25,
            26: 26,
            27: 27,
            28: 28,
            29: 29,
            30: 30,
            31: 31,
            32: 32,
            33: 33,
            34: 34,
            35: 35,
            40: 40,
            45: 45,
            48: 48,
            49: 49,
            53: 53,
            54: 54,
            55: 55,
            56: 56,
            57: 57,
            58: 58,
            8: 8,
            9: 9
          }
        ],
        8: [
          function(t, e, n) {
            'use strict';
            e.exports = function(t) {
              t.Bar = function(e, n) {
                return (n.type = 'bar'), new t(e, n);
              };
            };
          },
          {}
        ],
        9: [
          function(t, e, n) {
            'use strict';
            e.exports = function(t) {
              t.Bubble = function(e, n) {
                return (n.type = 'bubble'), new t(e, n);
              };
            };
          },
          {}
        ],
        10: [
          function(t, e, n) {
            'use strict';
            e.exports = function(t) {
              t.Doughnut = function(e, n) {
                return (n.type = 'doughnut'), new t(e, n);
              };
            };
          },
          {}
        ],
        11: [
          function(t, e, n) {
            'use strict';
            e.exports = function(t) {
              t.Line = function(e, n) {
                return (n.type = 'line'), new t(e, n);
              };
            };
          },
          {}
        ],
        12: [
          function(t, e, n) {
            'use strict';
            e.exports = function(t) {
              t.PolarArea = function(e, n) {
                return (n.type = 'polarArea'), new t(e, n);
              };
            };
          },
          {}
        ],
        13: [
          function(t, e, n) {
            'use strict';
            e.exports = function(t) {
              t.Radar = function(e, n) {
                return (n.type = 'radar'), new t(e, n);
              };
            };
          },
          {}
        ],
        14: [
          function(t, e, n) {
            'use strict';
            e.exports = function(t) {
              t.Scatter = function(e, n) {
                return (n.type = 'scatter'), new t(e, n);
              };
            };
          },
          {}
        ],
        15: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(40),
              o = t(45);
            i._set('bar', {
              hover: { mode: 'label' },
              scales: {
                xAxes: [
                  {
                    type: 'category',
                    categoryPercentage: 0.8,
                    barPercentage: 0.9,
                    offset: !0,
                    gridLines: { offsetGridLines: !0 }
                  }
                ],
                yAxes: [{ type: 'linear' }]
              }
            }),
              i._set('horizontalBar', {
                hover: { mode: 'index', axis: 'y' },
                scales: {
                  xAxes: [{ type: 'linear', position: 'bottom' }],
                  yAxes: [
                    {
                      position: 'left',
                      type: 'category',
                      categoryPercentage: 0.8,
                      barPercentage: 0.9,
                      offset: !0,
                      gridLines: { offsetGridLines: !0 }
                    }
                  ]
                },
                elements: { rectangle: { borderSkipped: 'left' } },
                tooltips: {
                  callbacks: {
                    title: function(t, e) {
                      var n = '';
                      return (
                        t.length > 0 &&
                          (t[0].yLabel
                            ? (n = t[0].yLabel)
                            : e.labels.length > 0 && t[0].index < e.labels.length && (n = e.labels[t[0].index])),
                        n
                      );
                    },
                    label: function(t, e) {
                      return (e.datasets[t.datasetIndex].label || '') + ': ' + t.xLabel;
                    }
                  },
                  mode: 'index',
                  axis: 'y'
                }
              }),
              (e.exports = function(t) {
                (t.controllers.bar = t.DatasetController.extend({
                  dataElementType: r.Rectangle,
                  initialize: function() {
                    var e;
                    t.DatasetController.prototype.initialize.apply(this, arguments),
                      ((e = this.getMeta()).stack = this.getDataset().stack),
                      (e.bar = !0);
                  },
                  update: function(t) {
                    var e,
                      n,
                      i = this.getMeta().data;
                    for (this._ruler = this.getRuler(), e = 0, n = i.length; e < n; ++e) this.updateElement(i[e], e, t);
                  },
                  updateElement: function(t, e, n) {
                    var i = this,
                      r = i.chart,
                      a = i.getMeta(),
                      s = i.getDataset(),
                      l = t.custom || {},
                      u = r.options.elements.rectangle;
                    (t._xScale = i.getScaleForId(a.xAxisID)),
                      (t._yScale = i.getScaleForId(a.yAxisID)),
                      (t._datasetIndex = i.index),
                      (t._index = e),
                      (t._model = {
                        datasetLabel: s.label,
                        label: r.data.labels[e],
                        borderSkipped: l.borderSkipped ? l.borderSkipped : u.borderSkipped,
                        backgroundColor: l.backgroundColor
                          ? l.backgroundColor
                          : o.valueAtIndexOrDefault(s.backgroundColor, e, u.backgroundColor),
                        borderColor: l.borderColor
                          ? l.borderColor
                          : o.valueAtIndexOrDefault(s.borderColor, e, u.borderColor),
                        borderWidth: l.borderWidth
                          ? l.borderWidth
                          : o.valueAtIndexOrDefault(s.borderWidth, e, u.borderWidth)
                      }),
                      i.updateElementGeometry(t, e, n),
                      t.pivot();
                  },
                  updateElementGeometry: function(t, e, n) {
                    var i = this,
                      r = t._model,
                      o = i.getValueScale(),
                      a = o.getBasePixel(),
                      s = o.isHorizontal(),
                      l = i._ruler || i.getRuler(),
                      u = i.calculateBarValuePixels(i.index, e),
                      c = i.calculateBarIndexPixels(i.index, e, l);
                    (r.horizontal = s),
                      (r.base = n ? a : u.base),
                      (r.x = s ? (n ? a : u.head) : c.center),
                      (r.y = s ? c.center : n ? a : u.head),
                      (r.height = s ? c.size : void 0),
                      (r.width = s ? void 0 : c.size);
                  },
                  getValueScaleId: function() {
                    return this.getMeta().yAxisID;
                  },
                  getIndexScaleId: function() {
                    return this.getMeta().xAxisID;
                  },
                  getValueScale: function() {
                    return this.getScaleForId(this.getValueScaleId());
                  },
                  getIndexScale: function() {
                    return this.getScaleForId(this.getIndexScaleId());
                  },
                  _getStacks: function(t) {
                    var e,
                      n,
                      i = this.chart,
                      r = this.getIndexScale().options.stacked,
                      o = void 0 === t ? i.data.datasets.length : t + 1,
                      a = [];
                    for (e = 0; e < o; ++e)
                      (n = i.getDatasetMeta(e)).bar &&
                        i.isDatasetVisible(e) &&
                        (!1 === r ||
                          (!0 === r && -1 === a.indexOf(n.stack)) ||
                          (void 0 === r && (void 0 === n.stack || -1 === a.indexOf(n.stack)))) &&
                        a.push(n.stack);
                    return a;
                  },
                  getStackCount: function() {
                    return this._getStacks().length;
                  },
                  getStackIndex: function(t, e) {
                    var n = this._getStacks(t),
                      i = void 0 !== e ? n.indexOf(e) : -1;
                    return -1 === i ? n.length - 1 : i;
                  },
                  getRuler: function() {
                    var t,
                      e,
                      n = this.getIndexScale(),
                      i = this.getStackCount(),
                      r = this.index,
                      a = n.isHorizontal(),
                      s = a ? n.left : n.top,
                      l = s + (a ? n.width : n.height),
                      u = [];
                    for (t = 0, e = this.getMeta().data.length; t < e; ++t) u.push(n.getPixelForValue(null, t, r));
                    return {
                      min: o.isNullOrUndef(n.options.barThickness)
                        ? (function(t, e) {
                            var n,
                              i,
                              r,
                              o,
                              a = t.isHorizontal() ? t.width : t.height,
                              s = t.getTicks();
                            for (r = 1, o = e.length; r < o; ++r) a = Math.min(a, e[r] - e[r - 1]);
                            for (r = 0, o = s.length; r < o; ++r)
                              (i = t.getPixelForTick(r)), (a = r > 0 ? Math.min(a, i - n) : a), (n = i);
                            return a;
                          })(n, u)
                        : -1,
                      pixels: u,
                      start: s,
                      end: l,
                      stackCount: i,
                      scale: n
                    };
                  },
                  calculateBarValuePixels: function(t, e) {
                    var n,
                      i,
                      r,
                      o,
                      a,
                      s,
                      l = this.chart,
                      u = this.getMeta(),
                      c = this.getValueScale(),
                      d = l.data.datasets,
                      h = c.getRightValue(d[t].data[e]),
                      f = c.options.stacked,
                      p = u.stack,
                      g = 0;
                    if (f || (void 0 === f && void 0 !== p))
                      for (n = 0; n < t; ++n)
                        (i = l.getDatasetMeta(n)).bar &&
                          i.stack === p &&
                          i.controller.getValueScaleId() === c.id &&
                          l.isDatasetVisible(n) &&
                          ((r = c.getRightValue(d[n].data[e])), ((h < 0 && r < 0) || (h >= 0 && r > 0)) && (g += r));
                    return (
                      (o = c.getPixelForValue(g)),
                      { size: (s = ((a = c.getPixelForValue(g + h)) - o) / 2), base: o, head: a, center: a + s / 2 }
                    );
                  },
                  calculateBarIndexPixels: function(t, e, n) {
                    var i,
                      r,
                      a,
                      s,
                      l,
                      u,
                      c,
                      d,
                      h,
                      f,
                      p,
                      g,
                      m,
                      v,
                      b,
                      y = n.scale.options,
                      x =
                        'flex' === y.barThickness
                          ? ((g = (p = (h = n).pixels)[(d = e)]),
                            (v = d < p.length - 1 ? p[d + 1] : null),
                            null === (m = d > 0 ? p[d - 1] : null) && (m = g - (null === v ? h.end - g : v - g)),
                            null === v && (v = g + g - m),
                            {
                              chunk: (((v - m) / 2) * (b = (f = y).categoryPercentage)) / h.stackCount,
                              ratio: f.barPercentage,
                              start: g - ((g - m) / 2) * b
                            })
                          : ((l = (r = y).barThickness),
                            (u = (i = n).stackCount),
                            (c = i.pixels[e]),
                            o.isNullOrUndef(l)
                              ? ((a = i.min * r.categoryPercentage), (s = r.barPercentage))
                              : ((a = l * u), (s = 1)),
                            { chunk: a / u, ratio: s, start: c - a / 2 }),
                      w = this.getStackIndex(t, this.getMeta().stack),
                      _ = x.start + x.chunk * w + x.chunk / 2,
                      C = Math.min(o.valueOrDefault(y.maxBarThickness, 1 / 0), x.chunk * x.ratio);
                    return { base: _ - C / 2, head: _ + C / 2, center: _, size: C };
                  },
                  draw: function() {
                    var t = this.chart,
                      e = this.getValueScale(),
                      n = this.getMeta().data,
                      i = this.getDataset(),
                      r = n.length,
                      a = 0;
                    for (o.canvas.clipArea(t.ctx, t.chartArea); a < r; ++a)
                      isNaN(e.getRightValue(i.data[a])) || n[a].draw();
                    o.canvas.unclipArea(t.ctx);
                  },
                  setHoverStyle: function(t) {
                    var e = this.chart.data.datasets[t._datasetIndex],
                      n = t._index,
                      i = t.custom || {},
                      r = t._model;
                    (r.backgroundColor = i.hoverBackgroundColor
                      ? i.hoverBackgroundColor
                      : o.valueAtIndexOrDefault(e.hoverBackgroundColor, n, o.getHoverColor(r.backgroundColor))),
                      (r.borderColor = i.hoverBorderColor
                        ? i.hoverBorderColor
                        : o.valueAtIndexOrDefault(e.hoverBorderColor, n, o.getHoverColor(r.borderColor))),
                      (r.borderWidth = i.hoverBorderWidth
                        ? i.hoverBorderWidth
                        : o.valueAtIndexOrDefault(e.hoverBorderWidth, n, r.borderWidth));
                  },
                  removeHoverStyle: function(t) {
                    var e = this.chart.data.datasets[t._datasetIndex],
                      n = t._index,
                      i = t.custom || {},
                      r = t._model,
                      a = this.chart.options.elements.rectangle;
                    (r.backgroundColor = i.backgroundColor
                      ? i.backgroundColor
                      : o.valueAtIndexOrDefault(e.backgroundColor, n, a.backgroundColor)),
                      (r.borderColor = i.borderColor
                        ? i.borderColor
                        : o.valueAtIndexOrDefault(e.borderColor, n, a.borderColor)),
                      (r.borderWidth = i.borderWidth
                        ? i.borderWidth
                        : o.valueAtIndexOrDefault(e.borderWidth, n, a.borderWidth));
                  }
                })),
                  (t.controllers.horizontalBar = t.controllers.bar.extend({
                    getValueScaleId: function() {
                      return this.getMeta().xAxisID;
                    },
                    getIndexScaleId: function() {
                      return this.getMeta().yAxisID;
                    }
                  }));
              });
          },
          { 25: 25, 40: 40, 45: 45 }
        ],
        16: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(40),
              o = t(45);
            i._set('bubble', {
              hover: { mode: 'single' },
              scales: {
                xAxes: [{ type: 'linear', position: 'bottom', id: 'x-axis-0' }],
                yAxes: [{ type: 'linear', position: 'left', id: 'y-axis-0' }]
              },
              tooltips: {
                callbacks: {
                  title: function() {
                    return '';
                  },
                  label: function(t, e) {
                    return (
                      (e.datasets[t.datasetIndex].label || '') +
                      ': (' +
                      t.xLabel +
                      ', ' +
                      t.yLabel +
                      ', ' +
                      e.datasets[t.datasetIndex].data[t.index].r +
                      ')'
                    );
                  }
                }
              }
            }),
              (e.exports = function(t) {
                t.controllers.bubble = t.DatasetController.extend({
                  dataElementType: r.Point,
                  update: function(t) {
                    var e = this,
                      n = e.getMeta().data;
                    o.each(n, function(n, i) {
                      e.updateElement(n, i, t);
                    });
                  },
                  updateElement: function(t, e, n) {
                    var i = this,
                      r = i.getMeta(),
                      o = t.custom || {},
                      a = i.getScaleForId(r.xAxisID),
                      s = i.getScaleForId(r.yAxisID),
                      l = i._resolveElementOptions(t, e),
                      u = i.getDataset().data[e],
                      c = i.index,
                      d = n ? a.getPixelForDecimal(0.5) : a.getPixelForValue('object' == typeof u ? u : NaN, e, c),
                      h = n ? s.getBasePixel() : s.getPixelForValue(u, e, c);
                    (t._xScale = a),
                      (t._yScale = s),
                      (t._options = l),
                      (t._datasetIndex = c),
                      (t._index = e),
                      (t._model = {
                        backgroundColor: l.backgroundColor,
                        borderColor: l.borderColor,
                        borderWidth: l.borderWidth,
                        hitRadius: l.hitRadius,
                        pointStyle: l.pointStyle,
                        radius: n ? 0 : l.radius,
                        skip: o.skip || isNaN(d) || isNaN(h),
                        x: d,
                        y: h
                      }),
                      t.pivot();
                  },
                  setHoverStyle: function(t) {
                    var e = t._model,
                      n = t._options;
                    (e.backgroundColor = o.valueOrDefault(n.hoverBackgroundColor, o.getHoverColor(n.backgroundColor))),
                      (e.borderColor = o.valueOrDefault(n.hoverBorderColor, o.getHoverColor(n.borderColor))),
                      (e.borderWidth = o.valueOrDefault(n.hoverBorderWidth, n.borderWidth)),
                      (e.radius = n.radius + n.hoverRadius);
                  },
                  removeHoverStyle: function(t) {
                    var e = t._model,
                      n = t._options;
                    (e.backgroundColor = n.backgroundColor),
                      (e.borderColor = n.borderColor),
                      (e.borderWidth = n.borderWidth),
                      (e.radius = n.radius);
                  },
                  _resolveElementOptions: function(t, e) {
                    var n,
                      i,
                      r,
                      a = this.chart,
                      s = a.data.datasets[this.index],
                      l = t.custom || {},
                      u = a.options.elements.point,
                      c = o.options.resolve,
                      d = s.data[e],
                      h = {},
                      f = { chart: a, dataIndex: e, dataset: s, datasetIndex: this.index },
                      p = [
                        'backgroundColor',
                        'borderColor',
                        'borderWidth',
                        'hoverBackgroundColor',
                        'hoverBorderColor',
                        'hoverBorderWidth',
                        'hoverRadius',
                        'hitRadius',
                        'pointStyle'
                      ];
                    for (n = 0, i = p.length; n < i; ++n) h[(r = p[n])] = c([l[r], s[r], u[r]], f, e);
                    return (h.radius = c([l.radius, d ? d.r : void 0, s.radius, u.radius], f, e)), h;
                  }
                });
              });
          },
          { 25: 25, 40: 40, 45: 45 }
        ],
        17: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(40),
              o = t(45);
            i._set('doughnut', {
              animation: { animateRotate: !0, animateScale: !1 },
              hover: { mode: 'single' },
              legendCallback: function(t) {
                var e = [];
                e.push('<ul class="' + t.id + '-legend">');
                var n = t.data,
                  i = n.datasets,
                  r = n.labels;
                if (i.length)
                  for (var o = 0; o < i[0].data.length; ++o)
                    e.push('<li><span style="background-color:' + i[0].backgroundColor[o] + '"></span>'),
                      r[o] && e.push(r[o]),
                      e.push('</li>');
                return e.push('</ul>'), e.join('');
              },
              legend: {
                labels: {
                  generateLabels: function(t) {
                    var e = t.data;
                    return e.labels.length && e.datasets.length
                      ? e.labels.map(function(n, i) {
                          var r = t.getDatasetMeta(0),
                            a = e.datasets[0],
                            s = r.data[i],
                            l = (s && s.custom) || {},
                            u = o.valueAtIndexOrDefault,
                            c = t.options.elements.arc;
                          return {
                            text: n,
                            fillStyle: l.backgroundColor
                              ? l.backgroundColor
                              : u(a.backgroundColor, i, c.backgroundColor),
                            strokeStyle: l.borderColor ? l.borderColor : u(a.borderColor, i, c.borderColor),
                            lineWidth: l.borderWidth ? l.borderWidth : u(a.borderWidth, i, c.borderWidth),
                            hidden: isNaN(a.data[i]) || r.data[i].hidden,
                            index: i
                          };
                        })
                      : [];
                  }
                },
                onClick: function(t, e) {
                  var n,
                    i,
                    r,
                    o = e.index,
                    a = this.chart;
                  for (n = 0, i = (a.data.datasets || []).length; n < i; ++n)
                    (r = a.getDatasetMeta(n)).data[o] && (r.data[o].hidden = !r.data[o].hidden);
                  a.update();
                }
              },
              cutoutPercentage: 50,
              rotation: -0.5 * Math.PI,
              circumference: 2 * Math.PI,
              tooltips: {
                callbacks: {
                  title: function() {
                    return '';
                  },
                  label: function(t, e) {
                    var n = e.labels[t.index],
                      i = ': ' + e.datasets[t.datasetIndex].data[t.index];
                    return o.isArray(n) ? ((n = n.slice())[0] += i) : (n += i), n;
                  }
                }
              }
            }),
              i._set('pie', o.clone(i.doughnut)),
              i._set('pie', { cutoutPercentage: 0 }),
              (e.exports = function(t) {
                t.controllers.doughnut = t.controllers.pie = t.DatasetController.extend({
                  dataElementType: r.Arc,
                  linkScales: o.noop,
                  getRingIndex: function(t) {
                    for (var e = 0, n = 0; n < t; ++n) this.chart.isDatasetVisible(n) && ++e;
                    return e;
                  },
                  update: function(t) {
                    var e = this,
                      n = e.chart,
                      i = n.chartArea,
                      r = n.options,
                      a = r.elements.arc,
                      s = i.right - i.left - a.borderWidth,
                      l = i.bottom - i.top - a.borderWidth,
                      u = Math.min(s, l),
                      c = { x: 0, y: 0 },
                      d = e.getMeta(),
                      h = r.cutoutPercentage,
                      f = r.circumference;
                    if (f < 2 * Math.PI) {
                      var p = r.rotation % (2 * Math.PI),
                        g = (p += 2 * Math.PI * (p >= Math.PI ? -1 : p < -Math.PI ? 1 : 0)) + f,
                        m = Math.cos(p),
                        v = Math.sin(p),
                        b = Math.cos(g),
                        y = Math.sin(g),
                        x = (p <= 0 && g >= 0) || (p <= 2 * Math.PI && 2 * Math.PI <= g),
                        w = (p <= 0.5 * Math.PI && 0.5 * Math.PI <= g) || (p <= 2.5 * Math.PI && 2.5 * Math.PI <= g),
                        _ = (p <= -Math.PI && -Math.PI <= g) || (p <= Math.PI && Math.PI <= g),
                        C = (p <= 0.5 * -Math.PI && 0.5 * -Math.PI <= g) || (p <= 1.5 * Math.PI && 1.5 * Math.PI <= g),
                        k = h / 100,
                        S = _ ? -1 : Math.min(m * (m < 0 ? 1 : k), b * (b < 0 ? 1 : k)),
                        T = C ? -1 : Math.min(v * (v < 0 ? 1 : k), y * (y < 0 ? 1 : k)),
                        D = x ? 1 : Math.max(m * (m > 0 ? 1 : k), b * (b > 0 ? 1 : k)),
                        A = w ? 1 : Math.max(v * (v > 0 ? 1 : k), y * (y > 0 ? 1 : k));
                      (u = Math.min(s / (0.5 * (D - S)), l / (0.5 * (A - T)))),
                        (c = { x: -0.5 * (D + S), y: -0.5 * (A + T) });
                    }
                    (n.borderWidth = e.getMaxBorderWidth(d.data)),
                      (n.outerRadius = Math.max((u - n.borderWidth) / 2, 0)),
                      (n.innerRadius = Math.max(h ? (n.outerRadius / 100) * h : 0, 0)),
                      (n.radiusLength = (n.outerRadius - n.innerRadius) / n.getVisibleDatasetCount()),
                      (n.offsetX = c.x * n.outerRadius),
                      (n.offsetY = c.y * n.outerRadius),
                      (d.total = e.calculateTotal()),
                      (e.outerRadius = n.outerRadius - n.radiusLength * e.getRingIndex(e.index)),
                      (e.innerRadius = Math.max(e.outerRadius - n.radiusLength, 0)),
                      o.each(d.data, function(n, i) {
                        e.updateElement(n, i, t);
                      });
                  },
                  updateElement: function(t, e, n) {
                    var i = this,
                      r = i.chart,
                      a = r.chartArea,
                      s = r.options,
                      l = s.animation,
                      u = (a.left + a.right) / 2,
                      c = (a.top + a.bottom) / 2,
                      d = s.rotation,
                      h = s.rotation,
                      f = i.getDataset(),
                      p =
                        (n && l.animateRotate) || t.hidden
                          ? 0
                          : i.calculateCircumference(f.data[e]) * (s.circumference / (2 * Math.PI));
                    o.extend(t, {
                      _datasetIndex: i.index,
                      _index: e,
                      _model: {
                        x: u + r.offsetX,
                        y: c + r.offsetY,
                        startAngle: d,
                        endAngle: h,
                        circumference: p,
                        outerRadius: n && l.animateScale ? 0 : i.outerRadius,
                        innerRadius: n && l.animateScale ? 0 : i.innerRadius,
                        label: (0, o.valueAtIndexOrDefault)(f.label, e, r.data.labels[e])
                      }
                    });
                    var g = t._model;
                    this.removeHoverStyle(t),
                      (n && l.animateRotate) ||
                        ((g.startAngle = 0 === e ? s.rotation : i.getMeta().data[e - 1]._model.endAngle),
                        (g.endAngle = g.startAngle + g.circumference)),
                      t.pivot();
                  },
                  removeHoverStyle: function(e) {
                    t.DatasetController.prototype.removeHoverStyle.call(this, e, this.chart.options.elements.arc);
                  },
                  calculateTotal: function() {
                    var t,
                      e = this.getDataset(),
                      n = this.getMeta(),
                      i = 0;
                    return (
                      o.each(n.data, function(n, r) {
                        (t = e.data[r]), isNaN(t) || n.hidden || (i += Math.abs(t));
                      }),
                      i
                    );
                  },
                  calculateCircumference: function(t) {
                    var e = this.getMeta().total;
                    return e > 0 && !isNaN(t) ? 2 * Math.PI * (Math.abs(t) / e) : 0;
                  },
                  getMaxBorderWidth: function(t) {
                    for (var e, n, i = 0, r = this.index, o = t.length, a = 0; a < o; a++)
                      i =
                        (n = t[a]._chart ? t[a]._chart.config.data.datasets[r].hoverBorderWidth : 0) >
                        (i = (e = t[a]._model ? t[a]._model.borderWidth : 0) > i ? e : i)
                          ? n
                          : i;
                    return i;
                  }
                });
              });
          },
          { 25: 25, 40: 40, 45: 45 }
        ],
        18: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(40),
              o = t(45);
            i._set('line', {
              showLines: !0,
              spanGaps: !1,
              hover: { mode: 'label' },
              scales: { xAxes: [{ type: 'category', id: 'x-axis-0' }], yAxes: [{ type: 'linear', id: 'y-axis-0' }] }
            }),
              (e.exports = function(t) {
                function e(t, e) {
                  return o.valueOrDefault(t.showLine, e.showLines);
                }
                t.controllers.line = t.DatasetController.extend({
                  datasetElementType: r.Line,
                  dataElementType: r.Point,
                  update: function(t) {
                    var n,
                      i,
                      r,
                      a = this,
                      s = a.getMeta(),
                      l = s.dataset,
                      u = s.data || [],
                      c = a.chart.options,
                      d = c.elements.line,
                      h = a.getScaleForId(s.yAxisID),
                      f = a.getDataset(),
                      p = e(f, c);
                    for (
                      p &&
                        ((r = l.custom || {}),
                        void 0 !== f.tension && void 0 === f.lineTension && (f.lineTension = f.tension),
                        (l._scale = h),
                        (l._datasetIndex = a.index),
                        (l._children = u),
                        (l._model = {
                          spanGaps: f.spanGaps ? f.spanGaps : c.spanGaps,
                          tension: r.tension ? r.tension : o.valueOrDefault(f.lineTension, d.tension),
                          backgroundColor: r.backgroundColor
                            ? r.backgroundColor
                            : f.backgroundColor || d.backgroundColor,
                          borderWidth: r.borderWidth ? r.borderWidth : f.borderWidth || d.borderWidth,
                          borderColor: r.borderColor ? r.borderColor : f.borderColor || d.borderColor,
                          borderCapStyle: r.borderCapStyle ? r.borderCapStyle : f.borderCapStyle || d.borderCapStyle,
                          borderDash: r.borderDash ? r.borderDash : f.borderDash || d.borderDash,
                          borderDashOffset: r.borderDashOffset
                            ? r.borderDashOffset
                            : f.borderDashOffset || d.borderDashOffset,
                          borderJoinStyle: r.borderJoinStyle
                            ? r.borderJoinStyle
                            : f.borderJoinStyle || d.borderJoinStyle,
                          fill: r.fill ? r.fill : void 0 !== f.fill ? f.fill : d.fill,
                          steppedLine: r.steppedLine ? r.steppedLine : o.valueOrDefault(f.steppedLine, d.stepped),
                          cubicInterpolationMode: r.cubicInterpolationMode
                            ? r.cubicInterpolationMode
                            : o.valueOrDefault(f.cubicInterpolationMode, d.cubicInterpolationMode)
                        }),
                        l.pivot()),
                        n = 0,
                        i = u.length;
                      n < i;
                      ++n
                    )
                      a.updateElement(u[n], n, t);
                    for (p && 0 !== l._model.tension && a.updateBezierControlPoints(), n = 0, i = u.length; n < i; ++n)
                      u[n].pivot();
                  },
                  getPointBackgroundColor: function(t, e) {
                    var n = this.chart.options.elements.point.backgroundColor,
                      i = this.getDataset(),
                      r = t.custom || {};
                    return (
                      r.backgroundColor
                        ? (n = r.backgroundColor)
                        : i.pointBackgroundColor
                        ? (n = o.valueAtIndexOrDefault(i.pointBackgroundColor, e, n))
                        : i.backgroundColor && (n = i.backgroundColor),
                      n
                    );
                  },
                  getPointBorderColor: function(t, e) {
                    var n = this.chart.options.elements.point.borderColor,
                      i = this.getDataset(),
                      r = t.custom || {};
                    return (
                      r.borderColor
                        ? (n = r.borderColor)
                        : i.pointBorderColor
                        ? (n = o.valueAtIndexOrDefault(i.pointBorderColor, e, n))
                        : i.borderColor && (n = i.borderColor),
                      n
                    );
                  },
                  getPointBorderWidth: function(t, e) {
                    var n = this.chart.options.elements.point.borderWidth,
                      i = this.getDataset(),
                      r = t.custom || {};
                    return (
                      isNaN(r.borderWidth)
                        ? !isNaN(i.pointBorderWidth) || o.isArray(i.pointBorderWidth)
                          ? (n = o.valueAtIndexOrDefault(i.pointBorderWidth, e, n))
                          : isNaN(i.borderWidth) || (n = i.borderWidth)
                        : (n = r.borderWidth),
                      n
                    );
                  },
                  updateElement: function(t, e, n) {
                    var i,
                      r,
                      a = this,
                      s = a.getMeta(),
                      l = t.custom || {},
                      u = a.getDataset(),
                      c = a.index,
                      d = u.data[e],
                      h = a.getScaleForId(s.yAxisID),
                      f = a.getScaleForId(s.xAxisID),
                      p = a.chart.options.elements.point;
                    void 0 !== u.radius && void 0 === u.pointRadius && (u.pointRadius = u.radius),
                      void 0 !== u.hitRadius && void 0 === u.pointHitRadius && (u.pointHitRadius = u.hitRadius),
                      (i = f.getPixelForValue('object' == typeof d ? d : NaN, e, c)),
                      (r = n ? h.getBasePixel() : a.calculatePointY(d, e, c)),
                      (t._xScale = f),
                      (t._yScale = h),
                      (t._datasetIndex = c),
                      (t._index = e),
                      (t._model = {
                        x: i,
                        y: r,
                        skip: l.skip || isNaN(i) || isNaN(r),
                        radius: l.radius || o.valueAtIndexOrDefault(u.pointRadius, e, p.radius),
                        pointStyle: l.pointStyle || o.valueAtIndexOrDefault(u.pointStyle, e, p.pointStyle),
                        backgroundColor: a.getPointBackgroundColor(t, e),
                        borderColor: a.getPointBorderColor(t, e),
                        borderWidth: a.getPointBorderWidth(t, e),
                        tension: s.dataset._model ? s.dataset._model.tension : 0,
                        steppedLine: !!s.dataset._model && s.dataset._model.steppedLine,
                        hitRadius: l.hitRadius || o.valueAtIndexOrDefault(u.pointHitRadius, e, p.hitRadius)
                      });
                  },
                  calculatePointY: function(t, e, n) {
                    var i,
                      r,
                      o,
                      a = this.chart,
                      s = this.getMeta(),
                      l = this.getScaleForId(s.yAxisID),
                      u = 0,
                      c = 0;
                    if (l.options.stacked) {
                      for (i = 0; i < n; i++)
                        if (
                          ((r = a.data.datasets[i]),
                          'line' === (o = a.getDatasetMeta(i)).type && o.yAxisID === l.id && a.isDatasetVisible(i))
                        ) {
                          var d = Number(l.getRightValue(r.data[e]));
                          d < 0 ? (c += d || 0) : (u += d || 0);
                        }
                      var h = Number(l.getRightValue(t));
                      return l.getPixelForValue(h < 0 ? c + h : u + h);
                    }
                    return l.getPixelForValue(t);
                  },
                  updateBezierControlPoints: function() {
                    var t,
                      e,
                      n,
                      i,
                      r = this.getMeta(),
                      a = this.chart.chartArea,
                      s = r.data || [];
                    function l(t, e, n) {
                      return Math.max(Math.min(t, n), e);
                    }
                    if (
                      (r.dataset._model.spanGaps &&
                        (s = s.filter(function(t) {
                          return !t._model.skip;
                        })),
                      'monotone' === r.dataset._model.cubicInterpolationMode)
                    )
                      o.splineCurveMonotone(s);
                    else
                      for (t = 0, e = s.length; t < e; ++t)
                        (n = s[t]._model),
                          (i = o.splineCurve(
                            o.previousItem(s, t)._model,
                            n,
                            o.nextItem(s, t)._model,
                            r.dataset._model.tension
                          )),
                          (n.controlPointPreviousX = i.previous.x),
                          (n.controlPointPreviousY = i.previous.y),
                          (n.controlPointNextX = i.next.x),
                          (n.controlPointNextY = i.next.y);
                    if (this.chart.options.elements.line.capBezierPoints)
                      for (t = 0, e = s.length; t < e; ++t)
                        ((n = s[t]._model).controlPointPreviousX = l(n.controlPointPreviousX, a.left, a.right)),
                          (n.controlPointPreviousY = l(n.controlPointPreviousY, a.top, a.bottom)),
                          (n.controlPointNextX = l(n.controlPointNextX, a.left, a.right)),
                          (n.controlPointNextY = l(n.controlPointNextY, a.top, a.bottom));
                  },
                  draw: function() {
                    var t = this.chart,
                      n = this.getMeta(),
                      i = n.data || [],
                      r = t.chartArea,
                      a = i.length,
                      s = 0;
                    for (
                      o.canvas.clipArea(t.ctx, r),
                        e(this.getDataset(), t.options) && n.dataset.draw(),
                        o.canvas.unclipArea(t.ctx);
                      s < a;
                      ++s
                    )
                      i[s].draw(r);
                  },
                  setHoverStyle: function(t) {
                    var e = this.chart.data.datasets[t._datasetIndex],
                      n = t._index,
                      i = t.custom || {},
                      r = t._model;
                    (r.radius =
                      i.hoverRadius ||
                      o.valueAtIndexOrDefault(e.pointHoverRadius, n, this.chart.options.elements.point.hoverRadius)),
                      (r.backgroundColor =
                        i.hoverBackgroundColor ||
                        o.valueAtIndexOrDefault(e.pointHoverBackgroundColor, n, o.getHoverColor(r.backgroundColor))),
                      (r.borderColor =
                        i.hoverBorderColor ||
                        o.valueAtIndexOrDefault(e.pointHoverBorderColor, n, o.getHoverColor(r.borderColor))),
                      (r.borderWidth =
                        i.hoverBorderWidth || o.valueAtIndexOrDefault(e.pointHoverBorderWidth, n, r.borderWidth));
                  },
                  removeHoverStyle: function(t) {
                    var e = this,
                      n = e.chart.data.datasets[t._datasetIndex],
                      i = t._index,
                      r = t.custom || {},
                      a = t._model;
                    void 0 !== n.radius && void 0 === n.pointRadius && (n.pointRadius = n.radius),
                      (a.radius =
                        r.radius || o.valueAtIndexOrDefault(n.pointRadius, i, e.chart.options.elements.point.radius)),
                      (a.backgroundColor = e.getPointBackgroundColor(t, i)),
                      (a.borderColor = e.getPointBorderColor(t, i)),
                      (a.borderWidth = e.getPointBorderWidth(t, i));
                  }
                });
              });
          },
          { 25: 25, 40: 40, 45: 45 }
        ],
        19: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(40),
              o = t(45);
            i._set('polarArea', {
              scale: {
                type: 'radialLinear',
                angleLines: { display: !1 },
                gridLines: { circular: !0 },
                pointLabels: { display: !1 },
                ticks: { beginAtZero: !0 }
              },
              animation: { animateRotate: !0, animateScale: !0 },
              startAngle: -0.5 * Math.PI,
              legendCallback: function(t) {
                var e = [];
                e.push('<ul class="' + t.id + '-legend">');
                var n = t.data,
                  i = n.datasets,
                  r = n.labels;
                if (i.length)
                  for (var o = 0; o < i[0].data.length; ++o)
                    e.push('<li><span style="background-color:' + i[0].backgroundColor[o] + '"></span>'),
                      r[o] && e.push(r[o]),
                      e.push('</li>');
                return e.push('</ul>'), e.join('');
              },
              legend: {
                labels: {
                  generateLabels: function(t) {
                    var e = t.data;
                    return e.labels.length && e.datasets.length
                      ? e.labels.map(function(n, i) {
                          var r = t.getDatasetMeta(0),
                            a = e.datasets[0],
                            s = r.data[i].custom || {},
                            l = o.valueAtIndexOrDefault,
                            u = t.options.elements.arc;
                          return {
                            text: n,
                            fillStyle: s.backgroundColor
                              ? s.backgroundColor
                              : l(a.backgroundColor, i, u.backgroundColor),
                            strokeStyle: s.borderColor ? s.borderColor : l(a.borderColor, i, u.borderColor),
                            lineWidth: s.borderWidth ? s.borderWidth : l(a.borderWidth, i, u.borderWidth),
                            hidden: isNaN(a.data[i]) || r.data[i].hidden,
                            index: i
                          };
                        })
                      : [];
                  }
                },
                onClick: function(t, e) {
                  var n,
                    i,
                    r,
                    o = e.index,
                    a = this.chart;
                  for (n = 0, i = (a.data.datasets || []).length; n < i; ++n)
                    (r = a.getDatasetMeta(n)).data[o].hidden = !r.data[o].hidden;
                  a.update();
                }
              },
              tooltips: {
                callbacks: {
                  title: function() {
                    return '';
                  },
                  label: function(t, e) {
                    return e.labels[t.index] + ': ' + t.yLabel;
                  }
                }
              }
            }),
              (e.exports = function(t) {
                t.controllers.polarArea = t.DatasetController.extend({
                  dataElementType: r.Arc,
                  linkScales: o.noop,
                  update: function(t) {
                    var e = this,
                      n = e.chart,
                      i = n.chartArea,
                      r = e.getMeta(),
                      a = n.options,
                      s = a.elements.arc,
                      l = Math.min(i.right - i.left, i.bottom - i.top);
                    (n.outerRadius = Math.max((l - s.borderWidth / 2) / 2, 0)),
                      (n.innerRadius = Math.max(
                        a.cutoutPercentage ? (n.outerRadius / 100) * a.cutoutPercentage : 1,
                        0
                      )),
                      (n.radiusLength = (n.outerRadius - n.innerRadius) / n.getVisibleDatasetCount()),
                      (e.outerRadius = n.outerRadius - n.radiusLength * e.index),
                      (e.innerRadius = e.outerRadius - n.radiusLength),
                      (r.count = e.countVisibleElements()),
                      o.each(r.data, function(n, i) {
                        e.updateElement(n, i, t);
                      });
                  },
                  updateElement: function(t, e, n) {
                    for (
                      var i = this,
                        r = i.chart,
                        a = i.getDataset(),
                        s = r.options,
                        l = s.animation,
                        u = r.scale,
                        c = r.data.labels,
                        d = i.calculateCircumference(a.data[e]),
                        h = u.xCenter,
                        f = u.yCenter,
                        p = 0,
                        g = i.getMeta(),
                        m = 0;
                      m < e;
                      ++m
                    )
                      isNaN(a.data[m]) || g.data[m].hidden || ++p;
                    var v = s.startAngle,
                      b = t.hidden ? 0 : u.getDistanceFromCenterForValue(a.data[e]),
                      y = v + d * p,
                      x = y + (t.hidden ? 0 : d),
                      w = l.animateScale ? 0 : u.getDistanceFromCenterForValue(a.data[e]);
                    o.extend(t, {
                      _datasetIndex: i.index,
                      _index: e,
                      _scale: u,
                      _model: {
                        x: h,
                        y: f,
                        innerRadius: 0,
                        outerRadius: n ? w : b,
                        startAngle: n && l.animateRotate ? v : y,
                        endAngle: n && l.animateRotate ? v : x,
                        label: o.valueAtIndexOrDefault(c, e, c[e])
                      }
                    }),
                      i.removeHoverStyle(t),
                      t.pivot();
                  },
                  removeHoverStyle: function(e) {
                    t.DatasetController.prototype.removeHoverStyle.call(this, e, this.chart.options.elements.arc);
                  },
                  countVisibleElements: function() {
                    var t = this.getDataset(),
                      e = this.getMeta(),
                      n = 0;
                    return (
                      o.each(e.data, function(e, i) {
                        isNaN(t.data[i]) || e.hidden || n++;
                      }),
                      n
                    );
                  },
                  calculateCircumference: function(t) {
                    var e = this.getMeta().count;
                    return e > 0 && !isNaN(t) ? (2 * Math.PI) / e : 0;
                  }
                });
              });
          },
          { 25: 25, 40: 40, 45: 45 }
        ],
        20: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(40),
              o = t(45);
            i._set('radar', { scale: { type: 'radialLinear' }, elements: { line: { tension: 0 } } }),
              (e.exports = function(t) {
                t.controllers.radar = t.DatasetController.extend({
                  datasetElementType: r.Line,
                  dataElementType: r.Point,
                  linkScales: o.noop,
                  update: function(t) {
                    var e = this,
                      n = e.getMeta(),
                      i = n.data,
                      r = n.dataset.custom || {},
                      a = e.getDataset(),
                      s = e.chart.options.elements.line,
                      l = e.chart.scale;
                    void 0 !== a.tension && void 0 === a.lineTension && (a.lineTension = a.tension),
                      o.extend(n.dataset, {
                        _datasetIndex: e.index,
                        _scale: l,
                        _children: i,
                        _loop: !0,
                        _model: {
                          tension: r.tension ? r.tension : o.valueOrDefault(a.lineTension, s.tension),
                          backgroundColor: r.backgroundColor
                            ? r.backgroundColor
                            : a.backgroundColor || s.backgroundColor,
                          borderWidth: r.borderWidth ? r.borderWidth : a.borderWidth || s.borderWidth,
                          borderColor: r.borderColor ? r.borderColor : a.borderColor || s.borderColor,
                          fill: r.fill ? r.fill : void 0 !== a.fill ? a.fill : s.fill,
                          borderCapStyle: r.borderCapStyle ? r.borderCapStyle : a.borderCapStyle || s.borderCapStyle,
                          borderDash: r.borderDash ? r.borderDash : a.borderDash || s.borderDash,
                          borderDashOffset: r.borderDashOffset
                            ? r.borderDashOffset
                            : a.borderDashOffset || s.borderDashOffset,
                          borderJoinStyle: r.borderJoinStyle
                            ? r.borderJoinStyle
                            : a.borderJoinStyle || s.borderJoinStyle
                        }
                      }),
                      n.dataset.pivot(),
                      o.each(
                        i,
                        function(n, i) {
                          e.updateElement(n, i, t);
                        },
                        e
                      ),
                      e.updateBezierControlPoints();
                  },
                  updateElement: function(t, e, n) {
                    var i = this,
                      r = t.custom || {},
                      a = i.getDataset(),
                      s = i.chart.scale,
                      l = i.chart.options.elements.point,
                      u = s.getPointPositionForValue(e, a.data[e]);
                    void 0 !== a.radius && void 0 === a.pointRadius && (a.pointRadius = a.radius),
                      void 0 !== a.hitRadius && void 0 === a.pointHitRadius && (a.pointHitRadius = a.hitRadius),
                      o.extend(t, {
                        _datasetIndex: i.index,
                        _index: e,
                        _scale: s,
                        _model: {
                          x: n ? s.xCenter : u.x,
                          y: n ? s.yCenter : u.y,
                          tension: r.tension
                            ? r.tension
                            : o.valueOrDefault(a.lineTension, i.chart.options.elements.line.tension),
                          radius: r.radius ? r.radius : o.valueAtIndexOrDefault(a.pointRadius, e, l.radius),
                          backgroundColor: r.backgroundColor
                            ? r.backgroundColor
                            : o.valueAtIndexOrDefault(a.pointBackgroundColor, e, l.backgroundColor),
                          borderColor: r.borderColor
                            ? r.borderColor
                            : o.valueAtIndexOrDefault(a.pointBorderColor, e, l.borderColor),
                          borderWidth: r.borderWidth
                            ? r.borderWidth
                            : o.valueAtIndexOrDefault(a.pointBorderWidth, e, l.borderWidth),
                          pointStyle: r.pointStyle
                            ? r.pointStyle
                            : o.valueAtIndexOrDefault(a.pointStyle, e, l.pointStyle),
                          hitRadius: r.hitRadius
                            ? r.hitRadius
                            : o.valueAtIndexOrDefault(a.pointHitRadius, e, l.hitRadius)
                        }
                      }),
                      (t._model.skip = r.skip ? r.skip : isNaN(t._model.x) || isNaN(t._model.y));
                  },
                  updateBezierControlPoints: function() {
                    var t = this.chart.chartArea,
                      e = this.getMeta();
                    o.each(e.data, function(n, i) {
                      var r = n._model,
                        a = o.splineCurve(
                          o.previousItem(e.data, i, !0)._model,
                          r,
                          o.nextItem(e.data, i, !0)._model,
                          r.tension
                        );
                      (r.controlPointPreviousX = Math.max(Math.min(a.previous.x, t.right), t.left)),
                        (r.controlPointPreviousY = Math.max(Math.min(a.previous.y, t.bottom), t.top)),
                        (r.controlPointNextX = Math.max(Math.min(a.next.x, t.right), t.left)),
                        (r.controlPointNextY = Math.max(Math.min(a.next.y, t.bottom), t.top)),
                        n.pivot();
                    });
                  },
                  setHoverStyle: function(t) {
                    var e = this.chart.data.datasets[t._datasetIndex],
                      n = t.custom || {},
                      i = t._index,
                      r = t._model;
                    (r.radius = n.hoverRadius
                      ? n.hoverRadius
                      : o.valueAtIndexOrDefault(e.pointHoverRadius, i, this.chart.options.elements.point.hoverRadius)),
                      (r.backgroundColor = n.hoverBackgroundColor
                        ? n.hoverBackgroundColor
                        : o.valueAtIndexOrDefault(e.pointHoverBackgroundColor, i, o.getHoverColor(r.backgroundColor))),
                      (r.borderColor = n.hoverBorderColor
                        ? n.hoverBorderColor
                        : o.valueAtIndexOrDefault(e.pointHoverBorderColor, i, o.getHoverColor(r.borderColor))),
                      (r.borderWidth = n.hoverBorderWidth
                        ? n.hoverBorderWidth
                        : o.valueAtIndexOrDefault(e.pointHoverBorderWidth, i, r.borderWidth));
                  },
                  removeHoverStyle: function(t) {
                    var e = this.chart.data.datasets[t._datasetIndex],
                      n = t.custom || {},
                      i = t._index,
                      r = t._model,
                      a = this.chart.options.elements.point;
                    (r.radius = n.radius ? n.radius : o.valueAtIndexOrDefault(e.pointRadius, i, a.radius)),
                      (r.backgroundColor = n.backgroundColor
                        ? n.backgroundColor
                        : o.valueAtIndexOrDefault(e.pointBackgroundColor, i, a.backgroundColor)),
                      (r.borderColor = n.borderColor
                        ? n.borderColor
                        : o.valueAtIndexOrDefault(e.pointBorderColor, i, a.borderColor)),
                      (r.borderWidth = n.borderWidth
                        ? n.borderWidth
                        : o.valueAtIndexOrDefault(e.pointBorderWidth, i, a.borderWidth));
                  }
                });
              });
          },
          { 25: 25, 40: 40, 45: 45 }
        ],
        21: [
          function(t, e, n) {
            'use strict';
            t(25)._set('scatter', {
              hover: { mode: 'single' },
              scales: {
                xAxes: [{ id: 'x-axis-1', type: 'linear', position: 'bottom' }],
                yAxes: [{ id: 'y-axis-1', type: 'linear', position: 'left' }]
              },
              showLines: !1,
              tooltips: {
                callbacks: {
                  title: function() {
                    return '';
                  },
                  label: function(t) {
                    return '(' + t.xLabel + ', ' + t.yLabel + ')';
                  }
                }
              }
            }),
              (e.exports = function(t) {
                t.controllers.scatter = t.controllers.line;
              });
          },
          { 25: 25 }
        ],
        22: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(26),
              o = t(45);
            i._set('global', {
              animation: { duration: 1e3, easing: 'easeOutQuart', onProgress: o.noop, onComplete: o.noop }
            }),
              (e.exports = function(t) {
                (t.Animation = r.extend({
                  chart: null,
                  currentStep: 0,
                  numSteps: 60,
                  easing: '',
                  render: null,
                  onAnimationProgress: null,
                  onAnimationComplete: null
                })),
                  (t.animationService = {
                    frameDuration: 17,
                    animations: [],
                    dropFrames: 0,
                    request: null,
                    addAnimation: function(t, e, n, i) {
                      var r,
                        o,
                        a = this.animations;
                      for (e.chart = t, i || (t.animating = !0), r = 0, o = a.length; r < o; ++r)
                        if (a[r].chart === t) return void (a[r] = e);
                      a.push(e), 1 === a.length && this.requestAnimationFrame();
                    },
                    cancelAnimation: function(t) {
                      var e = o.findIndex(this.animations, function(e) {
                        return e.chart === t;
                      });
                      -1 !== e && (this.animations.splice(e, 1), (t.animating = !1));
                    },
                    requestAnimationFrame: function() {
                      var t = this;
                      null === t.request &&
                        (t.request = o.requestAnimFrame.call(window, function() {
                          (t.request = null), t.startDigest();
                        }));
                    },
                    startDigest: function() {
                      var t = this,
                        e = Date.now(),
                        n = 0;
                      t.dropFrames > 1 && ((n = Math.floor(t.dropFrames)), (t.dropFrames = t.dropFrames % 1)),
                        t.advance(1 + n);
                      var i = Date.now();
                      (t.dropFrames += (i - e) / t.frameDuration), t.animations.length > 0 && t.requestAnimationFrame();
                    },
                    advance: function(t) {
                      for (var e, n, i = this.animations, r = 0; r < i.length; )
                        (n = (e = i[r]).chart),
                          (e.currentStep = (e.currentStep || 0) + t),
                          (e.currentStep = Math.min(e.currentStep, e.numSteps)),
                          o.callback(e.render, [n, e], n),
                          o.callback(e.onAnimationProgress, [e], n),
                          e.currentStep >= e.numSteps
                            ? (o.callback(e.onAnimationComplete, [e], n), (n.animating = !1), i.splice(r, 1))
                            : ++r;
                    }
                  }),
                  Object.defineProperty(t.Animation.prototype, 'animationObject', {
                    get: function() {
                      return this;
                    }
                  }),
                  Object.defineProperty(t.Animation.prototype, 'chartInstance', {
                    get: function() {
                      return this.chart;
                    },
                    set: function(t) {
                      this.chart = t;
                    }
                  });
              });
          },
          { 25: 25, 26: 26, 45: 45 }
        ],
        23: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(45),
              o = t(28),
              a = t(30),
              s = t(48),
              l = t(31);
            e.exports = function(t) {
              function e(t) {
                return 'top' === t || 'bottom' === t;
              }
              (t.types = {}),
                (t.instances = {}),
                (t.controllers = {}),
                r.extend(t.prototype, {
                  construct: function(e, n) {
                    var o,
                      a,
                      l = this;
                    ((a = (o = (o = n) || {}).data = o.data || {}).datasets = a.datasets || []),
                      (a.labels = a.labels || []),
                      (o.options = r.configMerge(i.global, i[o.type], o.options || {}));
                    var u = s.acquireContext(e, (n = o)),
                      c = u && u.canvas,
                      d = c && c.height,
                      h = c && c.width;
                    (l.id = r.uid()),
                      (l.ctx = u),
                      (l.canvas = c),
                      (l.config = n),
                      (l.width = h),
                      (l.height = d),
                      (l.aspectRatio = d ? h / d : null),
                      (l.options = n.options),
                      (l._bufferedRender = !1),
                      (l.chart = l),
                      (l.controller = l),
                      (t.instances[l.id] = l),
                      Object.defineProperty(l, 'data', {
                        get: function() {
                          return l.config.data;
                        },
                        set: function(t) {
                          l.config.data = t;
                        }
                      }),
                      u && c
                        ? (l.initialize(), l.update())
                        : console.error("Failed to create chart: can't acquire context from the given item");
                  },
                  initialize: function() {
                    var t = this;
                    return (
                      l.notify(t, 'beforeInit'),
                      r.retinaScale(t, t.options.devicePixelRatio),
                      t.bindEvents(),
                      t.options.responsive && t.resize(!0),
                      t.ensureScalesHaveIDs(),
                      t.buildOrUpdateScales(),
                      t.initToolTip(),
                      l.notify(t, 'afterInit'),
                      t
                    );
                  },
                  clear: function() {
                    return r.canvas.clear(this), this;
                  },
                  stop: function() {
                    return t.animationService.cancelAnimation(this), this;
                  },
                  resize: function(t) {
                    var e = this,
                      n = e.options,
                      i = e.canvas,
                      o = (n.maintainAspectRatio && e.aspectRatio) || null,
                      a = Math.max(0, Math.floor(r.getMaximumWidth(i))),
                      s = Math.max(0, Math.floor(o ? a / o : r.getMaximumHeight(i)));
                    if (
                      (e.width !== a || e.height !== s) &&
                      ((i.width = e.width = a),
                      (i.height = e.height = s),
                      (i.style.width = a + 'px'),
                      (i.style.height = s + 'px'),
                      r.retinaScale(e, n.devicePixelRatio),
                      !t)
                    ) {
                      var u = { width: a, height: s };
                      l.notify(e, 'resize', [u]),
                        e.options.onResize && e.options.onResize(e, u),
                        e.stop(),
                        e.update(e.options.responsiveAnimationDuration);
                    }
                  },
                  ensureScalesHaveIDs: function() {
                    var t = this.options,
                      e = t.scales || {},
                      n = t.scale;
                    r.each(e.xAxes, function(t, e) {
                      t.id = t.id || 'x-axis-' + e;
                    }),
                      r.each(e.yAxes, function(t, e) {
                        t.id = t.id || 'y-axis-' + e;
                      }),
                      n && (n.id = n.id || 'scale');
                  },
                  buildOrUpdateScales: function() {
                    var n = this,
                      i = n.options,
                      o = n.scales || {},
                      a = [],
                      s = Object.keys(o).reduce(function(t, e) {
                        return (t[e] = !1), t;
                      }, {});
                    i.scales &&
                      (a = a.concat(
                        (i.scales.xAxes || []).map(function(t) {
                          return { options: t, dtype: 'category', dposition: 'bottom' };
                        }),
                        (i.scales.yAxes || []).map(function(t) {
                          return { options: t, dtype: 'linear', dposition: 'left' };
                        })
                      )),
                      i.scale &&
                        a.push({ options: i.scale, dtype: 'radialLinear', isDefault: !0, dposition: 'chartArea' }),
                      r.each(a, function(i) {
                        var a = i.options,
                          l = a.id,
                          u = r.valueOrDefault(a.type, i.dtype);
                        e(a.position) !== e(i.dposition) && (a.position = i.dposition), (s[l] = !0);
                        var c = null;
                        if (l in o && o[l].type === u) ((c = o[l]).options = a), (c.ctx = n.ctx), (c.chart = n);
                        else {
                          var d = t.scaleService.getScaleConstructor(u);
                          if (!d) return;
                          (c = new d({ id: l, type: u, options: a, ctx: n.ctx, chart: n })), (o[c.id] = c);
                        }
                        c.mergeTicksOptions(), i.isDefault && (n.scale = c);
                      }),
                      r.each(s, function(t, e) {
                        t || delete o[e];
                      }),
                      (n.scales = o),
                      t.scaleService.addScalesToLayout(this);
                  },
                  buildOrUpdateControllers: function() {
                    var e = this,
                      n = [],
                      i = [];
                    return (
                      r.each(
                        e.data.datasets,
                        function(r, o) {
                          var a = e.getDatasetMeta(o),
                            s = r.type || e.config.type;
                          if (
                            (a.type && a.type !== s && (e.destroyDatasetMeta(o), (a = e.getDatasetMeta(o))),
                            (a.type = s),
                            n.push(a.type),
                            a.controller)
                          )
                            a.controller.updateIndex(o), a.controller.linkScales();
                          else {
                            var l = t.controllers[a.type];
                            if (void 0 === l) throw new Error('"' + a.type + '" is not a chart type.');
                            (a.controller = new l(e, o)), i.push(a.controller);
                          }
                        },
                        e
                      ),
                      i
                    );
                  },
                  resetElements: function() {
                    var t = this;
                    r.each(
                      t.data.datasets,
                      function(e, n) {
                        t.getDatasetMeta(n).controller.reset();
                      },
                      t
                    );
                  },
                  reset: function() {
                    this.resetElements(), this.tooltip.initialize();
                  },
                  update: function(e) {
                    var n,
                      i,
                      o = this;
                    if (
                      ((e && 'object' == typeof e) || (e = { duration: e, lazy: arguments[1] }),
                      (i = (n = o).options),
                      r.each(n.scales, function(t) {
                        a.removeBox(n, t);
                      }),
                      (i = r.configMerge(t.defaults.global, t.defaults[n.config.type], i)),
                      (n.options = n.config.options = i),
                      n.ensureScalesHaveIDs(),
                      n.buildOrUpdateScales(),
                      (n.tooltip._options = i.tooltips),
                      n.tooltip.initialize(),
                      l._invalidate(o),
                      !1 !== l.notify(o, 'beforeUpdate'))
                    ) {
                      o.tooltip._data = o.data;
                      var s = o.buildOrUpdateControllers();
                      r.each(
                        o.data.datasets,
                        function(t, e) {
                          o.getDatasetMeta(e).controller.buildOrUpdateElements();
                        },
                        o
                      ),
                        o.updateLayout(),
                        o.options.animation &&
                          o.options.animation.duration &&
                          r.each(s, function(t) {
                            t.reset();
                          }),
                        o.updateDatasets(),
                        o.tooltip.initialize(),
                        (o.lastActive = []),
                        l.notify(o, 'afterUpdate'),
                        o._bufferedRender
                          ? (o._bufferedRequest = { duration: e.duration, easing: e.easing, lazy: e.lazy })
                          : o.render(e);
                    }
                  },
                  updateLayout: function() {
                    !1 !== l.notify(this, 'beforeLayout') &&
                      (a.update(this, this.width, this.height),
                      l.notify(this, 'afterScaleUpdate'),
                      l.notify(this, 'afterLayout'));
                  },
                  updateDatasets: function() {
                    if (!1 !== l.notify(this, 'beforeDatasetsUpdate')) {
                      for (var t = 0, e = this.data.datasets.length; t < e; ++t) this.updateDataset(t);
                      l.notify(this, 'afterDatasetsUpdate');
                    }
                  },
                  updateDataset: function(t) {
                    var e = this.getDatasetMeta(t),
                      n = { meta: e, index: t };
                    !1 !== l.notify(this, 'beforeDatasetUpdate', [n]) &&
                      (e.controller.update(), l.notify(this, 'afterDatasetUpdate', [n]));
                  },
                  render: function(e) {
                    var n = this;
                    (e && 'object' == typeof e) || (e = { duration: e, lazy: arguments[1] });
                    var i = e.duration,
                      o = e.lazy;
                    if (!1 !== l.notify(n, 'beforeRender')) {
                      var a = n.options.animation,
                        s = function(t) {
                          l.notify(n, 'afterRender'), r.callback(a && a.onComplete, [t], n);
                        };
                      if (a && ((void 0 !== i && 0 !== i) || (void 0 === i && 0 !== a.duration))) {
                        var u = new t.Animation({
                          numSteps: (i || a.duration) / 16.66,
                          easing: e.easing || a.easing,
                          render: function(t, e) {
                            var n = e.currentStep,
                              i = n / e.numSteps;
                            t.draw((0, r.easing.effects[e.easing])(i), i, n);
                          },
                          onAnimationProgress: a.onProgress,
                          onAnimationComplete: s
                        });
                        t.animationService.addAnimation(n, u, i, o);
                      } else n.draw(), s(new t.Animation({ numSteps: 0, chart: n }));
                      return n;
                    }
                  },
                  draw: function(t) {
                    var e = this;
                    e.clear(),
                      r.isNullOrUndef(t) && (t = 1),
                      e.transition(t),
                      !1 !== l.notify(e, 'beforeDraw', [t]) &&
                        (r.each(
                          e.boxes,
                          function(t) {
                            t.draw(e.chartArea);
                          },
                          e
                        ),
                        e.scale && e.scale.draw(),
                        e.drawDatasets(t),
                        e._drawTooltip(t),
                        l.notify(e, 'afterDraw', [t]));
                  },
                  transition: function(t) {
                    for (var e = 0, n = (this.data.datasets || []).length; e < n; ++e)
                      this.isDatasetVisible(e) && this.getDatasetMeta(e).controller.transition(t);
                    this.tooltip.transition(t);
                  },
                  drawDatasets: function(t) {
                    var e = this;
                    if (!1 !== l.notify(e, 'beforeDatasetsDraw', [t])) {
                      for (var n = (e.data.datasets || []).length - 1; n >= 0; --n)
                        e.isDatasetVisible(n) && e.drawDataset(n, t);
                      l.notify(e, 'afterDatasetsDraw', [t]);
                    }
                  },
                  drawDataset: function(t, e) {
                    var n = this.getDatasetMeta(t),
                      i = { meta: n, index: t, easingValue: e };
                    !1 !== l.notify(this, 'beforeDatasetDraw', [i]) &&
                      (n.controller.draw(e), l.notify(this, 'afterDatasetDraw', [i]));
                  },
                  _drawTooltip: function(t) {
                    var e = this.tooltip,
                      n = { tooltip: e, easingValue: t };
                    !1 !== l.notify(this, 'beforeTooltipDraw', [n]) &&
                      (e.draw(), l.notify(this, 'afterTooltipDraw', [n]));
                  },
                  getElementAtEvent: function(t) {
                    return o.modes.single(this, t);
                  },
                  getElementsAtEvent: function(t) {
                    return o.modes.label(this, t, { intersect: !0 });
                  },
                  getElementsAtXAxis: function(t) {
                    return o.modes['x-axis'](this, t, { intersect: !0 });
                  },
                  getElementsAtEventForMode: function(t, e, n) {
                    var i = o.modes[e];
                    return 'function' == typeof i ? i(this, t, n) : [];
                  },
                  getDatasetAtEvent: function(t) {
                    return o.modes.dataset(this, t, { intersect: !0 });
                  },
                  getDatasetMeta: function(t) {
                    var e = this.data.datasets[t];
                    e._meta || (e._meta = {});
                    var n = e._meta[this.id];
                    return (
                      n ||
                        (n = e._meta[this.id] = {
                          type: null,
                          data: [],
                          dataset: null,
                          controller: null,
                          hidden: null,
                          xAxisID: null,
                          yAxisID: null
                        }),
                      n
                    );
                  },
                  getVisibleDatasetCount: function() {
                    for (var t = 0, e = 0, n = this.data.datasets.length; e < n; ++e) this.isDatasetVisible(e) && t++;
                    return t;
                  },
                  isDatasetVisible: function(t) {
                    var e = this.getDatasetMeta(t);
                    return 'boolean' == typeof e.hidden ? !e.hidden : !this.data.datasets[t].hidden;
                  },
                  generateLegend: function() {
                    return this.options.legendCallback(this);
                  },
                  destroyDatasetMeta: function(t) {
                    var e = this.id,
                      n = this.data.datasets[t],
                      i = n._meta && n._meta[e];
                    i && (i.controller.destroy(), delete n._meta[e]);
                  },
                  destroy: function() {
                    var e,
                      n,
                      i = this,
                      o = i.canvas;
                    for (i.stop(), e = 0, n = i.data.datasets.length; e < n; ++e) i.destroyDatasetMeta(e);
                    o &&
                      (i.unbindEvents(), r.canvas.clear(i), s.releaseContext(i.ctx), (i.canvas = null), (i.ctx = null)),
                      l.notify(i, 'destroy'),
                      delete t.instances[i.id];
                  },
                  toBase64Image: function() {
                    return this.canvas.toDataURL.apply(this.canvas, arguments);
                  },
                  initToolTip: function() {
                    var e = this;
                    e.tooltip = new t.Tooltip(
                      { _chart: e, _chartInstance: e, _data: e.data, _options: e.options.tooltips },
                      e
                    );
                  },
                  bindEvents: function() {
                    var t = this,
                      e = (t._listeners = {}),
                      n = function() {
                        t.eventHandler.apply(t, arguments);
                      };
                    r.each(t.options.events, function(i) {
                      s.addEventListener(t, i, n), (e[i] = n);
                    }),
                      t.options.responsive &&
                        ((n = function() {
                          t.resize();
                        }),
                        s.addEventListener(t, 'resize', n),
                        (e.resize = n));
                  },
                  unbindEvents: function() {
                    var t = this,
                      e = t._listeners;
                    e &&
                      (delete t._listeners,
                      r.each(e, function(e, n) {
                        s.removeEventListener(t, n, e);
                      }));
                  },
                  updateHoverStyle: function(t, e, n) {
                    var i,
                      r,
                      o,
                      a = n ? 'setHoverStyle' : 'removeHoverStyle';
                    for (r = 0, o = t.length; r < o; ++r)
                      (i = t[r]) && this.getDatasetMeta(i._datasetIndex).controller[a](i);
                  },
                  eventHandler: function(t) {
                    var e = this,
                      n = e.tooltip;
                    if (!1 !== l.notify(e, 'beforeEvent', [t])) {
                      (e._bufferedRender = !0), (e._bufferedRequest = null);
                      var i = e.handleEvent(t);
                      n && (i = n._start ? n.handleEvent(t) : i | n.handleEvent(t)), l.notify(e, 'afterEvent', [t]);
                      var r = e._bufferedRequest;
                      return (
                        r
                          ? e.render(r)
                          : i && !e.animating && (e.stop(), e.render(e.options.hover.animationDuration, !0)),
                        (e._bufferedRender = !1),
                        (e._bufferedRequest = null),
                        e
                      );
                    }
                  },
                  handleEvent: function(t) {
                    var e,
                      n = this,
                      i = n.options || {},
                      o = i.hover;
                    return (
                      (n.lastActive = n.lastActive || []),
                      (n.active = 'mouseout' === t.type ? [] : n.getElementsAtEventForMode(t, o.mode, o)),
                      r.callback(i.onHover || i.hover.onHover, [t.native, n.active], n),
                      ('mouseup' !== t.type && 'click' !== t.type) ||
                        (i.onClick && i.onClick.call(n, t.native, n.active)),
                      n.lastActive.length && n.updateHoverStyle(n.lastActive, o.mode, !1),
                      n.active.length && o.mode && n.updateHoverStyle(n.active, o.mode, !0),
                      (e = !r.arrayEquals(n.active, n.lastActive)),
                      (n.lastActive = n.active),
                      e
                    );
                  }
                }),
                (t.Controller = t);
            };
          },
          { 25: 25, 28: 28, 30: 30, 31: 31, 45: 45, 48: 48 }
        ],
        24: [
          function(t, e, n) {
            'use strict';
            var i = t(45);
            e.exports = function(t) {
              var e = ['push', 'pop', 'shift', 'splice', 'unshift'];
              function n(t, n) {
                var i = t._chartjs;
                if (i) {
                  var r = i.listeners,
                    o = r.indexOf(n);
                  -1 !== o && r.splice(o, 1),
                    r.length > 0 ||
                      (e.forEach(function(e) {
                        delete t[e];
                      }),
                      delete t._chartjs);
                }
              }
              (t.DatasetController = function(t, e) {
                this.initialize(t, e);
              }),
                i.extend(t.DatasetController.prototype, {
                  datasetElementType: null,
                  dataElementType: null,
                  initialize: function(t, e) {
                    (this.chart = t), (this.index = e), this.linkScales(), this.addElements();
                  },
                  updateIndex: function(t) {
                    this.index = t;
                  },
                  linkScales: function() {
                    var t = this,
                      e = t.getMeta(),
                      n = t.getDataset();
                    (null !== e.xAxisID && e.xAxisID in t.chart.scales) ||
                      (e.xAxisID = n.xAxisID || t.chart.options.scales.xAxes[0].id),
                      (null !== e.yAxisID && e.yAxisID in t.chart.scales) ||
                        (e.yAxisID = n.yAxisID || t.chart.options.scales.yAxes[0].id);
                  },
                  getDataset: function() {
                    return this.chart.data.datasets[this.index];
                  },
                  getMeta: function() {
                    return this.chart.getDatasetMeta(this.index);
                  },
                  getScaleForId: function(t) {
                    return this.chart.scales[t];
                  },
                  reset: function() {
                    this.update(!0);
                  },
                  destroy: function() {
                    this._data && n(this._data, this);
                  },
                  createMetaDataset: function() {
                    var t = this.datasetElementType;
                    return t && new t({ _chart: this.chart, _datasetIndex: this.index });
                  },
                  createMetaData: function(t) {
                    var e = this.dataElementType;
                    return e && new e({ _chart: this.chart, _datasetIndex: this.index, _index: t });
                  },
                  addElements: function() {
                    var t,
                      e,
                      n = this.getMeta(),
                      i = this.getDataset().data || [],
                      r = n.data;
                    for (t = 0, e = i.length; t < e; ++t) r[t] = r[t] || this.createMetaData(t);
                    n.dataset = n.dataset || this.createMetaDataset();
                  },
                  addElementAndReset: function(t) {
                    var e = this.createMetaData(t);
                    this.getMeta().data.splice(t, 0, e), this.updateElement(e, t, !0);
                  },
                  buildOrUpdateElements: function() {
                    var t,
                      r,
                      o = this,
                      a = o.getDataset(),
                      s = a.data || (a.data = []);
                    o._data !== s &&
                      (o._data && n(o._data, o),
                      (r = o),
                      (t = s)._chartjs
                        ? t._chartjs.listeners.push(r)
                        : (Object.defineProperty(t, '_chartjs', {
                            configurable: !0,
                            enumerable: !1,
                            value: { listeners: [r] }
                          }),
                          e.forEach(function(e) {
                            var n = 'onData' + e.charAt(0).toUpperCase() + e.slice(1),
                              r = t[e];
                            Object.defineProperty(t, e, {
                              configurable: !0,
                              enumerable: !1,
                              value: function() {
                                var e = Array.prototype.slice.call(arguments),
                                  o = r.apply(this, e);
                                return (
                                  i.each(t._chartjs.listeners, function(t) {
                                    'function' == typeof t[n] && t[n].apply(t, e);
                                  }),
                                  o
                                );
                              }
                            });
                          })),
                      (o._data = s)),
                      o.resyncElements();
                  },
                  update: i.noop,
                  transition: function(t) {
                    for (var e = this.getMeta(), n = e.data || [], i = n.length, r = 0; r < i; ++r) n[r].transition(t);
                    e.dataset && e.dataset.transition(t);
                  },
                  draw: function() {
                    var t = this.getMeta(),
                      e = t.data || [],
                      n = e.length,
                      i = 0;
                    for (t.dataset && t.dataset.draw(); i < n; ++i) e[i].draw();
                  },
                  removeHoverStyle: function(t, e) {
                    var n = this.chart.data.datasets[t._datasetIndex],
                      r = t._index,
                      o = t.custom || {},
                      a = i.valueAtIndexOrDefault,
                      s = t._model;
                    (s.backgroundColor = o.backgroundColor
                      ? o.backgroundColor
                      : a(n.backgroundColor, r, e.backgroundColor)),
                      (s.borderColor = o.borderColor ? o.borderColor : a(n.borderColor, r, e.borderColor)),
                      (s.borderWidth = o.borderWidth ? o.borderWidth : a(n.borderWidth, r, e.borderWidth));
                  },
                  setHoverStyle: function(t) {
                    var e = this.chart.data.datasets[t._datasetIndex],
                      n = t._index,
                      r = t.custom || {},
                      o = i.valueAtIndexOrDefault,
                      a = i.getHoverColor,
                      s = t._model;
                    (s.backgroundColor = r.hoverBackgroundColor
                      ? r.hoverBackgroundColor
                      : o(e.hoverBackgroundColor, n, a(s.backgroundColor))),
                      (s.borderColor = r.hoverBorderColor
                        ? r.hoverBorderColor
                        : o(e.hoverBorderColor, n, a(s.borderColor))),
                      (s.borderWidth = r.hoverBorderWidth
                        ? r.hoverBorderWidth
                        : o(e.hoverBorderWidth, n, s.borderWidth));
                  },
                  resyncElements: function() {
                    var t = this.getMeta(),
                      e = this.getDataset().data,
                      n = t.data.length,
                      i = e.length;
                    i < n ? t.data.splice(i, n - i) : i > n && this.insertElements(n, i - n);
                  },
                  insertElements: function(t, e) {
                    for (var n = 0; n < e; ++n) this.addElementAndReset(t + n);
                  },
                  onDataPush: function() {
                    this.insertElements(this.getDataset().data.length - 1, arguments.length);
                  },
                  onDataPop: function() {
                    this.getMeta().data.pop();
                  },
                  onDataShift: function() {
                    this.getMeta().data.shift();
                  },
                  onDataSplice: function(t, e) {
                    this.getMeta().data.splice(t, e), this.insertElements(t, arguments.length - 2);
                  },
                  onDataUnshift: function() {
                    this.insertElements(0, arguments.length);
                  }
                }),
                (t.DatasetController.extend = i.inherits);
            };
          },
          { 45: 45 }
        ],
        25: [
          function(t, e, n) {
            'use strict';
            var i = t(45);
            e.exports = {
              _set: function(t, e) {
                return i.merge(this[t] || (this[t] = {}), e);
              }
            };
          },
          { 45: 45 }
        ],
        26: [
          function(t, e, n) {
            'use strict';
            var i = t(3),
              r = t(45),
              o = function(t) {
                r.extend(this, t), this.initialize.apply(this, arguments);
              };
            r.extend(o.prototype, {
              initialize: function() {
                this.hidden = !1;
              },
              pivot: function() {
                var t = this;
                return t._view || (t._view = r.clone(t._model)), (t._start = {}), t;
              },
              transition: function(t) {
                var e = this,
                  n = e._model,
                  r = e._start,
                  o = e._view;
                return n && 1 !== t
                  ? (o || (o = e._view = {}),
                    r || (r = e._start = {}),
                    (function(t, e, n, r) {
                      var o,
                        a,
                        s,
                        l,
                        u,
                        c,
                        d,
                        h,
                        f,
                        p = Object.keys(n);
                      for (o = 0, a = p.length; o < a; ++o)
                        if (
                          ((c = n[(s = p[o])]), e.hasOwnProperty(s) || (e[s] = c), (l = e[s]) !== c && '_' !== s[0])
                        ) {
                          if ((t.hasOwnProperty(s) || (t[s] = l), (d = typeof c) == typeof (u = t[s])))
                            if ('string' === d) {
                              if ((h = i(u)).valid && (f = i(c)).valid) {
                                e[s] = f.mix(h, r).rgbString();
                                continue;
                              }
                            } else if ('number' === d && isFinite(u) && isFinite(c)) {
                              e[s] = u + (c - u) * r;
                              continue;
                            }
                          e[s] = c;
                        }
                    })(r, o, n, t),
                    e)
                  : ((e._view = n), (e._start = null), e);
              },
              tooltipPosition: function() {
                return { x: this._model.x, y: this._model.y };
              },
              hasValue: function() {
                return r.isNumber(this._model.x) && r.isNumber(this._model.y);
              }
            }),
              (o.extend = r.inherits),
              (e.exports = o);
          },
          { 3: 3, 45: 45 }
        ],
        27: [
          function(t, e, n) {
            'use strict';
            var i = t(3),
              r = t(25),
              o = t(45);
            e.exports = function(t) {
              function e(t, e, n) {
                var i;
                return (
                  'string' == typeof t
                    ? ((i = parseInt(t, 10)), -1 !== t.indexOf('%') && (i = (i / 100) * e.parentNode[n]))
                    : (i = t),
                  i
                );
              }
              function n(t) {
                return null != t && 'none' !== t;
              }
              function a(t, i, r) {
                var o = document.defaultView,
                  a = t.parentNode,
                  s = o.getComputedStyle(t)[i],
                  l = o.getComputedStyle(a)[i],
                  u = n(s),
                  c = n(l),
                  d = Number.POSITIVE_INFINITY;
                return u || c ? Math.min(u ? e(s, t, r) : d, c ? e(l, a, r) : d) : 'none';
              }
              (o.configMerge = function() {
                return o.merge(o.clone(arguments[0]), [].slice.call(arguments, 1), {
                  merger: function(e, n, i, r) {
                    var a = n[e] || {},
                      s = i[e];
                    'scales' === e
                      ? (n[e] = o.scaleMerge(a, s))
                      : 'scale' === e
                      ? (n[e] = o.merge(a, [t.scaleService.getScaleDefaults(s.type), s]))
                      : o._merger(e, n, i, r);
                  }
                });
              }),
                (o.scaleMerge = function() {
                  return o.merge(o.clone(arguments[0]), [].slice.call(arguments, 1), {
                    merger: function(e, n, i, r) {
                      if ('xAxes' === e || 'yAxes' === e) {
                        var a,
                          s,
                          l,
                          u = i[e].length;
                        for (n[e] || (n[e] = []), a = 0; a < u; ++a)
                          (s = o.valueOrDefault((l = i[e][a]).type, 'xAxes' === e ? 'category' : 'linear')),
                            a >= n[e].length && n[e].push({}),
                            o.merge(
                              n[e][a],
                              !n[e][a].type || (l.type && l.type !== n[e][a].type)
                                ? [t.scaleService.getScaleDefaults(s), l]
                                : l
                            );
                      } else o._merger(e, n, i, r);
                    }
                  });
                }),
                (o.where = function(t, e) {
                  if (o.isArray(t) && Array.prototype.filter) return t.filter(e);
                  var n = [];
                  return (
                    o.each(t, function(t) {
                      e(t) && n.push(t);
                    }),
                    n
                  );
                }),
                (o.findIndex = Array.prototype.findIndex
                  ? function(t, e, n) {
                      return t.findIndex(e, n);
                    }
                  : function(t, e, n) {
                      n = void 0 === n ? t : n;
                      for (var i = 0, r = t.length; i < r; ++i) if (e.call(n, t[i], i, t)) return i;
                      return -1;
                    }),
                (o.findNextWhere = function(t, e, n) {
                  o.isNullOrUndef(n) && (n = -1);
                  for (var i = n + 1; i < t.length; i++) {
                    var r = t[i];
                    if (e(r)) return r;
                  }
                }),
                (o.findPreviousWhere = function(t, e, n) {
                  o.isNullOrUndef(n) && (n = t.length);
                  for (var i = n - 1; i >= 0; i--) {
                    var r = t[i];
                    if (e(r)) return r;
                  }
                }),
                (o.isNumber = function(t) {
                  return !isNaN(parseFloat(t)) && isFinite(t);
                }),
                (o.almostEquals = function(t, e, n) {
                  return Math.abs(t - e) < n;
                }),
                (o.almostWhole = function(t, e) {
                  var n = Math.round(t);
                  return n - e < t && n + e > t;
                }),
                (o.max = function(t) {
                  return t.reduce(function(t, e) {
                    return isNaN(e) ? t : Math.max(t, e);
                  }, Number.NEGATIVE_INFINITY);
                }),
                (o.min = function(t) {
                  return t.reduce(function(t, e) {
                    return isNaN(e) ? t : Math.min(t, e);
                  }, Number.POSITIVE_INFINITY);
                }),
                (o.sign = Math.sign
                  ? function(t) {
                      return Math.sign(t);
                    }
                  : function(t) {
                      return 0 == (t = +t) || isNaN(t) ? t : t > 0 ? 1 : -1;
                    }),
                (o.log10 = Math.log10
                  ? function(t) {
                      return Math.log10(t);
                    }
                  : function(t) {
                      var e = Math.log(t) * Math.LOG10E,
                        n = Math.round(e);
                      return t === Math.pow(10, n) ? n : e;
                    }),
                (o.toRadians = function(t) {
                  return t * (Math.PI / 180);
                }),
                (o.toDegrees = function(t) {
                  return t * (180 / Math.PI);
                }),
                (o.getAngleFromPoint = function(t, e) {
                  var n = e.x - t.x,
                    i = e.y - t.y,
                    r = Math.sqrt(n * n + i * i),
                    o = Math.atan2(i, n);
                  return o < -0.5 * Math.PI && (o += 2 * Math.PI), { angle: o, distance: r };
                }),
                (o.distanceBetweenPoints = function(t, e) {
                  return Math.sqrt(Math.pow(e.x - t.x, 2) + Math.pow(e.y - t.y, 2));
                }),
                (o.aliasPixel = function(t) {
                  return t % 2 == 0 ? 0 : 0.5;
                }),
                (o.splineCurve = function(t, e, n, i) {
                  var r = t.skip ? e : t,
                    o = e,
                    a = n.skip ? e : n,
                    s = Math.sqrt(Math.pow(o.x - r.x, 2) + Math.pow(o.y - r.y, 2)),
                    l = Math.sqrt(Math.pow(a.x - o.x, 2) + Math.pow(a.y - o.y, 2)),
                    u = s / (s + l),
                    c = l / (s + l),
                    d = i * (u = isNaN(u) ? 0 : u),
                    h = i * (c = isNaN(c) ? 0 : c);
                  return {
                    previous: { x: o.x - d * (a.x - r.x), y: o.y - d * (a.y - r.y) },
                    next: { x: o.x + h * (a.x - r.x), y: o.y + h * (a.y - r.y) }
                  };
                }),
                (o.EPSILON = Number.EPSILON || 1e-14),
                (o.splineCurveMonotone = function(t) {
                  var e,
                    n,
                    i,
                    r,
                    a,
                    s,
                    l,
                    u,
                    c,
                    d = (t || []).map(function(t) {
                      return { model: t._model, deltaK: 0, mK: 0 };
                    }),
                    h = d.length;
                  for (e = 0; e < h; ++e)
                    if (!(i = d[e]).model.skip) {
                      if (((n = e > 0 ? d[e - 1] : null), (r = e < h - 1 ? d[e + 1] : null) && !r.model.skip)) {
                        var f = r.model.x - i.model.x;
                        i.deltaK = 0 !== f ? (r.model.y - i.model.y) / f : 0;
                      }
                      i.mK =
                        !n || n.model.skip
                          ? i.deltaK
                          : !r || r.model.skip
                          ? n.deltaK
                          : this.sign(n.deltaK) !== this.sign(i.deltaK)
                          ? 0
                          : (n.deltaK + i.deltaK) / 2;
                    }
                  for (e = 0; e < h - 1; ++e)
                    (r = d[e + 1]),
                      (i = d[e]).model.skip ||
                        r.model.skip ||
                        (o.almostEquals(i.deltaK, 0, this.EPSILON)
                          ? (i.mK = r.mK = 0)
                          : ((a = i.mK / i.deltaK),
                            (s = r.mK / i.deltaK),
                            (u = Math.pow(a, 2) + Math.pow(s, 2)) <= 9 ||
                              ((l = 3 / Math.sqrt(u)), (i.mK = a * l * i.deltaK), (r.mK = s * l * i.deltaK))));
                  for (e = 0; e < h; ++e)
                    (i = d[e]).model.skip ||
                      ((r = e < h - 1 ? d[e + 1] : null),
                      (n = e > 0 ? d[e - 1] : null) &&
                        !n.model.skip &&
                        ((i.model.controlPointPreviousX = i.model.x - (c = (i.model.x - n.model.x) / 3)),
                        (i.model.controlPointPreviousY = i.model.y - c * i.mK)),
                      r &&
                        !r.model.skip &&
                        ((i.model.controlPointNextX = i.model.x + (c = (r.model.x - i.model.x) / 3)),
                        (i.model.controlPointNextY = i.model.y + c * i.mK)));
                }),
                (o.nextItem = function(t, e, n) {
                  return n ? (e >= t.length - 1 ? t[0] : t[e + 1]) : e >= t.length - 1 ? t[t.length - 1] : t[e + 1];
                }),
                (o.previousItem = function(t, e, n) {
                  return n ? (e <= 0 ? t[t.length - 1] : t[e - 1]) : e <= 0 ? t[0] : t[e - 1];
                }),
                (o.niceNum = function(t, e) {
                  var n = Math.floor(o.log10(t)),
                    i = t / Math.pow(10, n);
                  return (
                    (e ? (i < 1.5 ? 1 : i < 3 ? 2 : i < 7 ? 5 : 10) : i <= 1 ? 1 : i <= 2 ? 2 : i <= 5 ? 5 : 10) *
                    Math.pow(10, n)
                  );
                }),
                (o.requestAnimFrame =
                  'undefined' == typeof window
                    ? function(t) {
                        t();
                      }
                    : window.requestAnimationFrame ||
                      window.webkitRequestAnimationFrame ||
                      window.mozRequestAnimationFrame ||
                      window.oRequestAnimationFrame ||
                      window.msRequestAnimationFrame ||
                      function(t) {
                        return window.setTimeout(t, 1e3 / 60);
                      }),
                (o.getRelativePosition = function(t, e) {
                  var n,
                    i,
                    r = t.originalEvent || t,
                    a = t.currentTarget || t.srcElement,
                    s = a.getBoundingClientRect(),
                    l = r.touches;
                  l && l.length > 0 ? ((n = l[0].clientX), (i = l[0].clientY)) : ((n = r.clientX), (i = r.clientY));
                  var u = parseFloat(o.getStyle(a, 'padding-left')),
                    c = parseFloat(o.getStyle(a, 'padding-top')),
                    d = parseFloat(o.getStyle(a, 'padding-right')),
                    h = parseFloat(o.getStyle(a, 'padding-bottom')),
                    f = s.bottom - s.top - c - h;
                  return {
                    x: (n = Math.round(
                      (((n - s.left - u) / (s.right - s.left - u - d)) * a.width) / e.currentDevicePixelRatio
                    )),
                    y: (i = Math.round((((i - s.top - c) / f) * a.height) / e.currentDevicePixelRatio))
                  };
                }),
                (o.getConstraintWidth = function(t) {
                  return a(t, 'max-width', 'clientWidth');
                }),
                (o.getConstraintHeight = function(t) {
                  return a(t, 'max-height', 'clientHeight');
                }),
                (o.getMaximumWidth = function(t) {
                  var e = t.parentNode;
                  if (!e) return t.clientWidth;
                  var n = parseInt(o.getStyle(e, 'padding-left'), 10),
                    i = parseInt(o.getStyle(e, 'padding-right'), 10),
                    r = e.clientWidth - n - i,
                    a = o.getConstraintWidth(t);
                  return isNaN(a) ? r : Math.min(r, a);
                }),
                (o.getMaximumHeight = function(t) {
                  var e = t.parentNode;
                  if (!e) return t.clientHeight;
                  var n = parseInt(o.getStyle(e, 'padding-top'), 10),
                    i = parseInt(o.getStyle(e, 'padding-bottom'), 10),
                    r = e.clientHeight - n - i,
                    a = o.getConstraintHeight(t);
                  return isNaN(a) ? r : Math.min(r, a);
                }),
                (o.getStyle = function(t, e) {
                  return t.currentStyle
                    ? t.currentStyle[e]
                    : document.defaultView.getComputedStyle(t, null).getPropertyValue(e);
                }),
                (o.retinaScale = function(t, e) {
                  var n = (t.currentDevicePixelRatio = e || window.devicePixelRatio || 1);
                  if (1 !== n) {
                    var i = t.canvas,
                      r = t.height,
                      o = t.width;
                    (i.height = r * n),
                      (i.width = o * n),
                      t.ctx.scale(n, n),
                      i.style.height || i.style.width || ((i.style.height = r + 'px'), (i.style.width = o + 'px'));
                  }
                }),
                (o.fontString = function(t, e, n) {
                  return e + ' ' + t + 'px ' + n;
                }),
                (o.longestText = function(t, e, n, i) {
                  var r = ((i = i || {}).data = i.data || {}),
                    a = (i.garbageCollect = i.garbageCollect || []);
                  i.font !== e && ((r = i.data = {}), (a = i.garbageCollect = []), (i.font = e)), (t.font = e);
                  var s = 0;
                  o.each(n, function(e) {
                    null != e && !0 !== o.isArray(e)
                      ? (s = o.measureText(t, r, a, s, e))
                      : o.isArray(e) &&
                        o.each(e, function(e) {
                          null == e || o.isArray(e) || (s = o.measureText(t, r, a, s, e));
                        });
                  });
                  var l = a.length / 2;
                  if (l > n.length) {
                    for (var u = 0; u < l; u++) delete r[a[u]];
                    a.splice(0, l);
                  }
                  return s;
                }),
                (o.measureText = function(t, e, n, i, r) {
                  var o = e[r];
                  return o || ((o = e[r] = t.measureText(r).width), n.push(r)), o > i && (i = o), i;
                }),
                (o.numberOfLabelLines = function(t) {
                  var e = 1;
                  return (
                    o.each(t, function(t) {
                      o.isArray(t) && t.length > e && (e = t.length);
                    }),
                    e
                  );
                }),
                (o.color = i
                  ? function(t) {
                      return t instanceof CanvasGradient && (t = r.global.defaultColor), i(t);
                    }
                  : function(t) {
                      return console.error('Color.js not found!'), t;
                    }),
                (o.getHoverColor = function(t) {
                  return t instanceof CanvasPattern
                    ? t
                    : o
                        .color(t)
                        .saturate(0.5)
                        .darken(0.1)
                        .rgbString();
                });
            };
          },
          { 25: 25, 3: 3, 45: 45 }
        ],
        28: [
          function(t, e, n) {
            'use strict';
            var i = t(45);
            function r(t, e) {
              return t.native ? { x: t.x, y: t.y } : i.getRelativePosition(t, e);
            }
            function o(t, e) {
              var n, i, r, o, a;
              for (i = 0, o = t.data.datasets.length; i < o; ++i)
                if (t.isDatasetVisible(i))
                  for (r = 0, a = (n = t.getDatasetMeta(i)).data.length; r < a; ++r) {
                    var s = n.data[r];
                    s._view.skip || e(s);
                  }
            }
            function a(t, e) {
              var n = [];
              return (
                o(t, function(t) {
                  t.inRange(e.x, e.y) && n.push(t);
                }),
                n
              );
            }
            function s(t, e, n, i) {
              var r = Number.POSITIVE_INFINITY,
                a = [];
              return (
                o(t, function(t) {
                  if (!n || t.inRange(e.x, e.y)) {
                    var o = t.getCenterPoint(),
                      s = i(e, o);
                    s < r ? ((a = [t]), (r = s)) : s === r && a.push(t);
                  }
                }),
                a
              );
            }
            function l(t) {
              var e = -1 !== t.indexOf('x'),
                n = -1 !== t.indexOf('y');
              return function(t, i) {
                var r = e ? Math.abs(t.x - i.x) : 0,
                  o = n ? Math.abs(t.y - i.y) : 0;
                return Math.sqrt(Math.pow(r, 2) + Math.pow(o, 2));
              };
            }
            function u(t, e, n) {
              var i = r(e, t);
              n.axis = n.axis || 'x';
              var o = l(n.axis),
                u = n.intersect ? a(t, i) : s(t, i, !1, o),
                c = [];
              return u.length
                ? (t.data.datasets.forEach(function(e, n) {
                    if (t.isDatasetVisible(n)) {
                      var i = t.getDatasetMeta(n).data[u[0]._index];
                      i && !i._view.skip && c.push(i);
                    }
                  }),
                  c)
                : [];
            }
            e.exports = {
              modes: {
                single: function(t, e) {
                  var n = r(e, t),
                    i = [];
                  return (
                    o(t, function(t) {
                      if (t.inRange(n.x, n.y)) return i.push(t), i;
                    }),
                    i.slice(0, 1)
                  );
                },
                label: u,
                index: u,
                dataset: function(t, e, n) {
                  var i = r(e, t);
                  n.axis = n.axis || 'xy';
                  var o = l(n.axis),
                    u = n.intersect ? a(t, i) : s(t, i, !1, o);
                  return u.length > 0 && (u = t.getDatasetMeta(u[0]._datasetIndex).data), u;
                },
                'x-axis': function(t, e) {
                  return u(t, e, { intersect: !1 });
                },
                point: function(t, e) {
                  return a(t, r(e, t));
                },
                nearest: function(t, e, n) {
                  var i = r(e, t);
                  n.axis = n.axis || 'xy';
                  var o = l(n.axis),
                    a = s(t, i, n.intersect, o);
                  return (
                    a.length > 1 &&
                      a.sort(function(t, e) {
                        var n = t.getArea() - e.getArea();
                        return 0 === n && (n = t._datasetIndex - e._datasetIndex), n;
                      }),
                    a.slice(0, 1)
                  );
                },
                x: function(t, e, n) {
                  var i = r(e, t),
                    a = [],
                    s = !1;
                  return (
                    o(t, function(t) {
                      t.inXRange(i.x) && a.push(t), t.inRange(i.x, i.y) && (s = !0);
                    }),
                    n.intersect && !s && (a = []),
                    a
                  );
                },
                y: function(t, e, n) {
                  var i = r(e, t),
                    a = [],
                    s = !1;
                  return (
                    o(t, function(t) {
                      t.inYRange(i.y) && a.push(t), t.inRange(i.x, i.y) && (s = !0);
                    }),
                    n.intersect && !s && (a = []),
                    a
                  );
                }
              }
            };
          },
          { 45: 45 }
        ],
        29: [
          function(t, e, n) {
            'use strict';
            t(25)._set('global', {
              responsive: !0,
              responsiveAnimationDuration: 0,
              maintainAspectRatio: !0,
              events: ['mousemove', 'mouseout', 'click', 'touchstart', 'touchmove'],
              hover: { onHover: null, mode: 'nearest', intersect: !0, animationDuration: 400 },
              onClick: null,
              defaultColor: 'rgba(0,0,0,0.1)',
              defaultFontColor: '#666',
              defaultFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
              defaultFontSize: 12,
              defaultFontStyle: 'normal',
              showLines: !0,
              elements: {},
              layout: { padding: { top: 0, right: 0, bottom: 0, left: 0 } }
            }),
              (e.exports = function() {
                var t = function(t, e) {
                  return this.construct(t, e), this;
                };
                return (t.Chart = t), t;
              });
          },
          { 25: 25 }
        ],
        30: [
          function(t, e, n) {
            'use strict';
            var i = t(45);
            function r(t, e) {
              return i.where(t, function(t) {
                return t.position === e;
              });
            }
            function o(t, e) {
              t.forEach(function(t, e) {
                return (t._tmpIndex_ = e), t;
              }),
                t.sort(function(t, n) {
                  var i = e ? n : t,
                    r = e ? t : n;
                  return i.weight === r.weight ? i._tmpIndex_ - r._tmpIndex_ : i.weight - r.weight;
                }),
                t.forEach(function(t) {
                  delete t._tmpIndex_;
                });
            }
            e.exports = {
              defaults: {},
              addBox: function(t, e) {
                t.boxes || (t.boxes = []),
                  (e.fullWidth = e.fullWidth || !1),
                  (e.position = e.position || 'top'),
                  (e.weight = e.weight || 0),
                  t.boxes.push(e);
              },
              removeBox: function(t, e) {
                var n = t.boxes ? t.boxes.indexOf(e) : -1;
                -1 !== n && t.boxes.splice(n, 1);
              },
              configure: function(t, e, n) {
                for (var i, r = ['fullWidth', 'position', 'weight'], o = r.length, a = 0; a < o; ++a)
                  n.hasOwnProperty((i = r[a])) && (e[i] = n[i]);
              },
              update: function(t, e, n) {
                if (t) {
                  var a = i.options.toPadding((t.options.layout || {}).padding),
                    s = a.left,
                    l = a.right,
                    u = a.top,
                    c = a.bottom,
                    d = r(t.boxes, 'left'),
                    h = r(t.boxes, 'right'),
                    f = r(t.boxes, 'top'),
                    p = r(t.boxes, 'bottom'),
                    g = r(t.boxes, 'chartArea');
                  o(d, !0), o(h, !1), o(f, !0), o(p, !1);
                  var m = e - s - l,
                    v = n - u - c,
                    b = (e - m / 2) / (d.length + h.length),
                    y = (n - v / 2) / (f.length + p.length),
                    x = m,
                    w = v,
                    _ = [];
                  i.each(d.concat(h, f, p), function(t) {
                    var e,
                      n = t.isHorizontal();
                    n
                      ? ((e = t.update(t.fullWidth ? m : x, y)), (w -= e.height))
                      : ((e = t.update(b, w)), (x -= e.width)),
                      _.push({ horizontal: n, minSize: e, box: t });
                  });
                  var C = 0,
                    k = 0,
                    S = 0,
                    T = 0;
                  i.each(f.concat(p), function(t) {
                    if (t.getPadding) {
                      var e = t.getPadding();
                      (C = Math.max(C, e.left)), (k = Math.max(k, e.right));
                    }
                  }),
                    i.each(d.concat(h), function(t) {
                      if (t.getPadding) {
                        var e = t.getPadding();
                        (S = Math.max(S, e.top)), (T = Math.max(T, e.bottom));
                      }
                    });
                  var D = s,
                    A = l,
                    E = u,
                    M = c;
                  i.each(d.concat(h), R),
                    i.each(d, function(t) {
                      D += t.width;
                    }),
                    i.each(h, function(t) {
                      A += t.width;
                    }),
                    i.each(f.concat(p), R),
                    i.each(f, function(t) {
                      E += t.height;
                    }),
                    i.each(p, function(t) {
                      M += t.height;
                    }),
                    i.each(d.concat(h), function(t) {
                      var e = i.findNextWhere(_, function(e) {
                        return e.box === t;
                      });
                      e && t.update(e.minSize.width, w, { left: 0, right: 0, top: E, bottom: M });
                    }),
                    (D = s),
                    (A = l),
                    (E = u),
                    (M = c),
                    i.each(d, function(t) {
                      D += t.width;
                    }),
                    i.each(h, function(t) {
                      A += t.width;
                    }),
                    i.each(f, function(t) {
                      E += t.height;
                    }),
                    i.each(p, function(t) {
                      M += t.height;
                    });
                  var I = Math.max(C - D, 0);
                  (D += I), (A += Math.max(k - A, 0));
                  var P = Math.max(S - E, 0);
                  (E += P), (M += Math.max(T - M, 0));
                  var O = n - E - M,
                    N = e - D - A;
                  (N === x && O === w) ||
                    (i.each(d, function(t) {
                      t.height = O;
                    }),
                    i.each(h, function(t) {
                      t.height = O;
                    }),
                    i.each(f, function(t) {
                      t.fullWidth || (t.width = N);
                    }),
                    i.each(p, function(t) {
                      t.fullWidth || (t.width = N);
                    }),
                    (w = O),
                    (x = N));
                  var L = s + I,
                    F = u + P;
                  i.each(d.concat(f), j),
                    (L += x),
                    (F += w),
                    i.each(h, j),
                    i.each(p, j),
                    (t.chartArea = { left: D, top: E, right: D + x, bottom: E + w }),
                    i.each(g, function(e) {
                      (e.left = t.chartArea.left),
                        (e.top = t.chartArea.top),
                        (e.right = t.chartArea.right),
                        (e.bottom = t.chartArea.bottom),
                        e.update(x, w);
                    });
                }
                function R(t) {
                  var e = i.findNextWhere(_, function(e) {
                    return e.box === t;
                  });
                  if (e)
                    if (t.isHorizontal()) {
                      var n = { left: Math.max(D, C), right: Math.max(A, k), top: 0, bottom: 0 };
                      t.update(t.fullWidth ? m : x, v / 2, n);
                    } else t.update(e.minSize.width, w);
                }
                function j(t) {
                  t.isHorizontal()
                    ? ((t.left = t.fullWidth ? s : D),
                      (t.right = t.fullWidth ? e - l : D + x),
                      (t.top = F),
                      (t.bottom = F + t.height),
                      (F = t.bottom))
                    : ((t.left = L), (t.right = L + t.width), (t.top = E), (t.bottom = E + w), (L = t.right));
                }
              }
            };
          },
          { 45: 45 }
        ],
        31: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(45);
            i._set('global', { plugins: {} }),
              (e.exports = {
                _plugins: [],
                _cacheId: 0,
                register: function(t) {
                  var e = this._plugins;
                  [].concat(t).forEach(function(t) {
                    -1 === e.indexOf(t) && e.push(t);
                  }),
                    this._cacheId++;
                },
                unregister: function(t) {
                  var e = this._plugins;
                  [].concat(t).forEach(function(t) {
                    var n = e.indexOf(t);
                    -1 !== n && e.splice(n, 1);
                  }),
                    this._cacheId++;
                },
                clear: function() {
                  (this._plugins = []), this._cacheId++;
                },
                count: function() {
                  return this._plugins.length;
                },
                getAll: function() {
                  return this._plugins;
                },
                notify: function(t, e, n) {
                  var i,
                    r,
                    o,
                    a,
                    s,
                    l = this.descriptors(t),
                    u = l.length;
                  for (i = 0; i < u; ++i)
                    if (
                      'function' == typeof (s = (o = (r = l[i]).plugin)[e]) &&
                      ((a = [t].concat(n || [])).push(r.options), !1 === s.apply(o, a))
                    )
                      return !1;
                  return !0;
                },
                descriptors: function(t) {
                  var e = t.$plugins || (t.$plugins = {});
                  if (e.id === this._cacheId) return e.descriptors;
                  var n = [],
                    o = [],
                    a = (t && t.config) || {},
                    s = (a.options && a.options.plugins) || {};
                  return (
                    this._plugins.concat(a.plugins || []).forEach(function(t) {
                      if (-1 === n.indexOf(t)) {
                        var e = t.id,
                          a = s[e];
                        !1 !== a &&
                          (!0 === a && (a = r.clone(i.global.plugins[e])),
                          n.push(t),
                          o.push({ plugin: t, options: a || {} }));
                      }
                    }),
                    (e.descriptors = o),
                    (e.id = this._cacheId),
                    o
                  );
                },
                _invalidate: function(t) {
                  delete t.$plugins;
                }
              });
          },
          { 25: 25, 45: 45 }
        ],
        32: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(26),
              o = t(45),
              a = t(34);
            function s(t) {
              var e,
                n,
                i = [];
              for (e = 0, n = t.length; e < n; ++e) i.push(t[e].label);
              return i;
            }
            function l(t, e, n) {
              var i = t.getPixelForTick(e);
              return n && (i -= 0 === e ? (t.getPixelForTick(1) - i) / 2 : (i - t.getPixelForTick(e - 1)) / 2), i;
            }
            i._set('scale', {
              display: !0,
              position: 'left',
              offset: !1,
              gridLines: {
                display: !0,
                color: 'rgba(0, 0, 0, 0.1)',
                lineWidth: 1,
                drawBorder: !0,
                drawOnChartArea: !0,
                drawTicks: !0,
                tickMarkLength: 10,
                zeroLineWidth: 1,
                zeroLineColor: 'rgba(0,0,0,0.25)',
                zeroLineBorderDash: [],
                zeroLineBorderDashOffset: 0,
                offsetGridLines: !1,
                borderDash: [],
                borderDashOffset: 0
              },
              scaleLabel: { display: !1, labelString: '', lineHeight: 1.2, padding: { top: 4, bottom: 4 } },
              ticks: {
                beginAtZero: !1,
                minRotation: 0,
                maxRotation: 50,
                mirror: !1,
                padding: 0,
                reverse: !1,
                display: !0,
                autoSkip: !0,
                autoSkipPadding: 0,
                labelOffset: 0,
                callback: a.formatters.values,
                minor: {},
                major: {}
              }
            }),
              (e.exports = function(t) {
                function e(t, e, n) {
                  return o.isArray(e) ? o.longestText(t, n, e) : t.measureText(e).width;
                }
                function n(t) {
                  var e = o.valueOrDefault,
                    n = i.global,
                    r = e(t.fontSize, n.defaultFontSize),
                    a = e(t.fontStyle, n.defaultFontStyle),
                    s = e(t.fontFamily, n.defaultFontFamily);
                  return { size: r, style: a, family: s, font: o.fontString(r, a, s) };
                }
                function a(t) {
                  return o.options.toLineHeight(
                    o.valueOrDefault(t.lineHeight, 1.2),
                    o.valueOrDefault(t.fontSize, i.global.defaultFontSize)
                  );
                }
                t.Scale = r.extend({
                  getPadding: function() {
                    return {
                      left: this.paddingLeft || 0,
                      top: this.paddingTop || 0,
                      right: this.paddingRight || 0,
                      bottom: this.paddingBottom || 0
                    };
                  },
                  getTicks: function() {
                    return this._ticks;
                  },
                  mergeTicksOptions: function() {
                    var t = this.options.ticks;
                    for (var e in (!1 === t.minor && (t.minor = { display: !1 }),
                    !1 === t.major && (t.major = { display: !1 }),
                    t))
                      'major' !== e &&
                        'minor' !== e &&
                        (void 0 === t.minor[e] && (t.minor[e] = t[e]), void 0 === t.major[e] && (t.major[e] = t[e]));
                  },
                  beforeUpdate: function() {
                    o.callback(this.options.beforeUpdate, [this]);
                  },
                  update: function(t, e, n) {
                    var i,
                      r,
                      a,
                      s,
                      l,
                      u,
                      c = this;
                    for (
                      c.beforeUpdate(),
                        c.maxWidth = t,
                        c.maxHeight = e,
                        c.margins = o.extend({ left: 0, right: 0, top: 0, bottom: 0 }, n),
                        c.longestTextCache = c.longestTextCache || {},
                        c.beforeSetDimensions(),
                        c.setDimensions(),
                        c.afterSetDimensions(),
                        c.beforeDataLimits(),
                        c.determineDataLimits(),
                        c.afterDataLimits(),
                        c.beforeBuildTicks(),
                        l = c.buildTicks() || [],
                        c.afterBuildTicks(),
                        c.beforeTickToLabelConversion(),
                        a = c.convertTicksToLabels(l) || c.ticks,
                        c.afterTickToLabelConversion(),
                        c.ticks = a,
                        i = 0,
                        r = a.length;
                      i < r;
                      ++i
                    )
                      (s = a[i]), (u = l[i]) ? (u.label = s) : l.push((u = { label: s, major: !1 }));
                    return (
                      (c._ticks = l),
                      c.beforeCalculateTickRotation(),
                      c.calculateTickRotation(),
                      c.afterCalculateTickRotation(),
                      c.beforeFit(),
                      c.fit(),
                      c.afterFit(),
                      c.afterUpdate(),
                      c.minSize
                    );
                  },
                  afterUpdate: function() {
                    o.callback(this.options.afterUpdate, [this]);
                  },
                  beforeSetDimensions: function() {
                    o.callback(this.options.beforeSetDimensions, [this]);
                  },
                  setDimensions: function() {
                    var t = this;
                    t.isHorizontal()
                      ? ((t.width = t.maxWidth), (t.left = 0), (t.right = t.width))
                      : ((t.height = t.maxHeight), (t.top = 0), (t.bottom = t.height)),
                      (t.paddingLeft = 0),
                      (t.paddingTop = 0),
                      (t.paddingRight = 0),
                      (t.paddingBottom = 0);
                  },
                  afterSetDimensions: function() {
                    o.callback(this.options.afterSetDimensions, [this]);
                  },
                  beforeDataLimits: function() {
                    o.callback(this.options.beforeDataLimits, [this]);
                  },
                  determineDataLimits: o.noop,
                  afterDataLimits: function() {
                    o.callback(this.options.afterDataLimits, [this]);
                  },
                  beforeBuildTicks: function() {
                    o.callback(this.options.beforeBuildTicks, [this]);
                  },
                  buildTicks: o.noop,
                  afterBuildTicks: function() {
                    o.callback(this.options.afterBuildTicks, [this]);
                  },
                  beforeTickToLabelConversion: function() {
                    o.callback(this.options.beforeTickToLabelConversion, [this]);
                  },
                  convertTicksToLabels: function() {
                    var t = this.options.ticks;
                    this.ticks = this.ticks.map(t.userCallback || t.callback, this);
                  },
                  afterTickToLabelConversion: function() {
                    o.callback(this.options.afterTickToLabelConversion, [this]);
                  },
                  beforeCalculateTickRotation: function() {
                    o.callback(this.options.beforeCalculateTickRotation, [this]);
                  },
                  calculateTickRotation: function() {
                    var t = this,
                      e = t.ctx,
                      i = t.options.ticks,
                      r = s(t._ticks),
                      a = n(i);
                    e.font = a.font;
                    var l = i.minRotation || 0;
                    if (r.length && t.options.display && t.isHorizontal())
                      for (
                        var u,
                          c = o.longestText(e, a.font, r, t.longestTextCache),
                          d = c,
                          h = t.getPixelForTick(1) - t.getPixelForTick(0) - 6;
                        d > h && l < i.maxRotation;

                      ) {
                        var f = o.toRadians(l);
                        if (((u = Math.cos(f)), Math.sin(f) * c > t.maxHeight)) {
                          l--;
                          break;
                        }
                        l++, (d = u * c);
                      }
                    t.labelRotation = l;
                  },
                  afterCalculateTickRotation: function() {
                    o.callback(this.options.afterCalculateTickRotation, [this]);
                  },
                  beforeFit: function() {
                    o.callback(this.options.beforeFit, [this]);
                  },
                  fit: function() {
                    var t = this,
                      i = (t.minSize = { width: 0, height: 0 }),
                      r = s(t._ticks),
                      l = t.options,
                      u = l.ticks,
                      c = l.scaleLabel,
                      d = l.gridLines,
                      h = l.display,
                      f = t.isHorizontal(),
                      p = n(u),
                      g = l.gridLines.tickMarkLength;
                    if (
                      ((i.width = f
                        ? t.isFullWidth()
                          ? t.maxWidth - t.margins.left - t.margins.right
                          : t.maxWidth
                        : h && d.drawTicks
                        ? g
                        : 0),
                      (i.height = f ? (h && d.drawTicks ? g : 0) : t.maxHeight),
                      c.display && h)
                    ) {
                      var m = a(c) + o.options.toPadding(c.padding).height;
                      f ? (i.height += m) : (i.width += m);
                    }
                    if (u.display && h) {
                      var v = o.longestText(t.ctx, p.font, r, t.longestTextCache),
                        b = o.numberOfLabelLines(r),
                        y = 0.5 * p.size,
                        x = t.options.ticks.padding;
                      if (f) {
                        t.longestLabelWidth = v;
                        var w = o.toRadians(t.labelRotation),
                          _ = Math.cos(w),
                          C = Math.sin(w) * v + p.size * b + y * (b - 1) + y;
                        (i.height = Math.min(t.maxHeight, i.height + C + x)), (t.ctx.font = p.font);
                        var k = e(t.ctx, r[0], p.font),
                          S = e(t.ctx, r[r.length - 1], p.font);
                        0 !== t.labelRotation
                          ? ((t.paddingLeft = 'bottom' === l.position ? _ * k + 3 : _ * y + 3),
                            (t.paddingRight = 'bottom' === l.position ? _ * y + 3 : _ * S + 3))
                          : ((t.paddingLeft = k / 2 + 3), (t.paddingRight = S / 2 + 3));
                      } else
                        u.mirror ? (v = 0) : (v += x + y),
                          (i.width = Math.min(t.maxWidth, i.width + v)),
                          (t.paddingTop = p.size / 2),
                          (t.paddingBottom = p.size / 2);
                    }
                    t.handleMargins(), (t.width = i.width), (t.height = i.height);
                  },
                  handleMargins: function() {
                    var t = this;
                    t.margins &&
                      ((t.paddingLeft = Math.max(t.paddingLeft - t.margins.left, 0)),
                      (t.paddingTop = Math.max(t.paddingTop - t.margins.top, 0)),
                      (t.paddingRight = Math.max(t.paddingRight - t.margins.right, 0)),
                      (t.paddingBottom = Math.max(t.paddingBottom - t.margins.bottom, 0)));
                  },
                  afterFit: function() {
                    o.callback(this.options.afterFit, [this]);
                  },
                  isHorizontal: function() {
                    return 'top' === this.options.position || 'bottom' === this.options.position;
                  },
                  isFullWidth: function() {
                    return this.options.fullWidth;
                  },
                  getRightValue: function(t) {
                    if (o.isNullOrUndef(t)) return NaN;
                    if ('number' == typeof t && !isFinite(t)) return NaN;
                    if (t)
                      if (this.isHorizontal()) {
                        if (void 0 !== t.x) return this.getRightValue(t.x);
                      } else if (void 0 !== t.y) return this.getRightValue(t.y);
                    return t;
                  },
                  getLabelForIndex: o.noop,
                  getPixelForValue: o.noop,
                  getValueForPixel: o.noop,
                  getPixelForTick: function(t) {
                    var e = this,
                      n = e.options.offset;
                    if (e.isHorizontal()) {
                      var i = (e.width - (e.paddingLeft + e.paddingRight)) / Math.max(e._ticks.length - (n ? 0 : 1), 1),
                        r = i * t + e.paddingLeft;
                      return n && (r += i / 2), e.left + Math.round(r) + (e.isFullWidth() ? e.margins.left : 0);
                    }
                    return e.top + t * ((e.height - (e.paddingTop + e.paddingBottom)) / (e._ticks.length - 1));
                  },
                  getPixelForDecimal: function(t) {
                    var e = this;
                    return e.isHorizontal()
                      ? e.left +
                          Math.round((e.width - (e.paddingLeft + e.paddingRight)) * t + e.paddingLeft) +
                          (e.isFullWidth() ? e.margins.left : 0)
                      : e.top + t * e.height;
                  },
                  getBasePixel: function() {
                    return this.getPixelForValue(this.getBaseValue());
                  },
                  getBaseValue: function() {
                    var t = this.min,
                      e = this.max;
                    return this.beginAtZero ? 0 : t < 0 && e < 0 ? e : t > 0 && e > 0 ? t : 0;
                  },
                  _autoSkip: function(t) {
                    var e,
                      n,
                      i,
                      r,
                      a = this,
                      s = a.isHorizontal(),
                      l = a.options.ticks.minor,
                      u = t.length,
                      c = o.toRadians(a.labelRotation),
                      d = Math.cos(c),
                      h = a.longestLabelWidth * d,
                      f = [];
                    for (
                      l.maxTicksLimit && (r = l.maxTicksLimit),
                        s &&
                          ((e = !1),
                          (h + l.autoSkipPadding) * u > a.width - (a.paddingLeft + a.paddingRight) &&
                            (e =
                              1 +
                              Math.floor(((h + l.autoSkipPadding) * u) / (a.width - (a.paddingLeft + a.paddingRight)))),
                          r && u > r && (e = Math.max(e, Math.floor(u / r)))),
                        n = 0;
                      n < u;
                      n++
                    )
                      (i = t[n]),
                        ((e > 1 && n % e > 0) || (n % e == 0 && n + e >= u)) && n !== u - 1 && delete i.label,
                        f.push(i);
                    return f;
                  },
                  draw: function(t) {
                    var e = this,
                      r = e.options;
                    if (r.display) {
                      var s = e.ctx,
                        u = i.global,
                        c = r.ticks.minor,
                        d = r.ticks.major || c,
                        h = r.gridLines,
                        f = r.scaleLabel,
                        p = 0 !== e.labelRotation,
                        g = e.isHorizontal(),
                        m = c.autoSkip ? e._autoSkip(e.getTicks()) : e.getTicks(),
                        v = o.valueOrDefault(c.fontColor, u.defaultFontColor),
                        b = n(c),
                        y = o.valueOrDefault(d.fontColor, u.defaultFontColor),
                        x = n(d),
                        w = h.drawTicks ? h.tickMarkLength : 0,
                        _ = o.valueOrDefault(f.fontColor, u.defaultFontColor),
                        C = n(f),
                        k = o.options.toPadding(f.padding),
                        S = o.toRadians(e.labelRotation),
                        T = [],
                        D = e.options.gridLines.lineWidth,
                        A = 'right' === r.position ? e.right : e.right - D - w,
                        E = 'right' === r.position ? e.right + w : e.right,
                        M = 'bottom' === r.position ? e.top + D : e.bottom - w - D,
                        I = 'bottom' === r.position ? e.top + D + w : e.bottom + D;
                      if (
                        (o.each(m, function(n, i) {
                          if (!o.isNullOrUndef(n.label)) {
                            var a,
                              s,
                              d,
                              f,
                              v,
                              b,
                              y,
                              x,
                              _,
                              C,
                              k,
                              P,
                              O,
                              N,
                              L = n.label;
                            i === e.zeroLineIndex && r.offset === h.offsetGridLines
                              ? ((a = h.zeroLineWidth),
                                (s = h.zeroLineColor),
                                (d = h.zeroLineBorderDash),
                                (f = h.zeroLineBorderDashOffset))
                              : ((a = o.valueAtIndexOrDefault(h.lineWidth, i)),
                                (s = o.valueAtIndexOrDefault(h.color, i)),
                                (d = o.valueOrDefault(h.borderDash, u.borderDash)),
                                (f = o.valueOrDefault(h.borderDashOffset, u.borderDashOffset)));
                            var F = 'middle',
                              R = 'middle',
                              j = c.padding;
                            if (g) {
                              var H = w + j;
                              'bottom' === r.position
                                ? ((R = p ? 'middle' : 'top'), (F = p ? 'right' : 'center'), (N = e.top + H))
                                : ((R = p ? 'middle' : 'bottom'), (F = p ? 'left' : 'center'), (N = e.bottom - H));
                              var B = l(e, i, h.offsetGridLines && m.length > 1);
                              B < e.left && (s = 'rgba(0,0,0,0)'),
                                (B += o.aliasPixel(a)),
                                (O = e.getPixelForTick(i) + c.labelOffset),
                                (v = y = _ = k = B),
                                (b = M),
                                (x = I),
                                (C = t.top),
                                (P = t.bottom + D);
                            } else {
                              var W,
                                z = 'left' === r.position;
                              c.mirror
                                ? ((F = z ? 'left' : 'right'), (W = j))
                                : ((F = z ? 'right' : 'left'), (W = w + j)),
                                (O = z ? e.right - W : e.left + W);
                              var q = l(e, i, h.offsetGridLines && m.length > 1);
                              q < e.top && (s = 'rgba(0,0,0,0)'),
                                (q += o.aliasPixel(a)),
                                (N = e.getPixelForTick(i) + c.labelOffset),
                                (v = A),
                                (y = E),
                                (_ = t.left),
                                (k = t.right + D),
                                (b = x = C = P = q);
                            }
                            T.push({
                              tx1: v,
                              ty1: b,
                              tx2: y,
                              ty2: x,
                              x1: _,
                              y1: C,
                              x2: k,
                              y2: P,
                              labelX: O,
                              labelY: N,
                              glWidth: a,
                              glColor: s,
                              glBorderDash: d,
                              glBorderDashOffset: f,
                              rotation: -1 * S,
                              label: L,
                              major: n.major,
                              textBaseline: R,
                              textAlign: F
                            });
                          }
                        }),
                        o.each(T, function(t) {
                          if (
                            (h.display &&
                              (s.save(),
                              (s.lineWidth = t.glWidth),
                              (s.strokeStyle = t.glColor),
                              s.setLineDash &&
                                (s.setLineDash(t.glBorderDash), (s.lineDashOffset = t.glBorderDashOffset)),
                              s.beginPath(),
                              h.drawTicks && (s.moveTo(t.tx1, t.ty1), s.lineTo(t.tx2, t.ty2)),
                              h.drawOnChartArea && (s.moveTo(t.x1, t.y1), s.lineTo(t.x2, t.y2)),
                              s.stroke(),
                              s.restore()),
                            c.display)
                          ) {
                            s.save(),
                              s.translate(t.labelX, t.labelY),
                              s.rotate(t.rotation),
                              (s.font = t.major ? x.font : b.font),
                              (s.fillStyle = t.major ? y : v),
                              (s.textBaseline = t.textBaseline),
                              (s.textAlign = t.textAlign);
                            var n = t.label;
                            if (o.isArray(n))
                              for (
                                var i = n.length,
                                  r = 1.5 * b.size,
                                  a = e.isHorizontal() ? 0 : (-r * (i - 1)) / 2,
                                  l = 0;
                                l < i;
                                ++l
                              )
                                s.fillText('' + n[l], 0, a), (a += r);
                            else s.fillText(n, 0, 0);
                            s.restore();
                          }
                        }),
                        f.display)
                      ) {
                        var P,
                          O,
                          N = 0,
                          L = a(f) / 2;
                        if (g)
                          (P = e.left + (e.right - e.left) / 2),
                            (O = 'bottom' === r.position ? e.bottom - L - k.bottom : e.top + L + k.top);
                        else {
                          var F = 'left' === r.position;
                          (P = F ? e.left + L + k.top : e.right - L - k.top),
                            (O = e.top + (e.bottom - e.top) / 2),
                            (N = F ? -0.5 * Math.PI : 0.5 * Math.PI);
                        }
                        s.save(),
                          s.translate(P, O),
                          s.rotate(N),
                          (s.textAlign = 'center'),
                          (s.textBaseline = 'middle'),
                          (s.fillStyle = _),
                          (s.font = C.font),
                          s.fillText(f.labelString, 0, 0),
                          s.restore();
                      }
                      if (h.drawBorder) {
                        (s.lineWidth = o.valueAtIndexOrDefault(h.lineWidth, 0)),
                          (s.strokeStyle = o.valueAtIndexOrDefault(h.color, 0));
                        var R = e.left,
                          j = e.right + D,
                          H = e.top,
                          B = e.bottom + D,
                          W = o.aliasPixel(s.lineWidth);
                        g
                          ? ((H = B = 'top' === r.position ? e.bottom : e.top), (H += W), (B += W))
                          : ((R = j = 'left' === r.position ? e.right : e.left), (R += W), (j += W)),
                          s.beginPath(),
                          s.moveTo(R, H),
                          s.lineTo(j, B),
                          s.stroke();
                      }
                    }
                  }
                });
              });
          },
          { 25: 25, 26: 26, 34: 34, 45: 45 }
        ],
        33: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(45),
              o = t(30);
            e.exports = function(t) {
              t.scaleService = {
                constructors: {},
                defaults: {},
                registerScaleType: function(t, e, n) {
                  (this.constructors[t] = e), (this.defaults[t] = r.clone(n));
                },
                getScaleConstructor: function(t) {
                  return this.constructors.hasOwnProperty(t) ? this.constructors[t] : void 0;
                },
                getScaleDefaults: function(t) {
                  return this.defaults.hasOwnProperty(t) ? r.merge({}, [i.scale, this.defaults[t]]) : {};
                },
                updateScaleDefaults: function(t, e) {
                  this.defaults.hasOwnProperty(t) && (this.defaults[t] = r.extend(this.defaults[t], e));
                },
                addScalesToLayout: function(t) {
                  r.each(t.scales, function(e) {
                    (e.fullWidth = e.options.fullWidth),
                      (e.position = e.options.position),
                      (e.weight = e.options.weight),
                      o.addBox(t, e);
                  });
                }
              };
            };
          },
          { 25: 25, 30: 30, 45: 45 }
        ],
        34: [
          function(t, e, n) {
            'use strict';
            var i = t(45);
            e.exports = {
              formatters: {
                values: function(t) {
                  return i.isArray(t) ? t : '' + t;
                },
                linear: function(t, e, n) {
                  var r = n.length > 3 ? n[2] - n[1] : n[1] - n[0];
                  Math.abs(r) > 1 && t !== Math.floor(t) && (r = t - Math.floor(t));
                  var o = i.log10(Math.abs(r)),
                    a = '';
                  if (0 !== t) {
                    var s = -1 * Math.floor(o);
                    (s = Math.max(Math.min(s, 20), 0)), (a = t.toFixed(s));
                  } else a = '0';
                  return a;
                },
                logarithmic: function(t, e, n) {
                  var r = t / Math.pow(10, Math.floor(i.log10(t)));
                  return 0 === t
                    ? '0'
                    : 1 === r || 2 === r || 5 === r || 0 === e || e === n.length - 1
                    ? t.toExponential()
                    : '';
                }
              }
            };
          },
          { 45: 45 }
        ],
        35: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(26),
              o = t(45);
            i._set('global', {
              tooltips: {
                enabled: !0,
                custom: null,
                mode: 'nearest',
                position: 'average',
                intersect: !0,
                backgroundColor: 'rgba(0,0,0,0.8)',
                titleFontStyle: 'bold',
                titleSpacing: 2,
                titleMarginBottom: 6,
                titleFontColor: '#fff',
                titleAlign: 'left',
                bodySpacing: 2,
                bodyFontColor: '#fff',
                bodyAlign: 'left',
                footerFontStyle: 'bold',
                footerSpacing: 2,
                footerMarginTop: 6,
                footerFontColor: '#fff',
                footerAlign: 'left',
                yPadding: 6,
                xPadding: 6,
                caretPadding: 2,
                caretSize: 5,
                cornerRadius: 6,
                multiKeyBackground: '#fff',
                displayColors: !0,
                borderColor: 'rgba(0,0,0,0)',
                borderWidth: 0,
                callbacks: {
                  beforeTitle: o.noop,
                  title: function(t, e) {
                    var n = '',
                      i = e.labels,
                      r = i ? i.length : 0;
                    if (t.length > 0) {
                      var o = t[0];
                      o.xLabel ? (n = o.xLabel) : r > 0 && o.index < r && (n = i[o.index]);
                    }
                    return n;
                  },
                  afterTitle: o.noop,
                  beforeBody: o.noop,
                  beforeLabel: o.noop,
                  label: function(t, e) {
                    var n = e.datasets[t.datasetIndex].label || '';
                    return n && (n += ': '), n + t.yLabel;
                  },
                  labelColor: function(t, e) {
                    var n = e.getDatasetMeta(t.datasetIndex).data[t.index]._view;
                    return { borderColor: n.borderColor, backgroundColor: n.backgroundColor };
                  },
                  labelTextColor: function() {
                    return this._options.bodyFontColor;
                  },
                  afterLabel: o.noop,
                  afterBody: o.noop,
                  beforeFooter: o.noop,
                  footer: o.noop,
                  afterFooter: o.noop
                }
              }
            }),
              (e.exports = function(t) {
                function e(t, e) {
                  var n = o.color(t);
                  return n.alpha(e * n.alpha()).rgbaString();
                }
                function n(t, e) {
                  return e && (o.isArray(e) ? Array.prototype.push.apply(t, e) : t.push(e)), t;
                }
                function a(t) {
                  var e = i.global,
                    n = o.valueOrDefault;
                  return {
                    xPadding: t.xPadding,
                    yPadding: t.yPadding,
                    xAlign: t.xAlign,
                    yAlign: t.yAlign,
                    bodyFontColor: t.bodyFontColor,
                    _bodyFontFamily: n(t.bodyFontFamily, e.defaultFontFamily),
                    _bodyFontStyle: n(t.bodyFontStyle, e.defaultFontStyle),
                    _bodyAlign: t.bodyAlign,
                    bodyFontSize: n(t.bodyFontSize, e.defaultFontSize),
                    bodySpacing: t.bodySpacing,
                    titleFontColor: t.titleFontColor,
                    _titleFontFamily: n(t.titleFontFamily, e.defaultFontFamily),
                    _titleFontStyle: n(t.titleFontStyle, e.defaultFontStyle),
                    titleFontSize: n(t.titleFontSize, e.defaultFontSize),
                    _titleAlign: t.titleAlign,
                    titleSpacing: t.titleSpacing,
                    titleMarginBottom: t.titleMarginBottom,
                    footerFontColor: t.footerFontColor,
                    _footerFontFamily: n(t.footerFontFamily, e.defaultFontFamily),
                    _footerFontStyle: n(t.footerFontStyle, e.defaultFontStyle),
                    footerFontSize: n(t.footerFontSize, e.defaultFontSize),
                    _footerAlign: t.footerAlign,
                    footerSpacing: t.footerSpacing,
                    footerMarginTop: t.footerMarginTop,
                    caretSize: t.caretSize,
                    cornerRadius: t.cornerRadius,
                    backgroundColor: t.backgroundColor,
                    opacity: 0,
                    legendColorBackground: t.multiKeyBackground,
                    displayColors: t.displayColors,
                    borderColor: t.borderColor,
                    borderWidth: t.borderWidth
                  };
                }
                (t.Tooltip = r.extend({
                  initialize: function() {
                    (this._model = a(this._options)), (this._lastActive = []);
                  },
                  getTitle: function() {
                    var t = this._options.callbacks,
                      e = t.beforeTitle.apply(this, arguments),
                      i = t.title.apply(this, arguments),
                      r = t.afterTitle.apply(this, arguments),
                      o = [];
                    return n((o = n((o = n(o, e)), i)), r);
                  },
                  getBeforeBody: function() {
                    var t = this._options.callbacks.beforeBody.apply(this, arguments);
                    return o.isArray(t) ? t : void 0 !== t ? [t] : [];
                  },
                  getBody: function(t, e) {
                    var i = this,
                      r = i._options.callbacks,
                      a = [];
                    return (
                      o.each(t, function(t) {
                        var o = { before: [], lines: [], after: [] };
                        n(o.before, r.beforeLabel.call(i, t, e)),
                          n(o.lines, r.label.call(i, t, e)),
                          n(o.after, r.afterLabel.call(i, t, e)),
                          a.push(o);
                      }),
                      a
                    );
                  },
                  getAfterBody: function() {
                    var t = this._options.callbacks.afterBody.apply(this, arguments);
                    return o.isArray(t) ? t : void 0 !== t ? [t] : [];
                  },
                  getFooter: function() {
                    var t = this._options.callbacks,
                      e = t.beforeFooter.apply(this, arguments),
                      i = t.footer.apply(this, arguments),
                      r = t.afterFooter.apply(this, arguments),
                      o = [];
                    return n((o = n((o = n(o, e)), i)), r);
                  },
                  update: function(e) {
                    var n,
                      i,
                      r,
                      s,
                      l,
                      u,
                      c,
                      d,
                      h,
                      f,
                      p,
                      g,
                      m,
                      v,
                      b,
                      y,
                      x,
                      w,
                      _ = this,
                      C = _._options,
                      k = _._model,
                      S = (_._model = a(C)),
                      T = _._active,
                      D = _._data,
                      A = { xAlign: k.xAlign, yAlign: k.yAlign },
                      E = { x: k.x, y: k.y },
                      M = { width: k.width, height: k.height },
                      I = { x: k.caretX, y: k.caretY };
                    if (T.length) {
                      S.opacity = 1;
                      var P = [],
                        O = [];
                      I = t.Tooltip.positioners[C.position].call(_, T, _._eventPosition);
                      var N = [];
                      for (n = 0, i = T.length; n < i; ++n)
                        N.push(
                          ((b = void 0),
                          (y = void 0),
                          (y = (v = T[n])._yScale || v._scale),
                          (x = v._index),
                          (w = v._datasetIndex),
                          {
                            xLabel: (b = v._xScale) ? b.getLabelForIndex(x, w) : '',
                            yLabel: y ? y.getLabelForIndex(x, w) : '',
                            index: x,
                            datasetIndex: w,
                            x: v._model.x,
                            y: v._model.y
                          })
                        );
                      C.filter &&
                        (N = N.filter(function(t) {
                          return C.filter(t, D);
                        })),
                        C.itemSort &&
                          (N = N.sort(function(t, e) {
                            return C.itemSort(t, e, D);
                          })),
                        o.each(N, function(t) {
                          P.push(C.callbacks.labelColor.call(_, t, _._chart)),
                            O.push(C.callbacks.labelTextColor.call(_, t, _._chart));
                        }),
                        (S.title = _.getTitle(N, D)),
                        (S.beforeBody = _.getBeforeBody(N, D)),
                        (S.body = _.getBody(N, D)),
                        (S.afterBody = _.getAfterBody(N, D)),
                        (S.footer = _.getFooter(N, D)),
                        (S.x = Math.round(I.x)),
                        (S.y = Math.round(I.y)),
                        (S.caretPadding = C.caretPadding),
                        (S.labelColors = P),
                        (S.labelTextColors = O),
                        (S.dataPoints = N),
                        (A = (function(t, e) {
                          var n,
                            i,
                            r,
                            o,
                            a,
                            s = t._model,
                            l = t._chart,
                            u = t._chart.chartArea,
                            c = 'center',
                            d = 'center';
                          s.y < e.height ? (d = 'top') : s.y > l.height - e.height && (d = 'bottom');
                          var h = (u.left + u.right) / 2,
                            f = (u.top + u.bottom) / 2;
                          'center' === d
                            ? ((n = function(t) {
                                return t <= h;
                              }),
                              (i = function(t) {
                                return t > h;
                              }))
                            : ((n = function(t) {
                                return t <= e.width / 2;
                              }),
                              (i = function(t) {
                                return t >= l.width - e.width / 2;
                              })),
                            (r = function(t) {
                              return t + e.width + s.caretSize + s.caretPadding > l.width;
                            }),
                            (o = function(t) {
                              return t - e.width - s.caretSize - s.caretPadding < 0;
                            }),
                            (a = function(t) {
                              return t <= f ? 'top' : 'bottom';
                            }),
                            n(s.x)
                              ? ((c = 'left'), r(s.x) && ((c = 'center'), (d = a(s.y))))
                              : i(s.x) && ((c = 'right'), o(s.x) && ((c = 'center'), (d = a(s.y))));
                          var p = t._options;
                          return { xAlign: p.xAlign ? p.xAlign : c, yAlign: p.yAlign ? p.yAlign : d };
                        })(
                          this,
                          (M = (function(t, e) {
                            var n = t._chart.ctx,
                              i = 2 * e.yPadding,
                              r = 0,
                              a = e.body,
                              s = a.reduce(function(t, e) {
                                return t + e.before.length + e.lines.length + e.after.length;
                              }, 0),
                              l = e.title.length,
                              u = e.footer.length,
                              c = e.titleFontSize,
                              d = e.bodyFontSize,
                              h = e.footerFontSize;
                            (i += l * c),
                              (i += l ? (l - 1) * e.titleSpacing : 0),
                              (i += l ? e.titleMarginBottom : 0),
                              (i += (s += e.beforeBody.length + e.afterBody.length) * d),
                              (i += s ? (s - 1) * e.bodySpacing : 0),
                              (i += u ? e.footerMarginTop : 0),
                              (i += u * h),
                              (i += u ? (u - 1) * e.footerSpacing : 0);
                            var f = 0,
                              p = function(t) {
                                r = Math.max(r, n.measureText(t).width + f);
                              };
                            return (
                              (n.font = o.fontString(c, e._titleFontStyle, e._titleFontFamily)),
                              o.each(e.title, p),
                              (n.font = o.fontString(d, e._bodyFontStyle, e._bodyFontFamily)),
                              o.each(e.beforeBody.concat(e.afterBody), p),
                              (f = e.displayColors ? d + 2 : 0),
                              o.each(a, function(t) {
                                o.each(t.before, p), o.each(t.lines, p), o.each(t.after, p);
                              }),
                              (f = 0),
                              (n.font = o.fontString(h, e._footerFontStyle, e._footerFontFamily)),
                              o.each(e.footer, p),
                              { width: (r += 2 * e.xPadding), height: i }
                            );
                          })(this, S))
                        )),
                        (s = M),
                        (u = _._chart),
                        (c = (r = S).x),
                        (d = r.y),
                        (g = r.caretSize + (h = r.caretPadding)),
                        (m = r.cornerRadius + h),
                        'right' === (f = (l = A).xAlign)
                          ? (c -= s.width)
                          : 'center' === f &&
                            ((c -= s.width / 2) + s.width > u.width && (c = u.width - s.width), c < 0 && (c = 0)),
                        'top' === (p = l.yAlign) ? (d += g) : (d -= 'bottom' === p ? s.height + g : s.height / 2),
                        'center' === p
                          ? 'left' === f
                            ? (c += g)
                            : 'right' === f && (c -= g)
                          : 'left' === f
                          ? (c -= m)
                          : 'right' === f && (c += m),
                        (E = { x: c, y: d });
                    } else S.opacity = 0;
                    return (
                      (S.xAlign = A.xAlign),
                      (S.yAlign = A.yAlign),
                      (S.x = E.x),
                      (S.y = E.y),
                      (S.width = M.width),
                      (S.height = M.height),
                      (S.caretX = I.x),
                      (S.caretY = I.y),
                      (_._model = S),
                      e && C.custom && C.custom.call(_, S),
                      _
                    );
                  },
                  drawCaret: function(t, e) {
                    var n = this._chart.ctx,
                      i = this.getCaretPosition(t, e, this._view);
                    n.lineTo(i.x1, i.y1), n.lineTo(i.x2, i.y2), n.lineTo(i.x3, i.y3);
                  },
                  getCaretPosition: function(t, e, n) {
                    var i,
                      r,
                      o,
                      a,
                      s,
                      l,
                      u = n.caretSize,
                      c = n.cornerRadius,
                      d = n.xAlign,
                      h = n.yAlign,
                      f = t.x,
                      p = t.y,
                      g = e.width,
                      m = e.height;
                    if ('center' === h)
                      (s = p + m / 2),
                        'left' === d
                          ? ((r = (i = f) - u), (o = i), (a = s + u), (l = s - u))
                          : ((r = (i = f + g) + u), (o = i), (a = s - u), (l = s + u));
                    else if (
                      ('left' === d
                        ? ((i = (r = f + c + u) - u), (o = r + u))
                        : 'right' === d
                        ? ((i = (r = f + g - c - u) - u), (o = r + u))
                        : ((i = (r = n.caretX) - u), (o = r + u)),
                      'top' === h)
                    )
                      (s = (a = p) - u), (l = a);
                    else {
                      (s = (a = p + m) + u), (l = a);
                      var v = o;
                      (o = i), (i = v);
                    }
                    return { x1: i, x2: r, x3: o, y1: a, y2: s, y3: l };
                  },
                  drawTitle: function(t, n, i, r) {
                    var a = n.title;
                    if (a.length) {
                      (i.textAlign = n._titleAlign), (i.textBaseline = 'top');
                      var s,
                        l,
                        u = n.titleFontSize,
                        c = n.titleSpacing;
                      for (
                        i.fillStyle = e(n.titleFontColor, r),
                          i.font = o.fontString(u, n._titleFontStyle, n._titleFontFamily),
                          s = 0,
                          l = a.length;
                        s < l;
                        ++s
                      )
                        i.fillText(a[s], t.x, t.y),
                          (t.y += u + c),
                          s + 1 === a.length && (t.y += n.titleMarginBottom - c);
                    }
                  },
                  drawBody: function(t, n, i, r) {
                    var a = n.bodyFontSize,
                      s = n.bodySpacing,
                      l = n.body;
                    (i.textAlign = n._bodyAlign),
                      (i.textBaseline = 'top'),
                      (i.font = o.fontString(a, n._bodyFontStyle, n._bodyFontFamily));
                    var u = 0,
                      c = function(e) {
                        i.fillText(e, t.x + u, t.y), (t.y += a + s);
                      };
                    (i.fillStyle = e(n.bodyFontColor, r)), o.each(n.beforeBody, c);
                    var d = n.displayColors;
                    (u = d ? a + 2 : 0),
                      o.each(l, function(s, l) {
                        var u = e(n.labelTextColors[l], r);
                        (i.fillStyle = u),
                          o.each(s.before, c),
                          o.each(s.lines, function(o) {
                            d &&
                              ((i.fillStyle = e(n.legendColorBackground, r)),
                              i.fillRect(t.x, t.y, a, a),
                              (i.lineWidth = 1),
                              (i.strokeStyle = e(n.labelColors[l].borderColor, r)),
                              i.strokeRect(t.x, t.y, a, a),
                              (i.fillStyle = e(n.labelColors[l].backgroundColor, r)),
                              i.fillRect(t.x + 1, t.y + 1, a - 2, a - 2),
                              (i.fillStyle = u)),
                              c(o);
                          }),
                          o.each(s.after, c);
                      }),
                      (u = 0),
                      o.each(n.afterBody, c),
                      (t.y -= s);
                  },
                  drawFooter: function(t, n, i, r) {
                    var a = n.footer;
                    a.length &&
                      ((t.y += n.footerMarginTop),
                      (i.textAlign = n._footerAlign),
                      (i.textBaseline = 'top'),
                      (i.fillStyle = e(n.footerFontColor, r)),
                      (i.font = o.fontString(n.footerFontSize, n._footerFontStyle, n._footerFontFamily)),
                      o.each(a, function(e) {
                        i.fillText(e, t.x, t.y), (t.y += n.footerFontSize + n.footerSpacing);
                      }));
                  },
                  drawBackground: function(t, n, i, r, o) {
                    (i.fillStyle = e(n.backgroundColor, o)),
                      (i.strokeStyle = e(n.borderColor, o)),
                      (i.lineWidth = n.borderWidth);
                    var a = n.xAlign,
                      s = n.yAlign,
                      l = t.x,
                      u = t.y,
                      c = r.width,
                      d = r.height,
                      h = n.cornerRadius;
                    i.beginPath(),
                      i.moveTo(l + h, u),
                      'top' === s && this.drawCaret(t, r),
                      i.lineTo(l + c - h, u),
                      i.quadraticCurveTo(l + c, u, l + c, u + h),
                      'center' === s && 'right' === a && this.drawCaret(t, r),
                      i.lineTo(l + c, u + d - h),
                      i.quadraticCurveTo(l + c, u + d, l + c - h, u + d),
                      'bottom' === s && this.drawCaret(t, r),
                      i.lineTo(l + h, u + d),
                      i.quadraticCurveTo(l, u + d, l, u + d - h),
                      'center' === s && 'left' === a && this.drawCaret(t, r),
                      i.lineTo(l, u + h),
                      i.quadraticCurveTo(l, u, l + h, u),
                      i.closePath(),
                      i.fill(),
                      n.borderWidth > 0 && i.stroke();
                  },
                  draw: function() {
                    var t = this._chart.ctx,
                      e = this._view;
                    if (0 !== e.opacity) {
                      var n = { width: e.width, height: e.height },
                        i = { x: e.x, y: e.y },
                        r = Math.abs(e.opacity < 0.001) ? 0 : e.opacity;
                      this._options.enabled &&
                        (e.title.length ||
                          e.beforeBody.length ||
                          e.body.length ||
                          e.afterBody.length ||
                          e.footer.length) &&
                        (this.drawBackground(i, e, t, n, r),
                        (i.x += e.xPadding),
                        (i.y += e.yPadding),
                        this.drawTitle(i, e, t, r),
                        this.drawBody(i, e, t, r),
                        this.drawFooter(i, e, t, r));
                    }
                  },
                  handleEvent: function(t) {
                    var e,
                      n = this,
                      i = n._options;
                    return (
                      (n._lastActive = n._lastActive || []),
                      (n._active = 'mouseout' === t.type ? [] : n._chart.getElementsAtEventForMode(t, i.mode, i)),
                      (e = !o.arrayEquals(n._active, n._lastActive)) &&
                        ((n._lastActive = n._active),
                        (i.enabled || i.custom) && ((n._eventPosition = { x: t.x, y: t.y }), n.update(!0), n.pivot())),
                      e
                    );
                  }
                })),
                  (t.Tooltip.positioners = {
                    average: function(t) {
                      if (!t.length) return !1;
                      var e,
                        n,
                        i = 0,
                        r = 0,
                        o = 0;
                      for (e = 0, n = t.length; e < n; ++e) {
                        var a = t[e];
                        if (a && a.hasValue()) {
                          var s = a.tooltipPosition();
                          (i += s.x), (r += s.y), ++o;
                        }
                      }
                      return { x: Math.round(i / o), y: Math.round(r / o) };
                    },
                    nearest: function(t, e) {
                      var n,
                        i,
                        r,
                        a = e.x,
                        s = e.y,
                        l = Number.POSITIVE_INFINITY;
                      for (n = 0, i = t.length; n < i; ++n) {
                        var u = t[n];
                        if (u && u.hasValue()) {
                          var c = u.getCenterPoint(),
                            d = o.distanceBetweenPoints(e, c);
                          d < l && ((l = d), (r = u));
                        }
                      }
                      if (r) {
                        var h = r.tooltipPosition();
                        (a = h.x), (s = h.y);
                      }
                      return { x: a, y: s };
                    }
                  });
              });
          },
          { 25: 25, 26: 26, 45: 45 }
        ],
        36: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(26),
              o = t(45);
            i._set('global', {
              elements: { arc: { backgroundColor: i.global.defaultColor, borderColor: '#fff', borderWidth: 2 } }
            }),
              (e.exports = r.extend({
                inLabelRange: function(t) {
                  var e = this._view;
                  return !!e && Math.pow(t - e.x, 2) < Math.pow(e.radius + e.hoverRadius, 2);
                },
                inRange: function(t, e) {
                  var n = this._view;
                  if (n) {
                    for (
                      var i = o.getAngleFromPoint(n, { x: t, y: e }),
                        r = i.angle,
                        a = i.distance,
                        s = n.startAngle,
                        l = n.endAngle;
                      l < s;

                    )
                      l += 2 * Math.PI;
                    for (; r > l; ) r -= 2 * Math.PI;
                    for (; r < s; ) r += 2 * Math.PI;
                    return r >= s && r <= l && a >= n.innerRadius && a <= n.outerRadius;
                  }
                  return !1;
                },
                getCenterPoint: function() {
                  var t = this._view,
                    e = (t.startAngle + t.endAngle) / 2,
                    n = (t.innerRadius + t.outerRadius) / 2;
                  return { x: t.x + Math.cos(e) * n, y: t.y + Math.sin(e) * n };
                },
                getArea: function() {
                  var t = this._view;
                  return (
                    Math.PI *
                    ((t.endAngle - t.startAngle) / (2 * Math.PI)) *
                    (Math.pow(t.outerRadius, 2) - Math.pow(t.innerRadius, 2))
                  );
                },
                tooltipPosition: function() {
                  var t = this._view,
                    e = t.startAngle + (t.endAngle - t.startAngle) / 2,
                    n = (t.outerRadius - t.innerRadius) / 2 + t.innerRadius;
                  return { x: t.x + Math.cos(e) * n, y: t.y + Math.sin(e) * n };
                },
                draw: function() {
                  var t = this._chart.ctx,
                    e = this._view,
                    n = e.startAngle,
                    i = e.endAngle;
                  t.beginPath(),
                    t.arc(e.x, e.y, e.outerRadius, n, i),
                    t.arc(e.x, e.y, e.innerRadius, i, n, !0),
                    t.closePath(),
                    (t.strokeStyle = e.borderColor),
                    (t.lineWidth = e.borderWidth),
                    (t.fillStyle = e.backgroundColor),
                    t.fill(),
                    (t.lineJoin = 'bevel'),
                    e.borderWidth && t.stroke();
                }
              }));
          },
          { 25: 25, 26: 26, 45: 45 }
        ],
        37: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(26),
              o = t(45),
              a = i.global;
            i._set('global', {
              elements: {
                line: {
                  tension: 0.4,
                  backgroundColor: a.defaultColor,
                  borderWidth: 3,
                  borderColor: a.defaultColor,
                  borderCapStyle: 'butt',
                  borderDash: [],
                  borderDashOffset: 0,
                  borderJoinStyle: 'miter',
                  capBezierPoints: !0,
                  fill: !0
                }
              }
            }),
              (e.exports = r.extend({
                draw: function() {
                  var t,
                    e,
                    n,
                    i,
                    r = this._view,
                    s = this._chart.ctx,
                    l = r.spanGaps,
                    u = this._children.slice(),
                    c = a.elements.line,
                    d = -1;
                  for (
                    this._loop && u.length && u.push(u[0]),
                      s.save(),
                      s.lineCap = r.borderCapStyle || c.borderCapStyle,
                      s.setLineDash && s.setLineDash(r.borderDash || c.borderDash),
                      s.lineDashOffset = r.borderDashOffset || c.borderDashOffset,
                      s.lineJoin = r.borderJoinStyle || c.borderJoinStyle,
                      s.lineWidth = r.borderWidth || c.borderWidth,
                      s.strokeStyle = r.borderColor || a.defaultColor,
                      s.beginPath(),
                      d = -1,
                      t = 0;
                    t < u.length;
                    ++t
                  )
                    (e = u[t]),
                      (n = o.previousItem(u, t)),
                      (i = e._view),
                      0 === t
                        ? i.skip || (s.moveTo(i.x, i.y), (d = t))
                        : ((n = -1 === d ? n : u[d]),
                          i.skip ||
                            ((d !== t - 1 && !l) || -1 === d
                              ? s.moveTo(i.x, i.y)
                              : o.canvas.lineTo(s, n._view, e._view),
                            (d = t)));
                  s.stroke(), s.restore();
                }
              }));
          },
          { 25: 25, 26: 26, 45: 45 }
        ],
        38: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(26),
              o = t(45),
              a = i.global.defaultColor;
            function s(t) {
              var e = this._view;
              return !!e && Math.abs(t - e.x) < e.radius + e.hitRadius;
            }
            i._set('global', {
              elements: {
                point: {
                  radius: 3,
                  pointStyle: 'circle',
                  backgroundColor: a,
                  borderColor: a,
                  borderWidth: 1,
                  hitRadius: 1,
                  hoverRadius: 4,
                  hoverBorderWidth: 1
                }
              }
            }),
              (e.exports = r.extend({
                inRange: function(t, e) {
                  var n = this._view;
                  return !!n && Math.pow(t - n.x, 2) + Math.pow(e - n.y, 2) < Math.pow(n.hitRadius + n.radius, 2);
                },
                inLabelRange: s,
                inXRange: s,
                inYRange: function(t) {
                  var e = this._view;
                  return !!e && Math.abs(t - e.y) < e.radius + e.hitRadius;
                },
                getCenterPoint: function() {
                  var t = this._view;
                  return { x: t.x, y: t.y };
                },
                getArea: function() {
                  return Math.PI * Math.pow(this._view.radius, 2);
                },
                tooltipPosition: function() {
                  var t = this._view;
                  return { x: t.x, y: t.y, padding: t.radius + t.borderWidth };
                },
                draw: function(t) {
                  var e = this._view,
                    n = this._model,
                    r = this._chart.ctx,
                    s = e.pointStyle,
                    l = e.radius,
                    u = e.x,
                    c = e.y,
                    d = o.color,
                    h = 0;
                  e.skip ||
                    ((r.strokeStyle = e.borderColor || a),
                    (r.lineWidth = o.valueOrDefault(e.borderWidth, i.global.elements.point.borderWidth)),
                    (r.fillStyle = e.backgroundColor || a),
                    void 0 !== t &&
                      (n.x < t.left || 1.01 * t.right < n.x || n.y < t.top || 1.01 * t.bottom < n.y) &&
                      (n.x < t.left
                        ? (h = (u - n.x) / (t.left - n.x))
                        : 1.01 * t.right < n.x
                        ? (h = (n.x - u) / (n.x - t.right))
                        : n.y < t.top
                        ? (h = (c - n.y) / (t.top - n.y))
                        : 1.01 * t.bottom < n.y && (h = (n.y - c) / (n.y - t.bottom)),
                      (h = Math.round(100 * h) / 100),
                      (r.strokeStyle = d(r.strokeStyle)
                        .alpha(h)
                        .rgbString()),
                      (r.fillStyle = d(r.fillStyle)
                        .alpha(h)
                        .rgbString())),
                    o.canvas.drawPoint(r, s, l, u, c));
                }
              }));
          },
          { 25: 25, 26: 26, 45: 45 }
        ],
        39: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(26);
            function o(t) {
              return void 0 !== t._view.width;
            }
            function a(t) {
              var e,
                n,
                i,
                r,
                a = t._view;
              if (o(t)) {
                var s = a.width / 2;
                (e = a.x - s), (n = a.x + s), (i = Math.min(a.y, a.base)), (r = Math.max(a.y, a.base));
              } else {
                var l = a.height / 2;
                (e = Math.min(a.x, a.base)), (n = Math.max(a.x, a.base)), (i = a.y - l), (r = a.y + l);
              }
              return { left: e, top: i, right: n, bottom: r };
            }
            i._set('global', {
              elements: {
                rectangle: {
                  backgroundColor: i.global.defaultColor,
                  borderColor: i.global.defaultColor,
                  borderSkipped: 'bottom',
                  borderWidth: 0
                }
              }
            }),
              (e.exports = r.extend({
                draw: function() {
                  var t,
                    e,
                    n,
                    i,
                    r,
                    o,
                    a,
                    s = this._chart.ctx,
                    l = this._view,
                    u = l.borderWidth;
                  if (
                    (l.horizontal
                      ? ((n = l.y - l.height / 2),
                        (i = l.y + l.height / 2),
                        (r = (e = l.x) > (t = l.base) ? 1 : -1),
                        (o = 1),
                        (a = l.borderSkipped || 'left'))
                      : ((t = l.x - l.width / 2),
                        (e = l.x + l.width / 2),
                        (r = 1),
                        (o = (i = l.base) > (n = l.y) ? 1 : -1),
                        (a = l.borderSkipped || 'bottom')),
                    u)
                  ) {
                    var c = Math.min(Math.abs(t - e), Math.abs(n - i)),
                      d = (u = u > c ? c : u) / 2,
                      h = t + ('left' !== a ? d * r : 0),
                      f = e + ('right' !== a ? -d * r : 0),
                      p = n + ('top' !== a ? d * o : 0),
                      g = i + ('bottom' !== a ? -d * o : 0);
                    h !== f && ((n = p), (i = g)), p !== g && ((t = h), (e = f));
                  }
                  s.beginPath(), (s.fillStyle = l.backgroundColor), (s.strokeStyle = l.borderColor), (s.lineWidth = u);
                  var m = [
                      [t, i],
                      [t, n],
                      [e, n],
                      [e, i]
                    ],
                    v = ['bottom', 'left', 'top', 'right'].indexOf(a, 0);
                  function b(t) {
                    return m[(v + t) % 4];
                  }
                  -1 === v && (v = 0);
                  var y = b(0);
                  s.moveTo(y[0], y[1]);
                  for (var x = 1; x < 4; x++) (y = b(x)), s.lineTo(y[0], y[1]);
                  s.fill(), u && s.stroke();
                },
                height: function() {
                  var t = this._view;
                  return t.base - t.y;
                },
                inRange: function(t, e) {
                  var n = !1;
                  if (this._view) {
                    var i = a(this);
                    n = t >= i.left && t <= i.right && e >= i.top && e <= i.bottom;
                  }
                  return n;
                },
                inLabelRange: function(t, e) {
                  if (!this._view) return !1;
                  var n = a(this);
                  return o(this) ? t >= n.left && t <= n.right : e >= n.top && e <= n.bottom;
                },
                inXRange: function(t) {
                  var e = a(this);
                  return t >= e.left && t <= e.right;
                },
                inYRange: function(t) {
                  var e = a(this);
                  return t >= e.top && t <= e.bottom;
                },
                getCenterPoint: function() {
                  var t,
                    e,
                    n = this._view;
                  return (
                    o(this) ? ((t = n.x), (e = (n.y + n.base) / 2)) : ((t = (n.x + n.base) / 2), (e = n.y)),
                    { x: t, y: e }
                  );
                },
                getArea: function() {
                  var t = this._view;
                  return t.width * Math.abs(t.y - t.base);
                },
                tooltipPosition: function() {
                  var t = this._view;
                  return { x: t.x, y: t.y };
                }
              }));
          },
          { 25: 25, 26: 26 }
        ],
        40: [
          function(t, e, n) {
            'use strict';
            (e.exports = {}),
              (e.exports.Arc = t(36)),
              (e.exports.Line = t(37)),
              (e.exports.Point = t(38)),
              (e.exports.Rectangle = t(39));
          },
          { 36: 36, 37: 37, 38: 38, 39: 39 }
        ],
        41: [
          function(t, e, n) {
            'use strict';
            var i = t(42);
            (n = e.exports = {
              clear: function(t) {
                t.ctx.clearRect(0, 0, t.width, t.height);
              },
              roundedRect: function(t, e, n, i, r, o) {
                if (o) {
                  var a = Math.min(o, i / 2),
                    s = Math.min(o, r / 2);
                  t.moveTo(e + a, n),
                    t.lineTo(e + i - a, n),
                    t.quadraticCurveTo(e + i, n, e + i, n + s),
                    t.lineTo(e + i, n + r - s),
                    t.quadraticCurveTo(e + i, n + r, e + i - a, n + r),
                    t.lineTo(e + a, n + r),
                    t.quadraticCurveTo(e, n + r, e, n + r - s),
                    t.lineTo(e, n + s),
                    t.quadraticCurveTo(e, n, e + a, n);
                } else t.rect(e, n, i, r);
              },
              drawPoint: function(t, e, n, i, r) {
                var o, a, s, l, u, c;
                if (
                  !e ||
                  'object' != typeof e ||
                  ('[object HTMLImageElement]' !== (o = e.toString()) && '[object HTMLCanvasElement]' !== o)
                ) {
                  if (!(isNaN(n) || n <= 0)) {
                    switch (e) {
                      default:
                        t.beginPath(), t.arc(i, r, n, 0, 2 * Math.PI), t.closePath(), t.fill();
                        break;
                      case 'triangle':
                        t.beginPath(),
                          (u = ((a = (3 * n) / Math.sqrt(3)) * Math.sqrt(3)) / 2),
                          t.moveTo(i - a / 2, r + u / 3),
                          t.lineTo(i + a / 2, r + u / 3),
                          t.lineTo(i, r - (2 * u) / 3),
                          t.closePath(),
                          t.fill();
                        break;
                      case 'rect':
                        (c = (1 / Math.SQRT2) * n),
                          t.beginPath(),
                          t.fillRect(i - c, r - c, 2 * c, 2 * c),
                          t.strokeRect(i - c, r - c, 2 * c, 2 * c);
                        break;
                      case 'rectRounded':
                        var d = n / Math.SQRT2,
                          h = i - d,
                          f = r - d,
                          p = Math.SQRT2 * n;
                        t.beginPath(), this.roundedRect(t, h, f, p, p, n / 2), t.closePath(), t.fill();
                        break;
                      case 'rectRot':
                        (c = (1 / Math.SQRT2) * n),
                          t.beginPath(),
                          t.moveTo(i - c, r),
                          t.lineTo(i, r + c),
                          t.lineTo(i + c, r),
                          t.lineTo(i, r - c),
                          t.closePath(),
                          t.fill();
                        break;
                      case 'cross':
                        t.beginPath(),
                          t.moveTo(i, r + n),
                          t.lineTo(i, r - n),
                          t.moveTo(i - n, r),
                          t.lineTo(i + n, r),
                          t.closePath();
                        break;
                      case 'crossRot':
                        t.beginPath(),
                          (s = Math.cos(Math.PI / 4) * n),
                          (l = Math.sin(Math.PI / 4) * n),
                          t.moveTo(i - s, r - l),
                          t.lineTo(i + s, r + l),
                          t.moveTo(i - s, r + l),
                          t.lineTo(i + s, r - l),
                          t.closePath();
                        break;
                      case 'star':
                        t.beginPath(),
                          t.moveTo(i, r + n),
                          t.lineTo(i, r - n),
                          t.moveTo(i - n, r),
                          t.lineTo(i + n, r),
                          (s = Math.cos(Math.PI / 4) * n),
                          (l = Math.sin(Math.PI / 4) * n),
                          t.moveTo(i - s, r - l),
                          t.lineTo(i + s, r + l),
                          t.moveTo(i - s, r + l),
                          t.lineTo(i + s, r - l),
                          t.closePath();
                        break;
                      case 'line':
                        t.beginPath(), t.moveTo(i - n, r), t.lineTo(i + n, r), t.closePath();
                        break;
                      case 'dash':
                        t.beginPath(), t.moveTo(i, r), t.lineTo(i + n, r), t.closePath();
                    }
                    t.stroke();
                  }
                } else t.drawImage(e, i - e.width / 2, r - e.height / 2, e.width, e.height);
              },
              clipArea: function(t, e) {
                t.save(), t.beginPath(), t.rect(e.left, e.top, e.right - e.left, e.bottom - e.top), t.clip();
              },
              unclipArea: function(t) {
                t.restore();
              },
              lineTo: function(t, e, n, i) {
                if (n.steppedLine)
                  return (
                    ('after' === n.steppedLine && !i) || ('after' !== n.steppedLine && i)
                      ? t.lineTo(e.x, n.y)
                      : t.lineTo(n.x, e.y),
                    void t.lineTo(n.x, n.y)
                  );
                n.tension
                  ? t.bezierCurveTo(
                      i ? e.controlPointPreviousX : e.controlPointNextX,
                      i ? e.controlPointPreviousY : e.controlPointNextY,
                      i ? n.controlPointNextX : n.controlPointPreviousX,
                      i ? n.controlPointNextY : n.controlPointPreviousY,
                      n.x,
                      n.y
                    )
                  : t.lineTo(n.x, n.y);
              }
            }),
              (i.clear = n.clear),
              (i.drawRoundedRectangle = function(t) {
                t.beginPath(), n.roundedRect.apply(n, arguments), t.closePath();
              });
          },
          { 42: 42 }
        ],
        42: [
          function(t, e, n) {
            'use strict';
            var i,
              r = {
                noop: function() {},
                uid:
                  ((i = 0),
                  function() {
                    return i++;
                  }),
                isNullOrUndef: function(t) {
                  return null == t;
                },
                isArray: Array.isArray
                  ? Array.isArray
                  : function(t) {
                      return '[object Array]' === Object.prototype.toString.call(t);
                    },
                isObject: function(t) {
                  return null !== t && '[object Object]' === Object.prototype.toString.call(t);
                },
                valueOrDefault: function(t, e) {
                  return void 0 === t ? e : t;
                },
                valueAtIndexOrDefault: function(t, e, n) {
                  return r.valueOrDefault(r.isArray(t) ? t[e] : t, n);
                },
                callback: function(t, e, n) {
                  if (t && 'function' == typeof t.call) return t.apply(n, e);
                },
                each: function(t, e, n, i) {
                  var o, a, s;
                  if (r.isArray(t))
                    if (((a = t.length), i)) for (o = a - 1; o >= 0; o--) e.call(n, t[o], o);
                    else for (o = 0; o < a; o++) e.call(n, t[o], o);
                  else if (r.isObject(t))
                    for (a = (s = Object.keys(t)).length, o = 0; o < a; o++) e.call(n, t[s[o]], s[o]);
                },
                arrayEquals: function(t, e) {
                  var n, i, o, a;
                  if (!t || !e || t.length !== e.length) return !1;
                  for (n = 0, i = t.length; n < i; ++n)
                    if (((a = e[n]), (o = t[n]) instanceof Array && a instanceof Array)) {
                      if (!r.arrayEquals(o, a)) return !1;
                    } else if (o !== a) return !1;
                  return !0;
                },
                clone: function(t) {
                  if (r.isArray(t)) return t.map(r.clone);
                  if (r.isObject(t)) {
                    for (var e = {}, n = Object.keys(t), i = n.length, o = 0; o < i; ++o) e[n[o]] = r.clone(t[n[o]]);
                    return e;
                  }
                  return t;
                },
                _merger: function(t, e, n, i) {
                  var o = e[t],
                    a = n[t];
                  r.isObject(o) && r.isObject(a) ? r.merge(o, a, i) : (e[t] = r.clone(a));
                },
                _mergerIf: function(t, e, n) {
                  var i = e[t],
                    o = n[t];
                  r.isObject(i) && r.isObject(o) ? r.mergeIf(i, o) : e.hasOwnProperty(t) || (e[t] = r.clone(o));
                },
                merge: function(t, e, n) {
                  var i,
                    o,
                    a,
                    s,
                    l,
                    u = r.isArray(e) ? e : [e],
                    c = u.length;
                  if (!r.isObject(t)) return t;
                  for (i = (n = n || {}).merger || r._merger, o = 0; o < c; ++o)
                    if (r.isObject((e = u[o])))
                      for (l = 0, s = (a = Object.keys(e)).length; l < s; ++l) i(a[l], t, e, n);
                  return t;
                },
                mergeIf: function(t, e) {
                  return r.merge(t, e, { merger: r._mergerIf });
                },
                extend: function(t) {
                  for (
                    var e = function(e, n) {
                        t[n] = e;
                      },
                      n = 1,
                      i = arguments.length;
                    n < i;
                    ++n
                  )
                    r.each(arguments[n], e);
                  return t;
                },
                inherits: function(t) {
                  var e = this,
                    n =
                      t && t.hasOwnProperty('constructor')
                        ? t.constructor
                        : function() {
                            return e.apply(this, arguments);
                          },
                    i = function() {
                      this.constructor = n;
                    };
                  return (
                    (i.prototype = e.prototype),
                    (n.prototype = new i()),
                    (n.extend = r.inherits),
                    t && r.extend(n.prototype, t),
                    (n.__super__ = e.prototype),
                    n
                  );
                }
              };
            (e.exports = r),
              (r.callCallback = r.callback),
              (r.indexOf = function(t, e, n) {
                return Array.prototype.indexOf.call(t, e, n);
              }),
              (r.getValueOrDefault = r.valueOrDefault),
              (r.getValueAtIndexOrDefault = r.valueAtIndexOrDefault);
          },
          {}
        ],
        43: [
          function(t, e, n) {
            'use strict';
            var i = t(42),
              r = {
                linear: function(t) {
                  return t;
                },
                easeInQuad: function(t) {
                  return t * t;
                },
                easeOutQuad: function(t) {
                  return -t * (t - 2);
                },
                easeInOutQuad: function(t) {
                  return (t /= 0.5) < 1 ? 0.5 * t * t : -0.5 * (--t * (t - 2) - 1);
                },
                easeInCubic: function(t) {
                  return t * t * t;
                },
                easeOutCubic: function(t) {
                  return (t -= 1) * t * t + 1;
                },
                easeInOutCubic: function(t) {
                  return (t /= 0.5) < 1 ? 0.5 * t * t * t : 0.5 * ((t -= 2) * t * t + 2);
                },
                easeInQuart: function(t) {
                  return t * t * t * t;
                },
                easeOutQuart: function(t) {
                  return -((t -= 1) * t * t * t - 1);
                },
                easeInOutQuart: function(t) {
                  return (t /= 0.5) < 1 ? 0.5 * t * t * t * t : -0.5 * ((t -= 2) * t * t * t - 2);
                },
                easeInQuint: function(t) {
                  return t * t * t * t * t;
                },
                easeOutQuint: function(t) {
                  return (t -= 1) * t * t * t * t + 1;
                },
                easeInOutQuint: function(t) {
                  return (t /= 0.5) < 1 ? 0.5 * t * t * t * t * t : 0.5 * ((t -= 2) * t * t * t * t + 2);
                },
                easeInSine: function(t) {
                  return 1 - Math.cos(t * (Math.PI / 2));
                },
                easeOutSine: function(t) {
                  return Math.sin(t * (Math.PI / 2));
                },
                easeInOutSine: function(t) {
                  return -0.5 * (Math.cos(Math.PI * t) - 1);
                },
                easeInExpo: function(t) {
                  return 0 === t ? 0 : Math.pow(2, 10 * (t - 1));
                },
                easeOutExpo: function(t) {
                  return 1 === t ? 1 : 1 - Math.pow(2, -10 * t);
                },
                easeInOutExpo: function(t) {
                  return 0 === t
                    ? 0
                    : 1 === t
                    ? 1
                    : (t /= 0.5) < 1
                    ? 0.5 * Math.pow(2, 10 * (t - 1))
                    : 0.5 * (2 - Math.pow(2, -10 * --t));
                },
                easeInCirc: function(t) {
                  return t >= 1 ? t : -(Math.sqrt(1 - t * t) - 1);
                },
                easeOutCirc: function(t) {
                  return Math.sqrt(1 - (t -= 1) * t);
                },
                easeInOutCirc: function(t) {
                  return (t /= 0.5) < 1 ? -0.5 * (Math.sqrt(1 - t * t) - 1) : 0.5 * (Math.sqrt(1 - (t -= 2) * t) + 1);
                },
                easeInElastic: function(t) {
                  var e = 1.70158,
                    n = 0,
                    i = 1;
                  return 0 === t
                    ? 0
                    : 1 === t
                    ? 1
                    : (n || (n = 0.3),
                      i < 1 ? ((i = 1), (e = n / 4)) : (e = (n / (2 * Math.PI)) * Math.asin(1 / i)),
                      -i * Math.pow(2, 10 * (t -= 1)) * Math.sin(((t - e) * (2 * Math.PI)) / n));
                },
                easeOutElastic: function(t) {
                  var e = 1.70158,
                    n = 0,
                    i = 1;
                  return 0 === t
                    ? 0
                    : 1 === t
                    ? 1
                    : (n || (n = 0.3),
                      i < 1 ? ((i = 1), (e = n / 4)) : (e = (n / (2 * Math.PI)) * Math.asin(1 / i)),
                      i * Math.pow(2, -10 * t) * Math.sin(((t - e) * (2 * Math.PI)) / n) + 1);
                },
                easeInOutElastic: function(t) {
                  var e = 1.70158,
                    n = 0,
                    i = 1;
                  return 0 === t
                    ? 0
                    : 2 == (t /= 0.5)
                    ? 1
                    : (n || (n = 0.45),
                      i < 1 ? ((i = 1), (e = n / 4)) : (e = (n / (2 * Math.PI)) * Math.asin(1 / i)),
                      t < 1
                        ? i * Math.pow(2, 10 * (t -= 1)) * Math.sin(((t - e) * (2 * Math.PI)) / n) * -0.5
                        : i * Math.pow(2, -10 * (t -= 1)) * Math.sin(((t - e) * (2 * Math.PI)) / n) * 0.5 + 1);
                },
                easeInBack: function(t) {
                  return t * t * (2.70158 * t - 1.70158);
                },
                easeOutBack: function(t) {
                  return (t -= 1) * t * (2.70158 * t + 1.70158) + 1;
                },
                easeInOutBack: function(t) {
                  var e = 1.70158;
                  return (t /= 0.5) < 1
                    ? t * t * ((1 + (e *= 1.525)) * t - e) * 0.5
                    : 0.5 * ((t -= 2) * t * ((1 + (e *= 1.525)) * t + e) + 2);
                },
                easeInBounce: function(t) {
                  return 1 - r.easeOutBounce(1 - t);
                },
                easeOutBounce: function(t) {
                  return t < 1 / 2.75
                    ? 7.5625 * t * t
                    : t < 2 / 2.75
                    ? 7.5625 * (t -= 1.5 / 2.75) * t + 0.75
                    : t < 2.5 / 2.75
                    ? 7.5625 * (t -= 2.25 / 2.75) * t + 0.9375
                    : 7.5625 * (t -= 2.625 / 2.75) * t + 0.984375;
                },
                easeInOutBounce: function(t) {
                  return t < 0.5 ? 0.5 * r.easeInBounce(2 * t) : 0.5 * r.easeOutBounce(2 * t - 1) + 0.5;
                }
              };
            (e.exports = { effects: r }), (i.easingEffects = r);
          },
          { 42: 42 }
        ],
        44: [
          function(t, e, n) {
            'use strict';
            var i = t(42);
            e.exports = {
              toLineHeight: function(t, e) {
                var n = ('' + t).match(/^(normal|(\d+(?:\.\d+)?)(px|em|%)?)$/);
                if (!n || 'normal' === n[1]) return 1.2 * e;
                switch (((t = +n[2]), n[3])) {
                  case 'px':
                    return t;
                  case '%':
                    t /= 100;
                }
                return e * t;
              },
              toPadding: function(t) {
                var e, n, r, o;
                return (
                  i.isObject(t)
                    ? ((e = +t.top || 0), (n = +t.right || 0), (r = +t.bottom || 0), (o = +t.left || 0))
                    : (e = n = r = o = +t || 0),
                  { top: e, right: n, bottom: r, left: o, height: e + r, width: o + n }
                );
              },
              resolve: function(t, e, n) {
                var r, o, a;
                for (r = 0, o = t.length; r < o; ++r)
                  if (
                    void 0 !== (a = t[r]) &&
                    (void 0 !== e && 'function' == typeof a && (a = a(e)),
                    void 0 !== n && i.isArray(a) && (a = a[n]),
                    void 0 !== a)
                  )
                    return a;
              }
            };
          },
          { 42: 42 }
        ],
        45: [
          function(t, e, n) {
            'use strict';
            (e.exports = t(42)), (e.exports.easing = t(43)), (e.exports.canvas = t(41)), (e.exports.options = t(44));
          },
          { 41: 41, 42: 42, 43: 43, 44: 44 }
        ],
        46: [
          function(t, e, n) {
            e.exports = {
              acquireContext: function(t) {
                return t && t.canvas && (t = t.canvas), (t && t.getContext('2d')) || null;
              }
            };
          },
          {}
        ],
        47: [
          function(t, e, n) {
            'use strict';
            var i = t(45),
              r = '$chartjs',
              o = 'chartjs-',
              a = o + 'render-monitor',
              s = o + 'render-animation',
              l = ['animationstart', 'webkitAnimationStart'],
              u = {
                touchstart: 'mousedown',
                touchmove: 'mousemove',
                touchend: 'mouseup',
                pointerenter: 'mouseenter',
                pointerdown: 'mousedown',
                pointermove: 'mousemove',
                pointerup: 'mouseup',
                pointerleave: 'mouseout',
                pointerout: 'mouseout'
              };
            function c(t, e) {
              var n = i.getStyle(t, e),
                r = n && n.match(/^(\d+)(\.\d+)?px$/);
              return r ? Number(r[1]) : void 0;
            }
            var d = !!(function() {
              var t = !1;
              try {
                var e = Object.defineProperty({}, 'passive', {
                  get: function() {
                    t = !0;
                  }
                });
                window.addEventListener('e', null, e);
              } catch (t) {}
              return t;
            })() && { passive: !0 };
            function h(t, e, n) {
              t.addEventListener(e, n, d);
            }
            function f(t, e, n) {
              t.removeEventListener(e, n, d);
            }
            function p(t, e, n, i, r) {
              return { type: t, chart: e, native: r || null, x: void 0 !== n ? n : null, y: void 0 !== i ? i : null };
            }
            function g(t, e, n) {
              var u,
                c,
                d,
                f,
                g,
                m,
                v,
                b,
                y = t[r] || (t[r] = {}),
                x = (y.resizer = (function(t) {
                  var e = document.createElement('div'),
                    n = o + 'size-monitor',
                    i =
                      'position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;';
                  (e.style.cssText = i),
                    (e.className = n),
                    (e.innerHTML =
                      '<div class="' +
                      n +
                      '-expand" style="' +
                      i +
                      '"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="' +
                      n +
                      '-shrink" style="' +
                      i +
                      '"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div>');
                  var r = e.childNodes[0],
                    a = e.childNodes[1];
                  e._reset = function() {
                    (r.scrollLeft = 1e6), (r.scrollTop = 1e6), (a.scrollLeft = 1e6), (a.scrollTop = 1e6);
                  };
                  var s = function() {
                    e._reset(), t();
                  };
                  return h(r, 'scroll', s.bind(r, 'expand')), h(a, 'scroll', s.bind(a, 'shrink')), e;
                })(
                  ((u = function() {
                    if (y.resizer) return e(p('resize', n));
                  }),
                  (d = !1),
                  (f = []),
                  function() {
                    (f = Array.prototype.slice.call(arguments)),
                      (c = c || this),
                      d ||
                        ((d = !0),
                        i.requestAnimFrame.call(window, function() {
                          (d = !1), u.apply(c, f);
                        }));
                  })
                ));
              (m = function() {
                if (y.resizer) {
                  var e = t.parentNode;
                  e && e !== x.parentNode && e.insertBefore(x, e.firstChild), x._reset();
                }
              }),
                (v = (g = t)[r] || (g[r] = {})),
                (b = v.renderProxy = function(t) {
                  t.animationName === s && m();
                }),
                i.each(l, function(t) {
                  h(g, t, b);
                }),
                (v.reflow = !!g.offsetParent),
                g.classList.add(a);
            }
            function m(t) {
              var e,
                n,
                o,
                s = t[r] || {},
                u = s.resizer;
              delete s.resizer,
                (n = (e = t)[r] || {}),
                (o = n.renderProxy) &&
                  (i.each(l, function(t) {
                    f(e, t, o);
                  }),
                  delete n.renderProxy),
                e.classList.remove(a),
                u && u.parentNode && u.parentNode.removeChild(u);
            }
            (e.exports = {
              _enabled: 'undefined' != typeof window && 'undefined' != typeof document,
              initialize: function() {
                var t,
                  e,
                  n = 'from{opacity:0.99}to{opacity:1}';
                (t =
                  '@-webkit-keyframes ' +
                  s +
                  '{' +
                  n +
                  '}@keyframes ' +
                  s +
                  '{' +
                  n +
                  '}.' +
                  a +
                  '{-webkit-animation:' +
                  s +
                  ' 0.001s;animation:' +
                  s +
                  ' 0.001s;}'),
                  (e = this._style || document.createElement('style')),
                  this._style ||
                    ((this._style = e),
                    (t = '/* Chart.js */\n' + t),
                    e.setAttribute('type', 'text/css'),
                    document.getElementsByTagName('head')[0].appendChild(e)),
                  e.appendChild(document.createTextNode(t));
              },
              acquireContext: function(t, e) {
                'string' == typeof t ? (t = document.getElementById(t)) : t.length && (t = t[0]),
                  t && t.canvas && (t = t.canvas);
                var n = t && t.getContext && t.getContext('2d');
                return n && n.canvas === t
                  ? ((function(t, e) {
                      var n = t.style,
                        i = t.getAttribute('height'),
                        o = t.getAttribute('width');
                      if (
                        ((t[r] = {
                          initial: {
                            height: i,
                            width: o,
                            style: { display: n.display, height: n.height, width: n.width }
                          }
                        }),
                        (n.display = n.display || 'block'),
                        null === o || '' === o)
                      ) {
                        var a = c(t, 'width');
                        void 0 !== a && (t.width = a);
                      }
                      if (null === i || '' === i)
                        if ('' === t.style.height) t.height = t.width / (e.options.aspectRatio || 2);
                        else {
                          var s = c(t, 'height');
                          void 0 !== a && (t.height = s);
                        }
                    })(t, e),
                    n)
                  : null;
              },
              releaseContext: function(t) {
                var e = t.canvas;
                if (e[r]) {
                  var n = e[r].initial;
                  ['height', 'width'].forEach(function(t) {
                    var r = n[t];
                    i.isNullOrUndef(r) ? e.removeAttribute(t) : e.setAttribute(t, r);
                  }),
                    i.each(n.style || {}, function(t, n) {
                      e.style[n] = t;
                    }),
                    (e.width = e.width),
                    delete e[r];
                }
              },
              addEventListener: function(t, e, n) {
                var o = t.canvas;
                if ('resize' !== e) {
                  var a = n[r] || (n[r] = {});
                  h(
                    o,
                    e,
                    ((a.proxies || (a.proxies = {}))[t.id + '_' + e] = function(e) {
                      var r, o, a;
                      n(p(u[(r = e).type] || r.type, (o = t), (a = i.getRelativePosition(r, o)).x, a.y, r));
                    })
                  );
                } else g(o, n, t);
              },
              removeEventListener: function(t, e, n) {
                var i = t.canvas;
                if ('resize' !== e) {
                  var o = ((n[r] || {}).proxies || {})[t.id + '_' + e];
                  o && f(i, e, o);
                } else m(i);
              }
            }),
              (i.addEvent = h),
              (i.removeEvent = f);
          },
          { 45: 45 }
        ],
        48: [
          function(t, e, n) {
            'use strict';
            var i = t(45),
              r = t(46),
              o = t(47);
            e.exports = i.extend(
              {
                initialize: function() {},
                acquireContext: function() {},
                releaseContext: function() {},
                addEventListener: function() {},
                removeEventListener: function() {}
              },
              o._enabled ? o : r
            );
          },
          { 45: 45, 46: 46, 47: 47 }
        ],
        49: [
          function(t, e, n) {
            'use strict';
            (e.exports = {}), (e.exports.filler = t(50)), (e.exports.legend = t(51)), (e.exports.title = t(52));
          },
          { 50: 50, 51: 51, 52: 52 }
        ],
        50: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(40),
              o = t(45);
            i._set('global', { plugins: { filler: { propagate: !0 } } });
            var a = {
              dataset: function(t) {
                var e = t.fill,
                  n = t.chart,
                  i = n.getDatasetMeta(e),
                  r = (i && n.isDatasetVisible(e) && i.dataset._children) || [],
                  o = r.length || 0;
                return o
                  ? function(t, e) {
                      return (e < o && r[e]._view) || null;
                    }
                  : null;
              },
              boundary: function(t) {
                var e = t.boundary,
                  n = e ? e.x : null,
                  i = e ? e.y : null;
                return function(t) {
                  return { x: null === n ? t.x : n, y: null === i ? t.y : i };
                };
              }
            };
            function s(t, e, n) {
              var i,
                r = t._model || {},
                o = r.fill;
              if ((void 0 === o && (o = !!r.backgroundColor), !1 === o || null === o)) return !1;
              if (!0 === o) return 'origin';
              if (((i = parseFloat(o, 10)), isFinite(i) && Math.floor(i) === i))
                return ('-' !== o[0] && '+' !== o[0]) || (i = e + i), !(i === e || i < 0 || i >= n) && i;
              switch (o) {
                case 'bottom':
                  return 'start';
                case 'top':
                  return 'end';
                case 'zero':
                  return 'origin';
                case 'origin':
                case 'start':
                case 'end':
                  return o;
                default:
                  return !1;
              }
            }
            function l(t) {
              var e,
                n = t.el._model || {},
                i = t.el._scale || {},
                r = t.fill,
                o = null;
              if (isFinite(r)) return null;
              if (
                ('start' === r
                  ? (o = void 0 === n.scaleBottom ? i.bottom : n.scaleBottom)
                  : 'end' === r
                  ? (o = void 0 === n.scaleTop ? i.top : n.scaleTop)
                  : void 0 !== n.scaleZero
                  ? (o = n.scaleZero)
                  : i.getBasePosition
                  ? (o = i.getBasePosition())
                  : i.getBasePixel && (o = i.getBasePixel()),
                null != o)
              ) {
                if (void 0 !== o.x && void 0 !== o.y) return o;
                if ('number' == typeof o && isFinite(o))
                  return { x: (e = i.isHorizontal()) ? o : null, y: e ? null : o };
              }
              return null;
            }
            function u(t, e, n) {
              var i,
                r = t[e].fill,
                o = [e];
              if (!n) return r;
              for (; !1 !== r && -1 === o.indexOf(r); ) {
                if (!isFinite(r)) return r;
                if (!(i = t[r])) return !1;
                if (i.visible) return r;
                o.push(r), (r = i.fill);
              }
              return !1;
            }
            function c(t) {
              return t && !t.skip;
            }
            function d(t, e, n, i, r) {
              var a;
              if (i && r) {
                for (t.moveTo(e[0].x, e[0].y), a = 1; a < i; ++a) o.canvas.lineTo(t, e[a - 1], e[a]);
                for (t.lineTo(n[r - 1].x, n[r - 1].y), a = r - 1; a > 0; --a) o.canvas.lineTo(t, n[a], n[a - 1], !0);
              }
            }
            e.exports = {
              id: 'filler',
              afterDatasetsUpdate: function(t, e) {
                var n,
                  i,
                  o,
                  c,
                  d,
                  h,
                  f,
                  p = (t.data.datasets || []).length,
                  g = e.propagate,
                  m = [];
                for (i = 0; i < p; ++i)
                  (c = null),
                    (o = (n = t.getDatasetMeta(i)).dataset) &&
                      o._model &&
                      o instanceof r.Line &&
                      (c = { visible: t.isDatasetVisible(i), fill: s(o, i, p), chart: t, el: o }),
                    (n.$filler = c),
                    m.push(c);
                for (i = 0; i < p; ++i)
                  (c = m[i]) &&
                    ((c.fill = u(m, i, g)),
                    (c.boundary = l(c)),
                    (c.mapper =
                      ((f = void 0),
                      (f = 'dataset'),
                      !1 === (h = (d = c).fill) ? null : (isFinite(h) || (f = 'boundary'), a[f](d)))));
              },
              beforeDatasetDraw: function(t, e) {
                var n = e.meta.$filler;
                if (n) {
                  var r = t.ctx,
                    a = n.el,
                    s = a._view,
                    l = a._children || [],
                    u = n.mapper,
                    h = s.backgroundColor || i.global.defaultColor;
                  u &&
                    h &&
                    l.length &&
                    (o.canvas.clipArea(r, t.chartArea),
                    (function(t, e, n, i, r, o) {
                      var a,
                        s,
                        l,
                        u,
                        h,
                        f,
                        p,
                        g = e.length,
                        m = i.spanGaps,
                        v = [],
                        b = [],
                        y = 0,
                        x = 0;
                      for (t.beginPath(), a = 0, s = g + !!o; a < s; ++a)
                        (h = n((u = e[(l = a % g)]._view), l, i)),
                          (f = c(u)),
                          (p = c(h)),
                          f && p
                            ? ((y = v.push(u)), (x = b.push(h)))
                            : y &&
                              x &&
                              (m
                                ? (f && v.push(u), p && b.push(h))
                                : (d(t, v, b, y, x), (y = x = 0), (v = []), (b = [])));
                      d(t, v, b, y, x), t.closePath(), (t.fillStyle = r), t.fill();
                    })(r, l, u, s, h, a._loop),
                    o.canvas.unclipArea(r));
                }
              }
            };
          },
          { 25: 25, 40: 40, 45: 45 }
        ],
        51: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(26),
              o = t(45),
              a = t(30),
              s = o.noop;
            function l(t, e) {
              return t.usePointStyle ? e * Math.SQRT2 : t.boxWidth;
            }
            i._set('global', {
              legend: {
                display: !0,
                position: 'top',
                fullWidth: !0,
                reverse: !1,
                weight: 1e3,
                onClick: function(t, e) {
                  var n = e.datasetIndex,
                    i = this.chart,
                    r = i.getDatasetMeta(n);
                  (r.hidden = null === r.hidden ? !i.data.datasets[n].hidden : null), i.update();
                },
                onHover: null,
                labels: {
                  boxWidth: 40,
                  padding: 10,
                  generateLabels: function(t) {
                    var e = t.data;
                    return o.isArray(e.datasets)
                      ? e.datasets.map(function(e, n) {
                          return {
                            text: e.label,
                            fillStyle: o.isArray(e.backgroundColor) ? e.backgroundColor[0] : e.backgroundColor,
                            hidden: !t.isDatasetVisible(n),
                            lineCap: e.borderCapStyle,
                            lineDash: e.borderDash,
                            lineDashOffset: e.borderDashOffset,
                            lineJoin: e.borderJoinStyle,
                            lineWidth: e.borderWidth,
                            strokeStyle: e.borderColor,
                            pointStyle: e.pointStyle,
                            datasetIndex: n
                          };
                        }, this)
                      : [];
                  }
                }
              },
              legendCallback: function(t) {
                var e = [];
                e.push('<ul class="' + t.id + '-legend">');
                for (var n = 0; n < t.data.datasets.length; n++)
                  e.push('<li><span style="background-color:' + t.data.datasets[n].backgroundColor + '"></span>'),
                    t.data.datasets[n].label && e.push(t.data.datasets[n].label),
                    e.push('</li>');
                return e.push('</ul>'), e.join('');
              }
            });
            var u = r.extend({
              initialize: function(t) {
                o.extend(this, t), (this.legendHitBoxes = []), (this.doughnutMode = !1);
              },
              beforeUpdate: s,
              update: function(t, e, n) {
                var i = this;
                return (
                  i.beforeUpdate(),
                  (i.maxWidth = t),
                  (i.maxHeight = e),
                  (i.margins = n),
                  i.beforeSetDimensions(),
                  i.setDimensions(),
                  i.afterSetDimensions(),
                  i.beforeBuildLabels(),
                  i.buildLabels(),
                  i.afterBuildLabels(),
                  i.beforeFit(),
                  i.fit(),
                  i.afterFit(),
                  i.afterUpdate(),
                  i.minSize
                );
              },
              afterUpdate: s,
              beforeSetDimensions: s,
              setDimensions: function() {
                var t = this;
                t.isHorizontal()
                  ? ((t.width = t.maxWidth), (t.left = 0), (t.right = t.width))
                  : ((t.height = t.maxHeight), (t.top = 0), (t.bottom = t.height)),
                  (t.paddingLeft = 0),
                  (t.paddingTop = 0),
                  (t.paddingRight = 0),
                  (t.paddingBottom = 0),
                  (t.minSize = { width: 0, height: 0 });
              },
              afterSetDimensions: s,
              beforeBuildLabels: s,
              buildLabels: function() {
                var t = this,
                  e = t.options.labels || {},
                  n = o.callback(e.generateLabels, [t.chart], t) || [];
                e.filter &&
                  (n = n.filter(function(n) {
                    return e.filter(n, t.chart.data);
                  })),
                  t.options.reverse && n.reverse(),
                  (t.legendItems = n);
              },
              afterBuildLabels: s,
              beforeFit: s,
              fit: function() {
                var t = this,
                  e = t.options,
                  n = e.labels,
                  r = e.display,
                  a = t.ctx,
                  s = i.global,
                  u = o.valueOrDefault,
                  c = u(n.fontSize, s.defaultFontSize),
                  d = u(n.fontStyle, s.defaultFontStyle),
                  h = u(n.fontFamily, s.defaultFontFamily),
                  f = o.fontString(c, d, h),
                  p = (t.legendHitBoxes = []),
                  g = t.minSize,
                  m = t.isHorizontal();
                if (
                  (m
                    ? ((g.width = t.maxWidth), (g.height = r ? 10 : 0))
                    : ((g.width = r ? 10 : 0), (g.height = t.maxHeight)),
                  r)
                )
                  if (((a.font = f), m)) {
                    var v = (t.lineWidths = [0]),
                      b = t.legendItems.length ? c + n.padding : 0;
                    (a.textAlign = 'left'),
                      (a.textBaseline = 'top'),
                      o.each(t.legendItems, function(e, i) {
                        var r = l(n, c) + c / 2 + a.measureText(e.text).width;
                        v[v.length - 1] + r + n.padding >= t.width && ((b += c + n.padding), (v[v.length] = t.left)),
                          (p[i] = { left: 0, top: 0, width: r, height: c }),
                          (v[v.length - 1] += r + n.padding);
                      }),
                      (g.height += b);
                  } else {
                    var y = n.padding,
                      x = (t.columnWidths = []),
                      w = n.padding,
                      _ = 0,
                      C = 0,
                      k = c + y;
                    o.each(t.legendItems, function(t, e) {
                      var i = l(n, c) + c / 2 + a.measureText(t.text).width;
                      C + k > g.height && ((w += _ + n.padding), x.push(_), (_ = 0), (C = 0)),
                        (_ = Math.max(_, i)),
                        (C += k),
                        (p[e] = { left: 0, top: 0, width: i, height: c });
                    }),
                      (w += _),
                      x.push(_),
                      (g.width += w);
                  }
                (t.width = g.width), (t.height = g.height);
              },
              afterFit: s,
              isHorizontal: function() {
                return 'top' === this.options.position || 'bottom' === this.options.position;
              },
              draw: function() {
                var t = this,
                  e = t.options,
                  n = e.labels,
                  r = i.global,
                  a = r.elements.line,
                  s = t.width,
                  u = t.lineWidths;
                if (e.display) {
                  var c,
                    d = t.ctx,
                    h = o.valueOrDefault,
                    f = h(n.fontColor, r.defaultFontColor),
                    p = h(n.fontSize, r.defaultFontSize),
                    g = h(n.fontStyle, r.defaultFontStyle),
                    m = h(n.fontFamily, r.defaultFontFamily),
                    v = o.fontString(p, g, m);
                  (d.textAlign = 'left'),
                    (d.textBaseline = 'middle'),
                    (d.lineWidth = 0.5),
                    (d.strokeStyle = f),
                    (d.fillStyle = f),
                    (d.font = v);
                  var b = l(n, p),
                    y = t.legendHitBoxes,
                    x = t.isHorizontal();
                  c = x
                    ? { x: t.left + (s - u[0]) / 2, y: t.top + n.padding, line: 0 }
                    : { x: t.left + n.padding, y: t.top + n.padding, line: 0 };
                  var w = p + n.padding;
                  o.each(t.legendItems, function(i, l) {
                    var f,
                      g,
                      m,
                      v,
                      _,
                      C = d.measureText(i.text).width,
                      k = b + p / 2 + C,
                      S = c.x,
                      T = c.y;
                    x
                      ? S + k >= s && ((T = c.y += w), c.line++, (S = c.x = t.left + (s - u[c.line]) / 2))
                      : T + w > t.bottom &&
                        ((S = c.x = S + t.columnWidths[c.line] + n.padding), (T = c.y = t.top + n.padding), c.line++),
                      (function(t, n, i) {
                        if (!(isNaN(b) || b <= 0)) {
                          d.save(),
                            (d.fillStyle = h(i.fillStyle, r.defaultColor)),
                            (d.lineCap = h(i.lineCap, a.borderCapStyle)),
                            (d.lineDashOffset = h(i.lineDashOffset, a.borderDashOffset)),
                            (d.lineJoin = h(i.lineJoin, a.borderJoinStyle)),
                            (d.lineWidth = h(i.lineWidth, a.borderWidth)),
                            (d.strokeStyle = h(i.strokeStyle, r.defaultColor));
                          var s = 0 === h(i.lineWidth, a.borderWidth);
                          if (
                            (d.setLineDash && d.setLineDash(h(i.lineDash, a.borderDash)),
                            e.labels && e.labels.usePointStyle)
                          ) {
                            var l = (p * Math.SQRT2) / 2,
                              u = l / Math.SQRT2;
                            o.canvas.drawPoint(d, i.pointStyle, l, t + u, n + u);
                          } else s || d.strokeRect(t, n, b, p), d.fillRect(t, n, b, p);
                          d.restore();
                        }
                      })(S, T, i),
                      (y[l].left = S),
                      (y[l].top = T),
                      (g = C),
                      (v = b + (m = p / 2) + S),
                      d.fillText((f = i).text, v, (_ = T + m)),
                      f.hidden && (d.beginPath(), (d.lineWidth = 2), d.moveTo(v, _), d.lineTo(v + g, _), d.stroke()),
                      x ? (c.x += k + n.padding) : (c.y += w);
                  });
                }
              },
              handleEvent: function(t) {
                var e = this,
                  n = e.options,
                  i = 'mouseup' === t.type ? 'click' : t.type,
                  r = !1;
                if ('mousemove' === i) {
                  if (!n.onHover) return;
                } else {
                  if ('click' !== i) return;
                  if (!n.onClick) return;
                }
                var o = t.x,
                  a = t.y;
                if (o >= e.left && o <= e.right && a >= e.top && a <= e.bottom)
                  for (var s = e.legendHitBoxes, l = 0; l < s.length; ++l) {
                    var u = s[l];
                    if (o >= u.left && o <= u.left + u.width && a >= u.top && a <= u.top + u.height) {
                      if ('click' === i) {
                        n.onClick.call(e, t.native, e.legendItems[l]), (r = !0);
                        break;
                      }
                      if ('mousemove' === i) {
                        n.onHover.call(e, t.native, e.legendItems[l]), (r = !0);
                        break;
                      }
                    }
                  }
                return r;
              }
            });
            function c(t, e) {
              var n = new u({ ctx: t.ctx, options: e, chart: t });
              a.configure(t, n, e), a.addBox(t, n), (t.legend = n);
            }
            e.exports = {
              id: 'legend',
              _element: u,
              beforeInit: function(t) {
                var e = t.options.legend;
                e && c(t, e);
              },
              beforeUpdate: function(t) {
                var e = t.options.legend,
                  n = t.legend;
                e
                  ? (o.mergeIf(e, i.global.legend), n ? (a.configure(t, n, e), (n.options = e)) : c(t, e))
                  : n && (a.removeBox(t, n), delete t.legend);
              },
              afterEvent: function(t, e) {
                var n = t.legend;
                n && n.handleEvent(e);
              }
            };
          },
          { 25: 25, 26: 26, 30: 30, 45: 45 }
        ],
        52: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(26),
              o = t(45),
              a = t(30),
              s = o.noop;
            i._set('global', {
              title: {
                display: !1,
                fontStyle: 'bold',
                fullWidth: !0,
                lineHeight: 1.2,
                padding: 10,
                position: 'top',
                text: '',
                weight: 2e3
              }
            });
            var l = r.extend({
              initialize: function(t) {
                o.extend(this, t), (this.legendHitBoxes = []);
              },
              beforeUpdate: s,
              update: function(t, e, n) {
                var i = this;
                return (
                  i.beforeUpdate(),
                  (i.maxWidth = t),
                  (i.maxHeight = e),
                  (i.margins = n),
                  i.beforeSetDimensions(),
                  i.setDimensions(),
                  i.afterSetDimensions(),
                  i.beforeBuildLabels(),
                  i.buildLabels(),
                  i.afterBuildLabels(),
                  i.beforeFit(),
                  i.fit(),
                  i.afterFit(),
                  i.afterUpdate(),
                  i.minSize
                );
              },
              afterUpdate: s,
              beforeSetDimensions: s,
              setDimensions: function() {
                var t = this;
                t.isHorizontal()
                  ? ((t.width = t.maxWidth), (t.left = 0), (t.right = t.width))
                  : ((t.height = t.maxHeight), (t.top = 0), (t.bottom = t.height)),
                  (t.paddingLeft = 0),
                  (t.paddingTop = 0),
                  (t.paddingRight = 0),
                  (t.paddingBottom = 0),
                  (t.minSize = { width: 0, height: 0 });
              },
              afterSetDimensions: s,
              beforeBuildLabels: s,
              buildLabels: s,
              afterBuildLabels: s,
              beforeFit: s,
              fit: function() {
                var t = this,
                  e = t.options,
                  n = e.display,
                  r = (0, o.valueOrDefault)(e.fontSize, i.global.defaultFontSize),
                  a = t.minSize,
                  s = o.isArray(e.text) ? e.text.length : 1,
                  l = o.options.toLineHeight(e.lineHeight, r),
                  u = n ? s * l + 2 * e.padding : 0;
                t.isHorizontal() ? ((a.width = t.maxWidth), (a.height = u)) : ((a.width = u), (a.height = t.maxHeight)),
                  (t.width = a.width),
                  (t.height = a.height);
              },
              afterFit: s,
              isHorizontal: function() {
                var t = this.options.position;
                return 'top' === t || 'bottom' === t;
              },
              draw: function() {
                var t = this,
                  e = t.ctx,
                  n = o.valueOrDefault,
                  r = t.options,
                  a = i.global;
                if (r.display) {
                  var s,
                    l,
                    u,
                    c = n(r.fontSize, a.defaultFontSize),
                    d = n(r.fontStyle, a.defaultFontStyle),
                    h = n(r.fontFamily, a.defaultFontFamily),
                    f = o.fontString(c, d, h),
                    p = o.options.toLineHeight(r.lineHeight, c),
                    g = p / 2 + r.padding,
                    m = 0,
                    v = t.top,
                    b = t.left,
                    y = t.bottom,
                    x = t.right;
                  (e.fillStyle = n(r.fontColor, a.defaultFontColor)),
                    (e.font = f),
                    t.isHorizontal()
                      ? ((l = b + (x - b) / 2), (u = v + g), (s = x - b))
                      : ((l = 'left' === r.position ? b + g : x - g),
                        (u = v + (y - v) / 2),
                        (s = y - v),
                        (m = Math.PI * ('left' === r.position ? -0.5 : 0.5))),
                    e.save(),
                    e.translate(l, u),
                    e.rotate(m),
                    (e.textAlign = 'center'),
                    (e.textBaseline = 'middle');
                  var w = r.text;
                  if (o.isArray(w)) for (var _ = 0, C = 0; C < w.length; ++C) e.fillText(w[C], 0, _, s), (_ += p);
                  else e.fillText(w, 0, 0, s);
                  e.restore();
                }
              }
            });
            function u(t, e) {
              var n = new l({ ctx: t.ctx, options: e, chart: t });
              a.configure(t, n, e), a.addBox(t, n), (t.titleBlock = n);
            }
            e.exports = {
              id: 'title',
              _element: l,
              beforeInit: function(t) {
                var e = t.options.title;
                e && u(t, e);
              },
              beforeUpdate: function(t) {
                var e = t.options.title,
                  n = t.titleBlock;
                e
                  ? (o.mergeIf(e, i.global.title), n ? (a.configure(t, n, e), (n.options = e)) : u(t, e))
                  : n && (a.removeBox(t, n), delete t.titleBlock);
              }
            };
          },
          { 25: 25, 26: 26, 30: 30, 45: 45 }
        ],
        53: [
          function(t, e, n) {
            'use strict';
            e.exports = function(t) {
              var e = t.Scale.extend({
                getLabels: function() {
                  var t = this.chart.data;
                  return this.options.labels || (this.isHorizontal() ? t.xLabels : t.yLabels) || t.labels;
                },
                determineDataLimits: function() {
                  var t,
                    e = this,
                    n = e.getLabels();
                  (e.minIndex = 0),
                    (e.maxIndex = n.length - 1),
                    void 0 !== e.options.ticks.min &&
                      ((t = n.indexOf(e.options.ticks.min)), (e.minIndex = -1 !== t ? t : e.minIndex)),
                    void 0 !== e.options.ticks.max &&
                      ((t = n.indexOf(e.options.ticks.max)), (e.maxIndex = -1 !== t ? t : e.maxIndex)),
                    (e.min = n[e.minIndex]),
                    (e.max = n[e.maxIndex]);
                },
                buildTicks: function() {
                  var t = this,
                    e = t.getLabels();
                  t.ticks = 0 === t.minIndex && t.maxIndex === e.length - 1 ? e : e.slice(t.minIndex, t.maxIndex + 1);
                },
                getLabelForIndex: function(t, e) {
                  var n = this,
                    i = n.chart.data,
                    r = n.isHorizontal();
                  return i.yLabels && !r ? n.getRightValue(i.datasets[e].data[t]) : n.ticks[t - n.minIndex];
                },
                getPixelForValue: function(t, e) {
                  var n,
                    i = this,
                    r = i.options.offset,
                    o = Math.max(i.maxIndex + 1 - i.minIndex - (r ? 0 : 1), 1);
                  if ((null != t && (n = i.isHorizontal() ? t.x : t.y), void 0 !== n || (void 0 !== t && isNaN(e)))) {
                    t = n || t;
                    var a = i.getLabels().indexOf(t);
                    e = -1 !== a ? a : e;
                  }
                  if (i.isHorizontal()) {
                    var s = i.width / o,
                      l = s * (e - i.minIndex);
                    return r && (l += s / 2), i.left + Math.round(l);
                  }
                  var u = i.height / o,
                    c = u * (e - i.minIndex);
                  return r && (c += u / 2), i.top + Math.round(c);
                },
                getPixelForTick: function(t) {
                  return this.getPixelForValue(this.ticks[t], t + this.minIndex, null);
                },
                getValueForPixel: function(t) {
                  var e = this,
                    n = e.options.offset,
                    i = Math.max(e._ticks.length - (n ? 0 : 1), 1),
                    r = e.isHorizontal(),
                    o = (r ? e.width : e.height) / i;
                  return (t -= r ? e.left : e.top), n && (t -= o / 2), (t <= 0 ? 0 : Math.round(t / o)) + e.minIndex;
                },
                getBasePixel: function() {
                  return this.bottom;
                }
              });
              t.scaleService.registerScaleType('category', e, { position: 'bottom' });
            };
          },
          {}
        ],
        54: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(45),
              o = t(34);
            e.exports = function(t) {
              var e = { position: 'left', ticks: { callback: o.formatters.linear } },
                n = t.LinearScaleBase.extend({
                  determineDataLimits: function() {
                    var t = this,
                      e = t.options,
                      n = t.chart,
                      i = n.data.datasets,
                      o = t.isHorizontal();
                    function a(e) {
                      return o ? e.xAxisID === t.id : e.yAxisID === t.id;
                    }
                    (t.min = null), (t.max = null);
                    var s = e.stacked;
                    if (
                      (void 0 === s &&
                        r.each(i, function(t, e) {
                          if (!s) {
                            var i = n.getDatasetMeta(e);
                            n.isDatasetVisible(e) && a(i) && void 0 !== i.stack && (s = !0);
                          }
                        }),
                      e.stacked || s)
                    ) {
                      var l = {};
                      r.each(i, function(i, o) {
                        var s = n.getDatasetMeta(o),
                          u = [s.type, void 0 === e.stacked && void 0 === s.stack ? o : '', s.stack].join('.');
                        void 0 === l[u] && (l[u] = { positiveValues: [], negativeValues: [] });
                        var c = l[u].positiveValues,
                          d = l[u].negativeValues;
                        n.isDatasetVisible(o) &&
                          a(s) &&
                          r.each(i.data, function(n, i) {
                            var r = +t.getRightValue(n);
                            isNaN(r) ||
                              s.data[i].hidden ||
                              ((c[i] = c[i] || 0),
                              (d[i] = d[i] || 0),
                              e.relativePoints ? (c[i] = 100) : r < 0 ? (d[i] += r) : (c[i] += r));
                          });
                      }),
                        r.each(l, function(e) {
                          var n = e.positiveValues.concat(e.negativeValues),
                            i = r.min(n),
                            o = r.max(n);
                          (t.min = null === t.min ? i : Math.min(t.min, i)),
                            (t.max = null === t.max ? o : Math.max(t.max, o));
                        });
                    } else
                      r.each(i, function(e, i) {
                        var o = n.getDatasetMeta(i);
                        n.isDatasetVisible(i) &&
                          a(o) &&
                          r.each(e.data, function(e, n) {
                            var i = +t.getRightValue(e);
                            isNaN(i) ||
                              o.data[n].hidden ||
                              ((null === t.min || i < t.min) && (t.min = i),
                              (null === t.max || i > t.max) && (t.max = i));
                          });
                      });
                    (t.min = isFinite(t.min) && !isNaN(t.min) ? t.min : 0),
                      (t.max = isFinite(t.max) && !isNaN(t.max) ? t.max : 1),
                      this.handleTickRangeOptions();
                  },
                  getTickLimit: function() {
                    var t,
                      e = this.options.ticks;
                    if (this.isHorizontal())
                      t = Math.min(e.maxTicksLimit ? e.maxTicksLimit : 11, Math.ceil(this.width / 50));
                    else {
                      var n = r.valueOrDefault(e.fontSize, i.global.defaultFontSize);
                      t = Math.min(e.maxTicksLimit ? e.maxTicksLimit : 11, Math.ceil(this.height / (2 * n)));
                    }
                    return t;
                  },
                  handleDirectionalChanges: function() {
                    this.isHorizontal() || this.ticks.reverse();
                  },
                  getLabelForIndex: function(t, e) {
                    return +this.getRightValue(this.chart.data.datasets[e].data[t]);
                  },
                  getPixelForValue: function(t) {
                    var e = this,
                      n = e.start,
                      i = +e.getRightValue(t),
                      r = e.end - n;
                    return e.isHorizontal() ? e.left + (e.width / r) * (i - n) : e.bottom - (e.height / r) * (i - n);
                  },
                  getValueForPixel: function(t) {
                    var e = this,
                      n = e.isHorizontal();
                    return e.start + ((n ? t - e.left : e.bottom - t) / (n ? e.width : e.height)) * (e.end - e.start);
                  },
                  getPixelForTick: function(t) {
                    return this.getPixelForValue(this.ticksAsNumbers[t]);
                  }
                });
              t.scaleService.registerScaleType('linear', n, e);
            };
          },
          { 25: 25, 34: 34, 45: 45 }
        ],
        55: [
          function(t, e, n) {
            'use strict';
            var i = t(45);
            e.exports = function(t) {
              var e = i.noop;
              t.LinearScaleBase = t.Scale.extend({
                getRightValue: function(e) {
                  return 'string' == typeof e ? +e : t.Scale.prototype.getRightValue.call(this, e);
                },
                handleTickRangeOptions: function() {
                  var t = this,
                    e = t.options.ticks;
                  if (e.beginAtZero) {
                    var n = i.sign(t.min),
                      r = i.sign(t.max);
                    n < 0 && r < 0 ? (t.max = 0) : n > 0 && r > 0 && (t.min = 0);
                  }
                  var o = void 0 !== e.min || void 0 !== e.suggestedMin,
                    a = void 0 !== e.max || void 0 !== e.suggestedMax;
                  void 0 !== e.min
                    ? (t.min = e.min)
                    : void 0 !== e.suggestedMin &&
                      (t.min = null === t.min ? e.suggestedMin : Math.min(t.min, e.suggestedMin)),
                    void 0 !== e.max
                      ? (t.max = e.max)
                      : void 0 !== e.suggestedMax &&
                        (t.max = null === t.max ? e.suggestedMax : Math.max(t.max, e.suggestedMax)),
                    o !== a && t.min >= t.max && (o ? (t.max = t.min + 1) : (t.min = t.max - 1)),
                    t.min === t.max && (t.max++, e.beginAtZero || t.min--);
                },
                getTickLimit: e,
                handleDirectionalChanges: e,
                buildTicks: function() {
                  var t = this,
                    e = t.options.ticks,
                    n = t.getTickLimit(),
                    r = {
                      maxTicks: (n = Math.max(2, n)),
                      min: e.min,
                      max: e.max,
                      stepSize: i.valueOrDefault(e.fixedStepSize, e.stepSize)
                    },
                    o = (t.ticks = (function(t, e) {
                      var n,
                        r = [];
                      if (t.stepSize && t.stepSize > 0) n = t.stepSize;
                      else {
                        var o = i.niceNum(e.max - e.min, !1);
                        n = i.niceNum(o / (t.maxTicks - 1), !0);
                      }
                      var a = Math.floor(e.min / n) * n,
                        s = Math.ceil(e.max / n) * n;
                      t.min &&
                        t.max &&
                        t.stepSize &&
                        i.almostWhole((t.max - t.min) / t.stepSize, n / 1e3) &&
                        ((a = t.min), (s = t.max));
                      var l = (s - a) / n;
                      l = i.almostEquals(l, Math.round(l), n / 1e3) ? Math.round(l) : Math.ceil(l);
                      var u = 1;
                      n < 1 &&
                        ((u = Math.pow(10, n.toString().length - 2)),
                        (a = Math.round(a * u) / u),
                        (s = Math.round(s * u) / u)),
                        r.push(void 0 !== t.min ? t.min : a);
                      for (var c = 1; c < l; ++c) r.push(Math.round((a + c * n) * u) / u);
                      return r.push(void 0 !== t.max ? t.max : s), r;
                    })(r, t));
                  t.handleDirectionalChanges(),
                    (t.max = i.max(o)),
                    (t.min = i.min(o)),
                    e.reverse
                      ? (o.reverse(), (t.start = t.max), (t.end = t.min))
                      : ((t.start = t.min), (t.end = t.max));
                },
                convertTicksToLabels: function() {
                  var e = this;
                  (e.ticksAsNumbers = e.ticks.slice()),
                    (e.zeroLineIndex = e.ticks.indexOf(0)),
                    t.Scale.prototype.convertTicksToLabels.call(e);
                }
              });
            };
          },
          { 45: 45 }
        ],
        56: [
          function(t, e, n) {
            'use strict';
            var i = t(45),
              r = t(34);
            e.exports = function(t) {
              var e = { position: 'left', ticks: { callback: r.formatters.logarithmic } },
                n = t.Scale.extend({
                  determineDataLimits: function() {
                    var t = this,
                      e = t.options,
                      n = t.chart,
                      r = n.data.datasets,
                      o = t.isHorizontal();
                    function a(e) {
                      return o ? e.xAxisID === t.id : e.yAxisID === t.id;
                    }
                    (t.min = null), (t.max = null), (t.minNotZero = null);
                    var s = e.stacked;
                    if (
                      (void 0 === s &&
                        i.each(r, function(t, e) {
                          if (!s) {
                            var i = n.getDatasetMeta(e);
                            n.isDatasetVisible(e) && a(i) && void 0 !== i.stack && (s = !0);
                          }
                        }),
                      e.stacked || s)
                    ) {
                      var l = {};
                      i.each(r, function(r, o) {
                        var s = n.getDatasetMeta(o),
                          u = [s.type, void 0 === e.stacked && void 0 === s.stack ? o : '', s.stack].join('.');
                        n.isDatasetVisible(o) &&
                          a(s) &&
                          (void 0 === l[u] && (l[u] = []),
                          i.each(r.data, function(e, n) {
                            var i = l[u],
                              r = +t.getRightValue(e);
                            isNaN(r) || s.data[n].hidden || r < 0 || ((i[n] = i[n] || 0), (i[n] += r));
                          }));
                      }),
                        i.each(l, function(e) {
                          if (e.length > 0) {
                            var n = i.min(e),
                              r = i.max(e);
                            (t.min = null === t.min ? n : Math.min(t.min, n)),
                              (t.max = null === t.max ? r : Math.max(t.max, r));
                          }
                        });
                    } else
                      i.each(r, function(e, r) {
                        var o = n.getDatasetMeta(r);
                        n.isDatasetVisible(r) &&
                          a(o) &&
                          i.each(e.data, function(e, n) {
                            var i = +t.getRightValue(e);
                            isNaN(i) ||
                              o.data[n].hidden ||
                              i < 0 ||
                              ((null === t.min || i < t.min) && (t.min = i),
                              (null === t.max || i > t.max) && (t.max = i),
                              0 !== i && (null === t.minNotZero || i < t.minNotZero) && (t.minNotZero = i));
                          });
                      });
                    this.handleTickRangeOptions();
                  },
                  handleTickRangeOptions: function() {
                    var t = this,
                      e = t.options.ticks,
                      n = i.valueOrDefault;
                    (t.min = n(e.min, t.min)),
                      (t.max = n(e.max, t.max)),
                      t.min === t.max &&
                        (0 !== t.min && null !== t.min
                          ? ((t.min = Math.pow(10, Math.floor(i.log10(t.min)) - 1)),
                            (t.max = Math.pow(10, Math.floor(i.log10(t.max)) + 1)))
                          : ((t.min = 1), (t.max = 10))),
                      null === t.min && (t.min = Math.pow(10, Math.floor(i.log10(t.max)) - 1)),
                      null === t.max && (t.max = 0 !== t.min ? Math.pow(10, Math.floor(i.log10(t.min)) + 1) : 10),
                      null === t.minNotZero &&
                        (t.minNotZero = t.min > 0 ? t.min : t.max < 1 ? Math.pow(10, Math.floor(i.log10(t.max))) : 1);
                  },
                  buildTicks: function() {
                    var t = this,
                      e = t.options.ticks,
                      n = !t.isHorizontal(),
                      r = (t.ticks = (function(t, e) {
                        var n,
                          r,
                          o = [],
                          a = i.valueOrDefault,
                          s = a(t.min, Math.pow(10, Math.floor(i.log10(e.min)))),
                          l = Math.floor(i.log10(e.max)),
                          u = Math.ceil(e.max / Math.pow(10, l));
                        0 === s
                          ? ((n = Math.floor(i.log10(e.minNotZero))),
                            (r = Math.floor(e.minNotZero / Math.pow(10, n))),
                            o.push(s),
                            (s = r * Math.pow(10, n)))
                          : ((n = Math.floor(i.log10(s))), (r = Math.floor(s / Math.pow(10, n))));
                        for (
                          var c = n < 0 ? Math.pow(10, Math.abs(n)) : 1;
                          o.push(s),
                            10 == ++r && ((r = 1), (c = ++n >= 0 ? 1 : c)),
                            (s = Math.round(r * Math.pow(10, n) * c) / c),
                            n < l || (n === l && r < u);

                        );
                        var d = a(t.max, s);
                        return o.push(d), o;
                      })({ min: e.min, max: e.max }, t));
                    (t.max = i.max(r)),
                      (t.min = i.min(r)),
                      e.reverse ? ((n = !n), (t.start = t.max), (t.end = t.min)) : ((t.start = t.min), (t.end = t.max)),
                      n && r.reverse();
                  },
                  convertTicksToLabels: function() {
                    (this.tickValues = this.ticks.slice()), t.Scale.prototype.convertTicksToLabels.call(this);
                  },
                  getLabelForIndex: function(t, e) {
                    return +this.getRightValue(this.chart.data.datasets[e].data[t]);
                  },
                  getPixelForTick: function(t) {
                    return this.getPixelForValue(this.tickValues[t]);
                  },
                  _getFirstTickValue: function(t) {
                    var e = Math.floor(i.log10(t));
                    return Math.floor(t / Math.pow(10, e)) * Math.pow(10, e);
                  },
                  getPixelForValue: function(e) {
                    var n,
                      r,
                      o,
                      a,
                      s,
                      l = this,
                      u = l.options.ticks.reverse,
                      c = i.log10,
                      d = l._getFirstTickValue(l.minNotZero),
                      h = 0;
                    return (
                      (e = +l.getRightValue(e)),
                      u ? ((o = l.end), (a = l.start), (s = -1)) : ((o = l.start), (a = l.end), (s = 1)),
                      l.isHorizontal()
                        ? ((n = l.width), (r = u ? l.right : l.left))
                        : ((n = l.height), (s *= -1), (r = u ? l.top : l.bottom)),
                      e !== o &&
                        (0 === o &&
                          ((n -= h = i.getValueOrDefault(l.options.ticks.fontSize, t.defaults.global.defaultFontSize)),
                          (o = d)),
                        0 !== e && (h += (n / (c(a) - c(o))) * (c(e) - c(o))),
                        (r += s * h)),
                      r
                    );
                  },
                  getValueForPixel: function(e) {
                    var n,
                      r,
                      o,
                      a,
                      s = this,
                      l = s.options.ticks.reverse,
                      u = i.log10,
                      c = s._getFirstTickValue(s.minNotZero);
                    if (
                      (l ? ((r = s.end), (o = s.start)) : ((r = s.start), (o = s.end)),
                      s.isHorizontal()
                        ? ((n = s.width), (a = l ? s.right - e : e - s.left))
                        : ((n = s.height), (a = l ? e - s.top : s.bottom - e)),
                      a !== r)
                    ) {
                      if (0 === r) {
                        var d = i.getValueOrDefault(s.options.ticks.fontSize, t.defaults.global.defaultFontSize);
                        (a -= d), (n -= d), (r = c);
                      }
                      (a *= u(o) - u(r)), (a /= n), (a = Math.pow(10, u(r) + a));
                    }
                    return a;
                  }
                });
              t.scaleService.registerScaleType('logarithmic', n, e);
            };
          },
          { 34: 34, 45: 45 }
        ],
        57: [
          function(t, e, n) {
            'use strict';
            var i = t(25),
              r = t(45),
              o = t(34);
            e.exports = function(t) {
              var e = i.global,
                n = {
                  display: !0,
                  animate: !0,
                  position: 'chartArea',
                  angleLines: { display: !0, color: 'rgba(0, 0, 0, 0.1)', lineWidth: 1 },
                  gridLines: { circular: !1 },
                  ticks: {
                    showLabelBackdrop: !0,
                    backdropColor: 'rgba(255,255,255,0.75)',
                    backdropPaddingY: 2,
                    backdropPaddingX: 2,
                    callback: o.formatters.linear
                  },
                  pointLabels: {
                    display: !0,
                    fontSize: 10,
                    callback: function(t) {
                      return t;
                    }
                  }
                };
              function a(t) {
                var e = t.options;
                return e.angleLines.display || e.pointLabels.display ? t.chart.data.labels.length : 0;
              }
              function s(t) {
                var n = t.options.pointLabels,
                  i = r.valueOrDefault(n.fontSize, e.defaultFontSize),
                  o = r.valueOrDefault(n.fontStyle, e.defaultFontStyle),
                  a = r.valueOrDefault(n.fontFamily, e.defaultFontFamily);
                return { size: i, style: o, family: a, font: r.fontString(i, o, a) };
              }
              function l(t, e, n, i, r) {
                return t === i || t === r
                  ? { start: e - n / 2, end: e + n / 2 }
                  : t < i || t > r
                  ? { start: e - n - 5, end: e }
                  : { start: e, end: e + n + 5 };
              }
              function u(t, e, n, i) {
                if (r.isArray(e))
                  for (var o = n.y, a = 1.5 * i, s = 0; s < e.length; ++s) t.fillText(e[s], n.x, o), (o += a);
                else t.fillText(e, n.x, n.y);
              }
              function c(t) {
                return r.isNumber(t) ? t : 0;
              }
              var d = t.LinearScaleBase.extend({
                setDimensions: function() {
                  var t = this,
                    n = t.options,
                    i = n.ticks;
                  (t.width = t.maxWidth),
                    (t.height = t.maxHeight),
                    (t.xCenter = Math.round(t.width / 2)),
                    (t.yCenter = Math.round(t.height / 2));
                  var o = r.min([t.height, t.width]),
                    a = r.valueOrDefault(i.fontSize, e.defaultFontSize);
                  t.drawingArea = n.display ? o / 2 - (a / 2 + i.backdropPaddingY) : o / 2;
                },
                determineDataLimits: function() {
                  var t = this,
                    e = t.chart,
                    n = Number.POSITIVE_INFINITY,
                    i = Number.NEGATIVE_INFINITY;
                  r.each(e.data.datasets, function(o, a) {
                    if (e.isDatasetVisible(a)) {
                      var s = e.getDatasetMeta(a);
                      r.each(o.data, function(e, r) {
                        var o = +t.getRightValue(e);
                        isNaN(o) || s.data[r].hidden || ((n = Math.min(o, n)), (i = Math.max(o, i)));
                      });
                    }
                  }),
                    (t.min = n === Number.POSITIVE_INFINITY ? 0 : n),
                    (t.max = i === Number.NEGATIVE_INFINITY ? 0 : i),
                    t.handleTickRangeOptions();
                },
                getTickLimit: function() {
                  var t = this.options.ticks,
                    n = r.valueOrDefault(t.fontSize, e.defaultFontSize);
                  return Math.min(t.maxTicksLimit ? t.maxTicksLimit : 11, Math.ceil(this.drawingArea / (1.5 * n)));
                },
                convertTicksToLabels: function() {
                  var e = this;
                  t.LinearScaleBase.prototype.convertTicksToLabels.call(e),
                    (e.pointLabels = e.chart.data.labels.map(e.options.pointLabels.callback, e));
                },
                getLabelForIndex: function(t, e) {
                  return +this.getRightValue(this.chart.data.datasets[e].data[t]);
                },
                fit: function() {
                  var t, e;
                  this.options.pointLabels.display
                    ? (function(t) {
                        var e,
                          n,
                          i,
                          o = s(t),
                          u = Math.min(t.height / 2, t.width / 2),
                          c = { r: t.width, l: 0, t: t.height, b: 0 },
                          d = {};
                        (t.ctx.font = o.font), (t._pointLabelSizes = []);
                        var h,
                          f,
                          p,
                          g = a(t);
                        for (e = 0; e < g; e++) {
                          (i = t.getPointPosition(e, u)),
                            (h = t.ctx),
                            (f = o.size),
                            (n = r.isArray((p = t.pointLabels[e] || ''))
                              ? { w: r.longestText(h, h.font, p), h: p.length * f + 1.5 * (p.length - 1) * f }
                              : { w: h.measureText(p).width, h: f }),
                            (t._pointLabelSizes[e] = n);
                          var m = t.getIndexAngle(e),
                            v = r.toDegrees(m) % 360,
                            b = l(v, i.x, n.w, 0, 180),
                            y = l(v, i.y, n.h, 90, 270);
                          b.start < c.l && ((c.l = b.start), (d.l = m)),
                            b.end > c.r && ((c.r = b.end), (d.r = m)),
                            y.start < c.t && ((c.t = y.start), (d.t = m)),
                            y.end > c.b && ((c.b = y.end), (d.b = m));
                        }
                        t.setReductions(u, c, d);
                      })(this)
                    : ((t = this),
                      (e = Math.min(t.height / 2, t.width / 2)),
                      (t.drawingArea = Math.round(e)),
                      t.setCenterPoint(0, 0, 0, 0));
                },
                setReductions: function(t, e, n) {
                  var i = e.l / Math.sin(n.l),
                    r = Math.max(e.r - this.width, 0) / Math.sin(n.r),
                    o = -e.t / Math.cos(n.t),
                    a = -Math.max(e.b - this.height, 0) / Math.cos(n.b);
                  (i = c(i)),
                    (r = c(r)),
                    (o = c(o)),
                    (a = c(a)),
                    (this.drawingArea = Math.min(Math.round(t - (i + r) / 2), Math.round(t - (o + a) / 2))),
                    this.setCenterPoint(i, r, o, a);
                },
                setCenterPoint: function(t, e, n, i) {
                  var r = this,
                    o = n + r.drawingArea,
                    a = r.height - i - r.drawingArea;
                  (r.xCenter = Math.round((t + r.drawingArea + (r.width - e - r.drawingArea)) / 2 + r.left)),
                    (r.yCenter = Math.round((o + a) / 2 + r.top));
                },
                getIndexAngle: function(t) {
                  return (
                    t * ((2 * Math.PI) / a(this)) +
                    ((this.chart.options && this.chart.options.startAngle ? this.chart.options.startAngle : 0) *
                      Math.PI *
                      2) /
                      360
                  );
                },
                getDistanceFromCenterForValue: function(t) {
                  var e = this;
                  if (null === t) return 0;
                  var n = e.drawingArea / (e.max - e.min);
                  return e.options.ticks.reverse ? (e.max - t) * n : (t - e.min) * n;
                },
                getPointPosition: function(t, e) {
                  var n = this.getIndexAngle(t) - Math.PI / 2;
                  return {
                    x: Math.round(Math.cos(n) * e) + this.xCenter,
                    y: Math.round(Math.sin(n) * e) + this.yCenter
                  };
                },
                getPointPositionForValue: function(t, e) {
                  return this.getPointPosition(t, this.getDistanceFromCenterForValue(e));
                },
                getBasePosition: function() {
                  var t = this.min,
                    e = this.max;
                  return this.getPointPositionForValue(
                    0,
                    this.beginAtZero ? 0 : t < 0 && e < 0 ? e : t > 0 && e > 0 ? t : 0
                  );
                },
                draw: function() {
                  var t = this,
                    n = t.options,
                    i = n.gridLines,
                    o = n.ticks,
                    l = r.valueOrDefault;
                  if (n.display) {
                    var c = t.ctx,
                      d = this.getIndexAngle(0),
                      h = l(o.fontSize, e.defaultFontSize),
                      f = l(o.fontStyle, e.defaultFontStyle),
                      p = l(o.fontFamily, e.defaultFontFamily),
                      g = r.fontString(h, f, p);
                    r.each(t.ticks, function(n, s) {
                      if (s > 0 || o.reverse) {
                        var u = t.getDistanceFromCenterForValue(t.ticksAsNumbers[s]);
                        if (
                          (i.display &&
                            0 !== s &&
                            (function(t, e, n, i) {
                              var o = t.ctx;
                              if (
                                ((o.strokeStyle = r.valueAtIndexOrDefault(e.color, i - 1)),
                                (o.lineWidth = r.valueAtIndexOrDefault(e.lineWidth, i - 1)),
                                t.options.gridLines.circular)
                              )
                                o.beginPath(),
                                  o.arc(t.xCenter, t.yCenter, n, 0, 2 * Math.PI),
                                  o.closePath(),
                                  o.stroke();
                              else {
                                var s = a(t);
                                if (0 === s) return;
                                o.beginPath();
                                var l = t.getPointPosition(0, n);
                                o.moveTo(l.x, l.y);
                                for (var u = 1; u < s; u++) (l = t.getPointPosition(u, n)), o.lineTo(l.x, l.y);
                                o.closePath(), o.stroke();
                              }
                            })(t, i, u, s),
                          o.display)
                        ) {
                          var f = l(o.fontColor, e.defaultFontColor);
                          if (
                            ((c.font = g),
                            c.save(),
                            c.translate(t.xCenter, t.yCenter),
                            c.rotate(d),
                            o.showLabelBackdrop)
                          ) {
                            var p = c.measureText(n).width;
                            (c.fillStyle = o.backdropColor),
                              c.fillRect(
                                -p / 2 - o.backdropPaddingX,
                                -u - h / 2 - o.backdropPaddingY,
                                p + 2 * o.backdropPaddingX,
                                h + 2 * o.backdropPaddingY
                              );
                          }
                          (c.textAlign = 'center'),
                            (c.textBaseline = 'middle'),
                            (c.fillStyle = f),
                            c.fillText(n, 0, -u),
                            c.restore();
                        }
                      }
                    }),
                      (n.angleLines.display || n.pointLabels.display) &&
                        (function(t) {
                          var n = t.ctx,
                            i = t.options,
                            o = i.angleLines,
                            l = i.pointLabels;
                          (n.lineWidth = o.lineWidth), (n.strokeStyle = o.color);
                          var c,
                            d,
                            h,
                            f,
                            p = t.getDistanceFromCenterForValue(i.ticks.reverse ? t.min : t.max),
                            g = s(t);
                          n.textBaseline = 'top';
                          for (var m = a(t) - 1; m >= 0; m--) {
                            if (o.display) {
                              var v = t.getPointPosition(m, p);
                              n.beginPath(),
                                n.moveTo(t.xCenter, t.yCenter),
                                n.lineTo(v.x, v.y),
                                n.stroke(),
                                n.closePath();
                            }
                            if (l.display) {
                              var b = t.getPointPosition(m, p + 5),
                                y = r.valueAtIndexOrDefault(l.fontColor, m, e.defaultFontColor);
                              (n.font = g.font), (n.fillStyle = y);
                              var x = t.getIndexAngle(m),
                                w = r.toDegrees(x);
                              (n.textAlign = 0 === (f = w) || 180 === f ? 'center' : f < 180 ? 'left' : 'right'),
                                (d = t._pointLabelSizes[m]),
                                (h = b),
                                90 === (c = w) || 270 === c ? (h.y -= d.h / 2) : (c > 270 || c < 90) && (h.y -= d.h),
                                u(n, t.pointLabels[m] || '', b, g.size);
                            }
                          }
                        })(t);
                  }
                }
              });
              t.scaleService.registerScaleType('radialLinear', d, n);
            };
          },
          { 25: 25, 34: 34, 45: 45 }
        ],
        58: [
          function(t, e, n) {
            'use strict';
            var i = t(1);
            i = 'function' == typeof i ? i : window.moment;
            var r = t(25),
              o = t(45),
              a = Number.MIN_SAFE_INTEGER || -9007199254740991,
              s = Number.MAX_SAFE_INTEGER || 9007199254740991,
              l = {
                millisecond: { common: !0, size: 1, steps: [1, 2, 5, 10, 20, 50, 100, 250, 500] },
                second: { common: !0, size: 1e3, steps: [1, 2, 5, 10, 30] },
                minute: { common: !0, size: 6e4, steps: [1, 2, 5, 10, 30] },
                hour: { common: !0, size: 36e5, steps: [1, 2, 3, 6, 12] },
                day: { common: !0, size: 864e5, steps: [1, 2, 5] },
                week: { common: !1, size: 6048e5, steps: [1, 2, 3, 4] },
                month: { common: !0, size: 2628e6, steps: [1, 2, 3] },
                quarter: { common: !1, size: 7884e6, steps: [1, 2, 3, 4] },
                year: { common: !0, size: 3154e7 }
              },
              u = Object.keys(l);
            function c(t, e) {
              return t - e;
            }
            function d(t) {
              var e,
                n,
                i,
                r = {},
                o = [];
              for (e = 0, n = t.length; e < n; ++e) r[(i = t[e])] || ((r[i] = !0), o.push(i));
              return o;
            }
            function h(t, e, n, i) {
              var r = (function(t, e, n) {
                  for (var i, r, o, a = 0, s = t.length - 1; a >= 0 && a <= s; ) {
                    if (((r = t[(i = (a + s) >> 1) - 1] || null), (o = t[i]), !r)) return { lo: null, hi: o };
                    if (o[e] < n) a = i + 1;
                    else {
                      if (!(r[e] > n)) return { lo: r, hi: o };
                      s = i - 1;
                    }
                  }
                  return { lo: o, hi: null };
                })(t, e, n),
                o = r.lo ? (r.hi ? r.lo : t[t.length - 2]) : t[0],
                a = r.lo ? (r.hi ? r.hi : t[t.length - 1]) : t[1],
                s = a[e] - o[e];
              return o[i] + (a[i] - o[i]) * (s ? (n - o[e]) / s : 0);
            }
            function f(t, e) {
              var n = e.parser,
                r = e.parser || e.format;
              return 'function' == typeof n
                ? n(t)
                : 'string' == typeof t && 'string' == typeof r
                ? i(t, r)
                : (t instanceof i || (t = i(t)), t.isValid() ? t : 'function' == typeof r ? r(t) : t);
            }
            function p(t, e) {
              if (o.isNullOrUndef(t)) return null;
              var n = e.options.time,
                i = f(e.getRightValue(t), n);
              return i.isValid() ? (n.round && i.startOf(n.round), i.valueOf()) : null;
            }
            function g(t) {
              for (var e = u.indexOf(t) + 1, n = u.length; e < n; ++e) if (l[u[e]].common) return u[e];
            }
            function m(t, e, n, r) {
              var a,
                c = r.time,
                d =
                  c.unit ||
                  (function(t, e, n, i) {
                    var r,
                      o,
                      a,
                      c = u.length;
                    for (r = u.indexOf(t); r < c - 1; ++r)
                      if (
                        ((a = (o = l[u[r]]).steps ? o.steps[o.steps.length - 1] : s),
                        o.common && Math.ceil((n - e) / (a * o.size)) <= i)
                      )
                        return u[r];
                    return u[c - 1];
                  })(c.minUnit, t, e, n),
                h = g(d),
                f = o.valueOrDefault(c.stepSize, c.unitStepSize),
                p = 'week' === d && c.isoWeekday,
                m = r.ticks.major.enabled,
                v = l[d],
                b = i(t),
                y = i(e),
                x = [];
              for (
                f ||
                  (f = (function(t, e, n, i) {
                    var r,
                      o,
                      a,
                      s = e - t,
                      u = l[n],
                      c = u.size,
                      d = u.steps;
                    if (!d) return Math.ceil(s / (i * c));
                    for (r = 0, o = d.length; r < o && ((a = d[r]), !(Math.ceil(s / (c * a)) <= i)); ++r);
                    return a;
                  })(t, e, d, n)),
                  p && ((b = b.isoWeekday(p)), (y = y.isoWeekday(p))),
                  b = b.startOf(p ? 'day' : d),
                  (y = y.startOf(p ? 'day' : d)) < e && y.add(1, d),
                  a = i(b),
                  m && h && !p && !c.round && (a.startOf(h), a.add(~~((b - a) / (v.size * f)) * f, d));
                a < y;
                a.add(f, d)
              )
                x.push(+a);
              return x.push(+a), x;
            }
            e.exports = function(t) {
              var e = t.Scale.extend({
                initialize: function() {
                  if (!i)
                    throw new Error(
                      'Chart.js - Moment.js could not be found! You must include it before Chart.js to use the time scale. Download at https://momentjs.com'
                    );
                  this.mergeTicksOptions(), t.Scale.prototype.initialize.call(this);
                },
                update: function() {
                  var e = this.options;
                  return (
                    e.time &&
                      e.time.format &&
                      console.warn('options.time.format is deprecated and replaced by options.time.parser.'),
                    t.Scale.prototype.update.apply(this, arguments)
                  );
                },
                getRightValue: function(e) {
                  return e && void 0 !== e.t && (e = e.t), t.Scale.prototype.getRightValue.call(this, e);
                },
                determineDataLimits: function() {
                  var t,
                    e,
                    n,
                    r,
                    l,
                    u,
                    h = this,
                    f = h.chart,
                    g = h.options.time,
                    m = g.unit || 'day',
                    v = s,
                    b = a,
                    y = [],
                    x = [],
                    w = [];
                  for (t = 0, n = f.data.labels.length; t < n; ++t) w.push(p(f.data.labels[t], h));
                  for (t = 0, n = (f.data.datasets || []).length; t < n; ++t)
                    if (f.isDatasetVisible(t))
                      if (o.isObject((l = f.data.datasets[t].data)[0]))
                        for (x[t] = [], e = 0, r = l.length; e < r; ++e) (u = p(l[e], h)), y.push(u), (x[t][e] = u);
                      else y.push.apply(y, w), (x[t] = w.slice(0));
                    else x[t] = [];
                  w.length && ((w = d(w).sort(c)), (v = Math.min(v, w[0])), (b = Math.max(b, w[w.length - 1]))),
                    y.length && ((y = d(y).sort(c)), (v = Math.min(v, y[0])), (b = Math.max(b, y[y.length - 1]))),
                    (v = p(g.min, h) || v),
                    (b = p(g.max, h) || b),
                    (v = v === s ? +i().startOf(m) : v),
                    (b = b === a ? +i().endOf(m) + 1 : b),
                    (h.min = Math.min(v, b)),
                    (h.max = Math.max(v + 1, b)),
                    (h._horizontal = h.isHorizontal()),
                    (h._table = []),
                    (h._timestamps = { data: y, datasets: x, labels: w });
                },
                buildTicks: function() {
                  var t,
                    e,
                    n,
                    r,
                    o,
                    a,
                    s,
                    c,
                    d,
                    v,
                    b = this,
                    y = b.min,
                    x = b.max,
                    w = b.options,
                    _ = w.time,
                    C = [],
                    k = [];
                  switch (w.ticks.source) {
                    case 'data':
                      C = b._timestamps.data;
                      break;
                    case 'labels':
                      C = b._timestamps.labels;
                      break;
                    case 'auto':
                    default:
                      C = m(y, x, b.getLabelCapacity(y), w);
                  }
                  for (
                    'ticks' === w.bounds && C.length && ((y = C[0]), (x = C[C.length - 1])),
                      y = p(_.min, b) || y,
                      x = p(_.max, b) || x,
                      t = 0,
                      e = C.length;
                    t < e;
                    ++t
                  )
                    (n = C[t]) >= y && n <= x && k.push(n);
                  return (
                    (b.min = y),
                    (b.max = x),
                    (b._unit =
                      _.unit ||
                      (function(t, e, n, r) {
                        var o,
                          a,
                          s = i.duration(i(r).diff(i(n)));
                        for (o = u.length - 1; o >= u.indexOf(e); o--)
                          if (l[(a = u[o])].common && s.as(a) >= t.length) return a;
                        return u[e ? u.indexOf(e) : 0];
                      })(k, _.minUnit, b.min, b.max)),
                    (b._majorUnit = g(b._unit)),
                    (b._table = (function(t, e, n, i) {
                      if ('linear' === i || !t.length)
                        return [
                          { time: e, pos: 0 },
                          { time: n, pos: 1 }
                        ];
                      var r,
                        o,
                        a,
                        s,
                        l,
                        u = [],
                        c = [e];
                      for (r = 0, o = t.length; r < o; ++r) (s = t[r]) > e && s < n && c.push(s);
                      for (c.push(n), r = 0, o = c.length; r < o; ++r)
                        (l = c[r + 1]),
                          (s = c[r]),
                          (void 0 !== (a = c[r - 1]) && void 0 !== l && Math.round((l + a) / 2) === s) ||
                            u.push({ time: s, pos: r / (o - 1) });
                      return u;
                    })(b._timestamps.data, y, x, w.distribution)),
                    (b._offsets =
                      ((r = b._table),
                      (o = k),
                      (a = y),
                      (d = 0),
                      (v = 0),
                      (s = w).offset &&
                        o.length &&
                        (s.time.min ||
                          ((c = o[0]),
                          (d = (h(r, 'time', o.length > 1 ? o[1] : x, 'pos') - h(r, 'time', c, 'pos')) / 2)),
                        s.time.max ||
                          ((c = o.length > 1 ? o[o.length - 2] : a),
                          (v = (h(r, 'time', o[o.length - 1], 'pos') - h(r, 'time', c, 'pos')) / 2))),
                      { left: d, right: v })),
                    (b._labelFormat = (function(t, e) {
                      var n,
                        i,
                        r,
                        o = t.length;
                      for (n = 0; n < o; n++) {
                        if (0 !== (i = f(t[n], e)).millisecond()) return 'MMM D, YYYY h:mm:ss.SSS a';
                        (0 === i.second() && 0 === i.minute() && 0 === i.hour()) || (r = !0);
                      }
                      return r ? 'MMM D, YYYY h:mm:ss a' : 'MMM D, YYYY';
                    })(b._timestamps.data, _)),
                    (function(t, e) {
                      var n,
                        r,
                        o,
                        a,
                        s = [];
                      for (n = 0, r = t.length; n < r; ++n)
                        (o = t[n]), (a = !!e && o === +i(o).startOf(e)), s.push({ value: o, major: a });
                      return s;
                    })(k, b._majorUnit)
                  );
                },
                getLabelForIndex: function(t, e) {
                  var n = this.chart.data,
                    i = this.options.time,
                    r = n.labels && t < n.labels.length ? n.labels[t] : '',
                    a = n.datasets[e].data[t];
                  return (
                    o.isObject(a) && (r = this.getRightValue(a)),
                    i.tooltipFormat
                      ? f(r, i).format(i.tooltipFormat)
                      : 'string' == typeof r
                      ? r
                      : f(r, i).format(this._labelFormat)
                  );
                },
                tickFormatFunction: function(t, e, n, i) {
                  var r = this.options,
                    a = t.valueOf(),
                    s = r.time.displayFormats,
                    l = s[this._unit],
                    u = this._majorUnit,
                    c = s[u],
                    d = t
                      .clone()
                      .startOf(u)
                      .valueOf(),
                    h = r.ticks.major,
                    f = h.enabled && u && c && a === d,
                    p = t.format(i || (f ? c : l)),
                    g = f ? h : r.ticks.minor,
                    m = o.valueOrDefault(g.callback, g.userCallback);
                  return m ? m(p, e, n) : p;
                },
                convertTicksToLabels: function(t) {
                  var e,
                    n,
                    r = [];
                  for (e = 0, n = t.length; e < n; ++e) r.push(this.tickFormatFunction(i(t[e].value), e, t));
                  return r;
                },
                getPixelForOffset: function(t) {
                  var e = this,
                    n = e._horizontal ? e.width : e.height,
                    i = e._horizontal ? e.left : e.top,
                    r = h(e._table, 'time', t, 'pos');
                  return i + (n * (e._offsets.left + r)) / (e._offsets.left + 1 + e._offsets.right);
                },
                getPixelForValue: function(t, e, n) {
                  var i = null;
                  if (
                    (void 0 !== e && void 0 !== n && (i = this._timestamps.datasets[n][e]),
                    null === i && (i = p(t, this)),
                    null !== i)
                  )
                    return this.getPixelForOffset(i);
                },
                getPixelForTick: function(t) {
                  var e = this.getTicks();
                  return t >= 0 && t < e.length ? this.getPixelForOffset(e[t].value) : null;
                },
                getValueForPixel: function(t) {
                  var e = this,
                    n = e._horizontal ? e.width : e.height,
                    r = h(
                      e._table,
                      'pos',
                      (n ? (t - (e._horizontal ? e.left : e.top)) / n : 0) * (e._offsets.left + 1 + e._offsets.left) -
                        e._offsets.right,
                      'time'
                    );
                  return i(r);
                },
                getLabelWidth: function(t) {
                  var e = this.options.ticks,
                    n = this.ctx.measureText(t).width,
                    i = o.toRadians(e.maxRotation),
                    a = Math.cos(i),
                    s = Math.sin(i);
                  return n * a + o.valueOrDefault(e.fontSize, r.global.defaultFontSize) * s;
                },
                getLabelCapacity: function(t) {
                  var e = this,
                    n = e.options.time.displayFormats.millisecond,
                    r = e.tickFormatFunction(i(t), 0, [], n),
                    o = e.getLabelWidth(r),
                    a = e.isHorizontal() ? e.width : e.height,
                    s = Math.floor(a / o);
                  return s > 0 ? s : 1;
                }
              });
              t.scaleService.registerScaleType('time', e, {
                position: 'bottom',
                distribution: 'linear',
                bounds: 'data',
                time: {
                  parser: !1,
                  format: !1,
                  unit: !1,
                  round: !1,
                  displayFormat: !1,
                  isoWeekday: !1,
                  minUnit: 'millisecond',
                  displayFormats: {
                    millisecond: 'h:mm:ss.SSS a',
                    second: 'h:mm:ss a',
                    minute: 'h:mm a',
                    hour: 'hA',
                    day: 'MMM D',
                    week: 'll',
                    month: 'MMM YYYY',
                    quarter: '[Q]Q - YYYY',
                    year: 'YYYY'
                  }
                },
                ticks: { autoSkip: !1, source: 'auto', major: { enabled: !1 } }
              });
            };
          },
          { 1: 1, 25: 25, 45: 45 }
        ]
      },
      {},
      [7]
    )(7);
  }),
  (function(t) {
    'function' == typeof define && define.amd
      ? define(['jquery'], t)
      : 'object' == typeof module && module.exports
      ? (module.exports = t(require('jquery')))
      : t(jQuery);
  })(function(t) {
    t.extend(t.fn, {
      validate: function(e) {
        if (this.length) {
          var n = t.data(this[0], 'validator');
          return (
            n ||
            (this.attr('novalidate', 'novalidate'),
            (n = new t.validator(e, this[0])),
            t.data(this[0], 'validator', n),
            n.settings.onsubmit &&
              (this.on('click.validate', ':submit', function(e) {
                (n.submitButton = e.currentTarget),
                  t(this).hasClass('cancel') && (n.cancelSubmit = !0),
                  void 0 !== t(this).attr('formnovalidate') && (n.cancelSubmit = !0);
              }),
              this.on('submit.validate', function(e) {
                function i() {
                  var i, r;
                  return (
                    n.submitButton &&
                      (n.settings.submitHandler || n.formSubmitted) &&
                      (i = t("<input type='hidden'/>")
                        .attr('name', n.submitButton.name)
                        .val(t(n.submitButton).val())
                        .appendTo(n.currentForm)),
                    !n.settings.submitHandler ||
                      ((r = n.settings.submitHandler.call(n, n.currentForm, e)), i && i.remove(), void 0 !== r && r)
                  );
                }
                return (
                  n.settings.debug && e.preventDefault(),
                  n.cancelSubmit
                    ? ((n.cancelSubmit = !1), i())
                    : n.form()
                    ? n.pendingRequest
                      ? ((n.formSubmitted = !0), !1)
                      : i()
                    : (n.focusInvalid(), !1)
                );
              })),
            n)
          );
        }
        e && e.debug && window.console && console.warn("Nothing selected, can't validate, returning nothing.");
      },
      valid: function() {
        var e, n, i;
        return (
          t(this[0]).is('form')
            ? (e = this.validate().form())
            : ((i = []),
              (e = !0),
              (n = t(this[0].form).validate()),
              this.each(function() {
                (e = n.element(this) && e) || (i = i.concat(n.errorList));
              }),
              (n.errorList = i)),
          e
        );
      },
      rules: function(e, n) {
        var i,
          r,
          o,
          a,
          s,
          l,
          u = this[0];
        if (
          null != u &&
          (!u.form &&
            u.hasAttribute('contenteditable') &&
            ((u.form = this.closest('form')[0]), (u.name = this.attr('name'))),
          null != u.form)
        ) {
          if (e)
            switch (((i = t.data(u.form, 'validator').settings), (r = i.rules), (o = t.validator.staticRules(u)), e)) {
              case 'add':
                t.extend(o, t.validator.normalizeRule(n)),
                  delete o.messages,
                  (r[u.name] = o),
                  n.messages && (i.messages[u.name] = t.extend(i.messages[u.name], n.messages));
                break;
              case 'remove':
                return n
                  ? ((l = {}),
                    t.each(n.split(/\s/), function(t, e) {
                      (l[e] = o[e]), delete o[e];
                    }),
                    l)
                  : (delete r[u.name], o);
            }
          return (
            (a = t.validator.normalizeRules(
              t.extend(
                {},
                t.validator.classRules(u),
                t.validator.attributeRules(u),
                t.validator.dataRules(u),
                t.validator.staticRules(u)
              ),
              u
            )).required && ((s = a.required), delete a.required, (a = t.extend({ required: s }, a))),
            a.remote && ((s = a.remote), delete a.remote, (a = t.extend(a, { remote: s }))),
            a
          );
        }
      }
    }),
      t.extend(t.expr.pseudos || t.expr[':'], {
        blank: function(e) {
          return !t.trim('' + t(e).val());
        },
        filled: function(e) {
          var n = t(e).val();
          return null !== n && !!t.trim('' + n);
        },
        unchecked: function(e) {
          return !t(e).prop('checked');
        }
      }),
      (t.validator = function(e, n) {
        (this.settings = t.extend(!0, {}, t.validator.defaults, e)), (this.currentForm = n), this.init();
      }),
      (t.validator.format = function(e, n) {
        return 1 === arguments.length
          ? function() {
              var n = t.makeArray(arguments);
              return n.unshift(e), t.validator.format.apply(this, n);
            }
          : (void 0 === n ||
              (arguments.length > 2 && n.constructor !== Array && (n = t.makeArray(arguments).slice(1)),
              n.constructor !== Array && (n = [n]),
              t.each(n, function(t, n) {
                e = e.replace(new RegExp('\\{' + t + '\\}', 'g'), function() {
                  return n;
                });
              })),
            e);
      }),
      t.extend(t.validator, {
        defaults: {
          messages: {},
          groups: {},
          rules: {},
          errorClass: 'error',
          pendingClass: 'pending',
          validClass: 'valid',
          errorElement: 'label',
          focusCleanup: !1,
          focusInvalid: !0,
          errorContainer: t([]),
          errorLabelContainer: t([]),
          onsubmit: !0,
          ignore: ':hidden',
          ignoreTitle: !1,
          onfocusin: function(t) {
            (this.lastActive = t),
              this.settings.focusCleanup &&
                (this.settings.unhighlight &&
                  this.settings.unhighlight.call(this, t, this.settings.errorClass, this.settings.validClass),
                this.hideThese(this.errorsFor(t)));
          },
          onfocusout: function(t) {
            this.checkable(t) || (!(t.name in this.submitted) && this.optional(t)) || this.element(t);
          },
          onkeyup: function(e, n) {
            (9 === n.which && '' === this.elementValue(e)) ||
              -1 !== t.inArray(n.keyCode, [16, 17, 18, 20, 35, 36, 37, 38, 39, 40, 45, 144, 225]) ||
              ((e.name in this.submitted || e.name in this.invalid) && this.element(e));
          },
          onclick: function(t) {
            t.name in this.submitted
              ? this.element(t)
              : t.parentNode.name in this.submitted && this.element(t.parentNode);
          },
          highlight: function(e, n, i) {
            'radio' === e.type
              ? this.findByName(e.name)
                  .addClass(n)
                  .removeClass(i)
              : t(e)
                  .addClass(n)
                  .removeClass(i);
          },
          unhighlight: function(e, n, i) {
            'radio' === e.type
              ? this.findByName(e.name)
                  .removeClass(n)
                  .addClass(i)
              : t(e)
                  .removeClass(n)
                  .addClass(i);
          }
        },
        setDefaults: function(e) {
          t.extend(t.validator.defaults, e);
        },
        messages: {
          required: 'This field is required.',
          remote: 'Please fix this field.',
          email: 'Please enter a valid email address.',
          url: 'Please enter a valid URL.',
          date: 'Please enter a valid date.',
          dateISO: 'Please enter a valid date (ISO).',
          number: 'Please enter a valid number.',
          digits: 'Please enter only digits.',
          equalTo: 'Please enter the same value again.',
          maxlength: t.validator.format('Please enter no more than {0} characters.'),
          minlength: t.validator.format('Please enter at least {0} characters.'),
          rangelength: t.validator.format('Please enter a value between {0} and {1} characters long.'),
          range: t.validator.format('Please enter a value between {0} and {1}.'),
          max: t.validator.format('Please enter a value less than or equal to {0}.'),
          min: t.validator.format('Please enter a value greater than or equal to {0}.'),
          step: t.validator.format('Please enter a multiple of {0}.')
        },
        autoCreateRanges: !1,
        prototype: {
          init: function() {
            function e(e) {
              !this.form &&
                this.hasAttribute('contenteditable') &&
                ((this.form = t(this).closest('form')[0]), (this.name = t(this).attr('name')));
              var n = t.data(this.form, 'validator'),
                i = 'on' + e.type.replace(/^validate/, ''),
                r = n.settings;
              r[i] && !t(this).is(r.ignore) && r[i].call(n, this, e);
            }
            (this.labelContainer = t(this.settings.errorLabelContainer)),
              (this.errorContext = (this.labelContainer.length && this.labelContainer) || t(this.currentForm)),
              (this.containers = t(this.settings.errorContainer).add(this.settings.errorLabelContainer)),
              (this.submitted = {}),
              (this.valueCache = {}),
              (this.pendingRequest = 0),
              (this.pending = {}),
              (this.invalid = {}),
              this.reset();
            var n,
              i = (this.groups = {});
            t.each(this.settings.groups, function(e, n) {
              'string' == typeof n && (n = n.split(/\s/)),
                t.each(n, function(t, n) {
                  i[n] = e;
                });
            }),
              t.each((n = this.settings.rules), function(e, i) {
                n[e] = t.validator.normalizeRule(i);
              }),
              t(this.currentForm)
                .on(
                  'focusin.validate focusout.validate keyup.validate',
                  ":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'], [type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], [type='radio'], [type='checkbox'], [contenteditable], [type='button']",
                  e
                )
                .on('click.validate', "select, option, [type='radio'], [type='checkbox']", e),
              this.settings.invalidHandler &&
                t(this.currentForm).on('invalid-form.validate', this.settings.invalidHandler);
          },
          form: function() {
            return (
              this.checkForm(),
              t.extend(this.submitted, this.errorMap),
              (this.invalid = t.extend({}, this.errorMap)),
              this.valid() || t(this.currentForm).triggerHandler('invalid-form', [this]),
              this.showErrors(),
              this.valid()
            );
          },
          checkForm: function() {
            this.prepareForm();
            for (var t = 0, e = (this.currentElements = this.elements()); e[t]; t++) this.check(e[t]);
            return this.valid();
          },
          element: function(e) {
            var n,
              i,
              r = this.clean(e),
              o = this.validationTargetFor(r),
              a = this,
              s = !0;
            return (
              void 0 === o
                ? delete this.invalid[r.name]
                : (this.prepareElement(o),
                  (this.currentElements = t(o)),
                  (i = this.groups[o.name]) &&
                    t.each(this.groups, function(t, e) {
                      e === i &&
                        t !== o.name &&
                        (r = a.validationTargetFor(a.clean(a.findByName(t)))) &&
                        r.name in a.invalid &&
                        (a.currentElements.push(r), (s = a.check(r) && s));
                    }),
                  (n = !1 !== this.check(o)),
                  (s = s && n),
                  (this.invalid[o.name] = !n),
                  this.numberOfInvalids() || (this.toHide = this.toHide.add(this.containers)),
                  this.showErrors(),
                  t(e).attr('aria-invalid', !n)),
              s
            );
          },
          showErrors: function(e) {
            if (e) {
              var n = this;
              t.extend(this.errorMap, e),
                (this.errorList = t.map(this.errorMap, function(t, e) {
                  return { message: t, element: n.findByName(e)[0] };
                })),
                (this.successList = t.grep(this.successList, function(t) {
                  return !(t.name in e);
                }));
            }
            this.settings.showErrors
              ? this.settings.showErrors.call(this, this.errorMap, this.errorList)
              : this.defaultShowErrors();
          },
          resetForm: function() {
            t.fn.resetForm && t(this.currentForm).resetForm(),
              (this.invalid = {}),
              (this.submitted = {}),
              this.prepareForm(),
              this.hideErrors();
            var e = this.elements()
              .removeData('previousValue')
              .removeAttr('aria-invalid');
            this.resetElements(e);
          },
          resetElements: function(t) {
            var e;
            if (this.settings.unhighlight)
              for (e = 0; t[e]; e++)
                this.settings.unhighlight.call(this, t[e], this.settings.errorClass, ''),
                  this.findByName(t[e].name).removeClass(this.settings.validClass);
            else t.removeClass(this.settings.errorClass).removeClass(this.settings.validClass);
          },
          numberOfInvalids: function() {
            return this.objectLength(this.invalid);
          },
          objectLength: function(t) {
            var e,
              n = 0;
            for (e in t) null != t[e] && !1 !== t[e] && n++;
            return n;
          },
          hideErrors: function() {
            this.hideThese(this.toHide);
          },
          hideThese: function(t) {
            t.not(this.containers).text(''), this.addWrapper(t).hide();
          },
          valid: function() {
            return 0 === this.size();
          },
          size: function() {
            return this.errorList.length;
          },
          focusInvalid: function() {
            if (this.settings.focusInvalid)
              try {
                t(this.findLastActive() || (this.errorList.length && this.errorList[0].element) || [])
                  .filter(':visible')
                  .focus()
                  .trigger('focusin');
              } catch (e) {}
          },
          findLastActive: function() {
            var e = this.lastActive;
            return (
              e &&
              1 ===
                t.grep(this.errorList, function(t) {
                  return t.element.name === e.name;
                }).length &&
              e
            );
          },
          elements: function() {
            var e = this,
              n = {};
            return t(this.currentForm)
              .find('input, select, textarea, [contenteditable]')
              .not(':submit, :reset, :image, :disabled')
              .not(this.settings.ignore)
              .filter(function() {
                var i = this.name || t(this).attr('name');
                return (
                  !i && e.settings.debug && window.console && console.error('%o has no name assigned', this),
                  this.hasAttribute('contenteditable') && ((this.form = t(this).closest('form')[0]), (this.name = i)),
                  !(i in n || !e.objectLength(t(this).rules()) || ((n[i] = !0), 0))
                );
              });
          },
          clean: function(e) {
            return t(e)[0];
          },
          errors: function() {
            var e = this.settings.errorClass.split(' ').join('.');
            return t(this.settings.errorElement + '.' + e, this.errorContext);
          },
          resetInternals: function() {
            (this.successList = []),
              (this.errorList = []),
              (this.errorMap = {}),
              (this.toShow = t([])),
              (this.toHide = t([]));
          },
          reset: function() {
            this.resetInternals(), (this.currentElements = t([]));
          },
          prepareForm: function() {
            this.reset(), (this.toHide = this.errors().add(this.containers));
          },
          prepareElement: function(t) {
            this.reset(), (this.toHide = this.errorsFor(t));
          },
          elementValue: function(e) {
            var n,
              i,
              r = t(e),
              o = e.type;
            return 'radio' === o || 'checkbox' === o
              ? this.findByName(e.name)
                  .filter(':checked')
                  .val()
              : 'number' === o && void 0 !== e.validity
              ? e.validity.badInput
                ? 'NaN'
                : r.val()
              : ((n = e.hasAttribute('contenteditable') ? r.text() : r.val()),
                'file' === o
                  ? 'C:\\fakepath\\' === n.substr(0, 12)
                    ? n.substr(12)
                    : (i = n.lastIndexOf('/')) >= 0 || (i = n.lastIndexOf('\\')) >= 0
                    ? n.substr(i + 1)
                    : n
                  : 'string' == typeof n
                  ? n.replace(/\r/g, '')
                  : n);
          },
          check: function(e) {
            e = this.validationTargetFor(this.clean(e));
            var n,
              i,
              r,
              o,
              a = t(e).rules(),
              s = t.map(a, function(t, e) {
                return e;
              }).length,
              l = !1,
              u = this.elementValue(e);
            if (
              ('function' == typeof a.normalizer
                ? (o = a.normalizer)
                : 'function' == typeof this.settings.normalizer && (o = this.settings.normalizer),
              o)
            ) {
              if ('string' != typeof (u = o.call(e, u)))
                throw new TypeError('The normalizer should return a string value.');
              delete a.normalizer;
            }
            for (i in a) {
              r = { method: i, parameters: a[i] };
              try {
                if ('dependency-mismatch' === (n = t.validator.methods[i].call(this, u, e, r.parameters)) && 1 === s) {
                  l = !0;
                  continue;
                }
                if (((l = !1), 'pending' === n)) return void (this.toHide = this.toHide.not(this.errorsFor(e)));
                if (!n) return this.formatAndAdd(e, r), !1;
              } catch (c) {
                throw (this.settings.debug &&
                  window.console &&
                  console.log(
                    'Exception occurred when checking element ' + e.id + ", check the '" + r.method + "' method.",
                    c
                  ),
                c instanceof TypeError &&
                  (c.message +=
                    '.  Exception occurred when checking element ' + e.id + ", check the '" + r.method + "' method."),
                c);
              }
            }
            if (!l) return this.objectLength(a) && this.successList.push(e), !0;
          },
          customDataMessage: function(e, n) {
            return t(e).data('msg' + n.charAt(0).toUpperCase() + n.substring(1).toLowerCase()) || t(e).data('msg');
          },
          customMessage: function(t, e) {
            var n = this.settings.messages[t];
            return n && (n.constructor === String ? n : n[e]);
          },
          findDefined: function() {
            for (var t = 0; t < arguments.length; t++) if (void 0 !== arguments[t]) return arguments[t];
          },
          defaultMessage: function(e, n) {
            'string' == typeof n && (n = { method: n });
            var i = this.findDefined(
                this.customMessage(e.name, n.method),
                this.customDataMessage(e, n.method),
                (!this.settings.ignoreTitle && e.title) || void 0,
                t.validator.messages[n.method],
                '<strong>Warning: No message defined for ' + e.name + '</strong>'
              ),
              r = /\$?\{(\d+)\}/g;
            return (
              'function' == typeof i
                ? (i = i.call(this, n.parameters, e))
                : r.test(i) && (i = t.validator.format(i.replace(r, '{$1}'), n.parameters)),
              i
            );
          },
          formatAndAdd: function(t, e) {
            var n = this.defaultMessage(t, e);
            this.errorList.push({ message: n, element: t, method: e.method }),
              (this.errorMap[t.name] = n),
              (this.submitted[t.name] = n);
          },
          addWrapper: function(t) {
            return this.settings.wrapper && (t = t.add(t.parent(this.settings.wrapper))), t;
          },
          defaultShowErrors: function() {
            var t, e, n;
            for (t = 0; this.errorList[t]; t++)
              (n = this.errorList[t]),
                this.settings.highlight &&
                  this.settings.highlight.call(this, n.element, this.settings.errorClass, this.settings.validClass),
                this.showLabel(n.element, n.message);
            if ((this.errorList.length && (this.toShow = this.toShow.add(this.containers)), this.settings.success))
              for (t = 0; this.successList[t]; t++) this.showLabel(this.successList[t]);
            if (this.settings.unhighlight)
              for (t = 0, e = this.validElements(); e[t]; t++)
                this.settings.unhighlight.call(this, e[t], this.settings.errorClass, this.settings.validClass);
            (this.toHide = this.toHide.not(this.toShow)), this.hideErrors(), this.addWrapper(this.toShow).show();
          },
          validElements: function() {
            return this.currentElements.not(this.invalidElements());
          },
          invalidElements: function() {
            return t(this.errorList).map(function() {
              return this.element;
            });
          },
          showLabel: function(e, n) {
            var i,
              r,
              o,
              a,
              s = this.errorsFor(e),
              l = this.idOrName(e),
              u = t(e).attr('aria-describedby');
            s.length
              ? (s.removeClass(this.settings.validClass).addClass(this.settings.errorClass), s.html(n))
              : ((i = s = t('<' + this.settings.errorElement + '>')
                  .attr('id', l + '-error')
                  .addClass(this.settings.errorClass)
                  .html(n || '')),
                this.settings.wrapper &&
                  (i = s
                    .hide()
                    .show()
                    .wrap('<' + this.settings.wrapper + '/>')
                    .parent()),
                this.labelContainer.length
                  ? this.labelContainer.append(i)
                  : this.settings.errorPlacement
                  ? this.settings.errorPlacement.call(this, i, t(e))
                  : i.insertAfter(e),
                s.is('label')
                  ? s.attr('for', l)
                  : 0 === s.parents("label[for='" + this.escapeCssMeta(l) + "']").length &&
                    ((o = s.attr('id')),
                    u ? u.match(new RegExp('\\b' + this.escapeCssMeta(o) + '\\b')) || (u += ' ' + o) : (u = o),
                    t(e).attr('aria-describedby', u),
                    (r = this.groups[e.name]) &&
                      t.each((a = this).groups, function(e, n) {
                        n === r &&
                          t("[name='" + a.escapeCssMeta(e) + "']", a.currentForm).attr(
                            'aria-describedby',
                            s.attr('id')
                          );
                      }))),
              !n &&
                this.settings.success &&
                (s.text(''),
                'string' == typeof this.settings.success
                  ? s.addClass(this.settings.success)
                  : this.settings.success(s, e)),
              (this.toShow = this.toShow.add(s));
          },
          errorsFor: function(e) {
            var n = this.escapeCssMeta(this.idOrName(e)),
              i = t(e).attr('aria-describedby'),
              r = "label[for='" + n + "'], label[for='" + n + "'] *";
            return i && (r = r + ', #' + this.escapeCssMeta(i).replace(/\s+/g, ', #')), this.errors().filter(r);
          },
          escapeCssMeta: function(t) {
            return t.replace(/([\\!"#$%&'()*+,.\/:;<=>?@\[\]^`{|}~])/g, '\\$1');
          },
          idOrName: function(t) {
            return this.groups[t.name] || (this.checkable(t) ? t.name : t.id || t.name);
          },
          validationTargetFor: function(e) {
            return this.checkable(e) && (e = this.findByName(e.name)), t(e).not(this.settings.ignore)[0];
          },
          checkable: function(t) {
            return /radio|checkbox/i.test(t.type);
          },
          findByName: function(e) {
            return t(this.currentForm).find("[name='" + this.escapeCssMeta(e) + "']");
          },
          getLength: function(e, n) {
            switch (n.nodeName.toLowerCase()) {
              case 'select':
                return t('option:selected', n).length;
              case 'input':
                if (this.checkable(n)) return this.findByName(n.name).filter(':checked').length;
            }
            return e.length;
          },
          depend: function(t, e) {
            return !this.dependTypes[typeof t] || this.dependTypes[typeof t](t, e);
          },
          dependTypes: {
            boolean: function(t) {
              return t;
            },
            string: function(e, n) {
              return !!t(e, n.form).length;
            },
            function: function(t, e) {
              return t(e);
            }
          },
          optional: function(e) {
            var n = this.elementValue(e);
            return !t.validator.methods.required.call(this, n, e) && 'dependency-mismatch';
          },
          startRequest: function(e) {
            this.pending[e.name] ||
              (this.pendingRequest++, t(e).addClass(this.settings.pendingClass), (this.pending[e.name] = !0));
          },
          stopRequest: function(e, n) {
            this.pendingRequest--,
              this.pendingRequest < 0 && (this.pendingRequest = 0),
              delete this.pending[e.name],
              t(e).removeClass(this.settings.pendingClass),
              n && 0 === this.pendingRequest && this.formSubmitted && this.form()
                ? (t(this.currentForm).submit(),
                  this.submitButton &&
                    t("input:hidden[name='" + this.submitButton.name + "']", this.currentForm).remove(),
                  (this.formSubmitted = !1))
                : !n &&
                  0 === this.pendingRequest &&
                  this.formSubmitted &&
                  (t(this.currentForm).triggerHandler('invalid-form', [this]), (this.formSubmitted = !1));
          },
          previousValue: function(e, n) {
            return (
              (n = ('string' == typeof n && n) || 'remote'),
              t.data(e, 'previousValue') ||
                t.data(e, 'previousValue', { old: null, valid: !0, message: this.defaultMessage(e, { method: n }) })
            );
          },
          destroy: function() {
            this.resetForm(),
              t(this.currentForm)
                .off('.validate')
                .removeData('validator')
                .find('.validate-equalTo-blur')
                .off('.validate-equalTo')
                .removeClass('validate-equalTo-blur');
          }
        },
        classRuleSettings: {
          required: { required: !0 },
          email: { email: !0 },
          url: { url: !0 },
          date: { date: !0 },
          dateISO: { dateISO: !0 },
          number: { number: !0 },
          digits: { digits: !0 },
          creditcard: { creditcard: !0 }
        },
        addClassRules: function(e, n) {
          e.constructor === String ? (this.classRuleSettings[e] = n) : t.extend(this.classRuleSettings, e);
        },
        classRules: function(e) {
          var n = {},
            i = t(e).attr('class');
          return (
            i &&
              t.each(i.split(' '), function() {
                this in t.validator.classRuleSettings && t.extend(n, t.validator.classRuleSettings[this]);
              }),
            n
          );
        },
        normalizeAttributeRule: function(t, e, n, i) {
          /min|max|step/.test(n) &&
            (null === e || /number|range|text/.test(e)) &&
            ((i = Number(i)), isNaN(i) && (i = void 0)),
            i || 0 === i ? (t[n] = i) : e === n && 'range' !== e && (t[n] = !0);
        },
        attributeRules: function(e) {
          var n,
            i,
            r = {},
            o = t(e),
            a = e.getAttribute('type');
          for (n in t.validator.methods)
            'required' === n ? ('' === (i = e.getAttribute(n)) && (i = !0), (i = !!i)) : (i = o.attr(n)),
              this.normalizeAttributeRule(r, a, n, i);
          return r.maxlength && /-1|2147483647|524288/.test(r.maxlength) && delete r.maxlength, r;
        },
        dataRules: function(e) {
          var n,
            i,
            r = {},
            o = t(e),
            a = e.getAttribute('type');
          for (n in t.validator.methods)
            (i = o.data('rule' + n.charAt(0).toUpperCase() + n.substring(1).toLowerCase())),
              this.normalizeAttributeRule(r, a, n, i);
          return r;
        },
        staticRules: function(e) {
          var n = {},
            i = t.data(e.form, 'validator');
          return i.settings.rules && (n = t.validator.normalizeRule(i.settings.rules[e.name]) || {}), n;
        },
        normalizeRules: function(e, n) {
          return (
            t.each(e, function(i, r) {
              if (!1 !== r) {
                if (r.param || r.depends) {
                  var o = !0;
                  switch (typeof r.depends) {
                    case 'string':
                      o = !!t(r.depends, n.form).length;
                      break;
                    case 'function':
                      o = r.depends.call(n, n);
                  }
                  o
                    ? (e[i] = void 0 === r.param || r.param)
                    : (t.data(n.form, 'validator').resetElements(t(n)), delete e[i]);
                }
              } else delete e[i];
            }),
            t.each(e, function(i, r) {
              e[i] = t.isFunction(r) && 'normalizer' !== i ? r(n) : r;
            }),
            t.each(['minlength', 'maxlength'], function() {
              e[this] && (e[this] = Number(e[this]));
            }),
            t.each(['rangelength', 'range'], function() {
              var n;
              e[this] &&
                (t.isArray(e[this])
                  ? (e[this] = [Number(e[this][0]), Number(e[this][1])])
                  : 'string' == typeof e[this] &&
                    ((n = e[this].replace(/[\[\]]/g, '').split(/[\s,]+/)), (e[this] = [Number(n[0]), Number(n[1])])));
            }),
            t.validator.autoCreateRanges &&
              (null != e.min && null != e.max && ((e.range = [e.min, e.max]), delete e.min, delete e.max),
              null != e.minlength &&
                null != e.maxlength &&
                ((e.rangelength = [e.minlength, e.maxlength]), delete e.minlength, delete e.maxlength)),
            e
          );
        },
        normalizeRule: function(e) {
          if ('string' == typeof e) {
            var n = {};
            t.each(e.split(/\s/), function() {
              n[this] = !0;
            }),
              (e = n);
          }
          return e;
        },
        addMethod: function(e, n, i) {
          (t.validator.methods[e] = n),
            (t.validator.messages[e] = void 0 !== i ? i : t.validator.messages[e]),
            n.length < 3 && t.validator.addClassRules(e, t.validator.normalizeRule(e));
        },
        methods: {
          required: function(e, n, i) {
            if (!this.depend(i, n)) return 'dependency-mismatch';
            if ('select' === n.nodeName.toLowerCase()) {
              var r = t(n).val();
              return r && r.length > 0;
            }
            return this.checkable(n) ? this.getLength(e, n) > 0 : e.length > 0;
          },
          email: function(t, e) {
            return (
              this.optional(e) ||
              /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(
                t
              )
            );
          },
          url: function(t, e) {
            return (
              this.optional(e) ||
              /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[\/?#]\S*)?$/i.test(
                t
              )
            );
          },
          date: function(t, e) {
            return this.optional(e) || !/Invalid|NaN/.test(new Date(t).toString());
          },
          dateISO: function(t, e) {
            return this.optional(e) || /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(t);
          },
          number: function(t, e) {
            return this.optional(e) || /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(t);
          },
          digits: function(t, e) {
            return this.optional(e) || /^\d+$/.test(t);
          },
          minlength: function(e, n, i) {
            var r = t.isArray(e) ? e.length : this.getLength(e, n);
            return this.optional(n) || r >= i;
          },
          maxlength: function(e, n, i) {
            var r = t.isArray(e) ? e.length : this.getLength(e, n);
            return this.optional(n) || r <= i;
          },
          rangelength: function(e, n, i) {
            var r = t.isArray(e) ? e.length : this.getLength(e, n);
            return this.optional(n) || (r >= i[0] && r <= i[1]);
          },
          min: function(t, e, n) {
            return this.optional(e) || t >= n;
          },
          max: function(t, e, n) {
            return this.optional(e) || t <= n;
          },
          range: function(t, e, n) {
            return this.optional(e) || (t >= n[0] && t <= n[1]);
          },
          step: function(e, n, i) {
            var r,
              o = t(n).attr('type'),
              a = 'Step attribute on input type ' + o + ' is not supported.',
              s = new RegExp('\\b' + o + '\\b'),
              l = function(t) {
                var e = ('' + t).match(/(?:\.(\d+))?$/);
                return e && e[1] ? e[1].length : 0;
              },
              u = function(t) {
                return Math.round(t * Math.pow(10, r));
              },
              c = !0;
            if (o && !s.test(['text', 'number', 'range'].join())) throw new Error(a);
            return (r = l(i)), (l(e) > r || u(e) % u(i) != 0) && (c = !1), this.optional(n) || c;
          },
          equalTo: function(e, n, i) {
            var r = t(i);
            return (
              this.settings.onfocusout &&
                r.not('.validate-equalTo-blur').length &&
                r.addClass('validate-equalTo-blur').on('blur.validate-equalTo', function() {
                  t(n).valid();
                }),
              e === r.val()
            );
          },
          remote: function(e, n, i, r) {
            if (this.optional(n)) return 'dependency-mismatch';
            var o,
              a,
              s,
              l = this.previousValue(n, (r = ('string' == typeof r && r) || 'remote'));
            return (
              this.settings.messages[n.name] || (this.settings.messages[n.name] = {}),
              (l.originalMessage = l.originalMessage || this.settings.messages[n.name][r]),
              (this.settings.messages[n.name][r] = l.message),
              (s = t.param(t.extend({ data: e }, (i = ('string' == typeof i && { url: i }) || i).data))),
              l.old === s
                ? l.valid
                : ((l.old = s),
                  (o = this),
                  this.startRequest(n),
                  ((a = {})[n.name] = e),
                  t.ajax(
                    t.extend(
                      !0,
                      {
                        mode: 'abort',
                        port: 'validate' + n.name,
                        dataType: 'json',
                        data: a,
                        context: o.currentForm,
                        success: function(t) {
                          var i,
                            a,
                            s,
                            u = !0 === t || 'true' === t;
                          (o.settings.messages[n.name][r] = l.originalMessage),
                            u
                              ? ((s = o.formSubmitted),
                                o.resetInternals(),
                                (o.toHide = o.errorsFor(n)),
                                (o.formSubmitted = s),
                                o.successList.push(n),
                                (o.invalid[n.name] = !1),
                                o.showErrors())
                              : ((i = {}),
                                (a = t || o.defaultMessage(n, { method: r, parameters: e })),
                                (i[n.name] = l.message = a),
                                (o.invalid[n.name] = !0),
                                o.showErrors(i)),
                            (l.valid = u),
                            o.stopRequest(n, u);
                        }
                      },
                      i
                    )
                  ),
                  'pending')
            );
          }
        }
      });
    var e,
      n = {};
    return (
      t.ajaxPrefilter
        ? t.ajaxPrefilter(function(t, e, i) {
            var r = t.port;
            'abort' === t.mode && (n[r] && n[r].abort(), (n[r] = i));
          })
        : ((e = t.ajax),
          (t.ajax = function(i) {
            var r = ('mode' in i ? i : t.ajaxSettings).mode,
              o = ('port' in i ? i : t.ajaxSettings).port;
            return 'abort' === r
              ? (n[o] && n[o].abort(), (n[o] = e.apply(this, arguments)), n[o])
              : e.apply(this, arguments);
          })),
      t
    );
  }),
  $(function() {
    $('[data-toggle="tooltip"]').tooltip(),
      $('.form-validate').each(function() {
        $(this).validate({
          errorElement: 'div',
          errorClass: 'is-invalid',
          validClass: 'is-valid',
          ignore: ':hidden:not(.summernote),.note-editable.card-block',
          errorPlacement: function(t, e) {
            t.addClass('invalid-feedback'),
              'checkbox' === e.prop('type') ? t.insertAfter(e.siblings('label')) : t.insertAfter(e);
          }
        });
      });
    var t = $('input.input-material');
    t
      .filter(function() {
        return '' !== $(this).val();
      })
      .siblings('.label-material')
      .addClass('active'),
      t.on('focus', function() {
        $(this)
          .siblings('.label-material')
          .addClass('active');
      }),
      t.on('blur', function() {
        $(this)
          .siblings('.label-material')
          .removeClass('active'),
          '' !== $(this).val()
            ? $(this)
                .siblings('.label-material')
                .addClass('active')
            : $(this)
                .siblings('.label-material')
                .removeClass('active');
      });
    var e = $('.page-content');
    function n() {
      var t = $('.footer__block').outerHeight();
      e.css('padding-bottom', t + 'px');
    }
    if (
      ($(document).on('sidebarChanged', function() {
        n();
      }),
      $(window).on('resize', function() {
        n();
      }),
      $('.dropdown').on('show.bs.dropdown', function() {
        $(this)
          .find('.dropdown-menu')
          .first()
          .stop(!0, !0)
          .fadeIn(100)
          .addClass('active');
      }),
      $('.dropdown').on('hide.bs.dropdown', function() {
        $(this)
          .find('.dropdown-menu')
          .first()
          .stop(!0, !0)
          .fadeOut(100)
          .removeClass('active');
      }),
      $('.search-open').on('click', function(t) {
        t.preventDefault(), $('.search-panel').fadeIn(100);
      }),
      $('.search-panel .close-btn').on('click', function() {
        $('.search-panel').fadeOut(100);
      }),
      $('.sidebar-toggle').on('click', function() {
        $(this).toggleClass('active'),
          $('#sidebar').toggleClass('shrinked'),
          $('.page-content').toggleClass('active'),
          $(document).trigger('sidebarChanged'),
          $('.sidebar-toggle').hasClass('active')
            ? ($('.navbar-brand .brand-sm').addClass('visible'),
              $('.navbar-brand .brand-big').removeClass('visible'),
              $(this)
                .find('i')
                .attr('class', 'fa fa-long-arrow-right'))
            : ($('.navbar-brand .brand-sm').removeClass('visible'),
              $('.navbar-brand .brand-big').addClass('visible'),
              $(this)
                .find('i')
                .attr('class', 'fa fa-long-arrow-left'));
      }),
      $('#style-switch').length > 0)
    ) {
      var i = $('link#theme-stylesheet');
      $("<link id='new-stylesheet' rel='stylesheet'>").insertAfter(i);
      var r = $('link#new-stylesheet');
      $.cookie('theme_csspath') && r.attr('href', $.cookie('theme_csspath')),
        $('#colour').change(function() {
          if ('' !== $(this).val()) {
            var t = 'css/style.' + $(this).val() + '.css';
            r.attr('href', t),
              $.cookie('theme_csspath', t, {
                expires: 365,
                path: document.URL.substr(0, document.URL.lastIndexOf('/'))
              });
          }
          return !1;
        });
    }
  });
