# Streaming Video Angular

# Getting started

1. Go to project folder and install dependencies:
 ```bash
 npm install
 ```
 
2. Launch development server, and open `localhost:4200` in your browser:
 ```bash
 npm start
 ```
 

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change
any of the source files.
You should not use `ng serve` directly, as it does not use the backend proxy configuration by default.

## Deployment to Server
Install `Nodejs`(https://nodejs.org/en/)
After installation of nodejs, install `Angular CLI`:
```bash
npm install -g @angular/cli
```
On angular root folder:
1. Clone the repository
```bash
git clone git@bitbucket.org:jdptolentino/video-streaming-angular.git
```
2. Install the node packages
```bash
npm install
```
3. Get to latest code (update)
```bash
git pull origin master
```
4. Build angular app. `dist` folder is automatically generated upon success build
```bash
npm run build --env=production
```

http://stream.jellatolentino.com should redirect to ```dist```
Sample subdomain apache config: 
```
<VirtualHost *:80>
        ServerAdmin webmaster@dev.mydomain.com
        ServerName stream.jellatolentino.com
        DocumentRoot /var/www/dev

        <Directory /var/www/stream.jellatolentino.com/dist>
            Options Indexes FollowSymLinks
            AllowOverride All
            Require all granted
        </Directory>

        <files xmlrpc.php>
            order allow,deny
            deny from all
        </files>

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```
Then open browser and go to http://stream.jellatolentino.com
